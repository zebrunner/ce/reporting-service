package com.zebrunner.reporting.messaging.config;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RoutingKey {

    public static final String SEND_EMAIL = "send";

}
