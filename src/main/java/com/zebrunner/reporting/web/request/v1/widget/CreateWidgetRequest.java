package com.zebrunner.reporting.web.request.v1.widget;

import com.googlecode.jmapper.annotations.JMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class CreateWidgetRequest {

    @JMap
    @NotBlank
    private String title;
    @JMap
    private String description;
    @JMap
    private String paramsConfig;
    @JMap
    private String legendConfig;
    @NotNull
    private Long templateId;
    @NotNull
    private Long dashboardId;

}
