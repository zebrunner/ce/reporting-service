package com.zebrunner.reporting.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@Data
@ConfigurationProperties(prefix = "service.task.recalculate-failure-reason-hash")
public class RecalculateFailureReasonHashTaskProperties {

    private boolean enabled = false;
    private String redisLockKey = "recalculate-test-hashes-based-on-first-200-char";
    private Duration redisLockTtl = Duration.ofDays(28);
    private Duration redisLockRequestTimeout = Duration.ofSeconds(10);
    private int dbSelectQueryLimit = 500;
    private Duration reindexingDelay = Duration.ofMinutes(10);

}
