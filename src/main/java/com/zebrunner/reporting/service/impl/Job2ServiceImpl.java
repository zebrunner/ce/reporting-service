package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.InternalServiceException;
import com.zebrunner.reporting.domain.entity.Job;
import com.zebrunner.reporting.domain.entity.User;
import com.zebrunner.reporting.persistence.repository.JobRepository;
import com.zebrunner.reporting.service.Job2Service;
import com.zebrunner.reporting.service.exception.InternalServiceError;
import com.zebrunner.reporting.service.integration.jenkins.JenkinsConfigService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class Job2ServiceImpl implements Job2Service {

    private final JobRepository jobRepository;
    private final JenkinsConfigService jenkinsConfigService;

    @Override
    @Transactional
    public Optional<Job> retrieveOrCreateIfIntegrationExists(String jobUrl, User jobOwner) {
        // Replacing trailing slash we make sure that further operations based on splitting by slash will be performed correctly
        String normalizedJobUrl = jobUrl.replaceAll("/$", "");
        return jobRepository.findByJobUrl(normalizedJobUrl)
                            .or(() -> create(normalizedJobUrl, jobOwner));
    }

    private Optional<Job> create(String jobUrl, User jobOwner) {
        String jenkinsHost = extractJenkinsHost(jobUrl);
        return jenkinsConfigService.retrieveOptionalByUrl(jenkinsHost)
                                   .map(config -> Job.builder()
                                                     .name(extractJobName(jobUrl))
                                                     .jobUrl(jobUrl)
                                                     .jenkinsHost(jenkinsHost)
                                                     .user(jobOwner)
                                                     .build()
                                   )
                                   .map(jobRepository::save);
    }

    public static String extractJobName(String jobUrl) {
        try {
            String decodedJobUrl = URLDecoder.decode(jobUrl, StandardCharsets.UTF_8.toString());
            return StringUtils.substringAfterLast(decodedJobUrl, "/");
        } catch (UnsupportedEncodingException e) {
            throw new InternalServiceException(InternalServiceError.UNABLE_TO_PARSE_URI, e, jobUrl);
        }
    }

    private String extractJenkinsHost(String jobUrl) {
        if (jobUrl.contains("/view/")) {
            return jobUrl.split("/view/")[0];
        } else if (jobUrl.contains("/job/")) {
            return jobUrl.split("/job/")[0];
        }
        return StringUtils.EMPTY;
    }

}
