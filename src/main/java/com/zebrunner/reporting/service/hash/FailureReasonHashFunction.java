package com.zebrunner.reporting.service.hash;

import javax.annotation.Nullable;

public interface FailureReasonHashFunction {

    Integer calculateHash(@Nullable String reason);

}
