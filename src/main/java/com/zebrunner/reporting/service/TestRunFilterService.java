package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.TestRunFilter;

import java.util.List;

public interface TestRunFilterService {

    List<TestRunFilter> retrieveByUserId(Long userId);

    TestRunFilter create(TestRunFilter testRunFilter, Long userId);

    TestRunFilter retrieveById(Integer id);

    void patch(TestRunFilter testRunFilterPatch, Integer userId, Boolean isFavorite);

    void deleteById(Integer id);

}
