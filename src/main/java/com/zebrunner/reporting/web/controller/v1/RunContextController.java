package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.dto.ExchangeRunContextResult;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.vo.RerunCriteria;
import com.zebrunner.reporting.domain.vo.RunContext;
import com.zebrunner.reporting.service.RunContextService;
import com.zebrunner.reporting.web.request.v1.ExchangeRunContextRequest;
import com.zebrunner.reporting.web.response.v1.ExchangeRunContextResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/run-context-exchanges")
public class RunContextController {

    private final JMapper jMapper;
    private final RunContextService runContextService;

    @PostMapping
    public ExchangeRunContextResponse exchangeRunContext(@Valid @RequestBody ExchangeRunContextRequest request) {
        RerunCriteria rerunCriteria = jMapper.map(request.getRerunCriteria(), RerunCriteria.class);
        RunContext runContext = new RunContext(request.getTestRunUuid(), request.getMode(), rerunCriteria);

        // to be removed
        if (request.getId() != null) {
            runContext.setTestRunUuid(request.getId());
            runContext.setMode(RunContext.Mode.LEGACY);
        }
        if (CollectionUtils.isNotEmpty(request.getStatuses())) {
            runContext.getRerunCriteria().setAnyOfStatuses(request.getStatuses());
            runContext.getRerunCriteria().setKnownIssue(false);
            runContext.setMode(RunContext.Mode.RERUN);
        }

        ExchangeRunContextResult result = runContextService.exchange(runContext);

        List<ExchangeRunContextResponse.Test> testsToRun = jMapper.mapToList(
                result.getTestsToRun(), ExchangeRunContextResponse.Test.class
        );
        Set<Test.Status> anyOfStatuses = runContext.getRerunCriteria().getAnyOfStatuses();
        boolean rerunOnlyFailedTests = anyOfStatuses != null && anyOfStatuses.contains(Test.Status.FAILED);
        return ExchangeRunContextResponse.builder()
                                         .testRunUuid(runContext.getTestRunUuid())
                                         .mode(runContext.getMode())
                                         .runAllowed(result.isRunAllowed())
                                         .reason(result.getReason())
                                         .runOnlySpecificTests(result.getTestsToRun() != null)
                                         .testsToRun(testsToRun)
                                         .fullExecutionPlanContext(result.getFullExecutionPlanContext())
                                         .rerunOnlyFailedTests(rerunOnlyFailedTests)
                                         .build();
    }

}
