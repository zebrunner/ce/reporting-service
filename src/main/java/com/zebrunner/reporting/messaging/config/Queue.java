package com.zebrunner.reporting.messaging.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Queue {

    public static final String SAVE_TEST_ARTIFACT_REFERENCE = "reporting.test-run-artifact.save-reference.reporting";
//    public static final String SAVE_TEST_SESSION_ARTIFACT_REFERENCE = "reporting.test-session-artifact.save-reference.reporting";
    public static final String USER_SAVED = "user.saved.reporting";
    public static final String USER_DELETED = "user.deleted.reporting";

}
