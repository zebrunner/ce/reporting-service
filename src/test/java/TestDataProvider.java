import com.zebrunner.reporting.domain.entity.Dashboard;
import com.zebrunner.reporting.domain.entity.Setting;
import com.zebrunner.reporting.domain.entity.Widget;
import org.junit.jupiter.params.provider.Arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

public class TestDataProvider {

    public static Stream<Arguments> provideSettings() {

        List<Arguments> settings = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            Setting setting = new Setting();
            setting.setName("TestSetting" + UUID.randomUUID().getLeastSignificantBits());
            setting.setId((long) i);
            setting.setValue("TestValue" + UUID.randomUUID().getLeastSignificantBits());

            settings.add(Arguments.of(setting));
        }


        return settings.stream();
    }

    public static Stream<Arguments> provideWidgets() {
        List<Arguments> widgets = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            Widget widget = new Widget();
            widget.setTitle("WidgetTestTitle");
            widgets.add(Arguments.of(widget));
        }

        return widgets.stream();
    }

    public static Stream<Arguments> provideWidgetIdsAndDashboardIds() {
        return Stream.of(
                Arguments.of(1001L, 1001L),
                Arguments.of(1002L, 1002L),
                Arguments.of(1003L, 1003L),
                Arguments.of(1004L, 1004L)
        );
    }

    public static Stream<Arguments> provideDashboards() {
        List<Arguments> dashboards = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            Dashboard dashboard = new Dashboard();
            dashboard.setTitle("TestDashboardTitle");

            dashboards.add(Arguments.of(dashboard));
        }

        return dashboards.stream();
    }

    public static Stream<Arguments> provideDashboardIdsAndPosition() {
        return Stream.of(
                Arguments.of(1001L, 1001),
                Arguments.of(1002L, 1002),
                Arguments.of(1003L, 1003),
                Arguments.of(1004L, 1004)
        );
    }

    public static Stream<Arguments> provideWidgetsTitlesAndTemplateIds() {
        return Stream.of(
                Arguments.of("Title1", 1001L),
                Arguments.of("Title2", 1002L),
                Arguments.of("Title3", 1003L),
                Arguments.of("Title4", 1004L)
        );
    }
}
