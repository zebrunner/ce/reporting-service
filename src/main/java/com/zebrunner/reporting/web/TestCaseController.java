package com.zebrunner.reporting.web;

import com.zebrunner.reporting.domain.db.TestCase;
import com.zebrunner.reporting.domain.dto.TestCaseType;
import com.zebrunner.reporting.service.TestCaseService;
import lombok.RequiredArgsConstructor;
import org.dozer.Mapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api/tests/cases", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestCaseController {

    private final Mapper mapper;
    private final TestCaseService testCaseService;

    @PostMapping
    public TestCaseType createTestCase(@RequestBody @Valid TestCaseType testCase) {
        TestCase tc = mapper.map(testCase, TestCase.class);
        tc = testCaseService.save(tc);
        return mapper.map(tc, TestCaseType.class);
    }

}
