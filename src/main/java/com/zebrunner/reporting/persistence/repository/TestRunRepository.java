package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestRun;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TestRunRepository extends JpaRepository<TestRun, Long> {

    @Query("SELECT tr FROM TestRun tr " +
            "LEFT JOIN FETCH tr.labels " +
            "LEFT JOIN FETCH tr.statistic")
    List<TestRun> findAll();

    boolean existsByCiRunId(String ciRunId);

    Optional<TestRun> findByCiRunId(String ciRunId);

    @Modifying
    @Query("UPDATE TestRun tr SET tr.user.id = :toUserId WHERE tr.user.id = :fromUserId")
    void reassignUser(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

}
