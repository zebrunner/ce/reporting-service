package com.zebrunner.reporting.web.response.v1;

public enum ErrorReason {

    NOT_FOUND,
    ILLEGAL_ASSIGNMENT

}
