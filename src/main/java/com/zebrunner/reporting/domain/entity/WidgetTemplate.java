package com.zebrunner.reporting.domain.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;

@Data
@NoArgsConstructor
@Entity
@Table(name = "widget_templates", schema = "management")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class WidgetTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @Enumerated(EnumType.STRING)
    private Type type;
    private String sql;
    private String chartConfig;
    @Column(columnDefinition = "jsonb")
    @org.hibernate.annotations.Type(type = "jsonb")
    private Map<String, WidgetTemplateParameter> paramsConfig;
    private String legendConfig;
    private Boolean hidden;

    public enum Type {
        PIE,
        LINE,
        BAR,
        TABLE,
        OTHER
    }

}
