package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.db.Job;
import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import com.zebrunner.reporting.persistence.dao.mysql.application.JobMapper;
import com.zebrunner.reporting.service.integration.jenkins.JenkinsConfigService;
import com.zebrunner.reporting.service.user.UserReassignmentListener;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class JobsService implements UserReassignmentListener {

    private final JobMapper jobMapper;
    private final JenkinsConfigService jenkinsConfigService;

    @Transactional
    public Job createOrUpdate(Job newJob) {
        Optional<Job> maybeJob = jobMapper.findByJobUrl(newJob.getJobURL());
        if (maybeJob.isPresent()) {
            Job job = maybeJob.get();
            if (!job.equals(newJob)) {
                newJob.setId(job.getId());
                jobMapper.update(job);
            } else {
                newJob = job;
            }
        } else {
            Optional<JenkinsConfig> maybeJenkinsConfig = jenkinsConfigService.retrieveOptionalByUrl(newJob.getJenkinsHost(), false);
            if (maybeJenkinsConfig.isPresent()) {
                newJob.setJenkinsConfigId(maybeJenkinsConfig.get().getId());
            }
            jobMapper.create(newJob);
        }
        return newJob;
    }

    @Override
    @Transactional
    public void onUserReassignment(Long fromId, Long toId) {
        jobMapper.reassignUser(fromId, toId);
    }

}
