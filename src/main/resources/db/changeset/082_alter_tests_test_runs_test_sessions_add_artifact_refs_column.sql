ALTER TABLE IF EXISTS tests
    DROP COLUMN IF EXISTS artifact_references,
    ADD COLUMN IF NOT EXISTS artifact_references JSONB NOT NULL DEFAULT '[]';

ALTER TABLE IF EXISTS test_runs
    DROP COLUMN IF EXISTS artifact_references,
    ADD COLUMN IF NOT EXISTS artifact_references JSONB NOT NULL DEFAULT '[]';

ALTER TABLE IF EXISTS test_sessions
    DROP COLUMN IF EXISTS artifact_references,
    ADD COLUMN IF NOT EXISTS artifact_references JSONB NOT NULL DEFAULT '[]';

WITH tests_artifacts AS (
    SELECT tar.test_id,
           jsonb_agg(json_build_object('name', ar.name,
                                       'value', ar.value,
                                       'createdAt', replace(concat(ar.created_at, 'Z'), ' ', 'T'))
           ) AS artifact_references
    FROM tests_artifact_references tar JOIN artifact_references ar ON ar.id = tar.artifact_reference_id
    GROUP BY tar.test_id
)
UPDATE tests t
SET artifact_references = ta.artifact_references FROM tests_artifacts ta
WHERE t.id = ta.test_id;


WITH test_runs_artifacts AS (
    SELECT trar.test_run_id,
           jsonb_agg(json_build_object('name', ar.name,
                                       'value', ar.value,
                                       'createdAt', replace(concat(ar.created_at, 'Z'), ' ', 'T'))
           ) AS artifact_references
    FROM test_runs_artifact_references trar JOIN artifact_references ar ON ar.id = trar.artifact_reference_id
    GROUP BY trar.test_run_id
)
UPDATE test_runs tr
SET artifact_references = tra.artifact_references FROM test_runs_artifacts tra
WHERE tr.id = tra.test_run_id;


WITH test_sessions_artifacts AS (
    SELECT tsar.test_session_id,
           jsonb_agg(json_build_object('name', ar.name,
                                       'value', ar.value,
                                       'createdAt', replace(concat(ar.created_at, 'Z'), ' ', 'T'))
           ) AS artifact_references
    FROM test_sessions_artifact_references tsar JOIN artifact_references ar ON ar.id = tsar.artifact_reference_id
    GROUP BY tsar.test_session_id
)
UPDATE test_sessions ts
SET artifact_references = tsa.artifact_references FROM test_sessions_artifacts tsa
WHERE ts.id = tsa.test_session_id;
