package com.zebrunner.reporting.domain.dto.reporting;

import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.entity.Test;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestFinish {

    private OffsetDateTime endedAt;
    private Test.Status result;
    private String reason;
    // todo: for backward compatibility
    private Set<Label> labels = Set.of();

}
