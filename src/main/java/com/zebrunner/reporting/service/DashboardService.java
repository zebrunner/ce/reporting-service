package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import com.zebrunner.reporting.domain.entity.Dashboard;
import com.zebrunner.reporting.domain.push.events.Attachment;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

public interface DashboardService {

    Dashboard create(Dashboard dashboard);

    Page<Dashboard> search(SearchCriteria searchCriteria);

    Dashboard retrieveById(Long id);

    Dashboard retrieveByTitle(String name);

    List<Dashboard> retrieveAll();

    Page<Dashboard> retrieveLast(Long userId, Integer limit);

    void verifyExistenceById(Long id);

    Dashboard update(Long id, Dashboard updatedDashboard);

    void deleteById(Long id);

    void sendByEmail(Long id, String subject, String body, List<Attachment> attachments, Set<String> toEmails);

}
