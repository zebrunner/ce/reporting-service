package com.zebrunner.reporting.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "test_suite_execution_statistics")
public class TestRunStatistic {

    @Id
    private Long testSuiteExecutionId;

    private int passedAmount;
    private int failedAmount;
    private int knownIssuesAmount;
    private int skippedAmount;
    private int inProgressAmount;
    private int abortedAmount;

    @MapsId
    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private TestRun testSuiteExecution;

    public TestRunStatistic(TestRun testSuiteExecution) {
        this.testSuiteExecution = testSuiteExecution;
    }

    public void trackTestStart() {
        this.inProgressAmount += 1;
    }

    public void trackTestPass() {
        this.inProgressAmount -= 1;
        this.passedAmount += 1;
    }

    public void trackTestFailure() {
        this.inProgressAmount -= 1;
        this.failedAmount += 1;
    }

    public void trackTestAbortion() {
        this.inProgressAmount -= 1;
        this.abortedAmount += 1;
    }

    public void trackTestSkip() {
        this.inProgressAmount -= 1;
        this.skippedAmount += 1;
    }

}
