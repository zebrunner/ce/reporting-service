package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestSearchCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Mapper
public interface TestMapper {

    void create(Test test);

    Optional<Test> findById(long id);

    Optional<Test> findByIdAndTestRunId(@Param("id") long id, @Param("testRunId") long testRunId);

    boolean existsById(long id);

    List<Test> findAllByIds(List<Long> ids);

    List<Test> findByTestRunId(long testRunId);

    List<TestResult> findResultsByStartTimeAndTestCaseId(@Param("testCaseId") Long testCaseId,
                                                         @Param("startTime") Date startTime,
                                                         @Param("limit") Long limit);

    List<Test> findByTestRunCiRunId(String ciRunId);

    void update(Test test);

    void updateStatuses(@Param("ids") List<Long> ids, @Param("status") Status status);

    void updateTestsNeedRerun(@Param("ids") List<Long> ids, @Param("rerun") boolean needRerun);

    List<Test> search(TestSearchCriteria sc);

    Integer findSearchCount(TestSearchCriteria sc);

}
