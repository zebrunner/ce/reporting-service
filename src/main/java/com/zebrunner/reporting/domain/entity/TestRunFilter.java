package com.zebrunner.reporting.domain.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;
import java.util.List;
import java.util.Map;

@Data
@Entity
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "test_suite_execution_filters")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class TestRunFilter {

    @Id
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Map<String, List<String>> items;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "owner_id")
    private User owner;

    private Boolean isPrivate;

    @EqualsAndHashCode.Exclude
    @Generated(GenerationTime.ALWAYS)
    @Column(name = "created_at", insertable = false, updatable = false)
    private Instant createdAt;

}
