package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.Instant;
import java.util.List;

@Value
public class AssignIssueReferencesResponse {

    List<Object> items;

    @Data
    @JGlobalMap
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NoArgsConstructor
    public static class IssueReference {

        private Long id;
        private String type;
        private String value;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Instant createdAt;

        @JMapConversion(from = {"type"}, to = {"type"})
        public String convertType(com.zebrunner.reporting.domain.entity.integration.IssueReference.Type type) {
            return type.name();
        }

    }

}
