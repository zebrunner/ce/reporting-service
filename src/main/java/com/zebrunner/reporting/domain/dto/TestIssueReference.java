package com.zebrunner.reporting.domain.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JGlobalMap
@NoArgsConstructor
public class TestIssueReference {

    private Long testId;
    private IssueReference.Type type;
    private String value;

    @JMapConversion(from = {"type"}, to = {"type"})
    public IssueReference.Type convertType(String type) {
        return IssueReference.Type.valueOf(type);
    }

}
