package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.HasPermission;
import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.service.integration.jenkins.JenkinsConfigService;
import com.zebrunner.reporting.service.util.StreamUtils;
import com.zebrunner.reporting.web.patch.PatchAction;
import com.zebrunner.reporting.web.patch.PatchOperation;
import com.zebrunner.reporting.web.patch.PatchRequest;
import com.zebrunner.reporting.web.request.v1.jenkins.CheckJenkinsConnectionRequest;
import com.zebrunner.reporting.web.request.v1.jenkins.CreateJenkinsConfigRequest;
import com.zebrunner.reporting.web.request.v1.jenkins.UpdateJenkinsConfigRequest;
import com.zebrunner.reporting.web.response.v1.CheckConnectionResponse;
import com.zebrunner.reporting.web.response.v1.jenkins.CreateJenkinsConfigResponse;
import com.zebrunner.reporting.web.response.v1.jenkins.GetJenkinsConfigResponse;
import com.zebrunner.reporting.web.response.v1.jenkins.ListJenkinsConfigsResponse;
import com.zebrunner.reporting.web.response.v1.jenkins.UpdateJenkinsConfigResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/integrations/jenkins", produces = MediaType.APPLICATION_JSON_VALUE)
public class JenkinsController {

    private final JMapper mapper;
    private final JenkinsConfigService configService;

    @GetMapping
    @PreAuthorize("hasPermission('reporting:integrations:read')")
    public ListJenkinsConfigsResponse getAll(@RequestParam(name = "onlyActive", defaultValue = "false") boolean onlyActive,
                                             @RequestParam(name = "decryptValues", defaultValue = "false") boolean decryptValues,
                                             @HasPermission("internal-access") boolean isInternalUser) {
        if (!isInternalUser) {
            decryptValues = false;
        }

        List<JenkinsConfig> configs = onlyActive
                                      ? configService.retrieveAllActive(decryptValues)
                                      : configService.retrieveAll(decryptValues);

        List<ListJenkinsConfigsResponse.Item> responseItems
                = StreamUtils.mapToList(configs, config -> mapper.map(config, ListJenkinsConfigsResponse.Item.class));
        return new ListJenkinsConfigsResponse(responseItems);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasPermission('reporting:integrations:read')")
    public GetJenkinsConfigResponse getById(@PathVariable("id") Long id,
                                            @RequestParam(name = "onlyActive", defaultValue = "false") boolean onlyActive,
                                            @RequestParam(name = "decryptValues", defaultValue = "false") boolean decryptValues,
                                            @HasPermission("internal-access") boolean isInternalUser) {
        if (!isInternalUser) {
            decryptValues = false;
        }

        JenkinsConfig config = onlyActive
                               ? configService.retrieveActiveById(id, decryptValues)
                               : configService.retrieveById(id, decryptValues);

        return mapper.map(config, GetJenkinsConfigResponse.class);
    }

    @PostMapping
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public CreateJenkinsConfigResponse create(@RequestBody @Valid CreateJenkinsConfigRequest request) {
        JenkinsConfig config = mapper.map(request, JenkinsConfig.class);
        config = configService.create(config);
        return mapper.map(config, CreateJenkinsConfigResponse.class);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public UpdateJenkinsConfigResponse update(@PathVariable Long id,
                                              @RequestBody @Valid UpdateJenkinsConfigRequest request) {
        JenkinsConfig config = mapper.map(request, JenkinsConfig.class);
        config = configService.update(id, config);
        return mapper.map(config, UpdateJenkinsConfigResponse.class);
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public void patch(@PathVariable Long id,
                      @PatchAction(op = PatchOperation.REPLACE, path = "/enabled")
                      @RequestBody PatchRequest patchRequest) {
        Boolean enabled = patchRequest.getReplaceValue("/enabled", Boolean.class);
        if (enabled != null) {
            configService.patchEnabledProperty(id, enabled);
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public void deleteById(@PathVariable("id") Long id) {
        configService.deleteById(id);
    }

    @PostMapping("/connectivity-checks")
    public CheckConnectionResponse checkConnection(@Valid @RequestBody CheckJenkinsConnectionRequest request) {
        JenkinsConfig config = mapper.map(request, JenkinsConfig.class);
        ReachabilityCheckResult result = configService.isReachable(config);
        return mapper.map(result, CheckConnectionResponse.class);
    }

}
