DROP TABLE IF EXISTS test_sessions CASCADE;
DROP TABLE IF EXISTS test_sessions_2 CASCADE;

DELETE
FROM test_sessions_transient
WHERE test_run_id IS NULL;

ALTER TABLE test_sessions_transient
  RENAME TO test_sessions;
ALTER TABLE test_sessions
  ALTER COLUMN test_run_id SET NOT NULL;

ALTER TABLE test_test_sessions_transient
  RENAME TO tests_test_sessions;
CREATE INDEX tests_test_sessions_test_id ON tests_test_sessions (test_id);
CREATE INDEX tests_test_sessions_test_session_id ON tests_test_sessions (test_session_id);
