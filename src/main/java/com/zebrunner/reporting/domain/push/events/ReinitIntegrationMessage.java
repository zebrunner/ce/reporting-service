package com.zebrunner.reporting.domain.push.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReinitIntegrationMessage {

    private Long integrationId;

}
