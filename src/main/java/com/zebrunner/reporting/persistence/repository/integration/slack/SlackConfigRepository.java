package com.zebrunner.reporting.persistence.repository.integration.slack;

import com.zebrunner.reporting.domain.entity.integration.SlackConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SlackConfigRepository extends JpaRepository<SlackConfig, Long> {

    Optional<SlackConfig> findFirstByOrderById();

    default Optional<SlackConfig> findEnabled() {
        return findFirstByEnabled(true);
    }

    Optional<SlackConfig> findFirstByEnabled(boolean enabled);

}
