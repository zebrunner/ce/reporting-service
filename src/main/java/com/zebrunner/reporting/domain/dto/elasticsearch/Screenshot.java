package com.zebrunner.reporting.domain.dto.elasticsearch;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Screenshot {

    @JsonProperty("src")
    private String message;
    private Long timestamp;

}
