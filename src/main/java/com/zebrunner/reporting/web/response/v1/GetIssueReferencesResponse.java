package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.time.Instant;
import java.util.List;

@Value
public class GetIssueReferencesResponse {

    List<Item> items;

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Item {

        private Long id;
        private String value;
        private String type;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Instant createdAt;

    }

}
