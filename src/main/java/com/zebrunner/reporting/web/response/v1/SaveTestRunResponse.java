package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.domain.entity.TestRun;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SaveTestRunResponse {

    private Long id;
    private String uuid;
    private String name;
    private TestRun.Status status;
    private OffsetDateTime startedAt;
    private OffsetDateTime endedAt;
    private String framework;
    private Config config;
    private List<Label> labels;
    private List<ArtifactReference> artifactReferences;

    @Value
    public static class Config {

        String environment;
        String build;

    }

    @Value
    public static class Label {

        String key;
        String value;

    }

    @Value
    public static class ArtifactReference {

        String name;
        String value;

    }

}
