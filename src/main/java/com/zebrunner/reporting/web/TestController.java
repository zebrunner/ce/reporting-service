package com.zebrunner.reporting.web;

import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestResult;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.TestResultDTO;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.domain.dto.TestType;
import com.zebrunner.reporting.domain.push.TestPush;
import com.zebrunner.reporting.domain.push.TestRunPush;
import com.zebrunner.reporting.domain.push.TestRunStatisticPush;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.messaging.websocket.Topic;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.SearchResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestSearchCriteria;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestRunService;
import com.zebrunner.reporting.service.TestRunStatisticsService;
import com.zebrunner.reporting.service.TestService;
import com.zebrunner.reporting.web.request.v1.AttachArtifactReferencesRequest;
import com.zebrunner.reporting.web.util.patch.BatchPatchDescriptor;
import com.zebrunner.reporting.web.util.patch.PatchDecorator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "api/tests", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestController {

    private final Mapper mapper;
    private final TestService testService;
    private final Test2Service test2Service;
    private final TestRunService testRunService;
    private final SimpMessagingTemplate websocketTemplate;
    private final TestRunStatisticsService statisticsService;

    @PostMapping
    public TestType startTest(@Valid @RequestBody TestType t) {
        Test test = testService.startTest(mapper.map(t, Test.class));
        TestRunStatistics testRunStatistic = statisticsService.get(test.getTestRunId());
        websocketTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistic));
        websocketTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test));
        return mapper.map(test, TestType.class);
    }

    @PostMapping("/{id}/finish")
    public TestType finishTest(@PathVariable("id") long id, @RequestBody TestType t) {
        Test test = mapper.map(t, Test.class);
        test.setId(id);
        testService.finishTest(test);

        // Needs to get clear data from Db to avoid confusion due finishTest action result construction
        test = testService.getTestById(id);
        TestRunStatistics testRunStatistic = statisticsService.get(test.getTestRunId());
        websocketTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistic));
        websocketTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test));
        return mapper.map(test, TestType.class);
    }

    @PutMapping
    @PreAuthorize("hasPermission('reporting:tests:update')")
    public Test updateTest(@RequestBody Test test) {
        Test updatedTest = testService.changeTestStatus(test.getId(), test.getStatus());

        TestRunStatistics testRunStatistic = statisticsService.get(updatedTest.getTestRunId());
        websocketTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistic));
        websocketTemplate.convertAndSend(Topic.tests(updatedTest.getTestRunId()), new TestPush(updatedTest));

        TestRun testRun = testRunService.getTestRunById(updatedTest.getTestRunId());
        websocketTemplate.convertAndSend(Topic.testRuns(), new TestRunPush(testRun));

        return updatedTest;
    }

    @PatchMapping("/runs/{testRunId}")
    @PreAuthorize("hasPermission('reporting:tests:update')")
    public List<Test> batchPatch(@Valid @RequestBody BatchPatchDescriptor descriptor,
                                 @PathVariable("testRunId") Long testRunId) {
        List<Test> tests = PatchDecorator.<List<Test>, PatchOperation>instance()
                                         .descriptor(descriptor)
                                         .operation(PatchOperation.class)
                                         .<Status>when(PatchOperation.STATUS_UPDATE)
                                         .withParameter(Status::valueOf)
                                         .then(status -> testService.batchStatusUpdate(testRunId, descriptor.getIds(), status))
                                         .after()
                                         .decorate();

        TestRunStatistics testRunStatistic = statisticsService.get(testRunId);
        websocketTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistic));

        tests.forEach(test -> websocketTemplate.convertAndSend(Topic.tests(testRunId), new TestPush(test)));

        TestRun testRun = testRunService.getTestRunById(testRunId);
        websocketTemplate.convertAndSend(Topic.testRuns(), new TestRunPush(testRun));

        return tests;
    }

    enum PatchOperation {
        STATUS_UPDATE
    }

    @DeleteMapping("/{id}")
    public void deleteTest(@PathVariable("id") Long id) {
        test2Service.deleteById(id);
    }

    @PostMapping("/search")
    public SearchResult<Test> searchTests(@RequestBody TestSearchCriteria sc) {
        return testService.searchTests(sc);
    }

    @GetMapping("/{id}/history")
    public List<TestResultDTO> getTestResultsById(@PathVariable("id") Long id,
                                                  @RequestParam("limit") Long limit) {
        List<TestResult> results = testService.getLatestTestResultsByTestId(id, limit);
        return results.stream()
                      .map(stability -> mapper.map(stability, TestResultDTO.class))
                      .collect(Collectors.toList());
    }

    @PostMapping("/{id}/artifacts")
    public void addTestArtifact(@PathVariable("id") Long id,
                                @Valid @RequestBody AttachArtifactReferencesRequest.ArtifactReference artifact) {
        ArtifactReference reference = mapper.map(artifact, ArtifactReference.class);
        test2Service.attachArtifactReferences(id, Set.of(reference));
        // Updating web client with latest artifacts
        Test test = testService.getTestById(id);
        websocketTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test));
    }

}
