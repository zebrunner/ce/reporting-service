package com.zebrunner.reporting.service.widgettemplate;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import com.zebrunner.reporting.persistence.repository.WidgetTemplateRepository;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class WidgetTemplateServiceImpl implements WidgetTemplateService {

    private final WidgetTemplateRepository widgetTemplateRepository;

    private final WidgetTemplateQueryExecutor widgetTemplateQueryExecutor;

    @Override
    @Transactional(readOnly = true)
    public WidgetTemplate retrieveById(Long id) {
        return widgetTemplateRepository.findById(id)
                                       .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.WIDGET_TEMPLATE_NOT_FOUND_BY_ID, id));
    }

    @Override
    @Transactional(readOnly = true)
    public List<WidgetTemplate> retrieveAllVisible() {
        return widgetTemplateRepository.findByOrderByNameAllIgnoreCaseAsc()
                                       .stream()
                                       .filter(widgetTemplate -> !widgetTemplate.getHidden())
                                       .peek(this::setNotNullParamsValues)
                                       .collect(Collectors.toList());
    }

    @Override
    @SneakyThrows
    public void setNotNullParamsValues(WidgetTemplate widgetTemplate) {
        if (widgetTemplate != null) {
            widgetTemplate.getParamsConfig()
                          .values()
                          .stream()
                          .filter(param -> param.getValuesQuery() != null && param.getValues() == null)
                          .forEach(param -> param.setValues(Collections.emptyList()));
        }
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public WidgetTemplate retrieveByIdWithParamsValues(Long id, String period) {
        WidgetTemplate widgetTemplate = retrieveById(id);

        for (WidgetTemplateParameter param : widgetTemplate.getParamsConfig().values()) {
            List<Object> queryParamValues = widgetTemplateQueryExecutor.executeTemplateParamQuery(param, period);
            param.setValues(queryParamValues);
        }

        return widgetTemplate;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> getQueryResults(Long templateId, Map<String, Object> queryParameters, Integer userId) {
        WidgetTemplate widgetTemplate = retrieveById(templateId);
        return widgetTemplateQueryExecutor.executeTemplateSql(widgetTemplate, queryParameters, userId);
    }

}
