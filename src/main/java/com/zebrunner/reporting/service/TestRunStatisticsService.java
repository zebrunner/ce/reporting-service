package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestRunMapper;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestRunStatisticsService {

    private final TestRunMapper testRunMapper;

    public TestRunStatistics get(Long testRunId) {
        return testRunMapper.findStatistics(testRunId)
                            .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_STATISTICS_NOT_FOUND_BY_TEST_RUN_ID, testRunId));
    }

}
