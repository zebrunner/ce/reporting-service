package com.zebrunner.reporting.service.cache.impl;

import com.zebrunner.reporting.domain.db.User;
import com.zebrunner.reporting.persistence.dao.mysql.application.UserMapper;
import com.zebrunner.reporting.service.cache.UserCacheableService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserCacheableServiceImpl implements UserCacheableService {

    private final UserMapper userMapper;

    @Override
    @CacheEvict(value = "users", condition = "#user.id != null", key = "T( com.zebrunner.reporting.persistence.PersistenceConfig).DEFAULT_SCHEMA + ':' + #user.id")
    public User updateUser(User user) {
        userMapper.update(user);
        return user;
    }

}
