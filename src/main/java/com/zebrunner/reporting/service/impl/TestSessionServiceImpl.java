package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.TestConfig;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.persistence.repository.TestSessionRepository;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestConfigService;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestSessionService;
import com.zebrunner.reporting.service.util.CapabilitiesManager;
import com.zebrunner.reporting.service.util.StreamUtils;
import com.zebrunner.reporting.service.util.TestSessionArtifactsManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.zebrunner.reporting.service.exception.BusinessConstraintError.ENDED_AT_MUST_BE_AFTER_STARTED_AT;
import static com.zebrunner.reporting.service.exception.BusinessConstraintError.FAILED_TEST_SESSION_CANNOT_BE_UPDATED;
import static com.zebrunner.reporting.service.exception.BusinessConstraintError.TEST_SESSION_WITH_SESSION_ID_ALREADY_EXISTS;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_NOT_FOUND_BY_ID;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_ID;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_SESSION_NOT_EXIST_BY_ID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestSessionServiceImpl implements TestSessionService {

    private final Test2Service test2Service;
    private final TestRun2Service testRun2Service;
    private final TestConfigService testConfigService;
    private final CapabilitiesManager capabilitiesManager;
    private final TestSessionRepository testSessionRepository;
    private final TestSessionArtifactsManager testSessionArtifactsManager;

    @Override
    @Transactional(readOnly = true)
    public List<TestSession> retrieveByTestRunId(Long testRunId) {
        if (!testRun2Service.existsById(testRunId)) {
            throw new ResourceNotFoundException(TEST_RUN_NOT_FOUND_BY_ID, testRunId);
        }
        return testSessionRepository.findByTestRunId(testRunId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TestSession> retrieveByTestRunIdAndTestId(Long testRunId, Long testId) {
        if (!test2Service.existsByTestRunIdAndId(testRunId, testId)) {
            throw new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID, testId);
        }
        return testSessionRepository.findByTestRunIdAndTestId(testRunId, testId);
    }

    @Override
    @Transactional
    public TestSession create(TestSession session, Collection<Long> testIds) {
        String sessionId = session.getSessionId();
        if (sessionId != null && testSessionRepository.existsBySessionId(sessionId)) {
            throw new BusinessConstraintException(TEST_SESSION_WITH_SESSION_ID_ALREADY_EXISTS, sessionId);
        }

        List<Test> tests = getTestsByIds(session.getTestRunId(), testIds);
        session.setTests(new HashSet<>(tests));

        capabilitiesManager.getBrowserName(session).ifPresent(session::setBrowserName);
        capabilitiesManager.getBrowserVersion(session).ifPresent(session::setBrowserVersion);
        capabilitiesManager.getPlatformName(session).ifPresent(session::setPlatformName);
        capabilitiesManager.getPlatformVersion(session).ifPresent(session::setPlatformVersion);
        capabilitiesManager.getDeviceName(session).ifPresent(session::setDeviceName);

        if (session.getStartedAt() != null) {
            testSessionArtifactsManager.attachVncArtifact(session);
            testSessionArtifactsManager.copySessionArtifactsToTests(session);
        }

        // internals of this invocation will throw exception, if there is not test run with given id
        updateTestRunConfig(session);

        testSessionRepository.save(session);

        return session;
    }

    private void updateTestRunConfig(TestSession session) {
        if (testSessionRepository.countByTestRunId(session.getTestRunId()) == 0) {
            TestConfig newConfig = TestConfig.builder()
                                             .browser(session.getBrowserName())
                                             .browserVersion(session.getBrowserVersion())
                                             .platform(session.getPlatformName())
                                             .platformVersion(session.getPlatformVersion())
                                             .device(session.getDeviceName())
                                             .build();
            testConfigService.updateTestRunConfig(session.getTestRunId(), newConfig);
        }
    }

    @Override
    @Transactional
    public TestSession update(TestSession session, Collection<Long> testIds) {
        Long id = session.getId();
        TestSession existingSession = testSessionRepository.findById(id)
                                                           .orElseThrow(() -> new ResourceNotFoundException(TEST_SESSION_NOT_EXIST_BY_ID, id));
        if (existingSession.getStatus() == TestSession.Status.FAILED) {
            throw new BusinessConstraintException(FAILED_TEST_SESSION_CANNOT_BE_UPDATED, TestSession.Status.FAILED);
        }

        addTestsToSession(existingSession, testIds);

        if (session.getEndedAt() != null) {
            if (session.getEndedAt().isAfter(existingSession.getStartedAt())) {
                existingSession.setEndedAt(session.getEndedAt());
                existingSession.setStatus(TestSession.Status.COMPLETED);
                testSessionArtifactsManager.attachCompletionArtifacts(existingSession);
                testSessionArtifactsManager.detachVncArtifact(existingSession);
            } else {
                throw new BusinessConstraintException(ENDED_AT_MUST_BE_AFTER_STARTED_AT);
            }
        }

        // at this point vnc artifact is already attached to the session
        testSessionArtifactsManager.copySessionArtifactsToTests(existingSession);

        return testSessionRepository.save(existingSession);
    }

    private List<Test> getTestsByIds(Long testRunId, Collection<Long> testIds) {
        return !CollectionUtils.isEmpty(testIds)
                ? test2Service.retrieveAllByTestRunIdAndIds(testRunId, testIds)
                : List.of();
    }

    private void addTestsToSession(TestSession session, Collection<Long> testIds) {
        if (!CollectionUtils.isEmpty(testIds)) {
            Set<Long> currentTestIds = StreamUtils.mapToSet(session.getTests(), Test::getId);

            testIds.removeAll(currentTestIds);
            List<Test> existingTests = getTestsByIds(session.getTestRunId(), testIds);

            session.getTests().addAll(existingTests);
        }
    }

    @Override
    @Transactional
    public void finishNotCompleted(Long testRunId, Instant endedAt) {
        List<TestSession> notFinishedSessions = testSessionRepository.findByTestRunIdAndEndedAtIsNull(testRunId);
        notFinishedSessions.stream()
                           .filter(testSession -> testSession.getStatus() != TestSession.Status.FAILED)
                           .map(testSession -> TestSession.builder()
                                                          .id(testSession.getId())
                                                          .endedAt(endedAt)
                                                          .build())
                           .forEach(session -> update(session, Set.of()));
    }

    @Override
    @Transactional
    public void attachArtifactReferences(Long id, Set<ArtifactReference> newReferences) {
        TestSession session = testSessionRepository.findById(id)
                                                   .orElseThrow(() -> new ResourceNotFoundException(TEST_SESSION_NOT_EXIST_BY_ID, id));
        session.getArtifactReferences().removeIf(sessionReference -> newReferences.stream().anyMatch(reference -> reference.getName().equals(sessionReference.getName())));
        session.getArtifactReferences().addAll(newReferences);
        testSessionRepository.save(session);
    }

}
