package com.zebrunner.reporting.web.converter.v1;

import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.web.response.v1.SaveTestRunResponse;
import com.zebrunner.reporting.web.response.v1.SaveTestRunResponse.ArtifactReference;
import com.zebrunner.reporting.web.response.v1.SaveTestRunResponse.Label;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SaveTestRunResponseConverter {

    public SaveTestRunResponse toSaveResponse(TestRun testRun) {
        return SaveTestRunResponse.builder()
                                  .id(testRun.getId())
                                  .name(testRun.getName())
                                  .uuid(testRun.getCiRunId())
                                  .status(testRun.getStatus())
                                  .startedAt(testRun.getStartedAt())
                                  .endedAt(testRun.getEndedAt())
                                  .framework(testRun.getFramework())
                                  .labels(convertLabels(testRun))
                                  .artifactReferences(convertArtifactReferences(testRun))
                                  .build();
    }

    private List<Label> convertLabels(TestRun testRun) {
        return testRun.getLabels()
                      .stream()
                      .map(label -> new Label(label.getKey(), label.getValue()))
                      .collect(Collectors.toList());
    }

    private List<ArtifactReference> convertArtifactReferences(TestRun testRun) {
        return testRun.getArtifactReferences()
                      .stream()
                      .map(reference -> new ArtifactReference(reference.getName(), reference.getValue()))
                      .collect(Collectors.toList());
    }

}
