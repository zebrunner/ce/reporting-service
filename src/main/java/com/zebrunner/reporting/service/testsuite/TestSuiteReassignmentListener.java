package com.zebrunner.reporting.service.testsuite;

public interface TestSuiteReassignmentListener {

    void onTestSuiteReassignment(Long fromId, Long toId);

}
