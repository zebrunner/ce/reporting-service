package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@JGlobalMap
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LegacySaveTestResponse {

    private Long id;
    private String name;
    private String className;
    private String methodName;
    private OffsetDateTime startedAt;
    private OffsetDateTime endedAt;
    private String maintainer;
    private String testCase;
    @JsonIgnore
    private List<Label> labels;
    private List<ArtifactReference> artifactReferences;
    private String result;
    private String reason;

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Label {

        private String key;
        private String value;

    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class ArtifactReference {

        private String name;
        private String value;

    }

}
