package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.TestSuite;
import com.zebrunner.reporting.persistence.repository.TestSuiteRepository;
import com.zebrunner.reporting.service.TestSuite2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TestSuite2ServiceImpl implements TestSuite2Service {

    private final TestSuiteRepository testSuiteRepository;

    @Override
    @Transactional
    public TestSuite getOrCreate(TestSuite testSuite) {
        String name = testSuite.getName();
        String fileName = testSuite.getFileName();
        Long userId = testSuite.getUser().getId();
        return testSuiteRepository.findByNameAndFileNameAndUserId(name, fileName, userId)
                                  .orElseGet(() -> testSuiteRepository.save(testSuite));
    }

}
