-- JIRA
DO
$$
    DECLARE
        JIRA_INTEGRATION_ID_VAR      integrations.id%TYPE;
        JIRA_INTEGRATION_NAME_VAR    integrations.name%TYPE;
        JIRA_URL_VAR                 integration_settings.value%TYPE;
        JIRA_USER_VAR                integration_settings.value%TYPE;
        JIRA_PASSWORD_VAR            integration_settings.value%TYPE;
        JIRA_INTEGRATION_ENABLED_VAR integrations.enabled%TYPE;
        JIRA_TYPE_VAR                VARCHAR;
        JIRA_PASSWORD_ENCRYPTED_VAR  integration_settings.encrypted%TYPE;
    BEGIN
        FOR JIRA_INTEGRATION_ID_VAR IN SELECT id
                                       FROM integrations
                                       WHERE integrations.integration_type_id = (SELECT id
                                                                                 FROM integration_types
                                                                                 WHERE integration_types.name = 'JIRA')
            LOOP

                SELECT value
                FROM integration_settings
                WHERE integration_settings.integration_id = JIRA_INTEGRATION_ID_VAR
                  AND integration_param_id = (SELECT id FROM integration_params WHERE name = 'JIRA_URL')
                INTO JIRA_URL_VAR;

                CONTINUE WHEN JIRA_URL_VAR IS NULL;

                SELECT name
                FROM integrations
                WHERE id = JIRA_INTEGRATION_ID_VAR
                INTO JIRA_INTEGRATION_NAME_VAR;

                SELECT value
                FROM integration_settings
                WHERE integration_id = JIRA_INTEGRATION_ID_VAR
                  AND integration_param_id = (SELECT id FROM integration_params WHERE name = 'JIRA_USER')
                INTO JIRA_USER_VAR;

                SELECT value
                FROM integration_settings
                WHERE integration_id = JIRA_INTEGRATION_ID_VAR
                  AND integration_param_id =
                      (SELECT id FROM integration_params WHERE name = 'JIRA_TOKEN')
                INTO JIRA_PASSWORD_VAR;

                SELECT enabled
                FROM integrations
                WHERE id = JIRA_INTEGRATION_ID_VAR
                INTO JIRA_INTEGRATION_ENABLED_VAR;

                SELECT encrypted
                FROM integration_settings
                WHERE integration_id = JIRA_INTEGRATION_ID_VAR
                  AND integration_param_id =
                      (SELECT id FROM integration_params WHERE name = 'JIRA_TOKEN')
                INTO JIRA_PASSWORD_ENCRYPTED_VAR;

                JIRA_TYPE_VAR := (SELECT CASE
                                             WHEN lower(JIRA_URL_VAR) LIKE '%atlassian.net%' THEN 'CLOUD'
                                             ELSE 'SERVER_DC' END);

                INSERT INTO integration_jira_configs (url, username, token, encrypted, enabled, type)
                VALUES (JIRA_URL_VAR, JIRA_USER_VAR, JIRA_PASSWORD_VAR,
                        JIRA_PASSWORD_ENCRYPTED_VAR, JIRA_INTEGRATION_ENABLED_VAR,
                        JIRA_TYPE_VAR)
                ON CONFLICT DO NOTHING;
            END LOOP;
    END
$$;

-- JENKINS
DO
$$
    DECLARE JENKINS_INTEGRATION_TYPE_ID         integration_types.id%TYPE;
    DECLARE JENKINS_INTEGRATION_ID              integrations.id%TYPE;
    DECLARE JENKINS_INTEGRATION_NAME            integrations.name%TYPE;
    DECLARE JENKINS_URL                         integration_settings.value%TYPE;
    DECLARE JENKINS_USERNAME                    integration_settings.value%TYPE;
    DECLARE JENKINS_API_TOKEN_OR_PASSWORD       integration_settings.value%TYPE;
    DECLARE JENKINS_INTEGRATION_ENABLED         integrations.enabled%TYPE;
    DECLARE JENKINS_TOKEN_OR_PASSWORD_ENCRYPTED integration_settings.encrypted%TYPE;

    BEGIN

        SELECT id FROM integration_types
        WHERE integration_types.name = 'JENKINS'
        INTO JENKINS_INTEGRATION_TYPE_ID;

        FOR JENKINS_INTEGRATION_ID IN SELECT id FROM integrations WHERE integrations.integration_type_id = JENKINS_INTEGRATION_TYPE_ID
            LOOP

                SELECT value
                FROM integration_settings
                WHERE integration_settings.integration_id = JENKINS_INTEGRATION_ID
                  AND integration_param_id = (SELECT id FROM integration_params WHERE name = 'JENKINS_URL')
                INTO JENKINS_URL;

                CONTINUE WHEN JENKINS_URL IS NULL;

                SELECT name
                FROM integrations
                WHERE id = JENKINS_INTEGRATION_ID
                INTO JENKINS_INTEGRATION_NAME;

                SELECT value
                FROM integration_settings
                WHERE integration_id = JENKINS_INTEGRATION_ID
                  AND integration_param_id = (SELECT id FROM integration_params WHERE name = 'JENKINS_USER')
                INTO JENKINS_USERNAME;

                SELECT value
                FROM integration_settings
                WHERE integration_id = JENKINS_INTEGRATION_ID
                  AND integration_param_id = (SELECT id FROM integration_params WHERE name = 'JENKINS_API_TOKEN_OR_PASSWORD')
                INTO JENKINS_API_TOKEN_OR_PASSWORD;

                SELECT enabled
                FROM integrations
                WHERE id = JENKINS_INTEGRATION_ID
                INTO JENKINS_INTEGRATION_ENABLED;

                SELECT encrypted
                FROM integration_settings
                WHERE integration_id = JENKINS_INTEGRATION_ID
                  AND integration_param_id = (SELECT id FROM integration_params WHERE name = 'JENKINS_API_TOKEN_OR_PASSWORD')
                INTO JENKINS_TOKEN_OR_PASSWORD_ENCRYPTED;

                INSERT INTO integration_jenkins_configs (name, url, username, token, encrypted, enabled)
                VALUES (JENKINS_INTEGRATION_NAME, JENKINS_URL, JENKINS_USERNAME, JENKINS_API_TOKEN_OR_PASSWORD,
                        JENKINS_TOKEN_OR_PASSWORD_ENCRYPTED, JENKINS_INTEGRATION_ENABLED)
                ON CONFLICT DO NOTHING;
            END LOOP;
    END
$$;
