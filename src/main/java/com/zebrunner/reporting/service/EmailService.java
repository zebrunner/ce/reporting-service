package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import com.zebrunner.reporting.domain.push.events.Attachment;
import com.zebrunner.reporting.domain.push.events.MailDataMessage;
import com.zebrunner.reporting.messaging.config.Exchange;
import com.zebrunner.reporting.messaging.config.RoutingKey;
import com.zebrunner.reporting.service.util.EmailUtils;
import com.zebrunner.reporting.service.util.EventPushService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class EmailService implements TestRunResultSummarySender {

    private static final int RECIPIENTS_LIMIT_FOR_TR_SUMMARY = 20;

    private final EventPushService eventPushService;

    public void sendDashboardEmail(String subject, String body, List<Attachment> attachments, Set<String> toEmails) {
        if (!toEmails.isEmpty()) { // else - do nothing or throw exception?
            Map<String, Object> templateModel = new HashMap<>();
            templateModel.put("text", body);
            templateModel.put("subject", subject);
            templateModel.put("attachments", attachments);

            MailDataMessage mailDataMessage = MailDataMessage.builder()
                                                             .templateName("dashboard.ftl")
                                                             .subject(subject)
                                                             .toEmails(toEmails)
                                                             .templateModel(templateModel)
                                                             .attachments(attachments)
                                                             .build();

            eventPushService.send(Exchange.EMAIL, RoutingKey.SEND_EMAIL, mailDataMessage);
        }
    }

    public void sendTestRunResultsEmail(TestRun testRun, Map<String, Object> templateModel, Set<String> toEmails) {
        String status = buildStatusText(testRun);

        String build = testRun.getBuild() != null ? testRun.getBuild() : "";
        String displayName = testRun.getName() + (StringUtils.isEmpty(build) ? "" : " [" + build + "]");

        String subject = String.format("%s: %s", status, displayName);
        MailDataMessage mailDataMessage = MailDataMessage.builder()
                                                         .templateName("test_run_results.ftl")
                                                         .subject(subject)
                                                         .toEmails(toEmails)
                                                         .templateModel(templateModel)
                                                         .build();

        eventPushService.send(Exchange.EMAIL, RoutingKey.SEND_EMAIL, mailDataMessage);
    }

    public static String buildStatusText(TestRun testRun) {
        return Status.PASSED.equals(testRun.getStatus()) && testRun.isKnownIssue()
                ? "PASSED (known issues)" : testRun.getStatus().name();
    }

    @Override
    public NotificationTarget.Type getType() {
        return NotificationTarget.Type.EMAIL_RECIPIENTS;
    }

    @Override
    public void sendTestRunResultSummary(String target, TestRunResultSummary resultSummary) {
        Set<String> toEmails = EmailUtils.obtainValidOnly(target)
                                         .stream()
                                         .limit(RECIPIENTS_LIMIT_FOR_TR_SUMMARY)
                                         .collect(Collectors.toSet());
        if (!toEmails.isEmpty()) {
            String subject = String.format("%s: %s", resultSummary.getStatus(), resultSummary.getName());
            MailDataMessage message = MailDataMessage.builder()
                                                     .templateName("test_run_result_summary.ftl")
                                                     .subject(subject)
                                                     .toEmails(toEmails)
                                                     .templateModel(Map.of("summary", resultSummary))
                                                     .build();

            eventPushService.send(Exchange.EMAIL, RoutingKey.SEND_EMAIL, message);
        } else {
            log.debug("Test run (id '{}') summaries not sent: no valid recipient addresses", resultSummary.getTestRunId());
        }
    }

}
