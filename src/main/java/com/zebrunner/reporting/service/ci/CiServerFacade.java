package com.zebrunner.reporting.service.ci;

import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.BuildParameterType;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface CiServerFacade {

    @Deprecated
    List<BuildParameterType> retrieveLegacyBuildParameters(Long testRunId);

    @Deprecated
    void populateLegacyJobsAndBuildNumbers(Collection<TestRun> testRuns);

    /**
     * Triggers new build on ci based on ci context of test run with given id.
     *
     * @param baseTestRunId id of base test run.
     * @param parameters    parameters to be passed into build.
     * @param userId        user id of the build initiator.
     */
    void build(Long baseTestRunId, Map<String, String> parameters, Long userId);

    /**
     * Aborts queued or running ci build.
     *
     * @param testRunId id of test run to be aborted.
     */
    void abort(Long testRunId);

    /**
     * Triggers rerun of test run based on its ci context.
     * CI context can be null in case when test run was launched in unknown ci environment.
     *
     * @param testRunId    id of test run that should be rerun
     * @param onlyFailures specifies whether only failed or all tests should be reran
     * @param userId       user id of the rerun initiator.
     */
    void rerun(Long testRunId, boolean onlyFailures, Long userId);

    /**
     * Aborts executing right now ci build that was recently initiated by Zebrunner.
     * The ci build will be aborted only if information about the ci build contains reference to item in build queue.
     * <p>
     * This method was created for the scheduled task which aborts stacked ci builds for test runs in QUEUED status.
     *
     * @return true - if ci build was aborted.
     * false - if either test run was not aborted or such kind of scenario is not applicable to the ci server.
     */
    boolean tryAbortExecutingNowQueuedBuild(Long testRunId);

}
