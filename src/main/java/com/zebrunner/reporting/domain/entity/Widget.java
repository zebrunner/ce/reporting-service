package com.zebrunner.reporting.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "widgets")
@NamedEntityGraph(
        name = "widget.expanded",
        attributeNodes = @NamedAttributeNode("widgetTemplate")
)
public class Widget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private String paramsConfig;
    private String legendConfig;
    private String type;

    @ManyToOne
    @JoinColumn(name = "widget_template_id")
    private WidgetTemplate widgetTemplate;

    public Widget(Long id) {
        this.id = id;
    }

}
