ALTER TABLE tests
    RENAME TO test_executions;

ALTER TABLE tests_labels
    RENAME TO test_executions_labels;

ALTER TABLE tests_artifact_references
    RENAME TO test_executions_artifact_references;

ALTER TABLE test_configs
    RENAME TO test_execution_configs;

ALTER TABLE test_work_items
    RENAME TO test_execution_work_items;

ALTER TABLE tests_test_sessions
    RENAME TO test_executions_test_sessions;

-- TEST CASES RELATED

ALTER TABLE test_cases
    RENAME TO test_functions;

-- TEST RUNS RELATED
ALTER TABLE test_runs
    RENAME TO test_suite_executions;

ALTER TABLE test_run_ci_contexts
    RENAME TO test_suite_execution_ci_contexts;

ALTER TABLE test_run_filters
    RENAME TO test_suite_execution_filters;

ALTER TABLE test_run_statistics
    RENAME TO test_suite_execution_statistics;

ALTER TABLE test_runs_artifact_references
    RENAME TO test_suite_executions_artifact_references;

ALTER TABLE test_runs_labels
    RENAME TO test_suite_executions_labels;

ALTER TABLE test_runs_notification_targets
    RENAME TO test_suite_executions_notification_targets;

ALTER TABLE users_favorite_test_run_filters
    RENAME TO users_favorite_test_suite_execution_filters;

-- COLUMNS
ALTER TABLE test_executions_labels
    RENAME test_id TO test_execution_id;

ALTER TABLE test_executions_artifact_references
    RENAME test_id TO test_execution_id;

ALTER TABLE test_executions
    RENAME test_config_id TO test_execution_config_id;

ALTER TABLE test_execution_work_items
    RENAME test_id TO test_execution_id;

ALTER TABLE test_executions_test_sessions
    RENAME test_id TO test_execution_id;

ALTER TABLE test_executions
    RENAME test_case_id TO test_function_id;

ALTER TABLE issue_reference_assignment_events
    RENAME test_case_id TO test_function_id;

ALTER TABLE work_items
    RENAME test_case_id TO test_function_id;

ALTER TABLE test_suite_execution_ci_contexts
    RENAME test_run_id TO test_suite_execution_id;

ALTER TABLE test_suite_execution_statistics
    RENAME test_run_id TO test_suite_execution_id;

ALTER TABLE test_suite_executions_artifact_references
    RENAME test_run_id TO test_suite_execution_id;

ALTER TABLE test_suite_executions_labels
    RENAME test_run_id TO test_suite_execution_id;

ALTER TABLE test_suite_executions_notification_targets
    RENAME test_run_id TO test_suite_execution_id;

ALTER TABLE users_favorite_test_suite_execution_filters
    RENAME test_run_filter_id TO test_suite_execution_filter_id;

ALTER TABLE test_executions
    RENAME test_run_id TO test_suite_execution_id;

ALTER TABLE test_sessions
    RENAME test_run_id TO test_suite_execution_id;
