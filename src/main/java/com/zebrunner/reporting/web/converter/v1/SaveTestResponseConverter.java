package com.zebrunner.reporting.web.converter.v1;

import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestCase;
import com.zebrunner.reporting.web.response.v1.SaveTestResponse;
import com.zebrunner.reporting.web.response.v1.SaveTestResponse.ArtifactReference;
import com.zebrunner.reporting.web.response.v1.SaveTestResponse.Label;
import com.zebrunner.reporting.web.response.v1.SaveTestResponse.SaveTestResponseBuilder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SaveTestResponseConverter {

    public SaveTestResponse toSaveResponse(Test test, boolean includeLabels) {
        TestCase testCase = test.getTestCase();
        SaveTestResponseBuilder response = SaveTestResponse.builder()
                                                           .id(test.getId())
                                                           .name(test.getName())
                                                           .correlationData(test.getCorrelationData())
                                                           .className(testCase.getTestClass())
                                                           .methodName(testCase.getTestMethod())
                                                           .testCase(testCase.getInfo())
                                                           .maintainer(test.getMaintainer().getUsername())
                                                           .startedAt(test.getStartedAt())
                                                           .endedAt(test.getEndedAt())
                                                           .result(test.getStatus())
                                                           .reason(test.getReason())
                                                           .artifactReferences(convertArtifactReferences(test));
        if (includeLabels) {
            response.labels(convertLabels(test));
        }

        return response.build();
    }

    private List<Label> convertLabels(Test test) {
        return test.getLabels()
                   .stream()
                   .map(label -> new Label(label.getKey(), label.getValue()))
                   .collect(Collectors.toList());
    }

    private List<ArtifactReference> convertArtifactReferences(Test test) {
        return test.getArtifactReferences()
                   .stream()
                   .map(reference -> new ArtifactReference(reference.getName(), reference.getValue()))
                   .collect(Collectors.toList());
    }

}
