package com.zebrunner.reporting.service.task;

import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestCase;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.persistence.repository.IssueReferenceRepository;
import com.zebrunner.reporting.service.config.RecalculateFailureReasonHashTaskProperties;
import com.zebrunner.reporting.service.hash.FailureReasonHashFunction;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.integration.jira.JiraService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(
        prefix = "service.task",
        name = {"enabled", "recalculate-failure-reason-hash.enabled"},
        havingValue = "true"
)
public class RecalculateFailureReasonHashTask implements CommandLineRunner {

    private Statistics statistics;

    private final JiraService jiraService;
    private final JdbcTemplate jdbcTemplate;
    private final RedissonClient redissonClient;
    private final JiraConfigService jiraConfigService;
    private final IssueReferenceRepository issueReferenceRepository;
    private final FailureReasonHashFunction failureReasonHashFunction;
    private final RecalculateFailureReasonHashTaskProperties properties;

    /**
     * Try to capture Redis lock by key ({@link RecalculateFailureReasonHashTaskProperties#redisLockKey}) and if success do recalculating hashCodes task.
     * <p>
     * {@link RecalculateFailureReasonHashTaskProperties#reindexingDelay} - task delay after application start.
     * </p>
     * <p>
     * {@link RecalculateFailureReasonHashTaskProperties#redisLockRequestTimeout} - task delay after application start.
     * </p>
     * Recalculating hashCodes includes two steps:
     * <ol>
     *     <li> {@link #recalculateDownstreamHashes(Long)} </li>
     *     <li> {@link #recalculateUpstreamHashes(Long)} </li>
     * </ol>
     */
    @Override
    public void run(String... args) {
        try {
            boolean lockAcquired = tryAcquireLock();
            if (lockAcquired) {
                log.info("Lock was successfully acquired.");

                long taskDelay = properties.getReindexingDelay().toMillis();
                Long maxTestId = findMaxTestId();
                Long maxTestRunId = findMaxTestRunId();

                Thread.sleep(taskDelay);
                recalculateHashes(maxTestId);
                updateTestRunStatusPassed(maxTestRunId);
            } else {
                log.info("Could not acquire redis lock. Skipping task execution.");
            }
        } catch (Exception e) {
            log.error("Task failed.", e);
        }
    }

    private boolean tryAcquireLock() throws InterruptedException {
        log.info("Trying to acquire redis lock by key '{}'...", properties.getRedisLockKey());
        RLock lock = redissonClient.getLock(properties.getRedisLockKey());

        long lockRequestTimeoutSeconds = properties.getRedisLockRequestTimeout().getSeconds();
        long lockTtlSeconds = properties.getRedisLockTtl().getSeconds();
        return lock.tryLock(lockRequestTimeoutSeconds, lockTtlSeconds, TimeUnit.SECONDS);
    }

    private void recalculateHashes(@Nullable Long fromId) {
        if (fromId != null) {
            statistics = new Statistics();
            statistics.setStartId(fromId);

            log.info("Start hashes recalculation ");
            try {
                recalculateDownstreamHashes(fromId);
                recalculateUpstreamHashes(fromId);
            } catch (Exception e) {
                log.warn("Hashes recalculation was interrupted:", e);
            }
            log.info("End hashes recalculation ");

            log.info("Statistics: {}", statistics);
        }
    }

    private void updateTestRunStatusPassed(Long fromId) {
        String query = "WITH test_suite_executions_not_to_update " +
                       " AS (SELECT DISTINCT(test_suite_execution_id) FROM test_executions WHERE (status = 'FAILED' OR status = 'SKIPPED') AND known_issue = FALSE)" +
                       " UPDATE test_suite_executions" +
                       " SET status = 'PASSED'" +
                       " WHERE (status = 'FAILED' OR status = 'SKIPPED')" +
                       " AND id > ?" +
                       " AND id NOT IN (SELECT * FROM test_suite_executions_not_to_update)";
        try {
            jdbcTemplate.update(query, fromId);
        } catch (Exception e) {
            log.warn("Unable to update testRuns status: ", e);
        }
    }

    /**
     * Recalculate hashCodes in descending order for all existed at the moment of application start tests and linked to that tests workItems.
     *
     * @param fromId start point of recalculating. All next ids will be less than fromId.
     *               In current task fromId - max id of test that was at the application start.
     */
    private void recalculateDownstreamHashes(Long fromId) {
        log.info("Downstream hashes recalculation started.");
        long queryLimit = properties.getDbSelectQueryLimit();

        List<Test> tests;
        while (!(tests = findTestsByIdLessThanAndInclusive(fromId, queryLimit)).isEmpty()) {
            tests.forEach(this::updateHashesIfRecalculated);
            fromId = Collections.min(tests, Comparator.comparingLong(Test::getId)).getId() - 1;
        }
        log.info("Downstream hashes recalculation finished.");
    }

    /**
     * Recalculate hashCodes for all tests in ascending order and link workItems with same hashCodes and testCaseId.
     *
     * @param fromId start point of recalculating. All next ids will be more than fromId.
     *               In current task fromId - max id of test that was at the application start.
     */
    private void recalculateUpstreamHashes(Long fromId) {
        log.info("Upstream hashes recalculation started.");
        long queryLimit = properties.getDbSelectQueryLimit();

        List<Test> tests;
        while (!(tests = findTestsByIdGreaterThan(fromId, queryLimit)).isEmpty()) {
            tests.stream()
                 .peek(this::updateHashesIfRecalculated)
                 .filter(test -> test.isLogicallyFailed()
                                 && !test.isKnownIssue()
                                 && jiraConfigService.isEnabled())
                 .forEach(this::tryLinkIssueToTest);

            fromId = Collections.max(tests, Comparator.comparingLong(Test::getId)).getId();
        }

        log.info("Upstream hashes recalculation finished.");
    }

    private Long findMaxTestRunId() {
        String query = "SELECT MAX(id) FROM test_suite_executions";
        return Optional.ofNullable(jdbcTemplate.queryForObject(query, Long.class))
                       .orElse(0L);
    }

    private Long findMaxTestId() {
        String query = "SELECT MAX(id) FROM test_executions";
        return Optional.ofNullable(jdbcTemplate.queryForObject(query, Long.class))
                       .orElse(0L);
    }

    private List<Test> findTestsByIdLessThanAndInclusive(long fromId, long limit) {
        String query = "SELECT * FROM test_executions" +
                       " WHERE id <= ? AND (status = 'FAILED' OR status = 'SKIPPED') AND message IS NOT NULL" +
                       " ORDER BY id DESC LIMIT ?";
        return jdbcTemplate.query(query, this::mapToTest, fromId, limit);
    }

    private List<Test> findTestsByIdGreaterThan(long fromId, long limit) {
        String query = "SELECT * FROM test_executions" +
                       " WHERE id > ? AND (status = 'FAILED' OR status = 'SKIPPED') AND message IS NOT NULL" +
                       " LIMIT ?";
        return jdbcTemplate.query(query, this::mapToTest, fromId, limit);
    }

    private void updateHashesIfRecalculated(Test test) {
        Integer legacyHash = test.getReasonHashCode();
        boolean hasNewHash = recalculateHash(test);

        if (hasNewHash) {
            updateHashOfTest(test);
            updateHashOfTestIssueEvent(test, legacyHash);
        }
    }

    /**
     * Apply hashCode function for {@link Test#reason}.
     * Set new hashCode for test if old and new hashCodes differ.
     * <p>
     * NOTE: this method is usually used in case of filtering tests with same hashCodes.
     * It prevents additional DataBase update calls.
     *
     * @param test test, which hashCode is candidate for updating
     * @return true if test's hashCode was updated, false otherwise
     */
    private boolean recalculateHash(Test test) {
        Integer newHashCode = failureReasonHashFunction.calculateHash(test.getReason());
        if (newHashCode != null && !newHashCode.equals(test.getReasonHashCode())) {
            statistics.increaseRecalculatedHashesCount();
            test.setReasonHashCode(newHashCode);
            return true;
        }
        return false;
    }

    private void updateHashOfTest(Test test) {
        String updateTestHashQuery = "UPDATE test_executions SET message_hash_code = ? WHERE id = ?";
        jdbcTemplate.update(updateTestHashQuery, test.getReasonHashCode(), test.getId());
    }

    private void updateHashOfTestIssueEvent(Test test, Integer legacyHash) {
        String updateIssueHashQuery = "UPDATE issue_reference_assignment_events " +
                                      "SET failure_reason_hash = ? " +
                                      "WHERE test_function_id = ? " +
                                      "AND failure_reason_hash = ?";
        jdbcTemplate.update(updateIssueHashQuery, test.getReasonHashCode(), test.getTestCase().getId(), legacyHash);
    }

    private void tryLinkIssueToTest(Test test) {
        Long testCaseId = test.getTestCase().getId();
        Integer reasonHash = test.getReasonHashCode();

        issueReferenceRepository.findLastLinkedByTestCaseIdAndFailureReasonHash(testCaseId, reasonHash)
                                .filter(issue -> !isCompletedByType(issue))
                                .ifPresent(knownIssue -> {
                                    statistics.increaseLinkedWorkItemsCount();
                                    test.setIssueReference(knownIssue);
                                    updateTestWithKnownIssue(test);
                                    updateTestRunWithKnownIssue(test.getTestRun().getId());
                                });
    }

    private boolean isCompletedByType(IssueReference issueReference) {
        String issueId = issueReference.getValue();
        if (IssueReference.Type.JIRA == issueReference.getType()) {
            try {
                return jiraService.isIssueCompleted(issueId);
            } catch (RuntimeException e) {
                log.warn("Failed to check if issue '{}' of type JIRA - assuming it is not", issueId, e);
                return false;
            }
        } else {
            log.warn("Failed to check if issue '{}' of type {} - currently unsupported", issueId, issueReference.getType());
            return false;
        }
    }

    private void updateTestWithKnownIssue(Test test) {
        String query = "UPDATE test_executions " +
                       "SET known_issue = TRUE, " +
                       "    issue_reference_id = ? " +
                       "WHERE id = ?";
        jdbcTemplate.update(query, test.getIssueReference().getId(), test.getId());
    }

    private void updateTestRunWithKnownIssue(Long testRunId) {
        String setTestRunKnownIssueTrueQuery = "UPDATE test_suite_executions SET known_issue = TRUE WHERE id = ?";
        jdbcTemplate.update(setTestRunKnownIssueTrueQuery, testRunId);

        String trackTestRunKnownIssue = "UPDATE test_suite_execution_statistics" +
                                        " SET known_issues_amount = known_issues_amount + 1," +
                                        "     failed_amount = failed_amount - 1" +
                                        " WHERE test_suite_execution_id = ?";
        jdbcTemplate.update(trackTestRunKnownIssue, testRunId);
    }

    private Test mapToTest(ResultSet rs, int rowNum) throws SQLException {
        return Test.builder()
                   .id(rs.getLong("id"))
                   .testRun(TestRun.builder().id(rs.getLong("test_suite_execution_id")).build())
                   .status(Test.Status.valueOf(rs.getString("status")))
                   .testCase(TestCase.builder().id(rs.getLong("test_function_id")).build())
                   .reason(rs.getString("message"))
                   .reasonHashCode(rs.getInt("message_hash_code"))
                   .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Statistics {

        private long startId;
        private long reassignedHashesTestsCount;
        private long newLinkedWorkItemsCount;

        public void increaseRecalculatedHashesCount() {
            reassignedHashesTestsCount++;
        }

        public void increaseLinkedWorkItemsCount() {
            newLinkedWorkItemsCount++;
        }

    }

}
