package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.reporting.service.DashboardSwitchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/dashboard-switches", produces = MediaType.APPLICATION_JSON_VALUE)
public class DashboardSwitchController {

    private final DashboardSwitchService dashboardSwitchService;

    @PutMapping
    @PreAuthorize("hasPermission('reporting:dashboards:access-private')")
    public void save(@RequestBody @Valid UpdateDashboardSwitchRequest updateDashboardSwitchRequest,
                     @PrincipalId Integer userId) {
        Long dashboardId = updateDashboardSwitchRequest.getDashboardId();
        dashboardSwitchService.save(dashboardId, userId.longValue());
    }

}
