package com.zebrunner.reporting.domain.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@EqualsAndHashCode(exclude = "id", callSuper = false)
public class LabelDTO extends AbstractType {

    @NotEmpty(message = "Key required")
    @Size(max = 50)
    private String key;

    @NotEmpty(message = "Value required")
    @Size(max = 255)
    private String value;

}
