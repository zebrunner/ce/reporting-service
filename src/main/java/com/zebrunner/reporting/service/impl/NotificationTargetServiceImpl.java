package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import com.zebrunner.reporting.persistence.repository.NotificationTargetRepository;
import com.zebrunner.reporting.service.NotificationTargetService;
import com.zebrunner.reporting.service.TestRunResultSummarySender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class NotificationTargetServiceImpl implements NotificationTargetService {

    private static final String ERR_MSG_SUMMARY_NOT_SENT_FORMAT = "Test run (id '%d') summary for target '%s' not sent completely or partially due to a sender error";

    private static final int MAX_VALUE_LENGTH = 4000;

    public NotificationTargetServiceImpl(NotificationTargetRepository notificationTargetRepository,
                                         List<TestRunResultSummarySender> senders) {
        this.notificationTargetRepository = notificationTargetRepository;
        this.typeToSender = senders.stream()
                                   .collect(Collectors.toMap(TestRunResultSummarySender::getType, Function.identity()));

    }

    private final NotificationTargetRepository notificationTargetRepository;
    private final Map<NotificationTarget.Type, TestRunResultSummarySender> typeToSender;

    @Override
    @Transactional
    public Set<NotificationTarget> save(Set<NotificationTarget> targets) {
        return targets.stream()
                      .map(this::readOrCreate)
                      .collect(Collectors.toSet());
    }

    @Override
    public void execute(Set<NotificationTarget> targets, TestRunResultSummary resultSummary) {
        targets.forEach(target -> {
            try {
                TestRunResultSummarySender sender = typeToSender.get(target.getType());
                sender.sendTestRunResultSummary(target.getValue(), resultSummary);
            } catch (Exception e) {
                String message = String.format(ERR_MSG_SUMMARY_NOT_SENT_FORMAT, resultSummary.getTestRunId(), target);
                log.warn(message, e);
            }
        });
    }

    private NotificationTarget readOrCreate(NotificationTarget target) {
        // only save allowed number of symbols without raising an error
        String value = target.getValue();
        value = value.length() > MAX_VALUE_LENGTH ? value.substring(0, MAX_VALUE_LENGTH - 1) : value;
        target.setValue(value);
        return notificationTargetRepository.findByTypeAndValue(target.getType(), value)
                                           .orElseGet(() -> notificationTargetRepository.save(target));
    }

}
