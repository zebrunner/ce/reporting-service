package com.zebrunner.reporting.web.request.v1.dashboard;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@JGlobalMap
@NoArgsConstructor
public class CreateDashboardRequest {

    @NotBlank
    private String title;

}
