package com.zebrunner.reporting.persistence.dao.mysql.application.search;

import com.zebrunner.reporting.domain.db.Status;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TestRunSearchCriteria extends SearchCriteria implements DateSearchCriteria {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date date;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date fromDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date toDate;

    private Long id;
    private Long testSuiteId;
    private List<String> environment;
    private List<String> platform;
    private List<String> browser;
    private List<Status> status;
    private Boolean reviewed;
    private List<String> locale;

    public TestRunSearchCriteria() {
        super.setSortOrder(SortOrder.DESC);
    }

}