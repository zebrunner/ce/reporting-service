package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.reporting.domain.dto.reporting.TestRunStart;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
@JGlobalMap(excluded = {"config", "SUPPORTED_NOTIFICATION_TARGETS", "SUPPORTED_CI_TYPES"})
public class StartTestRunRequest {

    private static final Set<String> SUPPORTED_NOTIFICATION_TARGETS =
            Stream.of(com.zebrunner.reporting.domain.entity.NotificationTarget.Type.values())
                  .map(Enum::name)
                  .collect(Collectors.toSet());
    private static final Set<String> SUPPORTED_CI_TYPES = Stream.of(TestRunCiContext.Type.values())
                                                                .map(Enum::name)
                                                                .collect(Collectors.toSet());

    private String uuid;
    @NotBlank
    private String name;
    @NotNull
    @PastOrPresent
    private OffsetDateTime startedAt;
    @NotNull
    private StartStatus status = StartStatus.IN_PROGRESS;
    @NotBlank
    private String framework;

    private JenkinsContext jenkinsContext;
    private Config config;
    @Valid
    private CiContext ciContext;

    private Set<@Valid NotificationTarget> notificationTargets;

    @Getter
    @AllArgsConstructor
    public enum StartStatus {

        QUEUED(TestRun.Status.QUEUED),
        IN_PROGRESS(TestRun.Status.IN_PROGRESS);

        private final TestRun.Status status;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JenkinsContext {

        private String jobUrl;
        private Integer jobNumber;
        private String parentJobUrl;
        private Integer parentJobNumber;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Config {

        private String environment;
        private String build;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CiContext {

        @NotBlank
        private String ciType;
        @NotEmpty
        private Map<String, String> envVariables = Map.of();

    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class NotificationTarget {

        @NotBlank
        private String type;
        @NotBlank
        private String value;

    }

    @JMapConversion(from = "status", to = "status")
    public TestRun.Status convertStatus(StartStatus startStatus) {
        return startStatus.getStatus();
    }

    @JMapConversion(from = "jenkinsContext", to = "jenkinsContext")
    public TestRunStart.JenkinsContext convertJenkinsContext(JenkinsContext jenkinsContext) {
        return new TestRunStart.JenkinsContext(
                jenkinsContext.getJobUrl(),
                jenkinsContext.getJobNumber(),
                jenkinsContext.getParentJobUrl(),
                jenkinsContext.getParentJobNumber()
        );
    }

    @JMapConversion(from = "ciContext", to = "ciContext")
    public TestRunStart.CiContext convertJenkinsContext(CiContext ciContext) {
        if (SUPPORTED_CI_TYPES.contains(ciContext.getCiType())) {
            return new TestRunStart.CiContext(
                    TestRunCiContext.Type.valueOf(ciContext.getCiType()),
                    ciContext.getEnvVariables()
            );
        }
        return null;
    }

    @JMapConversion(from = "notificationTargets", to = "notificationTargets")
    public Set<com.zebrunner.reporting.domain.entity.NotificationTarget> convertNotificationTargets(Set<NotificationTarget> notificationTargets) {
        return notificationTargets.stream()
                                  // simply ignore unknown/unsupported
                                  .filter(target -> SUPPORTED_NOTIFICATION_TARGETS.contains(target.type))
                                  .map(target -> com.zebrunner.reporting.domain.entity.NotificationTarget.builder()
                                                                                                         .type(com.zebrunner.reporting.domain.entity.NotificationTarget.Type.valueOf(target.type))
                                                                                                         .value(target.value)
                                                                                                         .build())
                                  .collect(Collectors.toSet());
    }

}
