UPDATE labels
SET key = 'com.zebrunner.app/tcm.testrail.testcase-id'
WHERE key = 'TESTRAIL_TESTCASE_UUID';

UPDATE labels
SET key = 'com.zebrunner.app/tcm.qtest.testcase-id'
WHERE key = 'QTEST_TESTCASE_UUID';
