package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IssueReferenceRepository extends JpaRepository<IssueReference, Long> {

    Optional<IssueReference> findByTypeAndValue(IssueReference.Type type, String value);

    @Query("SELECT ir " +
            "FROM IssueReferenceAssignmentEvent irae " +
            "JOIN irae.issueReference ir " +
            "WHERE irae.id = (" +
            "       SELECT MAX(irae1.id) FROM IssueReferenceAssignmentEvent irae1 " +
            "       WHERE irae1.testCaseId = :testCaseId AND irae1.failureReasonHash = :failureReasonHash" +
            ") AND irae.action = 'LINK'")
    Optional<IssueReference> findLastLinkedByTestCaseIdAndFailureReasonHash(Long testCaseId, Integer failureReasonHash);

}
