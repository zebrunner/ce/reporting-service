ALTER TABLE tests DROP COLUMN retry;
ALTER TABLE tests DROP COLUMN uuid;

ALTER TABLE test_runs DROP COLUMN work_item_id;
ALTER TABLE test_runs DROP COLUMN env;
ALTER TABLE test_runs DROP COLUMN app_version;
