package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.Test;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Data
@JGlobalMap
@NoArgsConstructor
public class UpdateTestRequest {

    private String name;
    private String correlationData;
    private String className;
    private String methodName;
    private String maintainer;
    private String testCase;

    private OffsetDateTime endedAt;
    private Test.Status result;
    private String reason;

    private Set<Label> labels = Set.of();

    // todo: for backward compatibility
    public void setLabels(Object object) {
        if (object instanceof Map) {
            deserializeFromMap((Map<?, ?>) object);
        } else if (object instanceof Collection) {
            deserializeFromCollection(((Collection<?>) object));
        }
    }

    private void deserializeFromMap(Map<?, ?> map) {
        this.labels = new HashSet<>();
        for (Object mapKey : map.keySet()) {
            if (mapKey != null) {
                Object mapValue = map.get(mapKey);
                if (mapValue instanceof Collection) {
                    for (Object mapValueItem : ((Collection<?>) mapValue)) {
                        if (mapValueItem != null) {
                            labels.add(new Label(mapKey.toString(), mapValueItem.toString()));
                        }
                    }
                }
            }
        }
    }

    private void deserializeFromCollection(Collection<?> collection) {
        this.labels = new HashSet<>();
        for (Object collectionItem : collection) {
            if (collectionItem instanceof Map) {
                Map<?, ?> collectionItemMap = (Map<?, ?>) collectionItem;

                Object key = collectionItemMap.get("key");
                Object value = collectionItemMap.get("value");

                if (key != null && value != null) {
                    labels.add(new Label(key.toString(), value.toString()));
                }
            }
        }
    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Label {

        private String key;
        private String value;

    }

}
