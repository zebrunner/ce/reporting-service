ALTER TABLE dashboards ADD COLUMN is_default BOOLEAN DEFAULT FALSE;
UPDATE dashboards
SET is_default = TRUE,
    editable = FALSE
WHERE title IN ('General', 'Personal', 'Failures analysis', 'User Performance', 'Stability');
