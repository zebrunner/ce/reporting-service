package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.entity.integration.SlackConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.service.integration.slack.SlackConfigService;
import com.zebrunner.reporting.web.patch.PatchAction;
import com.zebrunner.reporting.web.patch.PatchOperation;
import com.zebrunner.reporting.web.patch.PatchRequest;
import com.zebrunner.reporting.web.request.v1.slack.CheckSlackConnectionRequest;
import com.zebrunner.reporting.web.request.v1.slack.UpdateSlackConfigRequest;
import com.zebrunner.reporting.web.response.v1.CheckConnectionResponse;
import com.zebrunner.reporting.web.response.v1.slack.GetSlackConfigResponse;
import com.zebrunner.reporting.web.response.v1.slack.UpdateSlackConfigResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/integrations/slack", produces = MediaType.APPLICATION_JSON_VALUE)
public class SlackController {

    private final JMapper mapper;
    private final SlackConfigService configService;

    @PutMapping
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public UpdateSlackConfigResponse update(@Valid @RequestBody UpdateSlackConfigRequest request) {
        SlackConfig newConfig = mapper.map(request, SlackConfig.class);
        SlackConfig config = configService.update(newConfig);
        return mapper.map(config, UpdateSlackConfigResponse.class);
    }

    @GetMapping
    @PreAuthorize("hasPermission('reporting:integrations:read')")
    public GetSlackConfigResponse get() {
        SlackConfig existingSettings = configService.retrieve();
        return mapper.map(existingSettings, GetSlackConfigResponse.class);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public void patch(@PatchAction(op = PatchOperation.REPLACE, path = "/enabled")
                      @RequestBody PatchRequest request) {
        Boolean enabled = request.getReplaceValue("/enabled", Boolean.class);
        if (enabled != null) {
            configService.patchEnabledProperty(enabled);
        }
    }

    @PostMapping("/connectivity-checks")
    public CheckConnectionResponse checkConnection(@Valid @RequestBody CheckSlackConnectionRequest request) {
        SlackConfig config = mapper.map(request, SlackConfig.class);
        ReachabilityCheckResult result = configService.isReachable(config);
        return mapper.map(result, CheckConnectionResponse.class);
    }

}
