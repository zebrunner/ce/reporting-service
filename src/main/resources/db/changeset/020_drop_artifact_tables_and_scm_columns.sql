DROP TABLE IF EXISTS test_run_artifacts CASCADE;
DROP TABLE IF EXISTS test_artifacts CASCADE;

ALTER TABLE test_runs DROP COLUMN scm_branch;
ALTER TABLE test_runs DROP COLUMN scm_url;
ALTER TABLE test_runs DROP COLUMN scm_commit;
ALTER TABLE test_runs DROP COLUMN config_xml;