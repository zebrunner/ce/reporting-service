package com.zebrunner.reporting.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RerunMetadata {

    private Long userId;
    private Instant startedAt;
    private String serializedRunContext;

}
