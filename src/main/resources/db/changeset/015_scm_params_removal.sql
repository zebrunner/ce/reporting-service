INSERT INTO labels (key, value)
SELECT DISTINCT 'com.zebrunner.app/scm.branch', scm_branch
FROM test_runs
WHERE scm_branch IS NOT NULL
ON CONFLICT DO NOTHING;

INSERT INTO labels (key, value)
SELECT DISTINCT 'com.zebrunner.app/scm.git.repo-url', scm_url
FROM test_runs
WHERE scm_url IS NOT NULL
ON CONFLICT DO NOTHING;

INSERT INTO test_runs_labels (test_run_id, label_id, modified_at)
SELECT tr.id, l.id, now()
FROM test_runs tr
         INNER JOIN labels l ON tr.scm_branch = l.value
WHERE l.key = 'com.zebrunner.app/scm.branch'
ON CONFLICT DO NOTHING;

INSERT INTO test_runs_labels (test_run_id, label_id, modified_at)
SELECT tr.id, l.id, now()
FROM test_runs tr
         INNER JOIN labels l ON tr.scm_url = l.value
WHERE l.key = 'com.zebrunner.app/scm.git.repo-url'
ON CONFLICT DO NOTHING;