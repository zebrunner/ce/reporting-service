package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.notification.TestRunResultSummary;

public interface TestRunResultSummaryService {

    TestRunResultSummary retrieveByTestRunId(Long testRunId);

}
