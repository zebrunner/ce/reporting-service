CREATE TABLE test_run_statistics (
  test_run_id INT NOT NULL,
  passed_amount INT,
  failed_amount INT,
  known_issues_amount INT,
  blockers_amount INT,
  skipped_amount INT,
  in_progress_amount INT,
  aborted_amount INT,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (test_run_id),
  FOREIGN KEY (test_run_id) REFERENCES test_runs (id) ON DELETE CASCADE
);
CREATE TRIGGER update_timestamp_test_run_statistics
  BEFORE INSERT OR UPDATE ON test_run_statistics
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();
