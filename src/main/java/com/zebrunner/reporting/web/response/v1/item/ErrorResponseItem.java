package com.zebrunner.reporting.web.response.v1.item;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.web.response.v1.common.ErrorReason;
import com.zebrunner.reporting.web.response.v1.common.ErrorResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponseItem extends ResponseItem {

    private ErrorResponse error;

    private ErrorResponseItem(String key, boolean succeeded, ErrorReason reason, String message) {
        super(key, succeeded);
        this.error = new ErrorResponse(reason, message);
    }

    public static ErrorResponseItem failed(String key, ErrorReason reason) {
        return new ErrorResponseItem(key, false, reason, null);
    }

    public static ErrorResponseItem failed(String key, ErrorReason reason, String message) {
        return new ErrorResponseItem(key, false, reason, message);
    }

}
