package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JiraConfigRepository extends JpaRepository<JiraConfig, Long> {

    Optional<JiraConfig> findFirstByOrderById();

    default Optional<JiraConfig> findEnabled() {
        return findByEnabled(true);
    }

    Optional<JiraConfig> findByEnabled(boolean enabled);

}
