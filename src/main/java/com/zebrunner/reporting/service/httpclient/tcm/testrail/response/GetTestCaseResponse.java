package com.zebrunner.reporting.service.httpclient.tcm.testrail.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetTestCaseResponse {

    private Long id;
    private Long priorityId;
    private String title;
    private Instant createdOn;
    private Instant updatedOn;
    private String estimate;
    private String estimateForecast;
    private String refs;
    @JsonAlias("custom_preconds")
    private String preconditions;
    @JsonAlias("custom_steps")
    private String steps;
    @JsonAlias("custom_expected")
    private String expected;
    @JsonAlias("custom_steps_separated")
    private List<SeparatedStep> separatedSteps;
    @JsonAlias("custom_mission")
    private String mission;
    @JsonAlias("custom_goals")
    private String goals;

    @Data
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class SeparatedStep {

        private String content;
        private String expected;

    }

}
