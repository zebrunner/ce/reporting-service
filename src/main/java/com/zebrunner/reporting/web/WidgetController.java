package com.zebrunner.reporting.web;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.reporting.domain.dto.QueryParametersDTO;
import com.zebrunner.reporting.domain.entity.Widget;
import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import com.zebrunner.reporting.service.widget.WidgetService;
import com.zebrunner.reporting.service.widgettemplate.WidgetTemplateService;
import com.zebrunner.reporting.web.request.v1.widget.CreateWidgetRequest;
import com.zebrunner.reporting.web.request.v1.widget.UpdateWidgetRequest;
import com.zebrunner.reporting.web.response.v1.dashboard.WidgetTemplateResponseItem;
import com.zebrunner.reporting.web.response.v1.widget.CreateWidgetResponse;
import com.zebrunner.reporting.web.response.v1.widget.GetWidgetResponse;
import com.zebrunner.reporting.web.response.v1.widget.UpdateWidgetResponse;
import com.zebrunner.reporting.web.response.v1.widget.WidgetResponseItem;
import com.zebrunner.reporting.web.util.JMapper;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api("Widgets API")
@RequestMapping(path = "api/widgets", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class WidgetController {

    private final WidgetService widgetService;
    private final WidgetTemplateService widgetTemplateService;
    private final JMapper jMapper;

    @PostMapping
    @PreAuthorize("hasPermission('reporting:widgets:update')")
    public CreateWidgetResponse createWidget(@Valid @RequestBody CreateWidgetRequest request) {
        Widget newWidget = jMapper.map(request, Widget.class);
        Widget widget = widgetService.create(request.getDashboardId(), newWidget, request.getTemplateId());
        return jMapper.map(widget, CreateWidgetResponse.class);
    }

    @GetMapping("/{id}")
    public GetWidgetResponse getById(@PathVariable("id") Long id) {
        Widget widget = widgetService.retrieveById(id);
        return jMapper.map(widget, GetWidgetResponse.class);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasPermission('reporting:widgets:delete')")
    public void deleteWidget(@PathVariable("id") Long id) {
        widgetService.deleteById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasPermission('reporting:widgets:update')")
    public UpdateWidgetResponse updateWidget(@PathVariable("id") Long id,
                                             @Valid @RequestBody UpdateWidgetRequest request) {
        Widget newWidget = jMapper.map(request, Widget.class);
        Widget widget = widgetService.update(id, newWidget);
        return jMapper.map(widget, UpdateWidgetResponse.class);
    }

    @GetMapping
    public List<WidgetResponseItem> getAllWidgets() {
        return widgetService.retrieveAll()
                            .stream()
                            .peek(widget -> widgetTemplateService.setNotNullParamsValues(widget.getWidgetTemplate()))
                            .map(widget -> jMapper.map(widget, WidgetResponseItem.class))
                            .collect(Collectors.toList());
    }

    @GetMapping("/templates")
    public List<WidgetTemplateResponseItem> getAllWidgetTemplates() {
        List<WidgetTemplate> templates = widgetTemplateService.retrieveAllVisible();
        return templates.stream()
                        .map(widgetTemplate -> jMapper.map(widgetTemplate, WidgetTemplateResponseItem.class))
                        .collect(Collectors.toList());
    }

    @GetMapping("/templates/{templateId}/prepare")
    public WidgetTemplateResponseItem prepareWidgetTemplate(@PathVariable("templateId") Long templateId) {
        WidgetTemplate widgetTemplate = widgetTemplateService.retrieveByIdWithParamsValues(templateId, "Last 24 Hours");
        return jMapper.map(widgetTemplate, WidgetTemplateResponseItem.class);
    }

    @GetMapping("/templates/{templateId}/parameters")
    public Map<String, WidgetTemplateParameter> getWidgetTemplateParameters(@PathVariable("templateId") Long templateId,
                                                                            @RequestParam("period") String period) {
        WidgetTemplate template = widgetTemplateService.retrieveByIdWithParamsValues(templateId, period);
        return template.getParamsConfig();
    }

    @PostMapping("/templates/sql")
    public List<Map<String, Object>> executeSQL(@PrincipalId Integer userId,
                                                @Valid @RequestBody QueryParametersDTO queryParametersDTO) {
        Long templateId = queryParametersDTO.getTemplateId();
        Map<String, Object> queryParameters = queryParametersDTO.getParamsConfig();
        return widgetTemplateService.getQueryResults(templateId, queryParameters, userId);
    }

}
