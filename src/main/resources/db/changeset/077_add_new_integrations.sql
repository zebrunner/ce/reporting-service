CREATE TABLE integration_slack_configs
(
    id              BIGSERIAL       NOT NULL,
    token           TEXT            NOT NULL,
    bot_name        VARCHAR(50)     NOT NULL DEFAULT 'Zebrunner notification bot',
    bot_icon_url    VARCHAR(200),
    encrypted       BOOLEAN         NOT NULL DEFAULT FALSE,
    enabled         BOOLEAN         NOT NULL DEFAULT FALSE,
    created_at      TIMESTAMP       NOT NULL DEFAULT now(),
    modified_at     TIMESTAMP       NOT NULL,
    PRIMARY KEY (id)
);
CREATE TRIGGER update_timestamp_integration_slack_configs
    BEFORE INSERT OR UPDATE
    ON integration_slack_configs
    FOR EACH ROW
    EXECUTE PROCEDURE UPDATE_MODIFIED_AT();

CREATE TABLE IF NOT EXISTS integration_jira_configs
(
    id                BIGSERIAL     NOT NULL,
    url               VARCHAR(511)  NOT NULL,
    username          VARCHAR(255)  NOT NULL,
    token             VARCHAR(1023) NOT NULL,
    type              VARCHAR(63)   NOT NULL,
    encrypted         BOOLEAN       NOT NULL DEFAULT FALSE,
    enabled           BOOLEAN       NOT NULL DEFAULT FALSE,
    created_at        TIMESTAMP     NOT NULL DEFAULT NOW(),
    modified_at       TIMESTAMP     NOT NULL,
    PRIMARY KEY (id)
);
CREATE TRIGGER update_timestamp_integration_jira_configs
    BEFORE INSERT OR UPDATE
    ON integration_jira_configs
    FOR EACH ROW
    EXECUTE PROCEDURE update_modified_at();

ALTER TABLE testrail_settings RENAME TO integration_testrail_configs;
ALTER TRIGGER update_timestamp_testrail_settings ON integration_testrail_configs RENAME TO update_timestamp_integration_testrail_configs;

CREATE TABLE IF NOT EXISTS integration_jenkins_configs (
    id                  BIGSERIAL,
    name                VARCHAR(63)  NOT NULL,
    url                 VARCHAR(511) NOT NULL,
    username            VARCHAR(511) NOT NULL,
    token               VARCHAR(511) NOT NULL,
    launcher_job_name   VARCHAR(255) NULL,
    url_hidden          BOOLEAN      DEFAULT TRUE,
    encrypted           BOOLEAN      NOT NULL DEFAULT FALSE,
    enabled             BOOLEAN      NOT NULL DEFAULT FALSE,
    created_at          TIMESTAMP    NOT NULL DEFAULT NOW(),
    modified_at         TIMESTAMP    NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (url)
);
CREATE TRIGGER update_timestamp_integration_jenkins_configs
    BEFORE INSERT OR UPDATE
    ON integration_jenkins_configs
    FOR EACH ROW
    EXECUTE PROCEDURE update_modified_at();
