package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Setting;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SettingsRepository extends JpaRepository<Setting, Long> {

    Optional<Setting> findByName(String name);

}
