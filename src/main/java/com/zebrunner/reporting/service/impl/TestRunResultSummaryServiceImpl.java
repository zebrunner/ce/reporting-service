package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.db.TestConfig;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary.TestRunResultSummaryBuilder;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestRunResultSummaryService;
import com.zebrunner.reporting.service.TestRunService;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestRunResultSummaryServiceImpl implements TestRunResultSummaryService {

    private final JdbcTemplate jdbcTemplate;
    private final TestRunService testRunService;
    private final TestRun2Service testRun2Service;

    @Override
    @Transactional
    public TestRunResultSummary retrieveByTestRunId(Long testRunId) {
        TestRun testRun = testRun2Service.retrieveById(testRunId);
        Long duration = Optional.ofNullable(testRun.getEndedAt())
                                .map(endedAt -> Duration.between(testRun.getStartedAt(), endedAt))
                                .map(Duration::toSeconds)
                                .orElse(null);

        TestRunResultSummaryBuilder builder = TestRunResultSummary.builder()
                                                                  .testRunId(testRun.getId())
                                                                  .name(testRun.getName())
                                                                  .environment(testRun.getEnvironment())
                                                                  .build(testRun.getBuild())
                                                                  .status(testRun.getStatus().name())
                                                                  .duration(duration);
        populateTestStatistics(testRunId, builder);

        com.zebrunner.reporting.domain.db.TestRun legacyTestRun = testRunService.getTestRunById(testRunId);
        TestConfig config = legacyTestRun.getConfig();
        if (config != null) {
            builder.locale(config.getLocale())
                   .platform(config.getPlatform())
                   .platformVersion(config.getPlatformVersion())
                   .browser(config.getBrowser())
                   .browserVersion(config.getBrowserVersion());
        }

        return builder.build();
    }

    private void populateTestStatistics(Long testRunId, TestRunResultSummaryBuilder builder) {
        String selectRunStatisticsQuery =
                "WITH grouped_tests AS ( " +
                        "    SELECT COUNT(*) AS tests_count, status, known_issue " +
                        "    FROM test_executions " +
                        "    WHERE test_suite_execution_id = ? " +
                        "    GROUP BY status, known_issue " +
                        ") " +
                        "SELECT (SELECT COALESCE(SUM(tests_count), 0) FROM grouped_tests WHERE status = 'PASSED')  AS passed_amount, " +
                        "       (SELECT COALESCE(SUM(tests_count), 0) FROM grouped_tests WHERE status = 'FAILED')  AS failed_amount, " +
                        "       (SELECT COALESCE(SUM(tests_count), 0) FROM grouped_tests WHERE status = 'FAILED' " +
                        "                                                                  AND known_issue = TRUE) AS known_issues_amount, " +
                        "       (SELECT COALESCE(SUM(tests_count), 0) FROM grouped_tests WHERE status = 'SKIPPED') AS skipped_amount, " +
                        "       (SELECT COALESCE(SUM(tests_count), 0) FROM grouped_tests WHERE status = 'ABORTED') AS aborted_amount";

        jdbcTemplate.query(selectRunStatisticsQuery, getRowCallbackHandler(builder), testRunId);
    }

    private RowCallbackHandler getRowCallbackHandler(TestRunResultSummaryBuilder builder) {
        return rs -> builder.passedAmount(rs.getInt("passed_amount"))
                            .failedAmount(rs.getInt("failed_amount"))
                            .knownIssuesAmount(rs.getInt("known_issues_amount"))
                            .skippedAmount(rs.getInt("skipped_amount"))
                            .abortedAmount(rs.getInt("aborted_amount"));
    }

}
