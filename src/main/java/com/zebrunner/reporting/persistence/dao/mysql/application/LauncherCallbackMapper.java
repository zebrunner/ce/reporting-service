package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.launcher.LauncherCallback;
import org.apache.ibatis.annotations.Mapper;

import java.util.Optional;

@Mapper
public interface LauncherCallbackMapper {

    void create(LauncherCallback callback);

    Optional<LauncherCallback> findByCiRunId(String ciRunId);

    Optional<LauncherCallback> findByRef(String ref);

}
