package com.zebrunner.reporting.web.request.v1.testsession.validation;

import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.web.request.v1.testsession.StartTestSessionRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RunningSessionMustHaveCapabilitiesValidator implements ConstraintValidator<RunningSessionMustHaveCapabilities, StartTestSessionRequest> {

    @Override
    public boolean isValid(StartTestSessionRequest value, ConstraintValidatorContext context) {
        if (value.getStatus() != null && value.getStatus() == TestSession.Status.RUNNING) {
            return value.getCapabilities() != null && !value.getCapabilities().isEmpty();
        }
        return true;
    }

}
