package com.zebrunner.reporting.web.request.v1.dashboard;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@JGlobalMap
@NoArgsConstructor
public class UpdateDashboardRequest {

    @NotNull
    private Long id;
    @NotBlank
    private String title;

}
