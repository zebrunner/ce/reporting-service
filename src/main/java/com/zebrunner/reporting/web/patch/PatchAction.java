package com.zebrunner.reporting.web.patch;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Repeatable(PatchAction.List.class)
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PatchRequestValidator.class)
public @interface PatchAction {

    String message() default "Invalid patch request";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    PatchOperation op();

    String path();

    @Target({ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = PatchRequestListValidator.class)
    @interface List {

        String message() default "Invalid patch request";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};

        PatchAction[] value();

    }

}
