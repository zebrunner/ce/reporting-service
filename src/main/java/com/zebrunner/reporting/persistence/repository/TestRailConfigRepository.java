package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.integration.TestRailConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TestRailConfigRepository extends JpaRepository<TestRailConfig, Long> {

    default Optional<TestRailConfig> findEnabled() {
        return findByEnabled(true);
    }

    Optional<TestRailConfig> findFirstByOrderById();

    Optional<TestRailConfig> findByEnabled(boolean enabled);

}
