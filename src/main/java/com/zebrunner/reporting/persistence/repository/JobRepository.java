package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {

    Optional<Job> findByJobUrl(String jobUrl);

}
