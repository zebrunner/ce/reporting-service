ALTER TABLE IF EXISTS tests
    ADD COLUMN IF NOT EXISTS maintainer_id BIGINT NULL,
    DROP CONSTRAINT IF EXISTS tests_maintainer_id_fkey,
    ADD CONSTRAINT tests_maintainer_id_fkey
        FOREIGN KEY (maintainer_id) REFERENCES users (id)
            ON DELETE NO ACTION;

UPDATE tests t
SET maintainer_id = (
    SELECT primary_owner_id FROM test_cases tc
    WHERE t.test_case_id = tc.id
);

ALTER TABLE IF EXISTS tests ALTER COLUMN maintainer_id SET NOT NULL;

ALTER TABLE IF EXISTS test_cases
    ALTER COLUMN primary_owner_id DROP NOT NULL,
    DROP CONSTRAINT IF EXISTS test_cases_primary_owner_id_fkey;

DROP INDEX IF EXISTS fk_test_case_primary_owner_asc;


-- latest_unique_test_cases: table of test cases
--                           with unique rows according to new unique fields (class, method)
-- duplicate_test_cases_to_remove: table of test cases
--                                 that have the same unique values as test cases from latest_unique_test_cases
--                                 but lower id
-- latest_test_case_to_duplicate: max_id - id from latest_unique_test_cases
--                                duplicate_id - id of test case from duplicate_test_cases_to_remove
--                                               that matches test case with max_id by unique fields
CREATE TEMP TABLE IF NOT EXISTS latest_test_case_to_duplicate ON COMMIT DROP AS (
    WITH latest_unique_test_cases AS (SELECT MAX(id) id, test_class, test_method FROM test_cases
                                      GROUP BY test_class, test_method),
         duplicate_test_cases_to_remove AS (SELECT tc.id, tc.test_class, tc.test_method
                                            FROM test_cases tc LEFT JOIN latest_unique_test_cases m
                                                USING (id) WHERE m.id IS NULL)
    SELECT latest_unique_test_cases.id max_id,
           duplicate_test_cases_to_remove.id duplicate_id
    FROM latest_unique_test_cases JOIN duplicate_test_cases_to_remove USING (test_class, test_method)
);

-- latest_test_case_to_duplicate may look
-- max_id |  duplicate_id |
--    8   |       1       |
--    8   |       2       |
-- that means that test cases with id 1, 2, 8 are the same by unique fields,
-- but test case with id 8 should be retained

-- reassign test cases with id 1, 2 to test case with id 8
UPDATE tests SET test_case_id = pairs.max_id FROM latest_test_case_to_duplicate pairs
WHERE test_case_id = pairs.duplicate_id;

UPDATE work_items SET test_case_id = pairs.max_id FROM latest_test_case_to_duplicate pairs
WHERE test_case_id = pairs.duplicate_id;

-- delete from table test_cases test cases that are in duplicate_test_cases_to_remove
WITH pairs AS (SELECT duplicate_id FROM latest_test_case_to_duplicate)
DELETE FROM test_cases USING pairs WHERE id = pairs.duplicate_id;

ALTER TABLE test_cases DROP CONSTRAINT IF EXISTS testcases_ownership_unique;
DROP INDEX IF EXISTS testcases_ownership_unique;
CREATE UNIQUE INDEX IF NOT EXISTS test_cases_test_class_test_method_unique
    ON test_cases (test_class, test_method);
