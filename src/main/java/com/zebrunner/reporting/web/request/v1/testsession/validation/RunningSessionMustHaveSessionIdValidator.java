package com.zebrunner.reporting.web.request.v1.testsession.validation;

import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.web.request.v1.testsession.StartTestSessionRequest;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RunningSessionMustHaveSessionIdValidator implements ConstraintValidator<RunningSessionMustHaveSessionId, StartTestSessionRequest> {

    @Override
    public boolean isValid(StartTestSessionRequest value, ConstraintValidatorContext context) {
        if (value.getStatus() != null && value.getStatus() == TestSession.Status.RUNNING) {
            return StringUtils.isNotBlank(value.getSessionId());
        }
        return true;
    }

}
