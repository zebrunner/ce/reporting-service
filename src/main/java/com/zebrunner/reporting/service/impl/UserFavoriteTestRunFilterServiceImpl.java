package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.UserFavoriteTestRunFilter;
import com.zebrunner.reporting.persistence.repository.UserFavoriteTestRunFilterRepository;
import com.zebrunner.reporting.service.UserFavoriteTestRunFilterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserFavoriteTestRunFilterServiceImpl implements UserFavoriteTestRunFilterService {

    private final UserFavoriteTestRunFilterRepository userFavoriteTestRunFilterRepository;

    @Override
    public List<UserFavoriteTestRunFilter> retrieveFavoriteFilters(Integer userId) {
        return userFavoriteTestRunFilterRepository.findByUserId(userId);
    }

}
