package com.zebrunner.reporting.web.request.v1.dashboard;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
public class AddDashboardWidgetRequest {

    @NotBlank
    private String location;

}
