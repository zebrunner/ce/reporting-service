package com.zebrunner.reporting.service.dashboard;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.Dashboard;
import com.zebrunner.reporting.domain.entity.DashboardWidget;
import com.zebrunner.reporting.domain.entity.Widget;
import com.zebrunner.reporting.persistence.repository.DashboardWidgetRepository;
import com.zebrunner.reporting.service.DashboardService;
import com.zebrunner.reporting.service.widget.WidgetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.zebrunner.reporting.service.exception.BusinessConstraintError.DASHBOARD_CAN_NOT_BE_UPDATED;
import static com.zebrunner.reporting.service.exception.BusinessConstraintError.WIDGET_ON_DASHBOARD_ALREADY_EXISTS;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.DASHBOARD_WIDGET_NOT_FOUND_BY_DASHBOARD_ID_AND_WIDGET_ID;

@Service
@RequiredArgsConstructor
public class DashboardWidgetServiceImpl implements DashboardWidgetService {

    private final DashboardWidgetRepository dashboardWidgetRepository;

    private final WidgetService widgetService;
    private final DashboardService dashboardService;

    @Override
    @Transactional(readOnly = true)
    public List<DashboardWidget> retrieveAllByWidgetId(Long widgetId) {
        return dashboardWidgetRepository.findAllByWidgetId(widgetId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DashboardWidget> retrieveAllInDashboardsByTitleAndTemplateId(List<Long> dashboardIds, String title, Long templateId) {
        return dashboardWidgetRepository.findAllInDashboardsByTitleAndTemplateId(dashboardIds, title, templateId);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DashboardWidget> retrieveOptionalInDashboardByTitleAndTemplateId(Long dashboardId, String title, Long templateId) {
        return dashboardWidgetRepository.findInDashboardByTitleAndTemplateId(dashboardId, title, templateId);
    }

    @Override
    @Transactional
    public DashboardWidget addToDashboard(Long dashboardId, Long widgetId, String location) {
        dashboardService.verifyExistenceById(dashboardId);
        Widget widget = widgetService.retrieveById(widgetId);
        retrieveOptionalInDashboardByTitleAndTemplateId(dashboardId, widget.getTitle(), widget.getWidgetTemplate().getId())
                .ifPresent($ -> {
                    throw new BusinessConstraintException(WIDGET_ON_DASHBOARD_ALREADY_EXISTS, dashboardId);
                });

        DashboardWidget dashboardWidget = DashboardWidget.builder()
                                                         .dashboardId(dashboardId)
                                                         .widget(new Widget(widgetId))
                                                         .location(location)
                                                         .build();
        return dashboardWidgetRepository.save(dashboardWidget);
    }

    @Override
    @Transactional
    public List<DashboardWidget> batchUpdate(Long dashboardId, List<DashboardWidget> widgets) {
        Dashboard dashboard = dashboardService.retrieveById(dashboardId);

        if (!dashboard.isEditable()) {
            throw new BusinessConstraintException(DASHBOARD_CAN_NOT_BE_UPDATED);
        }

        return widgets.stream()
                      .map(widget -> update(dashboardId, widget))
                      .collect(Collectors.toList());
    }

    private DashboardWidget update(Long dashboardId, DashboardWidget newWidget) {
        DashboardWidget widget = dashboardWidgetRepository.findByDashboardIdAndWidgetId(dashboardId, newWidget.getWidget().getId())
                                                          .orElseThrow(() -> new ResourceNotFoundException(
                                                                  DASHBOARD_WIDGET_NOT_FOUND_BY_DASHBOARD_ID_AND_WIDGET_ID,
                                                                  dashboardId, newWidget.getWidget().getId()
                                                          ));
        widget.setLocation(newWidget.getLocation());
        return dashboardWidgetRepository.save(widget);
    }

    @Override
    @Transactional
    public void removeFromDashboard(Long dashboardId, Long widgetId) {
        dashboardWidgetRepository.findByDashboardIdAndWidgetId(dashboardId, widgetId)
                                 .orElseThrow(() -> new ResourceNotFoundException(DASHBOARD_WIDGET_NOT_FOUND_BY_DASHBOARD_ID_AND_WIDGET_ID, dashboardId, widgetId));
        dashboardWidgetRepository.deleteByDashboardIdAndWidgetId(dashboardId, widgetId);
    }

}
