package com.zebrunner.reporting.service.widget;

import com.zebrunner.reporting.domain.entity.Widget;

import java.util.List;

public interface WidgetService {

    Widget create(Long dashboardId, Widget widget, Long templateId);

    Widget retrieveById(Long id);

    void verifyExistenceById(Long id);

    List<Widget> retrieveAll();

    Widget update(Long id, Widget newWidget);

    void deleteById(Long id);

}
