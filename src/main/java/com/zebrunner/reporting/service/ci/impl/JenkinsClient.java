package com.zebrunner.reporting.service.ci.impl;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.Executable;
import com.offbytwo.jenkins.model.FolderJob;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueItem;
import com.offbytwo.jenkins.model.QueueReference;
import com.zebrunner.common.eh.exception.MalformedInputException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.Job;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.BuildParameterType;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import com.zebrunner.reporting.service.ci.CiServerAdapter;
import com.zebrunner.reporting.service.exception.MalformedInputError;
import com.zebrunner.reporting.service.integration.jenkins.JenkinsConfigService;
import com.zebrunner.reporting.service.util.URLNormalizer;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.JENKINS_CONFIG_NOT_FOUND_OR_DISABLED;

@Slf4j
@Service
@RequiredArgsConstructor
public class JenkinsClient implements CiServerAdapter {

    private static final String AGENT_RUN_CONTEXT_PARAM = "REPORTING_RUN_CONTEXT";

    private static final String JENKINS_URL_ENV_VARIABLE = "JENKINS_URL";
    private static final String BUILD_URL_ENV_VARIABLE = "BUILD_URL";
    private static final String BUILD_NUMBER_ENV_VARIABLE = "BUILD_NUMBER";
    private static final String JOB_URL_ENV_VARIABLE = "JOB_URL";
    private static final String JOB_NAME_ENV_VARIABLE = "JOB_NAME";

    /**
     * Artificial environment variable which is used to store link to queued Jenkins build.
     * It is added to ci context of test run when either new build is triggered or rerun is initiated.
     * It is basically used when queued ci build needs to be aborted.
     * <p>
     * NOTE: the value will be removed from ci context when the initiated by the queue item test run starts.
     */
    private static final String QUEUE_ITEM_URL_ENV_VARIABLE = "zebrunner.QUEUE_ITEM";

    private final static Set<String> ENV_VARIABLES_TO_RETRIEVE_BUILD_PARAMETERS = Set.of(
            JENKINS_URL_ENV_VARIABLE,
            BUILD_URL_ENV_VARIABLE
    );

    private final static Set<String> ENV_VARIABLES_TO_RETRIEVE_JOB = Set.of(
            JENKINS_URL_ENV_VARIABLE,
            JOB_URL_ENV_VARIABLE,
            JOB_NAME_ENV_VARIABLE,
            BUILD_NUMBER_ENV_VARIABLE
    );

    private final static Set<String> ENV_VARIABLES_TO_BUILD = Set.of(
            JENKINS_URL_ENV_VARIABLE,
            JOB_URL_ENV_VARIABLE,
            JOB_NAME_ENV_VARIABLE
    );

    private final static Set<String> ENV_VARIABLES_TO_ABORT = Set.of(
            JENKINS_URL_ENV_VARIABLE,
            BUILD_URL_ENV_VARIABLE
    );

    private final static Set<String> ENV_VARIABLES_TO_RERUN = Set.of(
            JENKINS_URL_ENV_VARIABLE,
            BUILD_URL_ENV_VARIABLE,
            JOB_URL_ENV_VARIABLE,
            JOB_NAME_ENV_VARIABLE
    );

    private final RetryTemplate retryTemplate;
    private final JenkinsConfigService jenkinsConfigService;

    @Override
    public TestRunCiContext.Type getType() {
        return TestRunCiContext.Type.JENKINS;
    }

    @Override
    public List<BuildParameterType> retrieveLegacyBuildParameters(TestRunCiContext ciContext) {
        Map<String, String> envVariables = ciContext.getEnvVariables();
        boolean containsRequiredEnvVariables = envVariables.keySet()
                                                           .containsAll(ENV_VARIABLES_TO_RETRIEVE_BUILD_PARAMETERS);
        if (containsRequiredEnvVariables) {
            String jenkinsUrl = envVariables.get(JENKINS_URL_ENV_VARIABLE);
            JenkinsServer jenkinsServer = obtainJenkinsServer(jenkinsUrl);

            BuildWithDetails build = fetchBuild(jenkinsServer, envVariables);
            return getJobParameters(build.getActions());
        }

        return List.of();
    }

    private List<BuildParameterType> getJobParameters(List<Map<String, Object>> actions) {
        Map<String, Object> parametersAction = actions
                .stream()
                // at the moment of writing, the desired action class is hudson.model.ParametersAction
                .filter(action -> action.get("_class") != null && action.get("_class").toString().contains("ParametersAction"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Could not find the ParametersAction"));

        List<BuildParameterType> result = new ArrayList<>();
        if (parametersAction != null) {
            Object allParameters = parametersAction.get("parameters");
            if (allParameters instanceof List) {
                List<?> parametersList = (List<?>) allParameters;
                for (Object parametersObject : parametersList) {
                    if (parametersObject instanceof Map) {
                        Map<?, ?> parametersEntry = (Map<?, ?>) parametersObject;

                        String parameterName = String.valueOf(parametersEntry.get("name"));
                        Object parameterValue = parametersEntry.get("value");
                        String parameterClass = String.valueOf(parametersEntry.get("_class"));

                        // we return a parameter if
                        // 1. it has non-null value (e.g. jenkins api doesn't return credentials value)
                        // 2. its name is not value of AGENT_RERUN_EXPRESSION_PARAM constant (to avoid unintentional rerun)
                        // 3. its name is not 'ci_run_id' (to support current version of qps pipelines)
                        if (parameterValue != null
                                && !AGENT_RUN_CONTEXT_PARAM.equals(parameterName)
                                && !"ci_run_id".equals(parameterName)) {
                            result.add(buildParameter(parameterName, parameterValue, parameterClass));
                        }
                    }
                }
            }
        }

        return result;
    }

    private BuildParameterType buildParameter(String parameterName, Object parameterValue, String parameterClass) {
        BuildParameterType.Builder builder = BuildParameterType.builder()
                                                               .name(parameterName)
                                                               .value(String.valueOf(parameterValue));

        if (parameterClass.contains("Hide")) {
            builder.parameterClass(BuildParameterType.BuildParameterClass.HIDDEN);
        } else if (parameterClass.contains("String")) {
            builder.parameterClass(BuildParameterType.BuildParameterClass.STRING);
        } else if (parameterClass.contains("Boolean")) {
            builder.parameterClass(BuildParameterType.BuildParameterClass.BOOLEAN);
        }

        return builder.build();
    }

    @Override
    public void populateLegacyJobAndBuildNumber(TestRun testRun, TestRunCiContext ciContext) {
        Map<String, String> envVariables = ciContext.getEnvVariables();
        boolean containsRequiredEnvVariables = envVariables.keySet().containsAll(ENV_VARIABLES_TO_RETRIEVE_JOB);

        if (containsRequiredEnvVariables) {
            Job job = new Job();
            job.setJenkinsHost(envVariables.get(JENKINS_URL_ENV_VARIABLE));
            job.setJobURL(envVariables.get(JOB_URL_ENV_VARIABLE));
            job.setName(envVariables.get(JOB_NAME_ENV_VARIABLE));

            testRun.setJob(job);
            testRun.setBuildNumber(Integer.valueOf(envVariables.get(BUILD_NUMBER_ENV_VARIABLE)));
        }
    }

    @Override
    @SneakyThrows
    public TestRunCiContext build(TestRunCiContext baseCiContext, Map<String, String> parameters, String runContext) {
        Map<String, String> envVariables = baseCiContext.getEnvVariables();
        if (!envVariables.keySet().containsAll(ENV_VARIABLES_TO_BUILD)) {
            throw new MalformedInputException(MalformedInputError.REQUIRED_ENV_VARIABLES_MISSED_FOR_CI, getType());
        }

        String jenkinsUrl = envVariables.get(JENKINS_URL_ENV_VARIABLE);
        JenkinsServer jenkinsServer = obtainJenkinsServer(jenkinsUrl);

        JobWithDetails job = fetchJob(jenkinsServer, envVariables);
        parameters.put(AGENT_RUN_CONTEXT_PARAM, runContext);
        QueueReference queueReference = job.build(parameters, true);

        return buildNewCiContext(queueReference, jenkinsUrl);
    }

    @SneakyThrows
    private TestRunCiContext buildNewCiContext(QueueReference queueReference, String jenkinsUrl) {
        Map<String, String> envVariables = new HashMap<>();
        envVariables.put(QUEUE_ITEM_URL_ENV_VARIABLE, queueReference.getQueueItemUrlPart());
        envVariables.put(JENKINS_URL_ENV_VARIABLE, jenkinsUrl);

        return TestRunCiContext.builder()
                               .envVariables(envVariables)
                               .type(TestRunCiContext.Type.JENKINS)
                               .build();
    }

    @Override
    @SneakyThrows
    public boolean tryAbortExecutingNowQueuedBuild(TestRunCiContext ciContext) {
        Map<String, String> envVariables = ciContext.getEnvVariables();

        if (envVariables.containsKey(QUEUE_ITEM_URL_ENV_VARIABLE)) {
            String jenkinsUrl = envVariables.get(JENKINS_URL_ENV_VARIABLE);
            JenkinsServer jenkinsServer = obtainJenkinsServer(jenkinsUrl);

            QueueReference queueReference = new QueueReference(envVariables.get(QUEUE_ITEM_URL_ENV_VARIABLE));
            QueueItem queueItem = jenkinsServer.getQueueItem(queueReference);

            if (queueItem.getExecutable() != null) {
                abort(ciContext);
                return true;
            }
        }
        return false;
    }

    @Override
    @SneakyThrows
    public void abort(TestRunCiContext ciContext) {
        Map<String, String> envVariables = ciContext.getEnvVariables();

        if (envVariables.containsKey(QUEUE_ITEM_URL_ENV_VARIABLE)) {
            // An item from build queue is not removed immediately.
            // There is a time gap between the moment when we fetch info about the queue item
            // and the moment when we perform the cancellation.
            // If we try to cancel an item which is not in the queue, an exception will be thrown.
            // Because of this limitation, it is better to retry the build cancellation.
            retryTemplate.execute(context -> {
                cancelByQueueItemOrExecutable(envVariables);
                return null;
            });

        } else if (envVariables.keySet().containsAll(ENV_VARIABLES_TO_ABORT)) {
            String jenkinsUrl = envVariables.get(JENKINS_URL_ENV_VARIABLE);
            JenkinsServer jenkinsServer = obtainJenkinsServer(jenkinsUrl);

            BuildWithDetails build = fetchBuild(jenkinsServer, envVariables);

            stopRunningBuild(build.getClient(), build.getUrl());
        } else {
            log.warn("Could not abort the test run. Captured environment variables do not contain all the required entries.");
        }
    }

    @SneakyThrows
    private void cancelByQueueItemOrExecutable(Map<String, String> envVariables) {
        String jenkinsUrl = envVariables.get(JENKINS_URL_ENV_VARIABLE);
        JenkinsServer jenkinsServer = obtainJenkinsServer(jenkinsUrl);

        QueueReference queueReference = new QueueReference(envVariables.get(QUEUE_ITEM_URL_ENV_VARIABLE));
        QueueItem queueItem = jenkinsServer.getQueueItem(queueReference);

        if (queueItem.getExecutable() == null) { // if the item is still in queue
            jenkinsUrl = StringUtils.removeEndIgnoreCase(jenkinsUrl, "/");

            String cancelQueueItemUrl = jenkinsUrl + "/queue/cancelItem?id=" + queueItem.getId();
            queueItem.getClient().post(cancelQueueItemUrl, true);
        } else {
            stopRunningBuild(queueItem.getClient(), queueItem.getExecutable().getUrl());
        }
    }

    @SneakyThrows
    private void stopRunningBuild(JenkinsHttpClient client, String buildUrl) {
        buildUrl = StringUtils.removeEndIgnoreCase(buildUrl, "/");

        String stopEndpoint = buildUrl + "/stop";
        client.post(stopEndpoint, true);
    }

    @Override
    @SneakyThrows
    public TestRunCiContext rerun(TestRunCiContext ciContext, String runContext) {
        Map<String, String> envVariables = ciContext.getEnvVariables();
        if (!envVariables.keySet().containsAll(ENV_VARIABLES_TO_RERUN)) {
            throw new MalformedInputException(MalformedInputError.REQUIRED_ENV_VARIABLES_MISSED_FOR_CI, getType());
        }

        String jenkinsUrl = envVariables.get(JENKINS_URL_ENV_VARIABLE);
        JenkinsServer jenkinsServer = obtainJenkinsServer(jenkinsUrl);

        Map<String, String> parameters = fetchBuildParameters(jenkinsServer, envVariables);
        parameters.put(AGENT_RUN_CONTEXT_PARAM, runContext);

        JobWithDetails job = fetchJob(jenkinsServer, envVariables);
        QueueReference queueReference = job.build(parameters, true);

        ciContext.getEnvVariables()
                 .put(QUEUE_ITEM_URL_ENV_VARIABLE, queueReference.getQueueItemUrlPart());
        return ciContext;
    }

    private JenkinsServer obtainJenkinsServer(String jenkinsUrl) {
        String normalizedJenkinsUrl = URLNormalizer.normalize(jenkinsUrl);
        JenkinsConfig config = jenkinsConfigService.retrieveOptionalByUrl(normalizedJenkinsUrl)
                                                   // fallback in case when normalized url is not found
                                                   .or(() -> jenkinsConfigService.retrieveOptionalByUrl(jenkinsUrl))
                                                   .or(() -> jenkinsConfigService.retrieveOptionalByUrl(jenkinsUrl.substring(0, jenkinsUrl.lastIndexOf("/"))))
                                                   .orElseThrow(() -> new ResourceNotFoundException(JENKINS_CONFIG_NOT_FOUND_OR_DISABLED));
        return toJenkinsServer(config);
    }

    @SneakyThrows
    private JenkinsServer toJenkinsServer(JenkinsConfig config) {
        String url = config.getUrl();
        String username = config.getUsername();
        String token = config.getToken();

        return new JenkinsServer(new URI(url), username, token);
    }

    @SneakyThrows
    private Map<String, String> fetchBuildParameters(JenkinsServer jenkinsServer, Map<String, String> envVariables) {
        String buildUrl = envVariables.get(BUILD_URL_ENV_VARIABLE);

        Executable executable = new Executable();
        executable.setUrl(buildUrl);

        QueueItem queueItem = new QueueItem();
        queueItem.setExecutable(executable);

        Build build = jenkinsServer.getBuild(queueItem);
        if (build != null) {
            return build.details().getParameters();
        } else {
            throw new RuntimeException("Could not find build by url " + buildUrl);
        }
    }

    @SneakyThrows
    private JobWithDetails fetchJob(JenkinsServer jenkinsServer, Map<String, String> envVariables) {
        String jobName = envVariables.get(JOB_NAME_ENV_VARIABLE);

        if (jobName.contains("/")) {
            String jobUrl = envVariables.get(JOB_URL_ENV_VARIABLE);
            jobUrl = jobUrl.substring(0, jobUrl.lastIndexOf("/job") + 1); // we need to keep the ending / symbol in url
            String jobBaseName = jobName.substring(jobName.lastIndexOf('/') + 1);

            FolderJob folderJob = new FolderJob(jobName, jobUrl);
            return jenkinsServer.getJob(folderJob, jobBaseName);
        } else {
            return jenkinsServer.getJob(jobName);
        }
    }

    @SneakyThrows
    private BuildWithDetails fetchBuild(JenkinsServer jenkinsServer, Map<String, String> envVariables) {
        Executable executable = new Executable();
        executable.setUrl(envVariables.get(BUILD_URL_ENV_VARIABLE));
        QueueItem queueItem = new QueueItem();
        queueItem.setExecutable(executable);

        Build build = jenkinsServer.getBuild(queueItem);
        return build.details();
    }

}
