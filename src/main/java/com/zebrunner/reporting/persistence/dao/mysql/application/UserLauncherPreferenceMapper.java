package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.launcher.UserLauncherPreference;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Optional;

@Mapper
public interface UserLauncherPreferenceMapper {

    void create(@Param("launcherId") Long launcherId, @Param("userId") Long userId, @Param("userLauncherPreference") UserLauncherPreference userLauncherPreference);

    Optional<UserLauncherPreference> findByLauncherIdAndUserId(@Param("launcherId") Long launcherId, @Param("userId") Long userId);

    void update(UserLauncherPreference preference);

    void deleteAllByUserId(@Param("userId") Long userId);

}
