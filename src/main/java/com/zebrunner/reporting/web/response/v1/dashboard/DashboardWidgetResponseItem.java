package com.zebrunner.reporting.web.response.v1.dashboard;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
public class DashboardWidgetResponseItem {

    private Long id;
    private String title;
    private String description;
    private String paramsConfig;
    private String legendConfig;
    private String type;
    private String location;
    private WidgetTemplateResponseItem widgetTemplate;

}
