package com.zebrunner.reporting.web.response.v1.dashboard;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.googlecode.jmapper.annotations.JMap;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddDashboardWidgetResponse {

    @JsonProperty("id")
    @JMap("${widget.id}")
    private Long widgetId;
    @JMap("location")
    private String location;

}
