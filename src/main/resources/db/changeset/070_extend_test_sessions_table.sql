ALTER TABLE test_sessions
  ALTER COLUMN started_at DROP NOT NULL;
ALTER TABLE test_sessions
  ALTER COLUMN session_id DROP NOT NULL;

ALTER TABLE test_sessions
  ADD COLUMN initiated_at TIMESTAMP NULL DEFAULT NOW();
ALTER TABLE test_sessions
  ADD COLUMN status VARCHAR(63) NOT NULL DEFAULT 'COMPLETED';

UPDATE test_sessions
SET status = 'RUNNING'
WHERE ended_at IS NULL;

ALTER TABLE test_sessions
  DROP CONSTRAINT test_sessions_transient_session_id_key;
CREATE UNIQUE INDEX test_sessions_session_id_unique ON test_sessions (session_id) WHERE session_id IS NOT NULL;
