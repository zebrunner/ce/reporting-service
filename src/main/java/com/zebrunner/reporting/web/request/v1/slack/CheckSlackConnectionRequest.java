package com.zebrunner.reporting.web.request.v1.slack;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@JGlobalMap
@NoArgsConstructor
public class CheckSlackConnectionRequest {

    @NotBlank
    private String token;
    private boolean encrypted;

}
