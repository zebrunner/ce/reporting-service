package com.zebrunner.reporting.web.request.v1.dashboard;

import com.googlecode.jmapper.annotations.JMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UpdateDashboardWidgetRequest {

    @NotNull
    @JMap("${widget.id}")
    private Long widgetId;
    @NotBlank
    @JMap("location")
    private String location;

}
