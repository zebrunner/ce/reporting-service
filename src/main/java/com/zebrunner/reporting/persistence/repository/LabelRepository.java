package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Label;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LabelRepository extends JpaRepository<Label, Integer> {

    Optional<Label> findByKeyAndValue(String key, String value);

}
