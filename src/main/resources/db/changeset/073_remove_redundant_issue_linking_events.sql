-- remove UNLINK action events which does not have a corresponding LINK action
-- such UNLINK events does not have any sense and does not provide useful information, thus we can simply remove them
WITH unlinkings_to_remove AS (
  SELECT MAX(id) AS id
  FROM issue_reference_assignment_events irae
  GROUP BY (test_case_id, issue_reference_id, failure_reason_hash)
  HAVING COUNT(*) = 1
     AND (SELECT action FROM issue_reference_assignment_events WHERE id = MAX(irae.id)) = 'UNLINK'
)
DELETE
FROM issue_reference_assignment_events irae
  USING unlinkings_to_remove utr
WHERE irae.id = utr.id;

-- remove LINK action events which are not the latest ones for the groups of test_case_id, issue_reference_id, failure_reason_hash
WITH linkings_to_remove AS (
  SELECT test_case_id,
         issue_reference_id,
         failure_reason_hash,
         MIN(created_at) AS min_created_at
  FROM issue_reference_assignment_events irae
  WHERE action = 'LINK'
  GROUP BY (test_case_id, issue_reference_id, failure_reason_hash)
  HAVING COUNT(*) > 1
)
DELETE
FROM issue_reference_assignment_events irae
  USING linkings_to_remove ltr
WHERE irae.action = 'LINK'
  AND irae.test_case_id = ltr.test_case_id
  AND irae.issue_reference_id = ltr.issue_reference_id
  AND irae.failure_reason_hash = ltr.failure_reason_hash
  AND irae.created_at <> ltr.min_created_at;
