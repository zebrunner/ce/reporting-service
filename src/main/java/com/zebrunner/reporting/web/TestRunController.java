package com.zebrunner.reporting.web;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.common.eh.exception.MalformedInputException;
import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.BuildParameterType;
import com.zebrunner.reporting.domain.dto.CommentType;
import com.zebrunner.reporting.domain.dto.EmailType;
import com.zebrunner.reporting.domain.dto.TestRunType;
import com.zebrunner.reporting.domain.dto.TestType;
import com.zebrunner.reporting.domain.push.TestPush;
import com.zebrunner.reporting.domain.push.TestRunPush;
import com.zebrunner.reporting.domain.push.TestRunStatisticPush;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.messaging.websocket.Topic;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.SearchResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestRunSearchCriteria;
import com.zebrunner.reporting.service.LauncherCallbackService;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestRunService;
import com.zebrunner.reporting.service.TestRunStatisticsService;
import com.zebrunner.reporting.service.TestService;
import com.zebrunner.reporting.service.exception.MalformedInputError;
import com.zebrunner.reporting.service.util.EmailUtils;
import com.zebrunner.reporting.service.util.StreamUtils;
import com.zebrunner.reporting.web.documented.TestRunDocumentedController;
import com.zebrunner.reporting.web.request.v1.AttachArtifactReferencesRequest;
import lombok.RequiredArgsConstructor;
import org.dozer.Mapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RequestMapping(path = "api/tests/runs", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class TestRunController implements TestRunDocumentedController {

    private final TestRunService testRunService;
    private final TestService testService;
    private final SimpMessagingTemplate websocketTemplate;
    private final TestRunStatisticsService statisticsService;
    private final LauncherCallbackService launcherCallbackService;
    private final Mapper mapper;

    private final TestRun2Service testRun2Service;

    @PostMapping
    @Override
    public TestRunType startTestRun(@RequestBody @Valid TestRunType testRunType) {
        TestRun testRun = mapper.map(testRunType, TestRun.class);
        testRun = testRunService.startTestRun(testRun, testRunType.getConfigXML());

        sendTestRunPush(testRun);
        websocketTemplate.convertAndSend(Topic
                .testRunStatistics(), new TestRunStatisticPush(statisticsService.get(testRun.getId())));

        return mapper.map(testRun, TestRunType.class);
    }

    @PutMapping
    @Override
    public TestRunType updateTestRun(@Valid @RequestBody TestRunType testRunType) {
        TestRun testRun = mapper.map(testRunType, TestRun.class);

        testRunService.updateTestRunConfig(testRun, testRunType.getConfigXML());

        sendTestRunPush(testRun);
        websocketTemplate.convertAndSend(Topic
                .testRunStatistics(), new TestRunStatisticPush(statisticsService.get(testRun.getId())));

        return mapper.map(testRun, TestRunType.class);
    }

    @PostMapping("/{id}/finish")
    @Override
    public TestRunType finishTestRun(@PathVariable("id") long id) {
        TestRun testRun = testRunService.finishRun(id);
        TestRun testRunFull = testRunService.getTestRunByIdFull(testRun.getId());

        launcherCallbackService.notifyOnTestRunFinish(testRun.getCiRunId());

        websocketTemplate
                .convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(statisticsService.get(id)));
        sendTestRunPush(testRunFull);

        return mapper.map(testRun, TestRunType.class);
    }

    @Override
    @PostMapping("/abort")
    @PreAuthorize("hasPermission('reporting:test-runs:update')")
    public TestRunType abortTestRun(@RequestParam(value = "id", required = false) Long id,
                                    @RequestParam(value = "ciRunId", required = false) String ciRunId,
                                    @RequestBody(required = false) CommentType abortCause) {
        TestRun testRun = testRunService.abortTestRun(id, ciRunId, abortCause);

        if (Status.IN_PROGRESS.equals(testRun.getStatus())) {
            List<Test> tests = testService.getTestsByTestRunId(testRun.getId());
            tests.stream()
                 .filter(test -> Status.ABORTED.equals(test.getStatus()))
                 .forEach(test ->
                         websocketTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test))
                 );

            sendTestRunPush(testRunService.getTestRunByIdFull(testRun.getId()));
            websocketTemplate.convertAndSend(
                    Topic.testRunStatistics(), new TestRunStatisticPush(statisticsService.get(testRun.getId()))
            );
        }
        return mapper.map(testRun, TestRunType.class);
    }

    @PostMapping("/queue")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void createQueuedTestRun() {
        // TODO by nsidorevich on 1/26/21: remove once pipeline code will be adjusted
    }

    @GetMapping("/{id}")
    @Override
    public TestRunType getTestRun(@PathVariable("id") long id) {
        TestRun testRun = testRunService.getTestRunById(id);
        return mapper.map(testRun, TestRunType.class);
    }

    @GetMapping("/search")
    @Override
    public SearchResult<TestRun> searchTestRuns(TestRunSearchCriteria sc) {
        return testRunService.search(sc);
    }

    @PostMapping("{id}/artifacts")
    @Override
    // TODO: 4/25/20 get rid of interface to validate list
    public Set<ArtifactReference> attachArtifacts(@PathVariable("id") Long testRunId,
                                                  @RequestBody @Valid List<AttachArtifactReferencesRequest.ArtifactReference> testRunArtifactReferences) {
        validate(testRunArtifactReferences);
        Set<ArtifactReference> references = StreamUtils.mapToSet(testRunArtifactReferences, requestReference -> mapper.map(requestReference, ArtifactReference.class));

        return testRun2Service.attachArtifactReferences(testRunId, references);
    }

    // todo 7/14/21 by edovnar: need to be refactored
    public void validate(List<AttachArtifactReferencesRequest.ArtifactReference> requestReferences) {
        Set<String> uniqueNames = new HashSet<>();
        List<String> duplicateNames = StreamUtils.mapToList(requestReferences, AttachArtifactReferencesRequest.ArtifactReference::getName)
                                                 .stream()
                                                 .filter(requestName -> !uniqueNames.add(requestName))
                                                 .collect(Collectors.toList());
        if (!duplicateNames.isEmpty()) {
            throw new MalformedInputException(MalformedInputError.DUPLICATE_ARTIFACT_REFERENCE_NAMES, duplicateNames);
        }
    }

    @GetMapping
    @Override
    public TestRunType getTestRunByCiRunId(@RequestParam("ciRunId") String ciRunId) {
        TestRun testRun = testRunService.getTestRunByCiRunId(ciRunId);
        return mapper.map(testRun, TestRunType.class);
    }

    //TODO: used only by zafira-client, might be deleted after (probably)
    @GetMapping("/{id}/results")
    @Override
    public List<TestType> getTestRunResults(@PathVariable("id") long id) {
        List<Test> tests = testService.getTestsByTestRunId(id);
        return tests.stream()
                    .map(test -> mapper.map(test, TestType.class))
                    .collect(Collectors.toList());
    }

    @PreAuthorize("hasPermission('reporting:test-runs:delete')")
    @DeleteMapping("/{id}")
    @Override
    public void deleteById(@PathVariable("id") Long id) {
        testRun2Service.deleteById(id);
    }

    @PostMapping("/{id}/email")
    @Override
    public void sendTestRunResultsEmail(
            @PathVariable("id") String id,
            @RequestBody @Valid EmailType email,
            @RequestParam(value = "showStacktrace", defaultValue = "true", required = false) boolean showStacktrace
    ) {
        Set<String> toEmails = EmailUtils.obtainValidOnly(email.getRecipients());
        testRunService.shareResultsViaEmail(id, showStacktrace, toEmails);
    }

    @PostMapping("/{id}/emailFailure")
    @Override
    public void sendTestRunFailureEmail(
            @PathVariable("id") String id,
            @RequestBody @Valid EmailType email,
            @RequestParam(name = "suiteOwner", defaultValue = "false") boolean suiteOwner,
            @RequestParam(name = "suiteRunner", defaultValue = "false") boolean suiteRunner
    ) {
        Set<String> toEmails = EmailUtils.obtainValidOnly(email.getRecipients());
        testRunService.sendTestRunResultsEmailFailure(id, suiteOwner, suiteRunner, toEmails);
    }

    @GetMapping(path = "/{id}/export", produces = "text/html;charset=UTF-8")
    @Override
    public String exportTestRunHTML(@PathVariable("id") String id) {
        return testRunService.compileResultsReportAsHtmlString(id);
    }

    @PreAuthorize("hasPermission('reporting:test-runs:update')")
    @PostMapping("/{id}/markReviewed")
    @Override
    public void markTestRunAsReviewed(@PathVariable("id") long id, @RequestBody @Valid CommentType comment) {
        TestRun testRun = testRunService.markAsReviewed(id, comment.getComment());
        websocketTemplate.convertAndSend(Topic
                .testRunStatistics(), new TestRunStatisticPush(statisticsService.get(testRun.getId())));
    }

    @Override
    @GetMapping("/{id}/rerun")
    @PreAuthorize("hasPermission('reporting:test-runs:read')")
    public void rerunTestRun(@PathVariable("id") long id,
                             @PrincipalId Integer userId,
                             @RequestParam(name = "rerunFailures", defaultValue = "false") boolean rerunFailures) {
        testRunService.rerunTestRun(id, rerunFailures, userId.longValue());
    }

    @Override
    @GetMapping("/abort/ci")
    @PreAuthorize("hasPermission('reporting:test-runs:update')")
    public void abortCIJob(@RequestParam(value = "id", required = false) Long id,
                           @RequestParam(value = "ciRunId", required = false) String ciRunId) {
        testRunService.abortTestRunJob(id, ciRunId);
    }

    @Override
    @PostMapping("/{id}/build")
    @PreAuthorize("hasPermission('reporting:test-runs:update')")
    public void buildTestRun(@PathVariable("id") long id,
                             @PrincipalId Integer userId,
                             @RequestBody Map<String, String> jobParameters) {
        testRunService.buildTestRunJob(id, jobParameters, userId.longValue());
    }

    @Override
    @GetMapping("/{id}/jobParameters")
    @PreAuthorize("hasPermission('reporting:test-runs:read')")
    public List<BuildParameterType> getJobParameters(@PathVariable("id") long id) {
        return testRunService.getTestRunJobParameters(id);
    }

    private void sendTestRunPush(TestRun testRun) {
        testRunService.hideJobUrlsIfNeed(List.of(testRun));
        websocketTemplate.convertAndSend(Topic.testRuns(), new TestRunPush(testRun));
    }

}
