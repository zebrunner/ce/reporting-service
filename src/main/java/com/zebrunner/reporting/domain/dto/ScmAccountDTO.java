package com.zebrunner.reporting.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.domain.db.ScmAccount;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScmAccountDTO extends AbstractType {

    private static final long serialVersionUID = 9120645976990419377L;

    @NotBlank
    private String accessToken;
    private String apiVersion;
    private String organizationName;
    private String repositoryName;
    private String avatarURL;
    private String repositoryURL;
    @Positive
    private Long userId;
    @NotNull
    private ScmAccount.Name name;
    private String defaultBranch;

}
