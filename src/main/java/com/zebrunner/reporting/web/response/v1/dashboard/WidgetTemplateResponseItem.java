package com.zebrunner.reporting.web.response.v1.dashboard;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WidgetTemplateResponseItem {

    private Long id;
    private String name;
    private String description;
    private WidgetTemplate.Type type;
    private String chartConfig;
    private Map<String, WidgetTemplateParameter> paramsConfig;
    private String legendConfig;
    private Boolean hidden;

}
