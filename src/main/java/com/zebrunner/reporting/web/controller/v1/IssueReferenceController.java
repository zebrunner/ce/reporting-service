package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.TestIssueReference;
import com.zebrunner.reporting.domain.dto.TestIssueReferenceAssignmentResult;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.domain.entity.integration.IssueReferenceAssignmentEvent;
import com.zebrunner.reporting.domain.push.TestPush;
import com.zebrunner.reporting.domain.push.TestRunPush;
import com.zebrunner.reporting.domain.push.TestRunStatisticPush;
import com.zebrunner.reporting.messaging.websocket.Topic;
import com.zebrunner.reporting.service.IssueReferenceAssignmentEventService;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestRunService;
import com.zebrunner.reporting.service.TestRunStatisticsService;
import com.zebrunner.reporting.service.TestService;
import com.zebrunner.reporting.web.request.v1.AssignIssueReferencesRequest;
import com.zebrunner.reporting.web.request.v1.UnlinkIssueReferencesRequest;
import com.zebrunner.reporting.web.response.v1.AssignIssueReferencesResponse;
import com.zebrunner.reporting.web.response.v1.AssignIssueReferencesResponse.IssueReference;
import com.zebrunner.reporting.web.response.v1.ErrorReason;
import com.zebrunner.reporting.web.response.v1.ErrorResponseItem;
import com.zebrunner.reporting.web.response.v1.GetIssueReferencesResponse;
import com.zebrunner.reporting.web.response.v1.SuccessResponseItem;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/issue-references", produces = MediaType.APPLICATION_JSON_VALUE)
public class IssueReferenceController {

    private final JMapper mapper;
    private final TestService testService;
    private final Test2Service test2Service;
    private final TestRunStatisticsService testRunStatisticsService;
    private final TestRunService testRunService;
    private final SimpMessagingTemplate websocketTemplate;
    private final IssueReferenceAssignmentEventService issueEventService;

    @PostMapping
    @ResponseStatus(HttpStatus.MULTI_STATUS)
    public AssignIssueReferencesResponse assignIssueReferences(@RequestBody @Valid AssignIssueReferencesRequest request, @PrincipalId Integer userId) {
        List<TestIssueReference> issueReferences = mapper.mapToList(request.getItems(), TestIssueReference.class);
        TestIssueReferenceAssignmentResult result = test2Service.linkIssues(issueReferences, userId.longValue());

        sendNotifications(result.getAssignedToTestItems());

        List<ErrorResponseItem> testNotFound = result.getTestNotFoundItems()
                                                     .stream()
                                                     .map(assignment -> ErrorResponseItem.failed(assignment.getTestId(),
                                                             ErrorReason.NOT_FOUND, "Test is not found"))
                                                     .collect(Collectors.toList());

        List<ErrorResponseItem> testNotLogicallyFailed = result.getTestNotLogicallyFailedItems()
                                                               .stream()
                                                               .map(assignment -> ErrorResponseItem.failed(assignment.getTestId(),
                                                                       ErrorReason.ILLEGAL_ASSIGNMENT, "Test is not logically failed"))
                                                               .collect(Collectors.toList());

        List<SuccessResponseItem<IssueReference>> assigned = result.getAssignedToTestItems()
                                                                   .stream()
                                                                   .map(assignment -> SuccessResponseItem.succeeded(assignment.getTestId(),
                                                                           mapper.map(assignment, IssueReference.class)))
                                                                   .collect(Collectors.toList());
        return new AssignIssueReferencesResponse(combineResponses(testNotFound, testNotLogicallyFailed, assigned));
    }

    private Map<Long, Test> mapAssignedTestIssueReferencesToTests(List<TestIssueReferenceAssignmentResult.Item> results) {
        List<Long> testIds = results.stream()
                                    .map(TestIssueReferenceAssignmentResult.Item::getTestId)
                                    .collect(Collectors.toList());
        return testService.retrieveAllByIds(testIds)
                          .stream()
                          .collect(Collectors.toMap(Test::getId, Function.identity(), (v1, v2) -> v1));
    }

    private List<Object> combineResponses(List<?>... responses) {
        return Arrays.stream(responses)
                     .flatMap(Collection::stream)
                     .collect(Collectors.toList());
    }

    @GetMapping
    public GetIssueReferencesResponse getTestCaseIssueReferences(@RequestParam("testId") Long testId) {
        Set<IssueReferenceAssignmentEvent> events = issueEventService.retrieveTestCaseEventsOfLinkingByTestId(testId);
        List<GetIssueReferencesResponse.Item> items = events.stream()
                                                            .map(event -> GetIssueReferencesResponse.Item.builder()
                                                                                                         .id(event.getIssueReference().getId())
                                                                                                         .value(event.getIssueReference().getValue())
                                                                                                         .type(event.getIssueReference().getType().name())
                                                                                                         .createdAt(event.getCreatedAt())
                                                                                                         .build())
                                                            .collect(Collectors.toList());
        return new GetIssueReferencesResponse(items);
    }

    @DeleteMapping
    public void unlinkIssueReference(@RequestBody @Valid UnlinkIssueReferencesRequest request, @PrincipalId Integer userId) {
        Long testId = request.getTestId();
        Long issueReferenceId = request.getIssueReferenceId();

        test2Service.unlinkIssue(testId, issueReferenceId, userId.longValue());

        Test test = testService.getTestById(testId);
        sendNotificationAboutTestUpdate(test);
        sendNotificationAboutTestRunUpdate(test.getTestRunId());
    }

    private void sendNotifications(List<TestIssueReferenceAssignmentResult.Item> assignmentResults) {
        Map<Long, Test> testIdToTest = mapAssignedTestIssueReferencesToTests(assignmentResults);
        assignmentResults.stream()
                         .map(result -> testIdToTest.get(result.getTestId()))
                         .peek(this::sendNotificationAboutTestUpdate)
                         .map(Test::getTestRunId)
                         .distinct()
                         .forEach(this::sendNotificationAboutTestRunUpdate);
    }

    private void sendNotificationAboutTestRunUpdate(Long testRunId) {
        TestRun testRun = testRunService.getTestRunById(testRunId);
        websocketTemplate.convertAndSend(Topic.testRuns(), new TestRunPush(testRun));

        TestRunStatistics testRunStatistic = testRunStatisticsService.get(testRunId);
        websocketTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistic));
    }

    private void sendNotificationAboutTestUpdate(Test test) {
        websocketTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test));
    }

}
