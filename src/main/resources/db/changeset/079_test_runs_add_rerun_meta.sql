ALTER TABLE test_runs
    ADD COLUMN rerun_metadatas jsonb NULL;

ALTER TABLE test_runs
    ALTER COLUMN build_number DROP NOT NULL;
