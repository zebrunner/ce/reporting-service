package com.zebrunner.reporting.service.httpclient.tcm.testrail.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRailPriorityResponse {

    private Long id;
    private String name;
    private Boolean isDefault;
    private Integer priority;
    private String shortName;

}
