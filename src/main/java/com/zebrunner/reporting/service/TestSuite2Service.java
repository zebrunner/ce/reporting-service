package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.TestSuite;

public interface TestSuite2Service {

    TestSuite getOrCreate(TestSuite testSuite);

}
