package com.zebrunner.reporting.service.widgettemplate;

import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;

import java.util.List;
import java.util.Map;

public interface WidgetTemplateQueryExecutor {

    List<Object> executeTemplateParamQuery(WidgetTemplateParameter widgetTemplateParameter, String period);

    List<Map<String, Object>> executeTemplateSql(WidgetTemplate widgetTemplate, Map<String, Object> queryParameters, Integer userId);

}
