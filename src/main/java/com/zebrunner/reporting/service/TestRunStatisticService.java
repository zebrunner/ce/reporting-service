package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.TestRunStatistic;

public interface TestRunStatisticService {

    TestRunStatistic getByTestRunId(Long testRunId);

    void save(TestRunStatistic statistic);

}
