package com.zebrunner.reporting.service.integration.jenkins.impl;

import com.offbytwo.jenkins.JenkinsServer;
import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.persistence.repository.integration.jenkins.JenkinsConfigRepository;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.integration.jenkins.JenkinsConfigService;
import com.zebrunner.reporting.service.util.URLNormalizer;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.JENKINS_CONFIG_NOT_FOUND_BY_ID;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.JENKINS_CONFIG_NOT_FOUND_OR_DISABLED;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.JENKINS_IS_UNREACHABLE_FOR_GIVEN_CONFIG;

@Slf4j
@Service
@RequiredArgsConstructor
public class JenkinsConfigServiceImpl implements JenkinsConfigService {

    private final CryptoService cryptoService;
    private final JenkinsConfigRepository configRepository;

    @Override
    @Transactional(readOnly = true)
    public List<JenkinsConfig> retrieveAll(boolean decryptValues) {
        return configRepository.findAll()
                               .stream()
                               .map(jenkinsConfig -> decryptValuesIfNeeded(jenkinsConfig, decryptValues))
                               .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<JenkinsConfig> retrieveAllActive(boolean decryptValues) {
        return configRepository.findAllByEnabledTrue()
                               .stream()
                               .map(jenkinsConfig -> decryptValuesIfNeeded(jenkinsConfig, decryptValues))
                               .map(this::checkReachability)
                               .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<JenkinsConfig> retrieveOptionalById(Long id, boolean decryptValues) {
        return configRepository.findById(id)
                               .map(jenkinsConfig -> decryptValuesIfNeeded(jenkinsConfig, decryptValues));
    }

    @Override
    @Transactional(readOnly = true)
    public JenkinsConfig retrieveById(Long id, boolean decryptValues) {
        return configRepository.findById(id)
                               .map(jenkinsConfig -> decryptValuesIfNeeded(jenkinsConfig, decryptValues))
                               .orElseThrow(() -> new ResourceNotFoundException(JENKINS_CONFIG_NOT_FOUND_OR_DISABLED));
    }

    @Override
    public JenkinsConfig retrieveActiveById(Long id, boolean decryptValues) {
        return configRepository.findByIdAndEnabledTrue(id)
                               .map(jenkinsConfig -> decryptValuesIfNeeded(jenkinsConfig, decryptValues))
                               .map(this::checkReachability)
                               .orElseThrow(() -> new ResourceNotFoundException(JENKINS_CONFIG_NOT_FOUND_OR_DISABLED));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<JenkinsConfig> retrieveOptionalByUrl(String url, boolean decryptValues) {
        return configRepository.findByUrlAndEnabledTrue(url)
                               .map(config -> decryptValuesIfNeeded(config, decryptValues));
    }

    private JenkinsConfig decryptValuesIfNeeded(JenkinsConfig config, boolean decryptValues) {
        if (decryptValues) {
            if (config.isEncrypted()) {
                config.setToken(cryptoService.decrypt(config.getToken()));
                config.setEncrypted(false);
            }
        } else if (!config.isEncrypted()) {
            config.setToken(cryptoService.encrypt(config.getToken()));
            config.setEncrypted(true);
        }
        return config;
    }

    private JenkinsConfig checkReachability(JenkinsConfig config) {
        ReachabilityCheckResult result = isReachable(config);
        if (!result.getReachable()) {
            log.warn("[Jenkins] Server is not reachable: {}", result.getMessage());
            throw new ResourceNotFoundException(JENKINS_IS_UNREACHABLE_FOR_GIVEN_CONFIG);
        }

        return config;
    }

    @Override
    @SneakyThrows
    public ReachabilityCheckResult isReachable(JenkinsConfig config) {
        String url = config.getUrl();
        String username = config.getUsername();
        try {
            String token = config.isEncrypted() ? cryptoService.decrypt(config.getToken()) : config.getToken();
            JenkinsServer jenkinsServer = new JenkinsServer(new URI(url), username, token);
            return jenkinsServer.isRunning()
                   ? ReachabilityCheckResult.reachable()
                   : ReachabilityCheckResult.unreachable("Jenkins is not running.");
        } catch (Exception e) {
            return ReachabilityCheckResult.unreachable(e);
        }
    }

    @Override
    @Transactional
    public JenkinsConfig create(JenkinsConfig config) {
        String normalizedUrl = URLNormalizer.normalize(config.getUrl());
        if (configRepository.existsByUrl(normalizedUrl)) {
            throw new BusinessConstraintException(BusinessConstraintError.JENKINS_CONFIG_ALREADY_EXISTS);
        }

        config.setUrl(normalizedUrl);
        String encryptedToken = cryptoService.encrypt(config.getToken());
        config.setToken(encryptedToken);
        config.setEnabled(true);
        config.setEncrypted(true);
        return configRepository.save(config);
    }

    @Override
    @Transactional
    public JenkinsConfig update(Long id, JenkinsConfig newConfig) {
        JenkinsConfig config = configRepository.findById(id)
                                               .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.JENKINS_CONFIG_NOT_FOUND_BY_ID));

        if (!Objects.equals(config.getToken(), newConfig.getToken())) {
            config.setToken(cryptoService.encrypt(newConfig.getToken()));
            config.setEncrypted(true);
        }

        updateUrl(config, newConfig.getUrl());
        config.setName(newConfig.getName());
        config.setUsername(newConfig.getUsername());
        config.setEnabled(newConfig.isEnabled());
        config.setLauncherJobName(newConfig.getLauncherJobName());

        return configRepository.save(config);
    }

    private void updateUrl(JenkinsConfig config, String newUrl) {
        String normalizedNewConfigUrl = URLNormalizer.normalize(newUrl);
        boolean sameUrl = config.getUrl().equals(normalizedNewConfigUrl);
        if (!sameUrl && configRepository.existsByUrl(normalizedNewConfigUrl)) {
            throw new BusinessConstraintException(BusinessConstraintError.JENKINS_CONFIG_ALREADY_EXISTS);
        }
        config.setUrl(normalizedNewConfigUrl);
    }

    @Override
    public void patchEnabledProperty(Long id, boolean enabled) {
        JenkinsConfig config = configRepository.findById(id)
                                               .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.JENKINS_CONFIG_NOT_FOUND_BY_ID));
        config.setEnabled(enabled);
        configRepository.save(config);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (!configRepository.existsById(id)) {
            throw new ResourceNotFoundException(JENKINS_CONFIG_NOT_FOUND_BY_ID, id);
        }
        configRepository.deleteById(id);
    }

}
