package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.service.TestSessionService;
import com.zebrunner.reporting.service.util.CapabilitiesManager;
import com.zebrunner.reporting.service.util.StreamUtils;
import com.zebrunner.reporting.web.request.v1.testsession.StartTestSessionRequest;
import com.zebrunner.reporting.web.request.v1.testsession.UpdateTestSessionRequest;
import com.zebrunner.reporting.web.response.v1.testsession.ListTestSessionsResponse;
import com.zebrunner.reporting.web.response.v1.testsession.ListTestSessionsResponse.Item;
import com.zebrunner.reporting.web.response.v1.testsession.SaveTestSessionResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/test-runs/{testRunId}/test-sessions", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestSessionController {

    private final JMapper jMapper;
    private final TestSessionService testSessionService;
    private final CapabilitiesManager capabilitiesManager;

    @GetMapping
    public ListTestSessionsResponse getByTestRunId(@PathVariable("testRunId") Long testRunId) {
        List<TestSession> testSessions = testSessionService.retrieveByTestRunId(testRunId);
        Map<Long, String> sessionIdToName = resolveSessionNames(testSessions);

        List<Item> responseItems = jMapper.mapToList(testSessions, Item.class);
        responseItems.forEach(item -> item.setName(sessionIdToName.get(item.getId())));

        return new ListTestSessionsResponse(responseItems);
    }

    @GetMapping(params = "testId")
    public ListTestSessionsResponse getByTestRunIdAndTestId(@PathVariable("testRunId") Long testRunId,
                                                            @RequestParam("testId") Long testId) {
        List<TestSession> testSessions = testSessionService.retrieveByTestRunIdAndTestId(testRunId, testId);
        Map<Long, String> sessionIdToName = resolveSessionNames(testSessions);

        List<Item> responseItems = jMapper.mapToList(testSessions, Item.class);
        responseItems.forEach(item -> item.setName(sessionIdToName.get(item.getId())));

        return new ListTestSessionsResponse(responseItems);
    }

    private Map<Long, String> resolveSessionNames(Collection<TestSession> testSessions) {
        Map<Long, String> testSessionIdToName = new HashMap<>();
        testSessions.forEach(testSession -> testSessionIdToName.put(testSession.getId(), capabilitiesManager.getSessionName(testSession)
                                                                                                            .orElseGet(testSession::getSessionId)));
        return testSessionIdToName;
    }

    @PostMapping
    public SaveTestSessionResponse start(@PathVariable("testRunId") @Positive Long testRunId,
                                         @Valid @RequestBody StartTestSessionRequest request) {
        TestSession session = jMapper.map(request, TestSession.class);
        session.setTestRunId(testRunId);

        session = testSessionService.create(session, request.getTestIds());

        SaveTestSessionResponse response = jMapper.map(session, SaveTestSessionResponse.class);
        response.setTestIds(StreamUtils.mapToSet(session.getTests(), Test::getId));

        return response;
    }

    @PutMapping("/{testSessionId}")
    public SaveTestSessionResponse update(@PathVariable("testRunId") @Positive Long testRunId,
                                          @PathVariable("testSessionId") @Positive Long testSessionId,
                                          @Valid @RequestBody UpdateTestSessionRequest request) {
        TestSession session = jMapper.map(request, TestSession.class);
        session.setTestRunId(testRunId);
        session.setId(testSessionId);

        session = testSessionService.update(session, request.getTestIds());

        SaveTestSessionResponse response = jMapper.map(session, SaveTestSessionResponse.class);
        response.setTestIds(StreamUtils.mapToSet(session.getTests(), Test::getId));
        return response;
    }

}
