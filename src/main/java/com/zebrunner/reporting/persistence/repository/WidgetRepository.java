package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Widget;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WidgetRepository extends JpaRepository<Widget, Long> {

    Optional<Widget> findByTitleAndWidgetTemplateId(String title, Long templateId);

}
