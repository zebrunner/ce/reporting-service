package com.zebrunner.reporting.web.response.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.reporting.domain.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@JGlobalMap(excluded = {"isFavorite"})
public class TestRunFilterResponse {

    private Integer id;
    private String name;
    private Map<String, List<String>> items;
    private String owner;
    private Boolean isPrivate;
    private Boolean isFavorite = false;
    private Instant createdAt;

    @JMapConversion(from = {"owner"}, to = {"owner"})
    public String convertOwner(User user) {
        return user.getUsername();
    }

}
