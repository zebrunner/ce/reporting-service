package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.TestInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TestInfoMapper {

    List<TestInfo> findByLabelKeyAndTestRunCiRunId(@Param("testRunCiRunId") String testRunCiRunId, @Param("labelKey") String labelKey);

}
