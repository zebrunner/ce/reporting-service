package com.zebrunner.reporting.web.response.v1.testsession;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.TestSession;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Set;

@Data
@NoArgsConstructor
@JGlobalMap(excluded = "testIds")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveTestSessionResponse {

    private Long id;
    private String sessionId;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant initiatedAt;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant startedAt;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant endedAt;
    private TestSession.Status status;
    private JsonNode capabilities;
    private JsonNode desiredCapabilities;
    private Set<Long> testIds;

}
