package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestRunFilter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TestRunFilterRepository extends JpaRepository<TestRunFilter, Integer> {

    List<TestRunFilter> findByOwnerId(Long ownerId);

    List<TestRunFilter> findAllByIsPrivateIsFalse();

}
