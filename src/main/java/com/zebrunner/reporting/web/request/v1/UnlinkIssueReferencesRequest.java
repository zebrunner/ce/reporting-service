package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@JGlobalMap
@NoArgsConstructor
public class UnlinkIssueReferencesRequest {

    @NotNull
    private Long testId;
    @NotNull
    private Long issueReferenceId;

}
