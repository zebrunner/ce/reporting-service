package com.zebrunner.reporting.domain.dto.widget;

import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Getter
@Setter
public class WidgetTemplateDTO {

    private Long id;
    private String name;
    private String description;

    @NotNull(message = "{error.type.required}")
    private WidgetTemplate.Type type;

    private String chartConfig;
    private Map<String, WidgetTemplateParameter> paramsConfig;
    private String legendConfig;
    private Boolean hidden;

}
