package com.zebrunner.reporting.service.cache.impl;

import com.zebrunner.reporting.domain.db.TestCase;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestCaseMapper;
import com.zebrunner.reporting.service.cache.TestCaseCache;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestCaseCacheImpl implements TestCaseCache {

    private static final String TEST_CASE_CACHE_NAME = "testCases";

    private final TestCaseMapper testCaseMapper;

    @Override
    @Cacheable(
            cacheNames = TEST_CASE_CACHE_NAME,
            unless = "#result == null",
            key = "{ T( com.zebrunner.reporting.persistence.PersistenceConfig).DEFAULT_SCHEMA, #testClass, #testMethod }"
    )
    public Optional<TestCase> getByTestClassAndTestMethod(String testClass, String testMethod) {
        return testCaseMapper.findByTestClassAndTestMethod(testClass, testMethod);
    }

}
