package com.zebrunner.reporting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ApplicationInitializer {

    //Set environment variable spring.profiles.active=dev for local development
    public static void main(String[] args) {
        SpringApplication.run(ApplicationInitializer.class, args);
    }

}
