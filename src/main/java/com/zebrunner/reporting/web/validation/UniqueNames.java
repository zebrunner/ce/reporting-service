package com.zebrunner.reporting.web.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = UniqueArtifactReferenceNameValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueNames {

    String message() default "Collection should contain unique names";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
