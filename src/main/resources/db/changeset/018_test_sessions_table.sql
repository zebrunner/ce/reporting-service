DROP TABLE IF EXISTS test_sessions_transient CASCADE;
CREATE TABLE IF NOT EXISTS test_sessions_transient (
  id BIGSERIAL NOT NULL,
  test_run_id BIGINT NULL,
  session_id VARCHAR(127) NOT NULL,
  started_at TIMESTAMP NOT NULL,
  ended_at TIMESTAMP NULL,
  browser_name VARCHAR(127) NULL,
  browser_version VARCHAR(127) NULL,
  platform_name VARCHAR(127) NULL,
  capabilities jsonb NOT NULL,
  desired_capabilities jsonb NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (test_run_id) REFERENCES test_runs (id) ON DELETE CASCADE,
  UNIQUE (session_id)
);
CREATE INDEX test_sessions_test_run_id ON test_sessions_transient (test_run_id);
CREATE TRIGGER update_timestamp_test_sessions
  BEFORE INSERT OR UPDATE
  ON test_sessions_transient
  FOR EACH ROW
EXECUTE PROCEDURE update_modified_at();


INSERT INTO test_sessions_transient (test_run_id, session_id, started_at, ended_at, browser_name, browser_version,
                                     platform_name, capabilities,
                                     desired_capabilities)
SELECT NULL,
       session_id,
       started_at,
       ended_at,
       browser_name,
       version,
       os_name,
       CAST('{ "caps": { "browserName": "' || COALESCE(browser_name, 'N\A')
              || '", "browserVersion": "' || COALESCE(version, 'N\A')
              || '", "platformName": "' || COALESCE(os_name, 'N\A')
              || '", "name": "' || COALESCE(test_name, 'Default name') || '" } }' AS json),
       CAST('{ "caps": { "browserName": "' || COALESCE(browser_name, 'N\A')
              || '", "browserVersion": "' || COALESCE(version, 'N\A')
              || '", "platformName": "' || COALESCE(os_name, 'N\A')
              || '", "name": "' || COALESCE(test_name, 'Default name') || '" } }' AS json)

FROM test_sessions;


DROP TABLE IF EXISTS test_test_sessions_transient CASCADE;
CREATE TABLE IF NOT EXISTS test_test_sessions_transient (
  id BIGSERIAL NOT NULL,
  test_id BIGINT NOT NULL,
  test_session_id BIGINT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id),
  FOREIGN KEY (test_id) REFERENCES tests (id) ON DELETE CASCADE,
  FOREIGN KEY (test_session_id) REFERENCES test_sessions_transient (id) ON DELETE CASCADE
);
