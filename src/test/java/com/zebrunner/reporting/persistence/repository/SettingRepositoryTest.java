package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Setting;
import com.zebrunner.reporting.persistence.H2Config;
import java.util.UUID;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {H2Config.class})
@ActiveProfiles("Test")
@Transactional
public class SettingRepositoryTest extends JpaRepositoryGenericTest<SettingsRepository, Setting> {

    @ParameterizedTest
    @MethodSource("TestDataProvider#provideSettings")
    public void update_ShouldUpdateSetting_WhenSettingNotUpdated(Setting setting) {
        String newName = "newSettingName" + UUID.randomUUID().getLeastSignificantBits();
        setting.setName(newName);
        repository.save(setting);

        Setting savedSetting = repository.findByName(newName).orElseThrow();
        Assertions.assertNotNull(savedSetting);
        Assertions.assertEquals(savedSetting.getName(), newName);
        repository.delete(savedSetting);
    }

    @ParameterizedTest
    @ValueSource(strings = {"TestSetting1001", "TestSetting1002", "TestSetting1003", "TestSetting1004"})
    @SqlGroup({
            @Sql(value = "/db/createSettings.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(value = "/db/deleteSettings.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    })
    public void findByName_ShouldFindSetting_WhenSettingWithSuchNameExists(String name) {
        Setting setting = repository.findByName(name).get();
        Assertions.assertNotNull(setting);
        Assertions.assertEquals(setting.getName(), name);
    }
}
