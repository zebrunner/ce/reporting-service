package com.zebrunner.reporting.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.domain.entity.Test;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RerunCriteria {

    private Set<Test.Status> anyOfStatuses;
    private Boolean knownIssue;

}
