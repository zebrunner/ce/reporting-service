package com.zebrunner.reporting.service.integration.slack.utils;

import com.slack.api.model.Attachment;
import com.slack.api.model.Field;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import org.apache.commons.lang.StringUtils;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class AttachmentBuilder {

    private static final String TEST_RUN_SHORT_SUMMARY_FORMAT = "${metadata}\n${goToLine}";

    private static final String GO_TO_LINE_FORMAT = "<${testRunUrl}|View in Zebrunner>";

    private static final String TEST_RESULTS_FORMAT = "Passed: %d, Failed: %d, Known Issues: %d, Skipped: %d, Aborted: %d";

    private final static Map<String, String> STATUS_TO_COLOUR_CODE = Map.of(
            "PASSED", "good",
            "FAILED", "danger",
            "SKIPPED", "warning",
            "ABORTED", "F5F5F5"
    );

    public static Attachment build(String zebrunnerUrl, TestRunResultSummary summary) {
        String testResultsText = buildTestResultsText(summary);
        List<Field> fields = new ArrayList<>();
        Field testResultsFiled = Field.builder().title("Test Results").value(testResultsText).build();
        fields.add(testResultsFiled);

        if (summary.getComment() != null) {
            Field comment = Field.builder().title("Comment").value(summary.getComment()).build();
            fields.add(comment);
            Field sender = Field.builder().title("Sent by").value(summary.getSenderUsername()).build();
            fields.add(sender);
        }

        return Attachment.builder()
                         .pretext(buildSummaryText(zebrunnerUrl, summary))
                         .color(STATUS_TO_COLOUR_CODE.get(summary.getStatus()))
                         .fields(fields)
                         .build();
    }

    private static String buildSummaryText(String zebrunnerUrl, TestRunResultSummary summary) {
        String metadata = buildMetaData(summary);

        String testRunUrl = zebrunnerUrl + "/test-runs/" + summary.getTestRunId();
        String goToLine = GO_TO_LINE_FORMAT.replace("${testRunUrl}", testRunUrl);

        return TEST_RUN_SHORT_SUMMARY_FORMAT.replace("${metadata}", metadata)
                                            .replace("${goToLine}", goToLine);
    }

    private static String buildMetaData(TestRunResultSummary summary) {
        Map<String, String> summaryItems = new LinkedHashMap<>();
        summaryItems.put("duration", LocalTime.ofSecondOfDay(summary.getDuration()).toString());
        summaryItems.put("env", summary.getEnvironment());

        String platform = StringUtils.isNotBlank(summary.getPlatformVersion())
                ? summary.getPlatform() + " " + summary.getPlatformVersion()
                : summary.getPlatform();
        summaryItems.put("platform", platform);

        String browser = StringUtils.isNotBlank(summary.getBrowserVersion())
                ? summary.getBrowser() + " " + summary.getBrowserVersion()
                : summary.getBrowser();
        summaryItems.put("browser", browser);

        summaryItems.put("build", summary.getBuild());
        summaryItems.put("locale", summary.getLocale());

        return summaryItems.entrySet()
                           .stream()
                           .filter(entry -> StringUtils.isNotBlank(entry.getValue()))
                           .map(entry -> entry.getKey() + " : " + entry.getValue())
                           .collect(Collectors.joining(" | "));
    }

    private static String buildTestResultsText(TestRunResultSummary summary) {
        return String.format(
                TEST_RESULTS_FORMAT,
                summary.getPassedAmount(),
                summary.getFailedAmount(),
                summary.getKnownIssuesAmount(),
                summary.getSkippedAmount(),
                summary.getAbortedAmount()
        );
    }

}
