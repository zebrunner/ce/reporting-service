package com.zebrunner.reporting.messaging.websocket;

import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.domain.push.TestPush;
import com.zebrunner.reporting.domain.push.TestRunPush;
import com.zebrunner.reporting.domain.push.TestRunStatisticPush;
import com.zebrunner.reporting.service.TestRunService;
import com.zebrunner.reporting.service.TestRunStatisticsService;
import com.zebrunner.reporting.service.TestService;
import com.zebrunner.reporting.service.ci.CiServerFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class WebsocketNotificationSender {

    private final TestService testService;
    @Lazy
    private final CiServerFacade ciServerFacade;
    private final TestRunService testRunService;
    private final SimpMessagingTemplate messagingTemplate;
    private final TestRunStatisticsService statisticsService;

    public void notifyAboutTestRun(Long testRunId) {
        TestRun testRun = testRunService.getTestRunByIdFull(testRunId);

        testRunService.hideJobUrlsIfNeed(List.of(testRun));
        notifyAboutTestRunStatistics(testRun.getId());

        if (testRun.getJob() == null) {
            ciServerFacade.populateLegacyJobsAndBuildNumbers(List.of(testRun));
        }
        messagingTemplate.convertAndSend(Topic.testRuns(), new TestRunPush(testRun));
    }

    public void notifyAboutTest(Long testId) {
        Test test = testService.getTestById(testId);
        notifyAboutTestRunStatistics(test.getTestRunId());
        messagingTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test));
    }

    public void notifyAboutTestRunStatistics(Long testRunId) {
        TestRunStatistics testRunStatistics = statisticsService.get(testRunId);
        TestRunStatisticPush testRunStatisticPush = new TestRunStatisticPush(testRunStatistics);
        messagingTemplate.convertAndSend(Topic.testRunStatistics(), testRunStatisticPush);
    }

}
