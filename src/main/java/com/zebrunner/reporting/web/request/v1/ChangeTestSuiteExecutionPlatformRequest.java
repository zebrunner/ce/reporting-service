package com.zebrunner.reporting.web.request.v1;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChangeTestSuiteExecutionPlatformRequest {

    private String name;
    private String version;

}
