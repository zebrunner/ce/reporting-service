package com.zebrunner.reporting.web.request.v1.testsession;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.web.request.v1.testsession.validation.RunningSessionMustHaveCapabilities;
import com.zebrunner.reporting.web.request.v1.testsession.validation.RunningSessionMustHaveSessionId;
import com.zebrunner.reporting.web.request.v1.testsession.validation.RunningSessionMustHaveStartedAt;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@JGlobalMap(excluded = "testIds")
@RunningSessionMustHaveSessionId
@RunningSessionMustHaveStartedAt
@RunningSessionMustHaveCapabilities
public class StartTestSessionRequest {

    private String sessionId;

    @NotNull
    @PastOrPresent
    private Instant initiatedAt;
    @PastOrPresent
    private Instant startedAt;
    @NotNull
    private TestSession.Status status = TestSession.Status.RUNNING;

    @NotNull
    private JsonNode capabilities = new ObjectNode(JsonNodeFactory.instance);
    @NotNull
    private JsonNode desiredCapabilities;

    private Set<Long> testIds = new HashSet<>();

    public void setStartedAt(Instant startedAt) {
        this.startedAt = startedAt;
        if (this.initiatedAt == null) {
            this.initiatedAt = startedAt;
        }
    }

    public void setStatus(TestSession.Status status) {
        if (status != null) {
            this.status = status;
        }
    }

}
