package com.zebrunner.reporting.domain.entity.integration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "integration_jenkins_configs")
public class JenkinsConfig {

    @Id
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String url;
    private String username;
    private String token;
    private String launcherJobName;
    private boolean urlHidden;

    @EqualsAndHashCode.Exclude
    private boolean encrypted;

    @EqualsAndHashCode.Exclude
    private boolean enabled;

}
