package com.zebrunner.reporting.messaging.listener;

import com.zebrunner.reporting.domain.push.events.TestSessionArtifactUploadedMessage;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.messaging.config.Queue;
import com.zebrunner.reporting.service.TestSessionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Set;

@Slf4j
//@Component
@RequiredArgsConstructor
public class TestSessionArtifactListener {

    private final TestSessionService testSessionService;

//    @RabbitListener(queues = Queue.SAVE_TEST_SESSION_ARTIFACT_REFERENCE)
    public void saveTestSessionArtifact(TestSessionArtifactUploadedMessage message) {
        try {
            ArtifactReference reference = ArtifactReference.builder()
                                                           .name(message.getName())
                                                           .value(message.getKey())
                                                           .build();
            testSessionService.attachArtifactReferences(message.getTestSessionId(), Set.of(reference));
        } catch (Exception e) {
            log.error("Could not save artifact reference to test session with id '{}': ", message.getTestSessionId(), e);
        }
    }

}
