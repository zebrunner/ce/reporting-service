package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.MalformedInputException;
import com.zebrunner.common.eh.exception.OperationNotImplementedException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestConfig;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.db.TestSuite;
import com.zebrunner.reporting.domain.dto.BuildParameterType;
import com.zebrunner.reporting.domain.dto.CommentType;
import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestRunMapper;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.SearchResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestRunSearchCriteria;
import com.zebrunner.reporting.persistence.repository.TestRunRepository;
import com.zebrunner.reporting.service.ci.CiServerFacade;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.MalformedInputError;
import com.zebrunner.reporting.service.exception.OperationNotImplementedError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.integration.jenkins.JenkinsConfigService;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.testsuite.TestSuiteReassignmentListener;
import com.zebrunner.reporting.service.util.DateTimeUtil;
import com.zebrunner.reporting.service.util.FreemarkerUtil;
import com.zebrunner.reporting.service.util.URLResolver;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestRunService implements TestSuiteReassignmentListener {

    private static final String DEFAULT_PLATFORM = "API";
    private static final String UNDEFINED_FAILURE_COMMENT = "undefined failure";
    private static final Set<Status> REVIEW_FORBIDDEN_STATUSES = Set.of(Status.IN_PROGRESS);

    private final TestRunMapper testRunMapper;
    private final FreemarkerUtil freemarker;
    private final EmailService emailService;
    private final URLResolver urlResolver;
    private final JiraConfigService jiraConfigService;
    private final JenkinsConfigService jenkinsConfigService;

    @Lazy
    private final TestSuiteService testSuiteService;
    @Lazy
    private final TestConfigService testConfigService;
    @Lazy
    private final TestService testService;
    @Lazy
    private final CiServerFacade ciServerFacade;

    private final TestRunRepository testRunRepository;

    @Getter
    @RequiredArgsConstructor
    public enum FailureCause {
        UNRECOGNIZED_FAILURE("UNRECOGNIZED FAILURE"),
        COMPILATION_FAILURE("COMPILATION FAILURE"),
        BUILD_FAILURE("BUILD FAILURE");

        private final String cause;

    }

    public TestRun createTestRun(TestRun testRun) {
        testRunMapper.create(testRun);
        return testRun;
    }

    @Transactional(readOnly = true)
    public TestRun getTestRunById(Long id) {
        return testRunMapper.findById(id)
                            .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_ID, id));
    }

    @Transactional(readOnly = true)
    public SearchResult<TestRun> search(TestRunSearchCriteria sc) {
        return searchTestRuns(sc);
    }

    public SearchResult<TestRun> searchTestRuns(TestRunSearchCriteria sc) {
        DateTimeUtil.actualizeSearchCriteriaDate(sc);

        List<TestRun> testRuns = testRunMapper.search(sc);

        testRuns.forEach(testRun -> {
            Set<ArtifactReference> sortedReferences = new TreeSet<>(Comparator.comparing((ArtifactReference reference) -> reference.getName().toLowerCase(Locale.ROOT))
                                                                              .thenComparing(ArtifactReference::getValue, Comparator.nullsLast(Comparator.naturalOrder())));
            sortedReferences.addAll(testRun.getArtifactReferences());
            testRun.setArtifactReferences(sortedReferences);
        });

        int count = testRunMapper.findSearchCount(sc);

        ciServerFacade.populateLegacyJobsAndBuildNumbers(testRuns);
        hideJobUrlsIfNeed(testRuns);

        return SearchResult.<TestRun>builder()
                           .page(sc.getPage())
                           .pageSize(sc.getPageSize())
                           .sortOrder(sc.getSortOrder())
                           .results(testRuns)
                           .totalResults(count)
                           .build();
    }

    public TestRun getTestRunByCiRunId(String ciRunId) {
        return testRunMapper.findByCiRunId(ciRunId)
                            .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_CI_RUN_ID, ciRunId));
    }

    @Transactional(readOnly = true)
    public TestRun getTestRunByIdFull(long id) {
        return testRunMapper.findByIdFull(id)
                            .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_ID, id));
    }

    private TestRun getTestRunByIdFull(String id) {
        return id.matches("\\d+") ? getTestRunByIdFull(Long.parseLong(id)) : getTestRunByCiRunIdFull(id);
    }

    public TestRun getTestRunByCiRunIdFull(String ciRunId) {
        return testRunMapper.findByCiRunIdFull(ciRunId)
                            .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_CI_RUN_ID, ciRunId));
    }

    @Transactional
    public void updateTestRunConfig(TestRun testRun, String configXML) {
        TestRun existingTestRun = getTestRunById(testRun.getId());
        existingTestRun.setConfig(getTestRunConfig(existingTestRun, configXML));
        update(existingTestRun);
    }

    @Transactional
    public TestRun update(TestRun testRun) {
        testRunMapper.update(testRun);
        return testRun;
    }

    @Transactional
    public TestRun startTestRun(TestRun testRun, String configXML) {
        String ciRunId = testRun.getCiRunId();
        boolean isNew = true;

        if (StringUtils.isNotBlank(ciRunId)) {
            isNew = !testRunRepository.existsByCiRunId(ciRunId);
        } else {
            ciRunId = UUID.randomUUID().toString();
        }

        if (isNew) {
            TestSuite testSuite = testSuiteService.getTestSuiteById(testRun.getTestSuite().getId());
            testRun.setName(testSuite.getName());
            testRun.setCiRunId(ciRunId);
            processStartedBy(testRun);
            setStartTestRunData(testRun, configXML);
            createTestRun(testRun);
        } else {
            TestRun existingTestRun = getTestRunByCiRunId(ciRunId);
            existingTestRun.setBuildNumber(testRun.getBuildNumber());
            existingTestRun.setTestSuite(testRun.getTestSuite());
            existingTestRun.setStartedAt(testRun.getStartedAt());
            testRun = existingTestRun;
            // TODO: investigate if startedBy should be also copied
            setStartTestRunData(testRun, configXML);
            update(testRun);
        }
        return getTestRunByIdFull(testRun.getId());
    }

    private void setStartTestRunData(TestRun testRun, String configXML) {
        // Initialize starting time
        if (testRun.getStartedAt() == null) {
            testRun.setStartedAt(Calendar.getInstance().getTime());
        }
        testRun.setConfig(getTestRunConfig(testRun, configXML));
        testRun.setReviewed(false);
        testRun.setStatus(Status.IN_PROGRESS);
    }

    private void processStartedBy(TestRun testRun) {
        switch (testRun.getStartedBy()) {
            case HUMAN:
                if (testRun.getUser() == null) {
                    throw new OperationNotImplementedException(OperationNotImplementedError.INVALID_TEST_RUN_INITIATED_BY_HUMAN);
                }
                break;
            case SCHEDULER:
                testRun.setUpstreamJobBuildNumber(null);
                testRun.setUpstreamJob(null);
                testRun.setUser(null);
                break;
            case UPSTREAM_JOB:
                if (testRun.getUpstreamJob() == null || testRun.getUpstreamJobBuildNumber() == null) {
                    throw new OperationNotImplementedException(OperationNotImplementedError.INVALID_TEST_RUN_INITIATED_BY_UPSTREAM_JOB);
                }
                break;
        }
    }

    public TestConfig getTestRunConfig(TestRun testRun, String configXML) {
        TestConfig config = null;
        if (StringUtils.isNotBlank(configXML)) {
            config = testConfigService.createTestConfigForTestRun(configXML);

            testRun.setEnvironment(config.getEnv());
            testRun.setBuild(config.getAppVersion());
        } else if (testRun.getConfig() != null) {
            config = testConfigService.getOrCreate(testRun.getConfig());
        }
        return config;
    }

    @Transactional
    public TestRun abortTestRun(Long testRunId, String ciRunId, CommentType abortCause) {
        if (testRunId == null && ciRunId == null) {
            throw new MalformedInputException(MalformedInputError.NO_TEST_RUN_REF_SUPPLIED);
        }

        TestRun testRun = testRunId != null ? getTestRunById(testRunId) : getTestRunByCiRunId(ciRunId);

        Status status = testRun.getStatus();
        if (Status.IN_PROGRESS.equals(status)) {
            boolean commentExists = abortCause != null && abortCause.getComment() != null;
            String abortCauseDecoded = commentExists
                    ? URLDecoder.decode(abortCause.getComment(), StandardCharsets.UTF_8)
                    : null;

            boolean validStatusToAbort = hasValidTestRunStatusToAbort(status, abortCauseDecoded);
            if (validStatusToAbort) {
                List<Test> tests = testService.getTestsByTestRunId(testRun.getId());
                tests.stream()
                     .filter(test -> Status.IN_PROGRESS.equals(test.getStatus()))
                     .forEach(test -> testService.abortTest(test, abortCauseDecoded));
            }

            testRun.setComments(abortCauseDecoded);
            testRun.setStatus(Status.ABORTED);
            testRun = update(testRun);
            finishRun(testRun.getId());
        }

        // TODO by nsidorevich on 1/25/21: else throw exception?
        return testRun;
    }

    @Transactional(readOnly = true)
    public void abortTestRunJob(Long testRunId, String ciRunId) {
        if (testRunId == null && ciRunId == null) {
            throw new MalformedInputException(MalformedInputError.NO_TEST_RUN_REF_SUPPLIED);
        }
        ciServerFacade.abort(testRunId);
    }

    public void buildTestRunJob(Long id, Map<String, String> jobParameters, Long userId) {
        TestRun testRun = getTestRunByIdFull(id);
        ciServerFacade.build(testRun.getId(), jobParameters, userId);
    }

    @Transactional(readOnly = true)
    public List<BuildParameterType> getTestRunJobParameters(Long id) {
        TestRun testRun = getTestRunByIdFull(id);
        return ciServerFacade.retrieveLegacyBuildParameters(testRun.getId());
    }

    @Transactional
    public void rerunTestRun(Long id, boolean rerunFailures, Long userId) {
        TestRun testRun = getTestRunByIdFull(id);
        if (Status.PASSED.equals(testRun.getStatus()) && rerunFailures) {
            throw new MalformedInputException(MalformedInputError.TEST_RUN_UNABLE_TO_RERUN_PASSED, testRun.getId());
        }
        testRun.setComments(null);
        testRun.setReviewed(false);
        update(testRun);
        ciServerFacade.rerun(testRun.getId(), rerunFailures, userId);
    }

    private boolean hasValidTestRunStatusToAbort(Status status, String abortCause) {
        return Status.IN_PROGRESS.equals(status) || isBuildFailure(abortCause);
    }

    private boolean isBuildFailure(String comments) {
        boolean isCommentExists = StringUtils.isNotBlank(comments);
        boolean isCommentContainsFailure = comments.contains(FailureCause.BUILD_FAILURE.getCause()) ||
                comments.contains(FailureCause.COMPILATION_FAILURE.getCause()) ||
                comments.contains(FailureCause.UNRECOGNIZED_FAILURE.getCause());
        return isCommentExists && isCommentContainsFailure;
    }

    @Transactional
    public TestRun markAsReviewed(Long id, String comment) {
        TestRun testRun = getTestRunById(id);

        if (REVIEW_FORBIDDEN_STATUSES.contains(testRun.getStatus())) {
            throw new BusinessConstraintException(BusinessConstraintError.TEST_RUN_WITH_STATUS_CANNOT_BE_REVIEWED, testRun.getStatus());
        }

        testRun.setComments(comment);

        boolean undefinedFailureComment = UNDEFINED_FAILURE_COMMENT.equalsIgnoreCase(comment);
        if (!undefinedFailureComment) {
            testRun.setReviewed(true);
        }

        testRun = update(testRun);
        return testRun;
    }

    public TestRun finishRun(long id) {
        TestRun testRun = getTestRunById(id);
        testRun.setEndedAt(new Date());
        return calculateTestRunResult(testRun, true);
    }

    @Transactional
    public TestRun calculateTestRunResult(long id) {
        TestRun testRun = getTestRunById(id);
        return calculateTestRunResult(testRun, false);
    }

    @Transactional
    public TestRun calculateTestRunResult(TestRun testRun, boolean finishTestRun) {
        List<Test> tests = testService.getTestsByTestRunId(testRun.getId());

        // Aborted testruns don't need status recalculation (already recalculated on abort end-point)
        if (!Status.ABORTED.equals(testRun.getStatus())) {
            // Do not update test run status if tests are running and one clicks mark as passed or mark as known issue
            // (https://github.com/zebrunner/zebrunner/issues/34)
            boolean onTestRunFinish = finishTestRun || !Status.IN_PROGRESS.equals(testRun.getStatus());
            if (onTestRunFinish) {
                tests.stream()
                     .filter(test -> Status.IN_PROGRESS.equals(test.getStatus()))
                     .forEach(testService::skipTest);

                Status status = tests.size() > 0 ? Status.PASSED : Status.SKIPPED;
                testRun.setStatus(status);
                testRun.setKnownIssue(false);
                setTestRunFailureStatus(testRun, tests);
            }
        }
        if (finishTestRun && testRun.getStartedAt() != null) {
            setTestRunElapsedTime(testRun);
        }

        setDefaultPlatform(testRun);
        testRun = update(testRun);
        testService.updateTestRerunFlags(tests);
        return testRun;
    }

    private void setDefaultPlatform(TestRun testRun) {
        if (testRun.getConfig() == null) {
            testRun.setConfig(new TestConfig());
        }

        if (testRun.getConfig().getPlatform() == null && testRun.getConfig().getBrowser() == null) {
            testRun.getConfig().setPlatform(DEFAULT_PLATFORM);

            TestConfig createdTestConfig = testConfigService.getOrCreate(testRun.getConfig());
            testRun.setConfig(createdTestConfig);
        }
    }

    private void setTestRunElapsedTime(TestRun testRun) {
        Integer elapsed = ((Long) DateTimeUtil.toSecondsSinceDateToNow(testRun.getStartedAt())).intValue();
        // according to https://github.com/zebrunner/zebrunner/issues/748
        Integer elapsedToInsert = testRun.getElapsed() != null ? testRun.getElapsed() + elapsed : elapsed;
        testRun.setElapsed(elapsedToInsert);
    }

    private void setTestRunFailureStatus(TestRun testRun, List<Test> tests) {
        boolean hasTestRunKnowIssues = tests.stream().anyMatch(Test::isKnownIssue);
        testRun.setKnownIssue(hasTestRunKnowIssues);

        tests.stream()
             .filter(test -> test.isLogicallyFailed() && !test.isKnownIssue())
             .findFirst()
             .ifPresent(test -> testRun.setStatus(Status.FAILED));
    }

    @Transactional(readOnly = true)
    public void shareResultsViaEmail(String testRunId, boolean showStacktrace, Set<String> toEmails) {
        if (!toEmails.isEmpty()) {
            TestRun testRun = getTestRunByIdFull(testRunId);
            List<Test> tests = testService.getTestsByTestRunId(testRunId);
            Map<String, Object> templateModel = collectDataForExport(testRun, tests, showStacktrace);
            emailService.sendTestRunResultsEmail(testRun, templateModel, toEmails);
        }
    }

    @Transactional(readOnly = true)
    public void sendTestRunResultsEmailFailure(String id, boolean toSuiteOwner, boolean toSuiteRunner, Set<String> toEmails) {
        if (toSuiteOwner) {
            Long testSuiteId = getTestRunByCiRunIdFull(id).getTestSuite().getId();
            TestSuite testSuite = testSuiteService.getTestSuiteById(testSuiteId);
            String suiteOwnerEmail = testSuite.getUser().getEmail();
            toEmails.add(suiteOwnerEmail);
        }
        if (toSuiteRunner) {
            String suiteRunnerEmail = getTestRunByCiRunIdFull(id).getUser().getEmail();
            toEmails.add(suiteRunnerEmail);
        }

        shareResultsViaEmail(id, true, toEmails);
    }

    /**
     * Generates a string with test run html report
     *
     * @param testRunIdOrCiRunId - test run testRunIdOrCiRunId or test run ciRunId to find
     * @return built test run report or null if test run is not found
     */
    @Transactional(readOnly = true)
    public String compileResultsReportAsHtmlString(String testRunIdOrCiRunId) {
        TestRun testRun = getTestRunByIdFull(testRunIdOrCiRunId);
        List<Test> tests = testService.getTestsByTestRunId(testRunIdOrCiRunId);
        Map<String, Object> templateData = collectDataForExport(testRun, tests, true);
        return freemarker.buildContentFromTemplate("test_run_results.ftl", templateData);
    }

    private Map<String, Object> collectDataForExport(TestRun testRun, List<Test> tests, boolean showStacktrace) {
        String elapsed = testRun.getElapsed() != null ? LocalTime.ofSecondOfDay(testRun.getElapsed()).toString() : null;

        Map<String, Object> data = new HashMap<>();
        data.put("testRun", testRun);
        data.put("tests", tests);
        data.put("workspaceURL", urlResolver.buildWebURL());
        data.put("jiraURL", getJiraUrl());
        data.put("showOnlyFailures", false);
        data.put("showStacktrace", showStacktrace);
        data.put("showJenkinsUrl", showJenkinsUrl(testRun));
        data.put("successRate", calculateSuccessRate(testRun));
        data.put("elapsed", elapsed);

        return data;
    }

    private String getJiraUrl() {
        return jiraConfigService.retrieveOptional()
                                .filter(config -> jiraConfigService.isReachable(config).getReachable())
                                .map(JiraConfig::getUrl)
                                .orElse("");
    }

    private boolean showJenkinsUrl(TestRun testRun) {
        if (testRun.getJob() == null || testRun.getJob().getJenkinsConfigId() == null) {
            return false;
        }
        Optional<JenkinsConfig> maybeConfig = jenkinsConfigService.retrieveOptionalById(testRun.getJob().getJenkinsConfigId());
        return maybeConfig.isEmpty() || maybeConfig.get().isUrlHidden();
    }

    public static int calculateSuccessRate(TestRun testRun) {
        int total = testRun.getPassed() + testRun.getFailed() + testRun.getSkipped();
        double rate = (double) testRun.getPassed() / (double) total;
        return total > 0 ? (new BigDecimal(rate).setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)))
                .intValue() : 0;
    }

    public void hideJobUrlsIfNeed(List<TestRun> testRuns) {
        testRuns.stream()
                .filter(testRun -> testRun.getJob() != null)
                .filter(testRun -> !showJenkinsUrl(testRun))
                .forEach(testRun -> testRun.getJob().setJobURL(null));
    }

    @Override
    @Transactional
    public void onTestSuiteReassignment(Long fromId, Long toId) {
        testRunMapper.reassignTestSuite(fromId, toId);
    }

    public void updateLocale(Long id, String locale) {
        TestRun testRun = getTestRunById(id);

        TestConfig config = testRun.getConfig();
        config.setLocale(locale);

        testConfigService.updateTestRunConfig(id, config);
    }

}
