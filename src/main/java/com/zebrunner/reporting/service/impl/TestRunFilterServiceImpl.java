package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.TestRunFilter;
import com.zebrunner.reporting.domain.entity.User;
import com.zebrunner.reporting.domain.entity.UserFavoriteTestRunFilter;
import com.zebrunner.reporting.persistence.repository.TestRunFilterRepository;
import com.zebrunner.reporting.persistence.repository.UserFavoriteTestRunFilterRepository;
import com.zebrunner.reporting.persistence.repository.UserRepository;
import com.zebrunner.reporting.service.TestRunFilterService;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class TestRunFilterServiceImpl implements TestRunFilterService {

    private final TestRunFilterRepository testRunFilterRepository;
    private final UserRepository userRepository;
    private final UserFavoriteTestRunFilterRepository userFavoriteTestRunFilterRepository;

    @Override
    public List<TestRunFilter> retrieveByUserId(Long userId) {
        List<TestRunFilter> publicFilters = testRunFilterRepository.findAllByIsPrivateIsFalse()
                                                                   .stream()
                                                                   .filter(filter -> !userId.equals(filter.getOwner().getId()))
                                                                   .collect(Collectors.toList());
        List<TestRunFilter> userFilters = testRunFilterRepository.findByOwnerId(userId);
        return Stream.concat(publicFilters.stream(), userFilters.stream())
                     .collect(Collectors.toList());
    }

    @Override
    public TestRunFilter retrieveById(Integer id) {
        return testRunFilterRepository.findById(id)
                                      .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_FILTER_NOT_FOUND_BY_ID, id));
    }

    @Override
    @Transactional
    public TestRunFilter create(TestRunFilter testRunFilter, Long userId) {
        User user = userRepository.findById(userId)
                                  .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.USER_NOT_FOUND_BY_ID, userId));

        boolean isPresent = testRunFilterRepository.findByOwnerId(userId)
                                                   .stream()
                                                   .anyMatch(testRunFilter::equals);
        if (isPresent) {
            throw new BusinessConstraintException(BusinessConstraintError.TEST_RUN_FILTER_IS_ALREADY_PRESENT, userId);
        }

        testRunFilter.setOwner(user);

        return testRunFilterRepository.save(testRunFilter);
    }

    @Override
    @Transactional
    public void patch(TestRunFilter testRunFilterPatch, Integer userId, Boolean isFavorite) {
        TestRunFilter testRunFilter = testRunFilterRepository.findById(testRunFilterPatch.getId())
                                                             .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_RUN_FILTER_NOT_FOUND_BY_ID, testRunFilterPatch.getId()));

        setIfNotNull(testRunFilterPatch.getItems(), testRunFilter::setItems);
        setIfNotNull(testRunFilterPatch.getIsPrivate(), testRunFilter::setIsPrivate);

        testRunFilter = testRunFilterRepository.save(testRunFilter);

        markAsFavorite(isFavorite, userId, testRunFilter);
    }

    private void markAsFavorite(Boolean isFavorite, Integer userId, TestRunFilter testRunFilter) {
        if (isFavorite == null)
            return;

        UserFavoriteTestRunFilter favoriteFilter = userFavoriteTestRunFilterRepository.findByUserIdAndTestRunFilterId(userId, testRunFilter.getId())
                                                                                      .orElse(buildUserFavoriteFilter(userId, testRunFilter.getId()));

        if (favoriteFilter.getId() != null && !isFavorite) {
            userFavoriteTestRunFilterRepository.deleteById(favoriteFilter.getId());
        }

        if (favoriteFilter.getId() == null && isFavorite) {
            userFavoriteTestRunFilterRepository.save(favoriteFilter);
        }

    }

    @Override
    public void deleteById(Integer id) {
        testRunFilterRepository.deleteById(id);
    }

    private UserFavoriteTestRunFilter buildUserFavoriteFilter(Integer userId, Integer testRunFilterId) {
        return UserFavoriteTestRunFilter.builder()
                                        .testRunFilterId(testRunFilterId)
                                        .userId(userId)
                                        .build();
    }

    private <T> void setIfNotNull(T value, Consumer<T> setter) {
        if (value != null) {
            setter.accept(value);
        }
    }

}
