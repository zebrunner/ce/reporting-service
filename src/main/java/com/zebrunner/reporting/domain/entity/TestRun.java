package com.zebrunner.reporting.domain.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "test_suite_executions")
@Builder(toBuilder = true, builderClassName = "Builder")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class TestRun {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String ciRunId;

    private OffsetDateTime startedAt;
    private OffsetDateTime endedAt;

    @Enumerated(EnumType.STRING)
    private Status status;

    private boolean knownIssue;

    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;
    @Column(name = "build_number")
    private Integer jobNumber;
    @ManyToOne
    @JoinColumn(name = "upstream_job_id")
    private Job upstreamJob;
    @Column(name = "upstream_job_build_number")
    private Integer upstreamJobNumber;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "test_suite_id")
    private TestSuite testSuite;

    @Column(name = "comments")
    private String comment;
    private Integer elapsed;
    private boolean reviewed;

    private String framework;
    private String environment;
    private String build;

    @lombok.Builder.Default
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "test_suite_executions_labels",
            joinColumns = @JoinColumn(name = "test_suite_execution_id"),
            inverseJoinColumns = @JoinColumn(name = "label_id")
    )
    private Set<Label> labels = Set.of();

    @lombok.Builder.Default
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<ArtifactReference> artifactReferences = Set.of();

    @lombok.Builder.Default
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "test_suite_executions_notification_targets",
            joinColumns = @JoinColumn(name = "test_suite_execution_id"),
            inverseJoinColumns = @JoinColumn(name = "notification_target_id")
    )
    private Set<NotificationTarget> notificationTargets = Set.of();

    @PrimaryKeyJoinColumn
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "testSuiteExecution")
    private TestRunStatistic statistic;

    @PrimaryKeyJoinColumn
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "testSuiteExecution", orphanRemoval = true)
    private TestRunCiContext ciContext;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<RerunMetadata> rerunMetadatas = new ArrayList<>();

    public void initStatistic() {
        if (statistic == null) {
            statistic = new TestRunStatistic(this);
        }
    }

    public void addRerunMetadata(RerunMetadata rerunMetadata) {
        if (this.rerunMetadatas == null) {
            this.rerunMetadatas = new ArrayList<>();
        }
        this.rerunMetadatas.add(rerunMetadata);
    }

    public enum Status {

        QUEUED,
        IN_PROGRESS,
        PASSED,
        FAILED,
        SKIPPED,
        ABORTED

    }

}
