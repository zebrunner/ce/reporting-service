CREATE TABLE artifact_references (
  id SERIAL,
  name VARCHAR(127) NOT NULL,
  value VARCHAR(2048) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (name, value)
);
CREATE TRIGGER update_timestamp_artifact_references
  BEFORE INSERT OR UPDATE ON artifact_references
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();


CREATE TABLE test_runs_artifact_references (
  id SERIAL,
  test_run_id INT NOT NULL,
  artifact_reference_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (test_run_id) REFERENCES test_runs (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  FOREIGN KEY (artifact_reference_id) REFERENCES artifact_references (id),
  UNIQUE (test_run_id, artifact_reference_id)
);
CREATE TRIGGER update_timestamp_test_runs_artifact_references
  BEFORE INSERT OR UPDATE ON test_runs_artifact_references
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();


CREATE TABLE tests_artifact_references (
  id SERIAL,
  test_id INT NOT NULL,
  artifact_reference_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (test_id) REFERENCES tests (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  FOREIGN KEY (artifact_reference_id) REFERENCES artifact_references (id),
  UNIQUE (test_id, artifact_reference_id)
);
CREATE TRIGGER update_timestamp_tests_artifact_references
  BEFORE INSERT OR UPDATE ON tests_artifact_references
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();
