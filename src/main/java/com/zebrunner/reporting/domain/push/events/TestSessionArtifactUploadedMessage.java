package com.zebrunner.reporting.domain.push.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestSessionArtifactUploadedMessage {

    private Long testRunId;
    private Long testSessionId;
    private String name;
    private String key;

}
