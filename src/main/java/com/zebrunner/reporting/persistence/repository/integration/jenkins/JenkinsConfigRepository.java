package com.zebrunner.reporting.persistence.repository.integration.jenkins;

import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface JenkinsConfigRepository extends JpaRepository<JenkinsConfig, Long> {

    Optional<JenkinsConfig> findByUrl(String url);

    Optional<JenkinsConfig> findByIdAndEnabledTrue(Long id);

    Optional<JenkinsConfig> findByUrlAndEnabledTrue(String url);

    List<JenkinsConfig> findAllByEnabledTrue();

    boolean existsByUrl(String url);

}
