package com.zebrunner.reporting.domain.vo;

import lombok.Value;

@Value
public class ReachabilityCheckResult {

    private final static String ON_CONNECTION_SUCCESS_TEMPLATE = "Connection request is succeeded.";
    private final static String ON_CONNECTION_EXCEPTION_TEMPLATE = "An error occurred with the following message: <%s>";
    private final static String INTERNAL_SERVICE_ON_CONNECTION_FAILED_TEMPLATE = "Connection request is failed.\nRaw response: %s";
    private final static String EXTERNAL_SYSTEM_ON_CONNECTION_FAILED_TEMPLATE = "Connection request is failed.\nStatus code: %d.\nRaw response: %s";

    Boolean reachable;
    String message;

    public static ReachabilityCheckResult reachable() {
        return new ReachabilityCheckResult(true, ON_CONNECTION_SUCCESS_TEMPLATE);
    }

    public static ReachabilityCheckResult unreachable(String response) {
        String message = String.format(INTERNAL_SERVICE_ON_CONNECTION_FAILED_TEMPLATE, response);
        return new ReachabilityCheckResult(false, message);
    }

    public static ReachabilityCheckResult unreachable(int statusCode, String response) {
        String message = String.format(EXTERNAL_SYSTEM_ON_CONNECTION_FAILED_TEMPLATE, statusCode, response);
        return new ReachabilityCheckResult(false, message);
    }

    public static ReachabilityCheckResult unreachable(Exception e) {
        String message = String.format(ON_CONNECTION_EXCEPTION_TEMPLATE, e.getMessage());
        return new ReachabilityCheckResult(false, message);
    }

}
