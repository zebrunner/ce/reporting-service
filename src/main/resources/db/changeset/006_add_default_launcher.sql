DO
$$
    DECLARE
        JOB_ID_VAR  JOBS.id%TYPE;
        SCM_ID_VAR  SCM.id%TYPE;
        JOB_URL_VAR VARCHAR;

    BEGIN
        JOB_URL_VAR := CONCAT('${JENKINS_URL}', '/job/launcher');
        INSERT INTO JOBS (NAME, JOB_URL, JENKINS_HOST)
        VALUES ('launcher', JOB_URL_VAR, '${JENKINS_URL}')
        RETURNING id INTO JOB_ID_VAR;

        INSERT INTO SCM (ACCESS_TOKEN, ORGANIZATION, NAME, REPO, REPOSITORY_URL)
        VALUES ('${SCM_DEFAULT_ACCOUNT_ACCESS_TOKEN}', '${SCM_DEFAULT_ACCOUNT_ORGANIZATION_NAME}', 'GITHUB',
                '${SCM_DEFAULT_ACCOUNT_REPOSITORY_NAME}', '${SCM_DEFAULT_ACCOUNT_REPOSITORY_URL}')
        RETURNING id INTO SCM_ID_VAR;
        INSERT INTO LAUNCHERS (NAME, JOB_ID, SCM_ID, TYPE, MODEL)
        VALUES ('Carina WEB', JOB_ID_VAR, SCM_ID_VAR, 'web',
                '{
                   "branch":"master",
                   "capabilities.browserName":"chrome",
                   "suite":"web"
                }');
        INSERT INTO LAUNCHERS (NAME, JOB_ID, SCM_ID, TYPE, MODEL)
        VALUES ('Carina API', JOB_ID_VAR, SCM_ID_VAR, 'api',
                '{
                   "capabilities.platformName":"API",
                   "branch":"master",
                   "suite":"api"
                }');

    END
$$;
