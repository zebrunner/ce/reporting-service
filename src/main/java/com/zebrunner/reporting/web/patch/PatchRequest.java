package com.zebrunner.reporting.web.patch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebrunner.common.eh.exception.MalformedInputException;
import com.zebrunner.reporting.service.exception.MalformedInputError;
import lombok.Data;
import lombok.experimental.Delegate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PatchRequest implements List<PatchRequest.Item> {

    @Valid
    @Delegate
    private final List<Item> items = new ArrayList<>();
    private final ObjectMapper objectMapper = new ObjectMapper();

    public <T> T getReplaceValue(String path, Class<T> clazz) {
        return items.stream()
                    .filter(item -> item.getOperation() == PatchOperation.REPLACE)
                    .filter(item -> item.getPath().equals(path))
                    .findFirst()
                    .map(Item::getValue)
                    .map(value -> mapToClass(value, clazz))
                    .orElse(null);
    }

    private <T> T mapToClass(Object value, Class<T> clazz) {
        try {
            return objectMapper.readValue(objectMapper.writeValueAsBytes(value), clazz);
        } catch (IOException e) {
            if (clazz.isEnum()) {
                String enumConstants = Arrays.stream(clazz.getEnumConstants())
                                             .map(Object::toString)
                                             .collect(Collectors.joining(", "));
                throw new MalformedInputException(MalformedInputError.PATCH_REQUEST_WRONG_ENUM_VALUE, enumConstants);
            }

            throw new MalformedInputException(MalformedInputError.PATCH_REQUEST_VALUE_HAS_WRONG_TYPE);
        }
    }

    @Data
    public static class Item {

        @NotNull
        @JsonProperty("op")
        private PatchOperation operation;
        @NotBlank
        private String path;
        private Object value;

    }

}
