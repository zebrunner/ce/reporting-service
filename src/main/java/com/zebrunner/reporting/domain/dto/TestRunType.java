package com.zebrunner.reporting.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.zebrunner.reporting.domain.db.Label;
import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TestRunType extends AbstractType {

    private String ciRunId;
    @NotNull
    private Long testSuiteId;
    private Status status;
    private String scmURL;
    private String scmBranch;
    private String scmCommit;
    private String configXML;
    @NotNull
    private Long jobId;
    private Long upstreamJobId;
    private Integer upstreamJobBuildNumber;
    @NotNull
    private Integer buildNumber;
    @NotNull
    private TestRun.Initiator startedBy;
    private Long userId;
    private boolean knownIssue;
    private boolean reviewed;

    @Valid
    private Set<ArtifactReference> artifacts = new HashSet<>();
    private Set<Label> labels;

}
