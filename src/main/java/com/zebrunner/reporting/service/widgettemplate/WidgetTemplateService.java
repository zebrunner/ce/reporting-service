package com.zebrunner.reporting.service.widgettemplate;

import com.zebrunner.reporting.domain.entity.WidgetTemplate;

import java.util.List;
import java.util.Map;

public interface WidgetTemplateService {

    WidgetTemplate retrieveById(Long id);

    WidgetTemplate retrieveByIdWithParamsValues(Long id, String period);

    List<WidgetTemplate> retrieveAllVisible();

    void setNotNullParamsValues(WidgetTemplate widgetTemplate);

    List<Map<String, Object>> getQueryResults(Long templateId, Map<String, Object> queryParameters, Integer userId);

}
