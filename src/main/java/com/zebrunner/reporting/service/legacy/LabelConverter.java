package com.zebrunner.reporting.service.legacy;

import com.zebrunner.reporting.domain.db.Label;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LabelConverter {

    private static final Map<String, String> OBSOLETE_TO_NEW_TCM_LABELS = Map.of(
            "TESTRAIL_TESTCASE_UUID", "com.zebrunner.app/tcm.testrail.testcase-id",
            "QTEST_TESTCASE_UUID", "com.zebrunner.app/tcm.qtest.testcase-id"
    );

    public static Set<com.zebrunner.reporting.domain.entity.Label> toNewer(Collection<Label> labels) {
        return labels.stream()
                     .peek(label -> label.setKey(OBSOLETE_TO_NEW_TCM_LABELS.getOrDefault(label.getKey(), label.getKey())))
                     .map(LabelConverter::toNewer)
                     .collect(Collectors.toSet());
    }

    public static com.zebrunner.reporting.domain.entity.Label toNewer(Label label) {
        return new com.zebrunner.reporting.domain.entity.Label(label.getKey(), label.getValue());
    }

    public static Set<Label> toOlder(Set<com.zebrunner.reporting.domain.entity.Label> labels) {
        return labels.stream().map(LabelConverter::toOlder).collect(Collectors.toSet());
    }

    public static Label toOlder(com.zebrunner.reporting.domain.entity.Label label) {
        Label l = new Label(label.getKey(), label.getValue());
        l.setId(label.getId().longValue());
        return l;
    }

}
