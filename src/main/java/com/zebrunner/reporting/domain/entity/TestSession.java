package com.zebrunner.reporting.domain.entity;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "test_sessions")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class TestSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "test_suite_execution_id")
    private Long testRunId;
    private String sessionId;
    private Instant initiatedAt;
    private Instant startedAt;
    private Instant endedAt;
    @Enumerated(EnumType.STRING)
    private Status status;
    private String browserName;
    private String browserVersion;
    private String platformName;
    private String platformVersion;
    private String deviceName;

    // The use of jackson data types here is deliberative solution,
    // because we don't care about the actual structure of the capabilities objects.
    // Jackson provides convenient API to operate with json structures, so, for now, it looks like good solution.
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private JsonNode capabilities;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private JsonNode desiredCapabilities;

    @Builder.Default
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "test_executions_test_sessions",
            joinColumns = @JoinColumn(name = "test_session_id"),
            inverseJoinColumns = @JoinColumn(name = "test_execution_id")
    )
    private Set<Test> tests = new HashSet<>();

    @Builder.Default
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Set<ArtifactReference> artifactReferences = new HashSet<>();

    public enum Status {

        RUNNING,
        COMPLETED,
        FAILED

    }

}
