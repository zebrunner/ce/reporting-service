package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.TestCase;
import com.zebrunner.reporting.persistence.repository.TestCaseRepository;
import com.zebrunner.reporting.service.TestCase2Service;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class TestCase2ServiceImpl implements TestCase2Service {

    private final TestCaseRepository testCaseRepository;

    @Override
    @Transactional
    public TestCase save(TestCase testCase) {
        String testClass = testCase.getTestClass();
        String testMethod = testCase.getTestMethod();

        return testCaseRepository.findByBusinessKey(testClass, testMethod)
                                 .orElseGet(() -> testCaseRepository.save(testCase));
    }

}
