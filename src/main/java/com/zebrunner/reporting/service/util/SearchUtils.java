package com.zebrunner.reporting.service.util;

import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import org.springframework.data.domain.Sort;

import java.util.Set;

public final class SearchUtils {

    public static Sort getSort(SearchCriteria searchCriteria, Set<String> allowedSortOptions, String defaultSortOption) {
        return searchCriteria.getSortBy() != null && allowedSortOptions.contains(searchCriteria.getSortBy())
                ? Sort.by(searchCriteria.getSortOrder().toDirection(), searchCriteria.getSortBy())
                : Sort.by(searchCriteria.getSortOrder().toDirection(), defaultSortOption);
    }

}
