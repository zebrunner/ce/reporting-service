package com.zebrunner.reporting.domain.dto.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SimpleSearchResult<T> {

    private Integer page;
    private Integer pageSize;

    private long totalPages;
    private long totalResults;
    private List<T> results;

    public static <T> SimpleSearchResult<T> noData(Integer page, Integer pageSize) {
        return new SimpleSearchResult<>(0, 0, 0, 0, Collections.emptyList());
    }

}
