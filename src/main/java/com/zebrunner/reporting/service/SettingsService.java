package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.Setting;
import com.zebrunner.reporting.persistence.repository.SettingsRepository;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SettingsService {

    private final SettingsRepository settingsRepository;

    @Transactional(readOnly = true)
    public Setting getSettingByName(String name) {
        return settingsRepository.findByName(name)
                                 .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.SETTING_NOT_FOUND_BY_NAME, name));
    }

    @Transactional
    public Setting updateSetting(Setting updatedSettings) {
        if (updatedSettings.getName().equals("KEY")) {
            throw new BusinessConstraintException(BusinessConstraintError.SETTING_WITH_NAME_KEY_CANNOT_BE_UPDATE);
        }

        Setting setting = getSettingByName(updatedSettings.getName());
        setting.setValue(updatedSettings.getValue());
        return settingsRepository.save(setting);
    }

    @Transactional
    public void saveNewKey(String newKey) {
        Setting setting = getSettingByName("KEY");
        setting.setValue(newKey);
        settingsRepository.save(setting);
    }

}
