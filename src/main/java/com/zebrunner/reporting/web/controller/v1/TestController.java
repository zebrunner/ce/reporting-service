package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.domain.dto.reporting.TestFinish;
import com.zebrunner.reporting.domain.dto.reporting.TestSave;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.push.TestPush;
import com.zebrunner.reporting.domain.push.TestRunStatisticPush;
import com.zebrunner.reporting.messaging.websocket.Topic;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestRunStatisticsService;
import com.zebrunner.reporting.service.TestService;
import com.zebrunner.reporting.web.converter.v1.ListTestResponseConverter;
import com.zebrunner.reporting.web.converter.v1.SaveTestResponseConverter;
import com.zebrunner.reporting.web.request.v1.AttachArtifactReferencesRequest;
import com.zebrunner.reporting.web.request.v1.AttachLabelsRequest;
import com.zebrunner.reporting.web.request.v1.ConfirmKnownIssueRequest;
import com.zebrunner.reporting.web.request.v1.FinishTestRequest;
import com.zebrunner.reporting.web.request.v1.StartHeadlessTestRequest;
import com.zebrunner.reporting.web.request.v1.StartTestRequest;
import com.zebrunner.reporting.web.request.v1.UpdateHeadlessTestRequest;
import com.zebrunner.reporting.web.request.v1.UpdateTestRequest;
import com.zebrunner.reporting.web.response.v1.ConfirmKnownIssueResponse;
import com.zebrunner.reporting.web.response.v1.ListTestsResponse;
import com.zebrunner.reporting.web.response.v1.SaveTestResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController("v1TestController")
@RequiredArgsConstructor
@RequestMapping(path = "/v1/test-runs/{testRunId}/tests", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestController {

    private final JMapper mapper;
    private final Validator validator;
    private final TestService testService;
    private final Test2Service test2Service;
    private final SimpMessagingTemplate messagingTemplate;
    private final TestRunStatisticsService statisticsService;
    private final SaveTestResponseConverter saveTestResponseConverter;
    private final ListTestResponseConverter listTestResponseConverter;

//    @GetMapping("/cool")
//    public ListTestsResponse getTests(@PathVariable("testRunId") Integer testRunId) {
//        List<Test> tests = test2Service.retrieveByTestRunId(testRunId);
//        List<ListTestsResponse.TestItem> items = mapper.mapToList(tests, ListTestsResponse.TestItem.class);
//        return new ListTestsResponse(items);
//    }

    @GetMapping
    @Deprecated
    public List<ListTestsResponse.Item> getTestsByCiRunId(@PathVariable("testRunId") String ciRunId,
                                                          @RequestParam(name = "statuses", required = false) Set<com.zebrunner.reporting.domain.entity.Test.Status> statuses,
                                                          @RequestParam(name = "tests", required = false) Set<Long> testIds) {
        return test2Service.retrieveByCiRunId(ciRunId, statuses, testIds)
                           .stream()
                           .map(listTestResponseConverter::toListResponseItem)
                           .collect(Collectors.toList());
    }

    @PostMapping
    public SaveTestResponse start(@PathVariable("testRunId") @NotNull @Positive Long testRunId,
                                  @RequestParam(name = "headless", defaultValue = "false") boolean headless,
                                  @RequestBody StartTestRequest startTestRequest) {
        Object request = headless
                ? mapper.map(startTestRequest, StartHeadlessTestRequest.class)
                : startTestRequest;
        validate(request);

        TestSave testSave = mapper.map(request, TestSave.class);
        com.zebrunner.reporting.domain.entity.Test test = test2Service.start(testRunId, testSave);

        Test oldTest = testService.getTestById(test.getId());
        notifyAboutTestByWebsocket(oldTest);

        return saveTestResponseConverter.toSaveResponse(test, true);

    }

    @PostMapping("/{testId}")
    public SaveTestResponse restart(@PathVariable("testRunId") @NotNull @Positive Long testRunId,
                                    @PathVariable("testId") @NotNull @Positive Long testId,
                                    @RequestParam(name = "headless", defaultValue = "false") boolean headless,
                                    @RequestBody StartTestRequest startTestRequest) {
        Object request = headless
                ? mapper.map(startTestRequest, StartHeadlessTestRequest.class)
                : startTestRequest;
        validate(request);

        TestSave testSave = mapper.map(request, TestSave.class);
        com.zebrunner.reporting.domain.entity.Test test = test2Service.restart(testRunId, testId, testSave);

        Test oldTest = testService.getTestById(test.getId());
        notifyAboutTestByWebsocket(oldTest);

        return saveTestResponseConverter.toSaveResponse(test, true);
    }

    @PutMapping("/{testId}")
    public SaveTestResponse updateOrFinish(@PathVariable("testRunId") @NotNull @Positive Long testRunId,
                                           @PathVariable("testId") @NotNull @Positive Long testId,
                                           @RequestParam(name = "headless", defaultValue = "false") boolean headless,
                                           @RequestBody UpdateTestRequest updateTestRequest) {
        SaveTestResponse response;

        if (headless) {
            UpdateHeadlessTestRequest request = mapper.map(updateTestRequest, UpdateHeadlessTestRequest.class);
            validate(request);

            TestSave testSave = mapper.map(request, TestSave.class);
            com.zebrunner.reporting.domain.entity.Test test = test2Service.update(testRunId, testId, testSave);

            response = saveTestResponseConverter.toSaveResponse(test, true);
        } else {
            FinishTestRequest request = mapper.map(updateTestRequest, FinishTestRequest.class);
            validate(request);

            TestFinish testFinish = mapper.map(request, TestFinish.class);
            com.zebrunner.reporting.domain.entity.Test test = test2Service.finish(testRunId, testId, testFinish);

            // todo: for backward compatibility - very first versions of agent will fail if we return labels on finish
            response = saveTestResponseConverter.toSaveResponse(test, false);
        }

        Test oldTest = testService.getTestById(testId);
        notifyAboutTestByWebsocket(oldTest);

        return response;
    }

    @PostMapping("/{testId}/known-issue-confirmations")
    public ConfirmKnownIssueResponse confirmKnownIssue(@PathVariable("testRunId") Long testRunId,
                                                       @PathVariable("testId") Long testId,
                                                       @RequestBody @Valid ConfirmKnownIssueRequest request) {
        String failureReason = request.getFailureReason();
        boolean knownIssue = test2Service.confirmKnownIssue(testRunId, testId, failureReason);

        return new ConfirmKnownIssueResponse(knownIssue);
    }

    @PutMapping("/{testId}/labels")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void attachLabels(@PathVariable("testId") Long testId,
                             @RequestBody @Valid AttachLabelsRequest request) {
        Set<Label> labels = mapper.mapToSet(request.getItems(), Label.class);
        test2Service.attachLabels(testId, labels);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{testId}/artifact-references")
    public void attachArtifactReferences(@PathVariable("testId") Long testId,
                                         @RequestBody @Valid AttachArtifactReferencesRequest request) {
        Set<ArtifactReference> references = mapper.mapToSet(request.getItems(), ArtifactReference.class);
        test2Service.attachArtifactReferences(testId, references);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("testRunId") Long testRunId,
                       @PathVariable("id") Long id) {
        test2Service.deleteByTestRunIdAndId(testRunId, id);
    }

    private void validate(Object object) {
        Set<ConstraintViolation<Object>> violations = validator.validate(object);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

    private void notifyAboutTestByWebsocket(Test test) {
        notifyAboutTestRunStatisticsByWebsocket(test.getTestRunId());
        messagingTemplate.convertAndSend(Topic.tests(test.getTestRunId()), new TestPush(test));
    }

    private void notifyAboutTestRunStatisticsByWebsocket(Long runId) {
        TestRunStatistics testRunStatistics = statisticsService.get(runId);
        messagingTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistics));
    }

}
