package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRunCiContextRepository extends JpaRepository<TestRunCiContext, Long> {
}
