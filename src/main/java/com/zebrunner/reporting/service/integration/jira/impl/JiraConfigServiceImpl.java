package com.zebrunner.reporting.service.integration.jira.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.persistence.repository.JiraConfigRepository;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.util.URLNormalizer;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.JIRA_CONFIG_UNREACHABLE_OR_DISABLED;

@Slf4j
@Service
@RequiredArgsConstructor
public class JiraConfigServiceImpl implements JiraConfigService {

    private final CryptoService cryptoService;
    private final JiraConfigRepository jiraConfigRepository;

    @Override
    @Transactional
    public JiraConfig update(JiraConfig newConfig) {
        JiraConfig config = jiraConfigRepository.findFirstByOrderById()
                                                .orElseGet(this::createConfigIfNotExist);
        config.setUrl(URLNormalizer.normalize(newConfig.getUrl()));
        config.setUsername(newConfig.getUsername());
        if (!Objects.equals(config.getToken(), newConfig.getToken())) {
            String token = cryptoService.encrypt(newConfig.getToken());
            config.setToken(token);
            config.setEncrypted(true);
        }
        config.setType(newConfig.getType());
        config.setEnabled(newConfig.isEnabled());
        return jiraConfigRepository.save(config);
    }

    @Override
    @Transactional
    public Optional<JiraConfig> retrieveOptional() {
        return jiraConfigRepository.findFirstByOrderById();
    }

    @Override
    @Transactional
    public JiraConfig retrieve() {
        return jiraConfigRepository.findFirstByOrderById()
                                   .orElseGet(this::createConfigIfNotExist);
    }

    private JiraConfig decryptValuesIfRequired(JiraConfig config, boolean decryptValues) {
        if (decryptValues) {
            if (config.isEncrypted()) {
                config.setToken(cryptoService.decrypt(config.getToken()));
                config.setEncrypted(false);
            }
        }
        return config;
    }

    @Override
    @Transactional
    public JiraConfig retrieveActive(boolean decryptValues) {
        return jiraConfigRepository.findEnabled()
                                   .map(this::checkReachability)
                                   .map(config -> decryptValuesIfRequired(config, decryptValues))
                                   .orElseThrow(() -> new ResourceNotFoundException(JIRA_CONFIG_UNREACHABLE_OR_DISABLED));
    }

    private JiraConfig checkReachability(JiraConfig config) {
        ReachabilityCheckResult result = isReachable(config);
        if (!result.getReachable()) {
            log.warn("[Jira] Is not reachable: {}", result.getMessage());
            throw new ResourceNotFoundException(ResourceNotFoundError.JIRA_IS_UNREACHABLE_FOR_GIVEN_CONFIG);
        }

        return config;
    }

    @Override
    @Transactional
    public void patchEnabledProperty(Boolean enabled) {
        JiraConfig jira = jiraConfigRepository.findFirstByOrderById()
                                              .orElseGet(this::createConfigIfNotExist);
        jira.setEnabled(enabled);
        jiraConfigRepository.save(jira);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isEnabled() {
        return jiraConfigRepository.findEnabled()
                                   .isPresent();
    }

    private JiraConfig createConfigIfNotExist() {
        JiraConfig config = JiraConfig.builder()
                                      .url("")
                                      .username("")
                                      .token("")
                                      .type(JiraConfig.Type.CLOUD)
                                      .build();
        return jiraConfigRepository.save(config);
    }

    @Override
    public ReachabilityCheckResult isReachable(JiraConfig config) {
        try {
            String decryptedToken = config.isEncrypted() ? cryptoService.decrypt(config.getToken()) : config.getToken();
            HttpResponse<String> response = Unirest.head(URLNormalizer.normalize(config.getUrl()) + "/rest/api/3/myself")
                                                   .basicAuth(config.getUsername(), decryptedToken)
                                                   .asString();
            return response.isSuccess()
                    ? ReachabilityCheckResult.reachable()
                    : ReachabilityCheckResult.unreachable(response.getStatus(), response.getStatusText());
        } catch (Exception e) {
            return ReachabilityCheckResult.unreachable(e);
        }
    }

}
