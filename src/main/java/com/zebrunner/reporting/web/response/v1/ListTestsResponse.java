package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.domain.entity.Test;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.OffsetDateTime;
import java.util.List;

@Value
public class ListTestsResponse {

    List<Item> items;

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class Item {

        private Long id;
        private String name;
        private String correlationData;
        private String className;
        private String methodName;
        private OffsetDateTime startedAt;
        private OffsetDateTime endedAt;
        private String maintainer;
        private String testCase;
        private List<Label> labels;
        private List<ArtifactReference> artifactReferences;
        private Test.Status result;
        private String reason;

        @Value
        public static class Label {

            String key;
            String value;

        }

        @Value
        public static class ArtifactReference {

            String name;
            String value;

        }

    }

}
