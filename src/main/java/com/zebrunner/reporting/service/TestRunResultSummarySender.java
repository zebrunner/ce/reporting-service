package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;

public interface TestRunResultSummarySender {

    NotificationTarget.Type getType();

    void sendTestRunResultSummary(String target, TestRunResultSummary resultSummary);

}
