package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import com.zebrunner.reporting.domain.integration.jira.IssuePreview;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.integration.jira.JiraService;
import com.zebrunner.reporting.web.patch.PatchAction;
import com.zebrunner.reporting.web.patch.PatchOperation;
import com.zebrunner.reporting.web.patch.PatchRequest;
import com.zebrunner.reporting.web.request.v1.jira.CheckJiraConnectionRequest;
import com.zebrunner.reporting.web.request.v1.jira.UpdateJiraConfigRequest;
import com.zebrunner.reporting.web.response.v1.BulkGetResponse;
import com.zebrunner.reporting.web.response.v1.CheckConnectionResponse;
import com.zebrunner.reporting.web.response.v1.common.ErrorReason;
import com.zebrunner.reporting.web.response.v1.item.DataResponseItem;
import com.zebrunner.reporting.web.response.v1.item.ErrorResponseItem;
import com.zebrunner.reporting.web.response.v1.item.ResponseItem;
import com.zebrunner.reporting.web.response.v1.jira.GetIssueResponse;
import com.zebrunner.reporting.web.response.v1.jira.GetJiraConfigResponse;
import com.zebrunner.reporting.web.response.v1.jira.IssuePreviewResponseItem;
import com.zebrunner.reporting.web.response.v1.jira.UpdateJiraConfigResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Validated
@RestController("v1JiraController")
@RequiredArgsConstructor
@RequestMapping(path = "/v1/integrations/jira", produces = MediaType.APPLICATION_JSON_VALUE)
public class JiraController {

    private final JMapper mapper;
    private final JiraService jiraService;
    private final JiraConfigService jiraConfigService;

    @PutMapping
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public UpdateJiraConfigResponse update(@Valid @RequestBody UpdateJiraConfigRequest request) {
        JiraConfig newConfig = mapper.map(request, JiraConfig.class);
        JiraConfig config = jiraConfigService.update(newConfig);
        return mapper.map(config, UpdateJiraConfigResponse.class);
    }

    @GetMapping
    @PreAuthorize("hasPermission('reporting:integrations:read')")
    public GetJiraConfigResponse retrieve() {
        JiraConfig config = jiraConfigService.retrieve();
        return mapper.map(config, GetJiraConfigResponse.class);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public void patch(@PatchAction(op = PatchOperation.REPLACE, path = "/enabled")
                      @RequestBody PatchRequest patchRequest) {
        Boolean enabled = patchRequest.getReplaceValue("/enabled", Boolean.class);
        if (enabled != null) {
            jiraConfigService.patchEnabledProperty(enabled);
        }
    }

    @GetMapping("/issues")
    @ResponseStatus(HttpStatus.MULTI_STATUS)
    public BulkGetResponse getIssuesByIds(@RequestParam("ids") @Size(min = 1, max = 20) Set<@NotBlank String> ids) {
        List<IssuePreview> issues = jiraService.retrieveIssuesByIds(ids);
        List<ResponseItem> items = mapper.mapToList(issues, IssuePreviewResponseItem.class)
                                         .stream()
                                         .map(issue -> DataResponseItem.succeeded(issue.getKey(), issue))
                                         .collect(Collectors.toList());
        List<ResponseItem> notFoundItems = ids.stream()
                                              .filter(id -> issues.stream().noneMatch(issue -> issue.getKey().equals(id)))
                                              .map(id -> ErrorResponseItem.failed(id, ErrorReason.NOT_FOUND))
                                              .collect(Collectors.toList());
        items.addAll(notFoundItems);
        return new BulkGetResponse(items);
    }

    @GetMapping("/issues/{issueId}")
    public GetIssueResponse getIssueById(@PathVariable("issueId") @NotBlank String issueId) {
        IssuePreview issue = jiraService.retrieveIssueById(issueId);
        return mapper.map(issue, GetIssueResponse.class);
    }

    @PostMapping("/connectivity-checks")
    public CheckConnectionResponse checkJiraConnection(@Valid @RequestBody CheckJiraConnectionRequest request) {
        JiraConfig config = mapper.map(request, JiraConfig.class);
        ReachabilityCheckResult result = jiraConfigService.isReachable(config);
        return mapper.map(result, CheckConnectionResponse.class);
    }

}
