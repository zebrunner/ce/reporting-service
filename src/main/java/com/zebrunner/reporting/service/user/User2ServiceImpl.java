package com.zebrunner.reporting.service.user;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.User;
import com.zebrunner.reporting.persistence.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.USER_NOT_FOUND_BY_ID;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.USER_NOT_FOUND_BY_USERNAME;

@Service
@RequiredArgsConstructor
public class User2ServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final List<UserReassignmentListener> userReassignmentListeners;

    @Override
    public User retrieveAnonymous() {
        return userRepository.findByUsername(ANONYMOUS_USERNAME)
                             .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_BY_USERNAME, ANONYMOUS_USERNAME));
    }

    @Override
    public Optional<User> retrieveByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User retrieveNoNullById(Long id) {
        return userRepository.findById(id)
                             .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_BY_ID, id));
    }

    @Override
    @Transactional
    public void deleteByName(String name) {
        User user = userRepository.findByUsername(name)
                                  .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_BY_USERNAME, name));

        User anonymous = userRepository.findByUsername(ANONYMOUS_USERNAME)
                                       .orElseThrow(() -> new ResourceNotFoundException(USER_NOT_FOUND_BY_USERNAME, ANONYMOUS_USERNAME));

        userReassignmentListeners.forEach(listener -> listener.onUserReassignment(user.getId(), anonymous.getId()));
        userRepository.deleteById(user.getId());
    }

}
