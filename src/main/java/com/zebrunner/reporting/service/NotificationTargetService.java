package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;

import java.util.Set;

public interface NotificationTargetService {

    Set<NotificationTarget> save(Set<NotificationTarget> targets);

    void execute(Set<NotificationTarget> targets, TestRunResultSummary resultSummary);

}
