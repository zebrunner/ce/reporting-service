package com.zebrunner.reporting.service.integration.testrail.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.integration.TestRailConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.persistence.repository.TestRailConfigRepository;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.TestRailClient;
import com.zebrunner.reporting.service.integration.testrail.TestRailConfigService;
import com.zebrunner.reporting.service.util.URLNormalizer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TESTRAIL_CONFIG_NOT_FOUND;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TESTRAIL_IS_UNREACHABLE_FOR_GIVEN_PROJECT;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestRailConfigServiceImpl implements TestRailConfigService {

    private final CryptoService cryptoService;
    private final TestRailConfigRepository testRailConfigRepository;

    @Override
    @Transactional
    public TestRailConfig update(TestRailConfig newConfig) {
        TestRailConfig config = retrieve();
        config.setUrl(URLNormalizer.normalize(newConfig.getUrl()));
        config.setUsername(newConfig.getUsername());
        if (!Objects.equals(config.getPassword(), newConfig.getPassword())) {
            String password = cryptoService.encrypt(newConfig.getPassword());
            config.setPassword(password);
            config.setEncrypted(true);
        }
        config.setEnabled(newConfig.isEnabled());
        return testRailConfigRepository.save(config);
    }

    @Override
    @Transactional
    public TestRailConfig retrieve() {
        return testRailConfigRepository.findFirstByOrderById()
                                       .orElseGet(this::createConfigIfNotExist);
    }

    @Override
    public TestRailConfig retrieveActive() {
        return testRailConfigRepository.findEnabled()
                                       .map(this::checkReachability)
                                       .orElseThrow(() -> new ResourceNotFoundException(TESTRAIL_CONFIG_NOT_FOUND));
    }

    private TestRailConfig checkReachability(TestRailConfig config) {
        ReachabilityCheckResult result = isReachable(config);
        if (!result.getReachable()) {
            log.warn("[TestRail] Server is not reachable: {}", result.getMessage());
            throw new ResourceNotFoundException(TESTRAIL_IS_UNREACHABLE_FOR_GIVEN_PROJECT);
        }

        return config;
    }

    @Override
    @Transactional
    public void patchEnabledProperty(Boolean enabled) {
        TestRailConfig testRailConfig = testRailConfigRepository.findFirstByOrderById()
                                                                .orElseThrow(() -> new ResourceNotFoundException(TESTRAIL_CONFIG_NOT_FOUND));
        testRailConfig.setEnabled(enabled);
        testRailConfigRepository.save(testRailConfig);
    }

    private TestRailConfig createConfigIfNotExist() {
        TestRailConfig config = TestRailConfig.builder()
                                              .url("")
                                              .username("")
                                              .password("")
                                              .build();
        return testRailConfigRepository.save(config);
    }

    @Override
    public ReachabilityCheckResult isReachable(TestRailConfig testRailConfig) {
        String rawPassword = testRailConfig.getPassword();
        String password = testRailConfig.isEncrypted() ? cryptoService.decrypt(rawPassword) : rawPassword;
        TestRailClient client = new TestRailClient(
                testRailConfig.getUrl(),
                testRailConfig.getUsername(),
                password,
                testRailConfig.isEnabled()
        );
        return client.isServerReachable();
    }

}
