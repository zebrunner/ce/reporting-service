CREATE TABLE test_run_filters
(
    id          BIGSERIAL,
    name        VARCHAR(256) NOT NULL,
    items       JSONB        NOT NULL,
    owner_id    INTEGER      NOT NULL,
    is_private  BOOLEAN      NOT NULL DEFAULT TRUE,
    created_at  TIMESTAMP    NOT NULL DEFAULT now(),
    modified_at TIMESTAMP    NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (owner_id) REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION
);
CREATE INDEX owner_id_index ON test_run_filters (owner_id);
CREATE INDEX is_private_index ON test_run_filters (is_private);
CREATE TRIGGER update_timestamp_test_run_filters
    BEFORE INSERT OR UPDATE
    ON test_run_filters
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_at();


CREATE TABLE users_favorite_test_run_filters
(
    id                 BIGSERIAL,
    user_id            INT       NOT NULL,
    test_run_filter_id INT       NOT NULL,
    created_at         TIMESTAMP NOT NULL DEFAULT now(),
    modified_at        TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION,
    FOREIGN KEY (test_run_filter_id) REFERENCES test_run_filters (id)
        ON DELETE CASCADE
        ON UPDATE NO ACTION,
    UNIQUE (user_id, test_run_filter_id)
);
CREATE TRIGGER update_timestamp_users_favorite_test_run_filters
    BEFORE INSERT OR UPDATE
    ON users_favorite_test_run_filters
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_at();
