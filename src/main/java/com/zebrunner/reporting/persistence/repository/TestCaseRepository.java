package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestCase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TestCaseRepository extends JpaRepository<TestCase, Long> {

    @Query("SELECT tc FROM TestCase tc " +
            "WHERE tc.testClass = :testClass " +
            "  AND tc.testMethod = :testMethod ")
    Optional<TestCase> findByBusinessKey(@Param("testClass") String testClass,
                                         @Param("testMethod") String testMethod);

}
