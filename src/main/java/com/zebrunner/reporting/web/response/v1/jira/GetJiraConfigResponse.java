package com.zebrunner.reporting.web.response.v1.jira;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JGlobalMap
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetJiraConfigResponse {

    private String url;
    private String username;
    private String token;
    private boolean encrypted;
    private boolean enabled;
    private JiraConfig.Type type;

}
