package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.integration.IssueReference;

import java.util.Optional;

public interface IssueReferenceService {

    Optional<IssueReference> retrieveLinkedToTestCaseByFailureReasonHash(Long testCaseId, Integer failureReasonHashCode);

    IssueReference retrieveOrCreate(String value, IssueReference.Type type);

    IssueReference retrieveOrCreate(IssueReference issueReference);

}
