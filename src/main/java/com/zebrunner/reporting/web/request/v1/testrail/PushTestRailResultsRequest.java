package com.zebrunner.reporting.web.request.v1.testrail;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class PushTestRailResultsRequest {

    private String testRunName;
    @NotNull
    private Boolean runExists;
    @NotNull
    private Boolean includeAll;

    private String milestone;
    private String assignee;
    private Integer searchInterval;

}
