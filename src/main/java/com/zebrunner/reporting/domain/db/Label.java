package com.zebrunner.reporting.domain.db;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "id", callSuper = false)
public class Label extends AbstractEntity {

    private String key;
    private String value;

}
