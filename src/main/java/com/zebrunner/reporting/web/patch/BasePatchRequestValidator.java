package com.zebrunner.reporting.web.patch;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

public abstract class BasePatchRequestValidator<A extends Annotation> implements ConstraintValidator<A, PatchRequest> {

    protected Map<PatchOperation, Set<String>> operationToAllowedPaths = Map.of();

    @Override
    public boolean isValid(PatchRequest request, ConstraintValidatorContext context) {
        boolean isValid = true;
        for (PatchRequest.Item item : request) {
            Set<String> allowedPaths = operationToAllowedPaths.getOrDefault(item.getOperation(), Set.of());

            if (!allowedPaths.contains(item.getPath())) {
                String opName = item.getOperation().name().toLowerCase();
                String message = String.format("Path '%s' is not allowed for operation '%s'", item.getPath(), opName);

                context.buildConstraintViolationWithTemplate(message)
                       .addPropertyNode("path")
                       .addConstraintViolation();
                isValid = false;
            }
        }
        return isValid;
    }

}
