package com.zebrunner.reporting.service.httpclient.tcm.testrail.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRailTestResponse {

    private Long id;
    private Long caseId;
    private Integer statusId;
    private String comment;
    private String defects;
    private Long assignedtoId;

}
