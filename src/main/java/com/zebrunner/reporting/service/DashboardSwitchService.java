package com.zebrunner.reporting.service;

public interface DashboardSwitchService {

    void save(Long dashboardId, Long userId);

    void delete(Long dashboardId, Long userId);

}
