package com.zebrunner.reporting.web.request.v1.testsession.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {RunningSessionMustHaveCapabilitiesValidator.class})
public @interface RunningSessionMustHaveCapabilities {

    String message() default "A Test Session with 'RUNNING' status must have capabilities.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
