package com.zebrunner.reporting.service.util;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class EventPushService {

    private final RabbitTemplate rabbitTemplate;
    private final Map<String, Queue> queues;

    public void send(String exchange, Object message) {
        send(exchange, "", message);
    }

    @SneakyThrows
    public void send(String exchange, String routingKey, Object message) {
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

    @SneakyThrows
    public void send(String routingKey, Object message, Map<String, ?> metadata) {
        send("", routingKey, message, metadata);
    }

    @SneakyThrows
    public void send(String exchange, String routingKey, Object message, Map<String, ?> metadata) {
        rabbitTemplate.convertAndSend(exchange, routingKey, message, rabbitmqMessage -> {
            metadata.forEach((key, value) -> rabbitmqMessage.getMessageProperties().setHeader(key, value));
            return rabbitmqMessage;
        });
    }

    @SneakyThrows
    public void sendFanout(String exchange, Object message, Map<String, ?> headers) {
        rabbitTemplate.convertAndSend(exchange, "", message, rabbitmqMessage -> {
            headers.forEach((key, value) -> rabbitmqMessage.getMessageProperties().setHeader(key, value));
            return rabbitmqMessage;
        });
    }

}
