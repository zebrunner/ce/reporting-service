package com.zebrunner.reporting.persistence.repository;

import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;


public abstract class JpaRepositoryGenericTest<T extends JpaRepository<Entity, Long>, Entity> {

    @Autowired
    protected T repository;

    public void saveTest(Entity entity) {
        Entity savedEntity = repository.save(entity);
        Assertions.assertNotNull(savedEntity);
        repository.delete(savedEntity);
    }

    public void findAllTest() {
        Assertions.assertNotNull(repository.findAll());
    }

    public void findByIdTest(Long id) {
        Assertions.assertNotNull(repository.findById(id));
    }


    public void deleteByIdTest(Long id) {
        repository.deleteById(id);
        Assertions.assertFalse(repository.findById(id).isPresent());
    }

}
