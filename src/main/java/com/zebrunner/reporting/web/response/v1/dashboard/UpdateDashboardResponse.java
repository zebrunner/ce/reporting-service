package com.zebrunner.reporting.web.response.v1.dashboard;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.reporting.domain.entity.DashboardWidget;
import com.zebrunner.reporting.web.converter.v1.DashboardWidgetResponseItemConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@JGlobalMap
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateDashboardResponse {

    private Long id;
    private String title;
    private boolean editable;
    private List<DashboardWidgetResponseItem> widgets;

    @JMapConversion(from = "widgets", to = "widgets")
    public List<DashboardWidgetResponseItem> convertWidgets(List<DashboardWidget> widgets) {
        return widgets.stream()
                      .map(DashboardWidgetResponseItemConverter::toResponseItem)
                      .collect(Collectors.toList());
    }

}
