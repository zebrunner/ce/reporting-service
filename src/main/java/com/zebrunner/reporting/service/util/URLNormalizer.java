package com.zebrunner.reporting.service.util;

import com.zebrunner.common.eh.exception.MalformedInputException;
import com.zebrunner.reporting.service.exception.MalformedInputError;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.Objects;

@Slf4j
public class URLNormalizer {

    private static final Map<String, Integer> PROTOCOL_TO_DEFAULT_PORT = Map.of("http", 80,
                                                                                "https", 443,
                                                                                "ftp", 21,
                                                                                "ws", 80,
                                                                                "wss", 443);
    private static final UrlValidator VALIDATOR = new UrlValidator(getProtocols(), UrlValidator.ALLOW_LOCAL_URLS);

    private static String[] getProtocols() {
        return PROTOCOL_TO_DEFAULT_PORT.keySet().toArray(new String[0]);
    }

    public static String normalize(String originalUrl) {
        String url = originalUrl.replaceAll("/*$", "");
        if (!VALIDATOR.isValid(url)) {
            throw new MalformedInputException(MalformedInputError.INVALID_URL_FORMAT, originalUrl);
        }
        try {
            URI uri = URI.create(url);
            boolean hasDefaultPort = Objects.equals(PROTOCOL_TO_DEFAULT_PORT.get(uri.getScheme()), uri.getPort());
            return hasDefaultPort ? UriComponentsBuilder.fromUri(uri).port(-1).toUriString() : uri.toString();
        } catch (Exception e) {
            log.warn(
                    "Provided url can't be parsed to uri.\nOriginal url: {}.\nUrl after trimming: {}.\nError message: {}.",
                    originalUrl, url, e.getMessage()
            );
            throw new MalformedInputException(MalformedInputError.INVALID_URL_FORMAT, originalUrl);
        }
    }

}
