ALTER TABLE test_sessions_transient
  ADD COLUMN platform_version VARCHAR(127) NULL;
ALTER TABLE test_sessions_transient
  ADD COLUMN device_name VARCHAR(127) NULL;
