-- recreate view: removal of work items of type task
DROP VIEW IF EXISTS quarterly_view;
DROP VIEW IF EXISTS last_7_days_view;
DROP VIEW IF EXISTS weekly_view;
DROP VIEW IF EXISTS monthly_view;
DROP VIEW IF EXISTS last_14_days_view;
DROP VIEW IF EXISTS last_30_days_view;
DROP VIEW IF EXISTS nightly_view CASCADE;

DROP VIEW IF EXISTS last_24_hours_view;
CREATE VIEW last_24_hours_view AS (
    SELECT
        row_number() OVER ()                                                    AS id,
        projects.name                                                           AS project,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner_username,
        test_configs.env                                                        AS env,
        CASE
            WHEN (test_configs.platform IS NULL OR test_configs.platform = '')
            THEN 'N/A'
            ELSE test_configs.platform
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        test_configs.browser                                                    AS browser,
        test_configs.browser_version                                            AS browser_version,
        CASE
            WHEN (test_configs.app_version IS NULL)
            THEN ''
            ELSE test_configs.app_version
        END                                                                     AS app_version,
        test_configs.device                                                     AS device,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job_name,
        test_suites.user_id                                                     AS job_owner_id,
        test_suites.name                                                        AS test_suite_name,
        test_suites.file_name                                                   AS test_suite_file,
        test_runs.id                                                            AS test_run_id,
        test_runs.status                                                        AS test_run_status,
        test_runs.build_number                                                  AS build_number,
        '<a href="test-runs/' || test_runs.id || '/tests/' || tests.id ||
        '" target="_blank">' || tests.name || '</a>'                            AS test_info_url,
        test_runs.elapsed                                                       AS elapsed,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS upstream_job_build_number,
        test_runs.reviewed                                                      AS reviewed,
        upstream_jobs.name                                                      AS upstream_job_name,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        priority_labels.value                                                   AS priority,
        feature_labels.value                                                    AS feature,
        bug_items.jira_id                                                       AS bug,
        bug_items.description                                                   AS bug_subject,
        tests.name                                                              AS test_name,
        tests.message                                                           AS message,
        tests.message_hash_code                                                 AS message_hashcode,
        tests.known_issue                                                       AS test_known_issue,
        test_cases.test_method                                                  AS test_method_name,
        '<a href="dashboards/' ||
        (SELECT id FROM dashboards WHERE title = 'Stability') ||
        '?testCaseId=' || test_cases.id || '">' || test_cases.test_method ||
        '</a>'                                                                  AS stability_url,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        AVG(tests.finish_time - tests.start_time)                               AS avg_time
    FROM tests
    JOIN test_runs ON test_runs.id = tests.test_run_id
    JOIN test_cases ON tests.test_case_id = test_cases.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    JOIN projects ON test_runs.project_id = projects.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    JOIN users ON test_cases.primary_owner_id = users.id
    JOIN test_suites ON test_runs.test_suite_id = test_suites.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    LEFT JOIN tests_labels feature_test_labels ON tests.id = feature_test_labels.test_id
        AND feature_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'feature')
    LEFT JOIN labels feature_labels ON feature_test_labels.label_id = feature_labels.id
        AND feature_labels.key = 'feature'
    LEFT JOIN test_work_items bug_work_items ON tests.id = bug_work_items.test_id
    LEFT JOIN work_items bug_items ON bug_work_items.work_item_id = bug_items.id
    WHERE test_runs.started_at >= (current_date - interval '1 day')
    GROUP BY
        projects.name,
        test_runs.id,
        users.id,
        test_configs.env,
        test_configs.platform,
        test_configs.platform_version,
        test_configs.browser,
        test_configs.browser_version,
        test_configs.device,
        test_configs.locale,
        test_configs.app_version,
        tests.id,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        test_suites.name,
        test_suites.file_name,
        test_suites.user_id,
        priority_labels.value,
        feature_labels.value,
        bug_items.jira_id,
        bug_items.description,
        tests.name,
        tests.message,
        tests.message_hash_code,
        tests.known_issue,
        test_cases.id
);

CREATE VIEW nightly_view AS (
    SELECT * FROM last_24_hours_view WHERE started_at >= current_date
);


DROP MATERIALIZED VIEW IF EXISTS last_32_days_view;
CREATE MATERIALIZED VIEW last_32_days_view AS (
    SELECT
        row_number() OVER ()                                                    AS id,
        projects.name                                                           AS project,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner_username,
        test_configs.env                                                        AS env,
        CASE
            WHEN (test_configs.platform IS NULL OR test_configs.platform = '')
            THEN 'N/A'
            ELSE test_configs.platform
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        test_configs.browser                                                    AS browser,
        test_configs.browser_version                                            AS browser_version,
        CASE
            WHEN (test_configs.app_version IS NULL)
            THEN ''
            ELSE test_configs.app_version
        END                                                                     AS app_version,
        test_configs.device                                                     AS device,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job_name,
        test_suites.user_id                                                     AS job_owner_id,
        test_suites.name                                                        AS test_suite_name,
        test_suites.file_name                                                   AS test_suite_file,
        test_runs.id                                                            AS test_run_id,
        test_runs.status                                                        AS test_run_status,
        test_runs.build_number                                                  AS build_number,
        '<a href="test-runs/' || test_runs.id || '/tests/' || tests.id ||
        '" target="_blank">' || tests.name || '</a>'                            AS test_info_url,
        test_runs.elapsed                                                       AS elapsed,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS upstream_job_build_number,
        test_runs.reviewed                                                      AS reviewed,
        upstream_jobs.name                                                      AS upstream_job_name,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        priority_labels.value                                                   AS priority,
        feature_labels.value                                                    AS feature,
        bug_items.jira_id                                                       AS bug,
        bug_items.description                                                   AS bug_subject,
        tests.name                                                              AS test_name,
        tests.message                                                           AS message,
        tests.message_hash_code                                                 AS message_hashcode,
        tests.known_issue                                                       AS test_known_issue,
        test_cases.test_method                                                  AS test_method_name,
        '<a href="dashboards/' ||
        (SELECT id FROM dashboards WHERE title = 'Stability')
        || '?testCaseId=' || test_cases.id || '">' || test_cases.test_method
        || '</a>'                                                               AS stability_url,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        AVG(tests.finish_time - tests.start_time)                               AS avg_time
    FROM tests
    JOIN test_runs ON test_runs.id = tests.test_run_id
    JOIN test_cases ON tests.test_case_id = test_cases.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    JOIN projects ON test_runs.project_id = projects.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    JOIN users ON test_cases.primary_owner_id = users.id
    JOIN test_suites ON test_runs.test_suite_id = test_suites.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    LEFT JOIN tests_labels feature_test_labels ON tests.id = feature_test_labels.test_id
        AND feature_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'feature')
    LEFT JOIN labels feature_labels ON feature_test_labels.label_id = feature_labels.id
        AND feature_labels.key = 'feature'
    LEFT JOIN test_work_items bug_work_items ON tests.id = bug_work_items.test_id
    LEFT JOIN work_items bug_items ON bug_work_items.work_item_id = bug_items.id
    WHERE test_runs.started_at > current_date - 32 AND test_runs.started_at < current_date
    GROUP BY
        projects.name,
        test_runs.id,
        tests.id,
        users.id,
        test_configs.env,
        test_configs.platform,
        test_configs.platform_version,
        test_configs.browser,
        test_configs.browser_version,
        test_configs.device,
        test_configs.locale,
        test_configs.app_version,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        test_suites.name,
        test_suites.file_name,
        test_suites.user_id,
        priority_labels.value,
        feature_labels.value,
        bug_items.jira_id,
        bug_items.description,
        tests.name,
        tests.message,
        tests.message_hash_code,
        tests.known_issue,
        test_cases.id
);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_INDEX;
CREATE UNIQUE INDEX LAST_32_DAYS_VIEW_INDEX ON LAST_32_DAYS_VIEW (ID);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_PROJECT_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_PROJECT_INDEX ON LAST_32_DAYS_VIEW (PROJECT);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_OWNER_ID_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_OWNER_ID_INDEX ON LAST_32_DAYS_VIEW (OWNER_ID);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_OWNER_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_OWNER_INDEX ON LAST_32_DAYS_VIEW (OWNER_USERNAME);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_PLATFORM_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_PLATFORM_INDEX ON LAST_32_DAYS_VIEW (PLATFORM);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_PLATFORM_VERSION_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_PLATFORM_VERSION_INDEX ON LAST_32_DAYS_VIEW (PLATFORM_VERSION);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_BROWSER_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_BROWSER_INDEX ON LAST_32_DAYS_VIEW (BROWSER);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_BROWSER_VERSION_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_BROWSER_VERSION_INDEX ON LAST_32_DAYS_VIEW (BROWSER_VERSION);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_APP_VERSION_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_APP_VERSION_INDEX ON LAST_32_DAYS_VIEW (APP_VERSION);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_DEVICE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_DEVICE_INDEX ON LAST_32_DAYS_VIEW (DEVICE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_LOCALE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_LOCALE_INDEX ON LAST_32_DAYS_VIEW (LOCALE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_JOB_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_JOB_INDEX ON LAST_32_DAYS_VIEW (JOB_URL);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_JOB_NAME_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_JOB_NAME_INDEX ON LAST_32_DAYS_VIEW (JOB_NAME);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_JOB_OWNER_ID_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_JOB_OWNER_ID_INDEX ON LAST_32_DAYS_VIEW (JOB_OWNER_ID);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_SUITE_NAME_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_SUITE_NAME_INDEX ON LAST_32_DAYS_VIEW (TEST_SUITE_NAME);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_SUITE_FILE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_SUITE_FILE_INDEX ON LAST_32_DAYS_VIEW (TEST_SUITE_FILE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_ENV_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_ENV_INDEX ON LAST_32_DAYS_VIEW (ENV);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_RUN_ID_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_RUN_ID_INDEX ON LAST_32_DAYS_VIEW (TEST_RUN_ID);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_RUN_STATUS_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_RUN_STATUS_INDEX ON LAST_32_DAYS_VIEW (TEST_RUN_STATUS);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_BUILD_NUMBER_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_BUILD_NUMBER_INDEX ON LAST_32_DAYS_VIEW (BUILD_NUMBER);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_INFO_URL_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_INFO_URL_INDEX ON LAST_32_DAYS_VIEW (TEST_INFO_URL);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_ELAPSED_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_ELAPSED_INDEX ON LAST_32_DAYS_VIEW (ELAPSED);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_STARTED_AT_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_STARTED_AT_INDEX ON LAST_32_DAYS_VIEW (STARTED_AT);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_UPSTREAM_JOB_ID_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_UPSTREAM_JOB_ID_INDEX ON LAST_32_DAYS_VIEW (UPSTREAM_JOB_ID);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_UPSTREAM_JOB_BUILD_NUMBER_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_UPSTREAM_JOB_BUILD_NUMBER_INDEX ON LAST_32_DAYS_VIEW (UPSTREAM_JOB_BUILD_NUMBER);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_REVIEWED_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_REVIEWED_INDEX ON LAST_32_DAYS_VIEW (REVIEWED);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_UPSTREAM_JOB_NAME_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_UPSTREAM_JOB_NAME_INDEX ON LAST_32_DAYS_VIEW (UPSTREAM_JOB_NAME);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_UPSTREAM_JOB_URL_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_UPSTREAM_JOB_URL_INDEX ON LAST_32_DAYS_VIEW (UPSTREAM_JOB_URL);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_PRIORITY_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_PRIORITY_INDEX ON LAST_32_DAYS_VIEW (PRIORITY);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_FEATURE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_FEATURE_INDEX ON LAST_32_DAYS_VIEW (FEATURE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_BUG_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_BUG_INDEX ON LAST_32_DAYS_VIEW (BUG);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_BUG_SUBJECT_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_BUG_SUBJECT_INDEX ON LAST_32_DAYS_VIEW (BUG_SUBJECT);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_NAME_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_NAME_INDEX ON LAST_32_DAYS_VIEW (TEST_NAME);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_MESSAGE_HASHCODE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_MESSAGE_HASHCODE_INDEX ON LAST_32_DAYS_VIEW (MESSAGE_HASHCODE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_KNOWN_ISSUE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_KNOWN_ISSUE_INDEX ON LAST_32_DAYS_VIEW (TEST_KNOWN_ISSUE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TEST_CASE_NAME_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TEST_CASE_NAME_INDEX ON LAST_32_DAYS_VIEW (TEST_METHOD_NAME);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_STABILITY_URL_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_STABILITY_URL_INDEX ON LAST_32_DAYS_VIEW (STABILITY_URL);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_PASSED_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_PASSED_INDEX ON LAST_32_DAYS_VIEW (PASSED);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_FAILED_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_FAILED_INDEX ON LAST_32_DAYS_VIEW (FAILED);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_KNOWN_ISSUE_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_KNOWN_ISSUE_INDEX ON LAST_32_DAYS_VIEW (KNOWN_ISSUE);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_SKIPPED_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_SKIPPED_INDEX ON LAST_32_DAYS_VIEW (SKIPPED);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_ABORTED_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_ABORTED_INDEX ON LAST_32_DAYS_VIEW (ABORTED);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_IN_PROGRESS_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_IN_PROGRESS_INDEX ON LAST_32_DAYS_VIEW (IN_PROGRESS);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TOTAL_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TOTAL_INDEX ON LAST_32_DAYS_VIEW (TOTAL);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_TOTAL_SECONDS_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_TOTAL_SECONDS_INDEX ON LAST_32_DAYS_VIEW (TOTAL_SECONDS);

DROP INDEX IF EXISTS LAST_32_DAYS_VIEW_AVG_TIME_INDEX;
CREATE INDEX LAST_32_DAYS_VIEW_AVG_TIME_INDEX ON LAST_32_DAYS_VIEW (AVG_TIME);


CREATE VIEW last_30_days_view AS (
    SELECT * FROM last_32_days_view WHERE started_at >= (current_date - interval '30 day')
    UNION ALL
    SELECT * FROM nightly_view
);


CREATE VIEW last_14_days_view AS (
    SELECT * FROM last_30_days_view WHERE started_at >= (current_date - interval '14 day')
);


CREATE VIEW monthly_view AS (
    SELECT * FROM last_30_days_view WHERE started_at >= date_trunc('month', current_date)
);


CREATE VIEW weekly_view AS (
    SELECT * FROM last_30_days_view WHERE started_at >= date_trunc('week', current_date) - interval '2 day'
);


CREATE VIEW last_7_days_view AS (
    SELECT * FROM last_30_days_view WHERE started_at >= current_date - interval '7 day'
);


DROP MATERIALIZED VIEW IF EXISTS last_93_days_view;
CREATE MATERIALIZED VIEW last_93_days_view AS (
    SELECT
        row_number() OVER ()                                                    AS ID,
        projects.name                                                           AS project,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner_username,
        test_configs.env                                                        AS env,
        CASE
            WHEN (test_configs.platform IS NULL OR test_configs.platform = '')
            THEN 'N/A'
            ELSE test_configs.platform
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        test_configs.browser                                                    AS browser,
        test_configs.browser_version                                            AS browser_version,
        CASE
            WHEN (test_configs.app_version IS NULL)
            THEN ''
            ELSE test_configs.app_version
        END                                                                     AS app_version,
        test_configs.device                                                     AS device,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job_name,
        test_suites.user_id                                                     AS job_owner_id,
        test_suites.name                                                        AS test_suite_name,
        test_suites.file_name                                                   AS test_suite_file,
        test_runs.id                                                            AS test_run_id,
        test_runs.status                                                        AS test_run_status,
        test_runs.build_number                                                  AS build_number,
        '<a href="test-runs/' || test_runs.id || '/tests/' || tests.id ||
        '" target="_blank">' || tests.name || '</a>'                            AS test_info_url,
        test_runs.elapsed                                                       AS elapsed,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS upstream_job_build_number,
        test_runs.reviewed                                                      AS reviewed,
        upstream_jobs.name                                                      AS upstream_job_name,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        priority_labels.value                                                   AS priority,
        feature_labels.value                                                    AS feature,
        bug_items.jira_id                                                       AS bug,
        bug_items.description                                                   AS bug_subject,
        tests.name                                                              AS test_name,
        tests.message                                                           AS message,
        tests.message_hash_code                                                 AS message_hashcode,
        tests.known_issue                                                       AS test_known_issue,
        test_cases.test_method                                                  AS test_method_name,
        '<a href="dashboards/' ||
        (SELECT id FROM dashboards WHERE title = 'Stability') || '?testCaseId='
        || test_cases.id || '">' || test_cases.test_method || '</a>'            AS stability_url,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        AVG(tests.finish_time - tests.start_time)                               AS avg_time
    FROM tests
    JOIN test_runs ON test_runs.id = tests.test_run_id
    JOIN test_cases ON tests.test_case_id = test_cases.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    JOIN projects ON test_runs.project_id = projects.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    JOIN users ON test_cases.primary_owner_id = users.id
    JOIN test_suites ON test_runs.test_suite_id = test_suites.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    LEFT JOIN tests_labels feature_test_labels ON tests.id = feature_test_labels.test_id
        AND feature_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'feature')
    LEFT JOIN labels feature_labels ON feature_test_labels.label_id = feature_labels.id
        AND feature_labels.key = 'feature'
    LEFT JOIN test_work_items bug_work_items ON tests.id = bug_work_items.test_id
    LEFT JOIN work_items bug_items ON bug_work_items.work_item_id = bug_items.id
    WHERE test_runs.started_at > current_date - 93 AND test_runs.started_at < current_date
    GROUP BY
        projects.name,
        test_runs.id,
        tests.id,
        users.id,
        test_configs.env,
        test_configs.platform,
        test_configs.platform_version,
        test_configs.browser,
        test_configs.browser_version,
        test_configs.device,
        test_configs.locale,
        test_configs.app_version,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        test_suites.name,
        test_suites.file_name,
        test_suites.user_id,
        priority_labels.value,
        feature_labels.value,
        bug_items.jira_id,
        bug_items.description,
        tests.name,
        tests.message,
        tests.message_hash_code,
        tests.known_issue,
        test_cases.id
);


DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_INDEX;
CREATE UNIQUE INDEX LAST_93_DAYS_VIEW_INDEX ON LAST_93_DAYS_VIEW (ID);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_PROJECT_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_PROJECT_INDEX ON LAST_93_DAYS_VIEW (PROJECT);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_OWNER_ID_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_OWNER_ID_INDEX ON LAST_93_DAYS_VIEW (OWNER_ID);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_OWNER_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_OWNER_INDEX ON LAST_93_DAYS_VIEW (OWNER_USERNAME);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_PLATFORM_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_PLATFORM_INDEX ON LAST_93_DAYS_VIEW (PLATFORM);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_PLATFORM_VERSION_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_PLATFORM_VERSION_INDEX ON LAST_93_DAYS_VIEW (PLATFORM_VERSION);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_BROWSER_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_BROWSER_INDEX ON LAST_93_DAYS_VIEW (BROWSER);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_BROWSER_VERSION_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_BROWSER_VERSION_INDEX ON LAST_93_DAYS_VIEW (BROWSER_VERSION);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_APP_VERSION_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_APP_VERSION_INDEX ON LAST_93_DAYS_VIEW (APP_VERSION);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_DEVICE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_DEVICE_INDEX ON LAST_93_DAYS_VIEW (DEVICE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_LOCALE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_LOCALE_INDEX ON LAST_93_DAYS_VIEW (LOCALE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_JOB_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_JOB_INDEX ON LAST_93_DAYS_VIEW (JOB_URL);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_JOB_NAME_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_JOB_NAME_INDEX ON LAST_93_DAYS_VIEW (JOB_NAME);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_JOB_OWNER_ID_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_JOB_OWNER_ID_INDEX ON LAST_93_DAYS_VIEW (JOB_OWNER_ID);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_SUITE_NAME_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_SUITE_NAME_INDEX ON LAST_93_DAYS_VIEW (TEST_SUITE_NAME);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_SUITE_FILE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_SUITE_FILE_INDEX ON LAST_93_DAYS_VIEW (TEST_SUITE_FILE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_ENV_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_ENV_INDEX ON LAST_93_DAYS_VIEW (ENV);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_RUN_ID_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_RUN_ID_INDEX ON LAST_93_DAYS_VIEW (TEST_RUN_ID);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_RUN_STATUS_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_RUN_STATUS_INDEX ON LAST_93_DAYS_VIEW (TEST_RUN_STATUS);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_BUILD_NUMBER_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_BUILD_NUMBER_INDEX ON LAST_93_DAYS_VIEW (BUILD_NUMBER);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_INFO_URL_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_INFO_URL_INDEX ON LAST_93_DAYS_VIEW (TEST_INFO_URL);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_ELAPSED_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_ELAPSED_INDEX ON LAST_93_DAYS_VIEW (ELAPSED);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_STARTED_AT_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_STARTED_AT_INDEX ON LAST_93_DAYS_VIEW (STARTED_AT);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_UPSTREAM_JOB_ID_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_UPSTREAM_JOB_ID_INDEX ON LAST_93_DAYS_VIEW (UPSTREAM_JOB_ID);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_UPSTREAM_JOB_BUILD_NUMBER_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_UPSTREAM_JOB_BUILD_NUMBER_INDEX ON LAST_93_DAYS_VIEW (UPSTREAM_JOB_BUILD_NUMBER);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_REVIEWED_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_REVIEWED_INDEX ON LAST_93_DAYS_VIEW (REVIEWED);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_UPSTREAM_JOB_NAME_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_UPSTREAM_JOB_NAME_INDEX ON LAST_93_DAYS_VIEW (UPSTREAM_JOB_NAME);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_UPSTREAM_JOB_URL_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_UPSTREAM_JOB_URL_INDEX ON LAST_93_DAYS_VIEW (UPSTREAM_JOB_URL);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_PRIORITY_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_PRIORITY_INDEX ON LAST_93_DAYS_VIEW (PRIORITY);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_FEATURE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_FEATURE_INDEX ON LAST_93_DAYS_VIEW (FEATURE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_BUG_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_BUG_INDEX ON LAST_93_DAYS_VIEW (BUG);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_BUG_SUBJECT_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_BUG_SUBJECT_INDEX ON LAST_93_DAYS_VIEW (BUG_SUBJECT);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_NAME_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_NAME_INDEX ON LAST_93_DAYS_VIEW (TEST_NAME);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_MESSAGE_HASHCODE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_MESSAGE_HASHCODE_INDEX ON LAST_93_DAYS_VIEW (MESSAGE_HASHCODE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_KNOWN_ISSUE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_KNOWN_ISSUE_INDEX ON LAST_93_DAYS_VIEW (TEST_KNOWN_ISSUE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TEST_CASE_NAME_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TEST_CASE_NAME_INDEX ON LAST_93_DAYS_VIEW (TEST_METHOD_NAME);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_STABILITY_URL_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_STABILITY_URL_INDEX ON LAST_93_DAYS_VIEW (STABILITY_URL);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_PASSED_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_PASSED_INDEX ON LAST_93_DAYS_VIEW (PASSED);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_FAILED_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_FAILED_INDEX ON LAST_93_DAYS_VIEW (FAILED);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_KNOWN_ISSUE_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_KNOWN_ISSUE_INDEX ON LAST_93_DAYS_VIEW (KNOWN_ISSUE);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_SKIPPED_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_SKIPPED_INDEX ON LAST_93_DAYS_VIEW (SKIPPED);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_ABORTED_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_ABORTED_INDEX ON LAST_93_DAYS_VIEW (ABORTED);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_IN_PROGRESS_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_IN_PROGRESS_INDEX ON LAST_93_DAYS_VIEW (IN_PROGRESS);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TOTAL_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TOTAL_INDEX ON LAST_93_DAYS_VIEW (TOTAL);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_TOTAL_SECONDS_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_TOTAL_SECONDS_INDEX ON LAST_93_DAYS_VIEW (TOTAL_SECONDS);

DROP INDEX IF EXISTS LAST_93_DAYS_VIEW_AVG_TIME_INDEX;
CREATE INDEX LAST_93_DAYS_VIEW_AVG_TIME_INDEX ON LAST_93_DAYS_VIEW (AVG_TIME);


CREATE VIEW quarterly_view AS (
    SELECT * FROM last_93_days_view WHERE started_at >= date_trunc('quarter', current_date)
    UNION ALL
    SELECT * FROM nightly_view
);




DROP MATERIALIZED VIEW IF EXISTS total_view;
CREATE MATERIALIZED VIEW total_view AS (
    SELECT
        row_number() OVER ()                                                                   AS id,
        projects.name                                                                          AS project,
        users.id                                                                               AS owner_id,
        users.username                                                                         AS owner_username,
        upstream_jobs.name                                                                     AS upstream_job_name,
        priority_labels.value                                                                  AS priority,
        feature_labels.value                                                                   AS feature,
        bug_items.jira_id                                                                      AS bug,
        test_configs.env                                                                       AS env,
        CASE
            WHEN (test_configs.platform IS NULL OR test_configs.platform = '')
            THEN 'N/A'
            ELSE test_configs.platform
        END                                                                                    AS platform,
        test_configs.browser                                                                   AS browser,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                               AS passed,
        SUM(CASE WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE THEN 1 ELSE 0 END) AS failed,
        SUM(CASE WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE THEN 1 ELSE 0 END)  AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)                              AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)                              AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                          AS in_progress,
        COUNT(*)                                                                               AS total,
        date_trunc('day', test_runs.started_at)                                                AS started_at,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint                AS total_seconds,
        CASE
            WHEN (date_trunc('month', TESTS.START_TIME) = date_trunc('month', current_date))
            THEN ROUND(SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time))) / EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day', date_trunc('month', current_date) + interval '1 month') - interval '1 day'))
            ELSE SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint
        END                                                                                    AS total_eta_seconds,
        AVG(tests.finish_time - tests.start_time)                                              AS avg_time
    FROM tests
    JOIN test_runs ON tests.test_run_id = test_runs.id
    JOIN test_cases ON tests.test_case_id = test_cases.id
    JOIN users ON test_cases.primary_owner_id = users.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    JOIN projects ON test_runs.project_id = projects.id
    JOIN test_configs ON test_runs.config_id = test_configs.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    LEFT JOIN tests_labels feature_test_labels ON tests.id = feature_test_labels.test_id
        AND feature_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'feature')
    LEFT JOIN labels feature_labels ON feature_test_labels.label_id = feature_labels.id
        AND feature_labels.key = 'feature'
    LEFT JOIN test_work_items bug_work_items ON tests.id = bug_work_items.test_id
    LEFT JOIN work_items bug_items ON bug_work_items.work_item_id = bug_items.id
    WHERE test_runs.started_at < current_date
    GROUP BY
        projects.name,
        owner_id,
        owner_username,
        upstream_job_name,
        test_configs.env,
        test_configs.platform, test_configs.browser,
        date_trunc('month', tests.start_time),
        date_trunc('day', test_runs.started_at),
        priority_labels.value,
        feature_labels.value,
        bug_items.jira_id
);

DROP INDEX IF EXISTS TOTAL_VIEW_INDEX;
CREATE UNIQUE INDEX TOTAL_VIEW_INDEX ON TOTAL_VIEW (ID);

DROP INDEX IF EXISTS TOTAL_VIEW_PROJECT_INDEX;
CREATE INDEX TOTAL_VIEW_PROJECT_INDEX ON TOTAL_VIEW (PROJECT);

DROP INDEX IF EXISTS TOTAL_VIEW_OWNER_ID_INDEX;
CREATE INDEX TOTAL_VIEW_OWNER_ID_INDEX ON TOTAL_VIEW (OWNER_ID);

DROP INDEX IF EXISTS TOTAL_VIEW_OWNER_INDEX;
CREATE INDEX TOTAL_VIEW_OWNER_INDEX ON TOTAL_VIEW (OWNER_USERNAME);

DROP INDEX IF EXISTS TOTAL_VIEW_UPSTREAM_JOB_NAME_INDEX;
CREATE INDEX TOTAL_VIEW_UPSTREAM_JOB_NAME_INDEX ON TOTAL_VIEW (UPSTREAM_JOB_NAME);

DROP INDEX IF EXISTS TOTAL_VIEW_PRIORITY_INDEX;
CREATE INDEX TOTAL_VIEW_PRIORITY_INDEX ON TOTAL_VIEW (PRIORITY);

DROP INDEX IF EXISTS TOTAL_VIEW_FEATURE_INDEX;
CREATE INDEX TOTAL_VIEW_FEATURE_INDEX ON TOTAL_VIEW (FEATURE);

DROP INDEX IF EXISTS TOTAL_VIEW_BUG_INDEX;
CREATE INDEX TOTAL_VIEW_BUG_INDEX ON TOTAL_VIEW (BUG);

DROP INDEX IF EXISTS TOTAL_VIEW_ENV_INDEX;
CREATE INDEX TOTAL_VIEW_ENV_INDEX ON TOTAL_VIEW (ENV);

DROP INDEX IF EXISTS TOTAL_VIEW_PLATFORM_INDEX;
CREATE INDEX TOTAL_VIEW_PLATFORM_INDEX ON TOTAL_VIEW (PLATFORM);

DROP INDEX IF EXISTS TOTAL_VIEW_BROWSER_INDEX;
CREATE INDEX TOTAL_VIEW_BROWSER_INDEX ON TOTAL_VIEW (BROWSER);

DROP INDEX IF EXISTS TOTAL_VIEW_PASSED_INDEX;
CREATE INDEX TOTAL_VIEW_PASSED_INDEX ON TOTAL_VIEW (PASSED);

DROP INDEX IF EXISTS TOTAL_VIEW_FAILED_INDEX;
CREATE INDEX TOTAL_VIEW_FAILED_INDEX ON TOTAL_VIEW (FAILED);

DROP INDEX IF EXISTS TOTAL_VIEW_KNOWN_ISSUE_INDEX;
CREATE INDEX TOTAL_VIEW_KNOWN_ISSUE_INDEX ON TOTAL_VIEW (KNOWN_ISSUE);

DROP INDEX IF EXISTS TOTAL_VIEW_SKIPPED_INDEX;
CREATE INDEX TOTAL_VIEW_SKIPPED_INDEX ON TOTAL_VIEW (SKIPPED);

DROP INDEX IF EXISTS TOTAL_VIEW_ABORTED_INDEX;
CREATE INDEX TOTAL_VIEW_ABORTED_INDEX ON TOTAL_VIEW (ABORTED);

DROP INDEX IF EXISTS TOTAL_VIEW_IN_PROGRESS_INDEX;
CREATE INDEX TOTAL_VIEW_IN_PROGRESS_INDEX ON TOTAL_VIEW (IN_PROGRESS);

DROP INDEX IF EXISTS TOTAL_VIEW_TOTAL_INDEX;
CREATE INDEX TOTAL_VIEW_TOTAL_INDEX ON TOTAL_VIEW (TOTAL);

DROP INDEX IF EXISTS TOTAL_VIEW_STARTED_AT_INDEX;
CREATE INDEX TOTAL_VIEW_STARTED_AT_INDEX ON TOTAL_VIEW (STARTED_AT);

DROP INDEX IF EXISTS TOTAL_VIEW_TOTAL_SECONDS_INDEX;
CREATE INDEX TOTAL_VIEW_TOTAL_SECONDS_INDEX ON TOTAL_VIEW (TOTAL_SECONDS);

DROP INDEX IF EXISTS TOTAL_VIEW_TOTAL_ETA_SECONDS_INDEX;
CREATE INDEX TOTAL_VIEW_TOTAL_ETA_SECONDS_INDEX ON TOTAL_VIEW (TOTAL_ETA_SECONDS);

DROP INDEX IF EXISTS TOTAL_VIEW_AVG_TIME_INDEX;
CREATE INDEX TOTAL_VIEW_AVG_TIME_INDEX ON TOTAL_VIEW (AVG_TIME);
