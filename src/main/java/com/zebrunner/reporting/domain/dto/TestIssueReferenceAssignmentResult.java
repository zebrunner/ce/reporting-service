package com.zebrunner.reporting.domain.dto;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.List;

@Value
public class TestIssueReferenceAssignmentResult {

    List<Item> testNotFoundItems;
    List<Item> testNotLogicallyFailedItems;
    List<Item> assignedToTestItems;

    @Value
    @Builder
    @JGlobalMap(excluded = "testId")
    public static class Item {

        Long testId;
        Long id;
        IssueReference.Type type;
        String value;
        Instant createdAt;

    }
}
