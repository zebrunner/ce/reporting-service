package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.db.TestCase;
import com.zebrunner.reporting.domain.db.TestResult;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestMapper;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.SearchResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestSearchCriteria;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.hash.FailureReasonHashFunction;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.integration.jira.JiraService;
import com.zebrunner.reporting.service.legacy.LabelConverter;
import com.zebrunner.reporting.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestService {

    private static final String INV_COUNT = "InvCount";
    private final TestMapper testMapper;
    private final JiraService jiraService;
    private final UserService userService;
    private final Test2Service test2Service;
    private final TestRunService testRunService;
    private final TestCaseService testCaseService;
    private final JiraConfigService jiraConfigService;
    private final FailureReasonHashFunction hashFunction;
    private final IssueReferenceService issueReferenceService;

    @Transactional
    public Test startTest(Test test) {
        return startTest(test, existsById(test.getId()));
    }

    @Transactional
    public Test startTest(Test test, boolean rerun) {
        Long anonymousId = userService.retrieveAnonymous().getId();
        test.setMaintainerId(anonymousId);

        boolean existsById = test.getId() != null && existsById(test.getId());
        Test existingTest = null;
        if (existsById) {
            existingTest = getTestById(test.getId());
        }
        if (rerun) {
            // do rerun only if it was explicitly requested
            rerun = test.getId() != null && existsById;
        }

        test.setStatus(Status.IN_PROGRESS);

        if (rerun) {
            unlinkStatisticsFailureItems(existingTest);
            test.setMessage(null);
            test.setFinishTime(null);
            testMapper.update(test);
        } else {
            if (existingTest == null) {
                testMapper.create(test);
            } else {
                testMapper.update(test);
            }
        }

        if (!CollectionUtils.isEmpty(test.getLabels())) {
            com.zebrunner.reporting.domain.entity.Test updatedWithLabels = test2Service.attachLabels(
                    test.getId(), LabelConverter.toNewer(test.getLabels())
            );
            test.setLabels(LabelConverter.toOlder(updatedWithLabels.getLabels()));
        }

        return test;
    }

    @Transactional
    public Test finishTest(Test test) {
        Test existingTest = getTestById(test.getId());

        Long testCaseId = existingTest.getTestCaseId();

        // Wrap all additional test finalization logic to make sure status saved
        try {
            String message = test.getMessage();
            if (message != null) {
                int messageHashcode = hashFunction.calculateHash(message);
                existingTest.setMessageHashCode(messageHashcode);
                existingTest.setMessage(message);
            }

            tryAssignKnownIssue(test, existingTest);

            updateTestCaseStatus(testCaseId, test.getStatus());

            if (!CollectionUtils.isEmpty(test.getLabels())) {
                com.zebrunner.reporting.domain.entity.Test updatedWithLabels = test2Service.attachLabels(
                        test.getId(), LabelConverter.toNewer(test.getLabels())
                );
                existingTest.setLabels(LabelConverter.toOlder(updatedWithLabels.getLabels()));
            }
        } catch (Exception e) {
            log.error("Test finalization error: " + e.getMessage());
        } finally {
            existingTest.setFinishTime(test.getFinishTime());
            existingTest.setStatus(test.getStatus());

            testMapper.update(existingTest);
        }

        return existingTest;
    }

    private void tryAssignKnownIssue(Test test, Test existingTest) {
        if (Status.FAILED.equals(test.getStatus()) && jiraConfigService.isEnabled()) {
            int reasonHashCode = hashFunction.calculateHash(test.getMessage());
            issueReferenceService.retrieveLinkedToTestCaseByFailureReasonHash(existingTest.getTestCaseId(), reasonHashCode)
                                 .filter(issueReference -> !isCompletedByType(issueReference))
                                 .ifPresent(knownIssue -> {
                                     existingTest.setKnownIssue(true);
                                     existingTest.setIssueReferences(List.of(knownIssue));
                                 });
        }
    }

    private boolean isCompletedByType(IssueReference issueReference) {
        String issueId = issueReference.getValue();
        if (IssueReference.Type.JIRA == issueReference.getType()) {
            try {
                return jiraService.isIssueCompleted(issueId);
            } catch (RuntimeException e) {
                log.warn("Failed to check if issue '{}' of type JIRA - assuming it is not", issueId, e);
                return false;
            }
        } else {
            log.warn("Failed to check if issue '{}' of type {} - currently unsupported", issueId, issueReference.getType());
            return false;
        }
    }

    public void skipTest(Test test) {
        test.setStatus(Status.SKIPPED);
        testMapper.update(test);
    }

    public void abortTest(Test test, String abortCause) {
        test.setStatus(Status.ABORTED);
        test.setMessage(abortCause);
        testMapper.update(test);
    }

    @Transactional
    public Test changeTestStatus(long id, Status newStatus) {
        Test test = getTestById(id);

        boolean markAsPassed = Status.FAILED.equals(test.getStatus()) && !Status.FAILED.equals(newStatus);
        if (markAsPassed) {
            unlinkStatisticsFailureItems(test);
        }

        test.setStatus(newStatus);
        testMapper.update(test);
        updateTestCaseStatus(test.getTestCaseId(), test.getStatus());
        testRunService.calculateTestRunResult(test.getTestRunId());

        return test;
    }

    /**
     * Need test update to avoid issues in the future
     *
     * @param test to proccess
     */
    private void unlinkStatisticsFailureItems(Test test) {
        if (test.isKnownIssue()) {
            test.setIssueReferences(null);
            test.setKnownIssue(false);
        }
    }

    private void updateTestCaseStatus(Long testCaseId, Status status) {
        testCaseService.updateStatuses(List.of(testCaseId), status);
    }

    @Transactional
    public List<Test> batchStatusUpdate(Long testRunId, List<Long> ids, Status status) {
        List<Test> tests = ids.stream()
                              .map(id -> getTestByIdAndTestRunId(id, testRunId))
                              .collect(Collectors.toList());
        List<Long> testCaseIds = tests.stream()
                                      .map(Test::getTestCaseId)
                                      .collect(Collectors.toList());

        tests.forEach(test -> {

            boolean markAsPassed = Status.FAILED.equals(test.getStatus()) && !Status.FAILED.equals(status);
            if (markAsPassed) {
                unlinkStatisticsFailureItems(test);
                testMapper.update(test); // need to update knownIssue = false
            }

            test.setStatus(status);
        });

        // Make sure that statuses updating follows after statistics change
        testMapper.updateStatuses(ids, status);
        testCaseService.updateStatuses(testCaseIds, status);

        testRunService.calculateTestRunResult(testRunId);

        return tests;
    }

    @Transactional(readOnly = true)
    public Test getTestById(Long id) {
        return testMapper.findById(id)
                         .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_NOT_FOUND_BY_ID, id));
    }

    public Test getTestByIdAndTestRunId(long id, long testRunId) {
        return testMapper.findByIdAndTestRunId(id, testRunId)
                         .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_NOT_FOUND_BY_ID, id));
    }

    private boolean existsById(Long id) {
        return testMapper.existsById(id);
    }

    @Transactional(readOnly = true)
    public List<Test> retrieveAllByIds(List<Long> ids) {
        return testMapper.findAllByIds(ids);
    }

    @Transactional(readOnly = true)
    public List<Test> getTestsByTestRunId(long testRunId) {
        return testMapper.findByTestRunId(testRunId);
    }

    @Transactional(readOnly = true)
    public List<TestResult> getLatestTestResultsByTestId(Long testId, Long limit) {
        Test test = getTestById(testId);
        List<TestResult> testResults = testMapper
                .findResultsByStartTimeAndTestCaseId(test.getTestCaseId(), test.getStartTime(), limit);
        testResults.forEach(result -> {
            LocalDateTime startTime = result.getStartTime();
            LocalDateTime finishTime = result.getFinishTime();
            if (startTime != null && finishTime != null) {
                result.setElapsed(Duration.between(startTime, finishTime).toMillis());
            }
        });
        return testResults;
    }

    public List<Test> getTestsByTestRunId(String testRunId) {
        return testRunId.matches("\\d+")
               ? testMapper.findByTestRunId(Long.parseLong(testRunId))
               : testMapper.findByTestRunCiRunId(testRunId);
    }

    @Transactional(readOnly = true)
    public SearchResult<Test> searchTests(TestSearchCriteria sc) {
        List<Test> tests = testMapper.search(sc);

        tests.forEach(test -> {
            // null out correlation data since it is necessary only for test agents
            test.setCorrelationData(null);
            Set<ArtifactReference> sortedArtifacts = new TreeSet<>(Comparator.comparing((ArtifactReference reference) -> reference.getName().toLowerCase(Locale.ROOT))
                                                                             .thenComparing(ArtifactReference::getValue, Comparator.nullsLast(Comparator.naturalOrder())));
            sortedArtifacts.addAll(test.getArtifactReferences());
            test.setArtifactReferences(sortedArtifacts);
        });

        int count = testMapper.findSearchCount(sc);

        return SearchResult.<Test>builder()
                           .page(sc.getPage())
                           .pageSize(sc.getPageSize())
                           .sortOrder(sc.getSortOrder())
                           .results(tests)
                           .totalResults(count)
                           .build();
    }

    @Transactional
    // TODO: 10/22/19 refactor it
    public void updateTestRerunFlags(List<Test> tests) {
        List<Long> testIds = tests.stream()
                                  .map(Test::getId)
                                  .collect(Collectors.toList());
        testMapper.updateTestsNeedRerun(testIds, false);

        try {
            // Look #Test implements comparable so that all ABORTED, SKIPPED and FAILED tests go first
            Collections.sort(tests);

            List<Long> testCaseIds = tests.stream()
                                          .map(Test::getTestCaseId)
                                          .collect(Collectors.toList());

            Map<Long, TestCase> testCasesById = new HashMap<>();
            Map<String, List<Long>> testCasesByMethod = new HashMap<>();

            List<TestCase> testCases = testCaseService.retrieveByIds(testCaseIds);


            for (TestCase testCase : testCases) {
                testCasesByMethod.putIfAbsent(testCase.getTestMethod(), new ArrayList<>());

                testCasesById.put(testCase.getId(), testCase);
                testCasesByMethod.get(testCase.getTestMethod()).add(testCase.getId());
            }

            Set<Long> testCasesToRerun = new HashSet<>();
            for (Test test : tests) {
                boolean isTestFailed = test.isLogicallyFailed() && !test.isKnownIssue();
                boolean isTestAborted = test.getStatus().equals(Status.ABORTED);
                if (isTestFailed || isTestAborted) {
                    String methodName = testCasesById.get(test.getTestCaseId()).getTestMethod();

                    if (test.getName().contains(INV_COUNT)) {
                        testCasesToRerun.addAll(testCasesByMethod.get(methodName));
                    } else {
                        testMapper.updateTestsNeedRerun(Collections.singletonList(test.getId()), true);
                    }

                    String dependsOnMethods = test.getDependsOnMethods();
                    if (StringUtils.isNotEmpty(dependsOnMethods)) {
                        for (String method : dependsOnMethods.split(" ")) {
                            testCasesToRerun.addAll(testCasesByMethod.get(method));
                        }
                    }

                }
            }

            testIds = tests.stream()
                           .filter(test -> testCasesToRerun.contains(test.getTestCaseId()))
                           .map(Test::getId)
                           .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Unable to calculate rurun flags", e);
        } finally {
            testMapper.updateTestsNeedRerun(testIds, true);
        }
    }

}
