package com.zebrunner.reporting.service.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OperationNotImplementedError implements com.zebrunner.common.eh.error.OperationNotImplementedError {

    INVALID_TEST_RUN_INITIATED_BY_HUMAN(2600),
    INVALID_TEST_RUN_INITIATED_BY_UPSTREAM_JOB(2601);

    private final int code;

}
