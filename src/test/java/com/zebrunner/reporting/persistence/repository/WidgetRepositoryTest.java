package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Widget;
import com.zebrunner.reporting.persistence.H2Config;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {H2Config.class})
@ActiveProfiles("Test")
@Transactional
public class WidgetRepositoryTest extends JpaRepositoryGenericTest<WidgetRepository, Widget> {

    @ParameterizedTest
    @ValueSource(longs = {1001, 1002, 1003, 1004})
    @SqlGroup({
            @Sql(value = "/db/createWidgetTemplates.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(value = "/db/createWidgets.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(value = "/db/deleteWidgets.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD),
            @Sql(value = "/db/deleteWidgetTemplates.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    })
    public void findById_ShouldFindWidget_WhenWidgetWithThisIdExists(Long id) {
        super.findByIdTest(id);
    }

    @ParameterizedTest
    @MethodSource("TestDataProvider#provideWidgets")
    public void save_ShouldInsertNewWidget_WhenNoSuchWidgetInDB(Widget widget) {
        super.saveTest(widget);
    }

    @ParameterizedTest
    @ValueSource(longs = {1001, 1002, 1003, 1004})
    @SqlGroup({
            @Sql(value = "/db/createWidgetTemplates.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(value = "/db/createWidgets.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(value = "/db/deleteWidgets.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD),
            @Sql(value = "/db/deleteWidgetTemplates.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    })
    public void deleteById_ShouldDeleteWidget_WhenWidgetWithThisIdExists(Long id) {
        super.deleteByIdTest(id);
    }

}
