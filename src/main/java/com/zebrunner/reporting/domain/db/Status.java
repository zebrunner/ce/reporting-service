package com.zebrunner.reporting.domain.db;

public enum Status {

    UNKNOWN, // only used for test cases
    IN_PROGRESS,
    PASSED,
    FAILED,
    SKIPPED,
    ABORTED,

    // for test runs only
    QUEUED

}
