package com.zebrunner.reporting.service.user;

public interface UserReassignmentListener {

    void onUserReassignment(Long fromId, Long toId);

}
