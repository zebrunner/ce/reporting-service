INSERT INTO artifact_references (name, value)
SELECT DISTINCT name, link
FROM test_artifacts
ON CONFLICT DO NOTHING;

INSERT INTO artifact_references (name, value)
SELECT DISTINCT name, link
FROM test_run_artifacts
ON CONFLICT DO NOTHING;

INSERT INTO tests_artifact_references (test_id, artifact_reference_id)
SELECT test_id, (SELECT id FROM artifact_references ar WHERE ar.name = ta.name and ar.value = ta.link)
FROM test_artifacts ta
ON CONFLICT DO NOTHING;

INSERT INTO test_runs_artifact_references (test_run_id, artifact_reference_id)
SELECT test_run_id, (SELECT id FROM artifact_references ar WHERE tra.name = tra.name and ar.value = tra.link)
FROM test_run_artifacts tra
ON CONFLICT DO NOTHING;
