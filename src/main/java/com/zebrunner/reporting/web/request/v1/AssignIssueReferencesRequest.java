package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@JGlobalMap
@NoArgsConstructor
public class AssignIssueReferencesRequest {

    @NotEmpty
    @Size(max = 50)
    List<@Valid Item> items;

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Item {

        @NotNull
        private Long testId;
        @NotEmpty
        private String type;
        @NotBlank
        private String value;

    }

}
