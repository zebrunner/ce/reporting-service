package com.zebrunner.reporting.web.response.v1;

import lombok.Value;

@Value
public class ErrorResponseItem {

    Long testId;
    boolean succeeded = false;
    ErrorResponse error;

    private ErrorResponseItem(Long testId, ErrorReason reason, String message) {
        this.testId = testId;
        this.error = new ErrorResponse(reason, message);
    }

    public static ErrorResponseItem failed(Long testId, ErrorReason reason) {
        return new ErrorResponseItem(testId, reason, null);
    }

    public static ErrorResponseItem failed(Long testId, ErrorReason reason, String message) {
        return new ErrorResponseItem(testId, reason, message);
    }

}
