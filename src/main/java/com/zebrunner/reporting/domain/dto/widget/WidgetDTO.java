package com.zebrunner.reporting.domain.dto.widget;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class WidgetDTO {

    private Long id;

    @NotEmpty(message = "{error.title.required}")
    private String title;

    private String description;
    private String paramsConfig;
    private String legendConfig;
    private String type;
    private String location;

    @Valid
    private WidgetTemplateDTO widgetTemplate;

}
