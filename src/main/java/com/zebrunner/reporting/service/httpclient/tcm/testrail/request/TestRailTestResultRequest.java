package com.zebrunner.reporting.service.httpclient.tcm.testrail.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRailTestResultRequest {

    private Long testId;
    private Integer statusId;
    private String comment;
    private String defects;
    private Long assignedtoId;

}
