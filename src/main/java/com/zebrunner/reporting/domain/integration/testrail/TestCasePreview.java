package com.zebrunner.reporting.domain.integration.testrail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestCasePreview {

    private String title;
    private String priority;
    private String preconditions;
    private String steps;
    private String expected;
    private List<TestCaseStep> separatedSteps;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TestCaseStep {

        private String content;
        private String expected;

    }

}
