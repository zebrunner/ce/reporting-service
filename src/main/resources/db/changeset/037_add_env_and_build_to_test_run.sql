ALTER TABLE test_runs
  ADD COLUMN environment varchar(63) NULL;
ALTER TABLE test_runs
  ADD COLUMN build varchar(255) NULL;

UPDATE test_runs tr
SET environment = (SELECT tc.env FROM test_configs tc WHERE tc.id = tr.config_id)
WHERE tr.config_id IS NOT NULL;

UPDATE test_runs tr
SET build = (SELECT tc.app_version FROM test_configs tc WHERE tc.id = tr.config_id)
WHERE tr.config_id IS NOT NULL;
