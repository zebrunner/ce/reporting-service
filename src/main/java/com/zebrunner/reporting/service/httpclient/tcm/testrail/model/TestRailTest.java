package com.zebrunner.reporting.service.httpclient.tcm.testrail.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRailTest {

    private Long caseId;
    private Integer statusId;
    private String comment;
    private String defects;
    private Long assignedtoId;

}
