package com.zebrunner.reporting.domain.entity.integration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "issue_reference_assignment_events")
public class IssueReferenceAssignmentEvent {

    @Id
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "issue_reference_id")
    private IssueReference issueReference;

    @Column(name = "test_function_id")
    private Long testCaseId;

    private Long userId;

    private Integer failureReasonHash;

    @Enumerated(EnumType.STRING)
    private Action action;

    @CreationTimestamp
    private Instant createdAt;

    public enum Action {

        LINK,
        UNLINK

    }

    public static IssueReferenceAssignmentEvent linkingOf(IssueReference issueReference, Long testCaseId, int failureReasonHash, Long reporterId) {
        return IssueReferenceAssignmentEvent.builder()
                                            .testCaseId(testCaseId)
                                            .issueReference(issueReference)
                                            .userId(reporterId)
                                            .failureReasonHash(failureReasonHash)
                                            .action(Action.LINK)
                                            .build();
    }

    public static IssueReferenceAssignmentEvent unlikingOf(IssueReference issueReference, Long testCaseId, int failureReasonHash, Long reporterId) {
        return IssueReferenceAssignmentEvent.builder()
                                            .testCaseId(testCaseId)
                                            .issueReference(issueReference)
                                            .userId(reporterId)
                                            .failureReasonHash(failureReasonHash)
                                            .action(Action.UNLINK)
                                            .build();
    }

}
