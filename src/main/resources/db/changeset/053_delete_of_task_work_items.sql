DELETE FROM test_work_items twi
    USING work_items wi
WHERE twi.work_item_id = wi.id AND wi.type = 'TASK';

DELETE FROM work_items WHERE type = 'TASK';
