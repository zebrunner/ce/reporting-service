package com.zebrunner.reporting.service.integration.slack;

import com.slack.api.bolt.App;
import com.slack.api.methods.MethodsClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SlackConfiguration {

    @Bean
    public MethodsClient client() {
        return new App().client();
    }

}
