package com.zebrunner.reporting.web.response.v1.testrail;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Value
public class GetTestCasePreviewResponse {

    List<Item> items;

    @Data
    @JGlobalMap
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Item {

        private String title;
        private String priority;
        private String preconditions;
        private String steps;
        private String expected;
        private List<SeparateStep> separatedSteps;

    }

    @Data
    @JGlobalMap
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SeparateStep {

        private String content;
        private String expected;

    }

}
