package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.DashboardWidget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface DashboardWidgetRepository extends JpaRepository<DashboardWidget, Long> {

    Optional<DashboardWidget> findByDashboardIdAndWidgetId(Long dashboardId, Long widgetId);

    @Query("SELECT dw FROM DashboardWidget dw " +
           "JOIN FETCH dw.widget w " +
           "JOIN FETCH w.widgetTemplate wt " +
           "WHERE dw.dashboardId = :dashboardId " +
           "  AND wt.id = :widgetTemplateId " +
           "  AND upper(w.title) = upper(:widgetTitle)")
    Optional<DashboardWidget> findInDashboardByTitleAndTemplateId(@Param("dashboardId") Long dashboardId,
                                                                  @Param("widgetTitle") String widgetTitle,
                                                                  @Param("widgetTemplateId") Long widgetTemplateId);

    @Query("SELECT dw FROM DashboardWidget dw " +
           "JOIN FETCH dw.widget w " +
           "JOIN FETCH w.widgetTemplate wt " +
           "WHERE dw.dashboardId IN (:dashboardIds) " +
           "  AND wt.id = :widgetTemplateId " +
           "  AND upper(w.title) = upper(:widgetTitle)")
    List<DashboardWidget> findAllInDashboardsByTitleAndTemplateId(@Param("dashboardIds") List<Long> dashboardIds,
                                                                  @Param("widgetTitle") String widgetTitle,
                                                                  @Param("widgetTemplateId") Long widgetTemplateId);

    List<DashboardWidget> findAllByWidgetId(Long widgetId);

    void deleteByDashboardIdAndWidgetId(Long dashboardId, Long widgetId);
}
