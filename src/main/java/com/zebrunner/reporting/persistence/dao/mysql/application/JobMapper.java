package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.Job;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface JobMapper {

    void create(Job job);

    List<Job> findAll();

    Optional<Job> findByJobUrl(String jobURL);

    void update(Job job);

    void reassignUser(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

}
