package com.zebrunner.reporting.domain.entity.integration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "integration_testrail_configs")
public class TestRailConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private Long id;
    private String url;
    private String username;
    private String password;

    @EqualsAndHashCode.Exclude
    private boolean enabled;

    @EqualsAndHashCode.Exclude
    private boolean encrypted;

}
