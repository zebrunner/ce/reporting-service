package com.zebrunner.reporting.web.patch;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PatchRequestListValidator extends BasePatchRequestValidator<PatchAction.List> {

    @Override
    public void initialize(PatchAction.List constraintAnnotation) {
        operationToAllowedPaths = Stream.of(constraintAnnotation.value())
                                        .collect(Collectors.toMap(
                                                PatchAction::op,
                                                patchAction -> Set.of(patchAction.path()),
                                                this::concat,
                                                () -> new EnumMap<>(PatchOperation.class)
                                        ));
    }

    private <T> Set<T> concat(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.addAll(set2);
        return set;
    }

}
