package com.zebrunner.reporting.service.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StreamUtils {

    public static <T, R> Set<R> mapToSet(Collection<T> collection, Function<T, R> mapper) {
        return collection.stream()
                         .map(mapper)
                         .collect(Collectors.toSet());
    }

    public static <T, R> List<R> mapToList(Collection<T> collection, Function<T, R> mapper) {
        return collection.stream()
                         .map(mapper)
                         .collect(Collectors.toList());
    }

    public static <T> Optional<T> findFirst(Collection<T> collection, Predicate<T> filter) {
        return collection.stream()
                         .filter(filter)
                         .findFirst();
    }

    public static <T> List<T> filterToList(Collection<T> collection, Predicate<T> filter) {
        return collection.stream()
                         .filter(filter)
                         .collect(Collectors.toList());
    }

    public static <C, K, V> Map<K, V> toMap(Collection<C> collection, Function<C, K> keyMapper, Function<C, V> valueMapper) {
        return collection.stream()
                         .collect(Collectors.toMap(keyMapper, valueMapper, (v, $) -> v));
    }

    public static <K, T> Map<K, T> mapToIdentityByKey(Collection<T> collection, Function<T, K> keyMapper) {
        return toMap(collection, keyMapper, Function.identity());
    }

    public static <T> List<T> concat(Collection<T> collection1, Collection<T> collection2) {
        return Stream.concat(collection1.stream(), collection2.stream())
                     .collect(Collectors.toList());
    }

}
