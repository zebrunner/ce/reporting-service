package com.zebrunner.reporting.persistence.dao.mysql.application.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchCriteria {

    private String query;
    private String orderBy;

    // Pages are zero-based
    private Integer page = 1;
    // The very default page size, just not to get NPE
    private Integer pageSize = 20;
    private SortOrder sortOrder = SortOrder.ASC;

    public enum SortOrder {
        ASC, DESC
    }

    public Integer getOffset() {
        return (page - 1) * pageSize;
    }

}
