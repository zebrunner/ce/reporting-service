package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.UserFavoriteTestRunFilter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserFavoriteTestRunFilterRepository extends JpaRepository<UserFavoriteTestRunFilter, Integer> {

    List<UserFavoriteTestRunFilter> findByUserId(Integer userId);

    Optional<UserFavoriteTestRunFilter> findByUserIdAndTestRunFilterId(Integer userId, Integer testRunFilterId);

}
