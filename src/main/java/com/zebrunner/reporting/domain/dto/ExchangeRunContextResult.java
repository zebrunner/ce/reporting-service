package com.zebrunner.reporting.domain.dto;

import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.vo.RunContext;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;

import java.util.List;

@Value
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ExchangeRunContextResult {

    boolean runAllowed;
    String reason;
    List<Test> testsToRun;
    RunContext fullExecutionPlanContext;

    public static ExchangeRunContextResult runNotAllowed(String reason) {
        return new ExchangeRunContextResult(false, reason, null, null);
    }

    public static ExchangeRunContextResult runAllowed() {
        return new ExchangeRunContextResult(true, null, null, null);
    }

    public static ExchangeRunContextResult runAllowedFor(List<Test> testsToRun) {
        return new ExchangeRunContextResult(true, null, testsToRun, null);
    }

    public static ExchangeRunContextResult runAllowedFor(List<Test> testsToRun, RunContext fullExecutionPlanContext) {
        return new ExchangeRunContextResult(true, null, testsToRun, fullExecutionPlanContext);
    }

}
