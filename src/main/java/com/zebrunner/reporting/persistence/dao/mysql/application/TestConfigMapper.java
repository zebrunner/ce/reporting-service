package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.TestConfig;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TestConfigMapper {

    void create(TestConfig testConfig);

    Optional<TestConfig> search(TestConfig testConfig);

    List<String> findPlatforms();

    List<String> findBrowsers();

    List<String> findEnvironments();

    List<String> findLocales();

}
