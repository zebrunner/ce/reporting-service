package com.zebrunner.reporting.domain.integration.jira;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IssuePreview {

    private String key;
    private String projectName;
    private String status;
    private String priority;
    private String issueType;
    private String assignee;
    private String reporter;
    private String summary;
    private String description;

}
