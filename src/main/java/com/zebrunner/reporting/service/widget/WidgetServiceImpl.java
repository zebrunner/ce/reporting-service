package com.zebrunner.reporting.service.widget;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.DashboardWidget;
import com.zebrunner.reporting.domain.entity.Widget;
import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.persistence.repository.WidgetRepository;
import com.zebrunner.reporting.service.DashboardService;
import com.zebrunner.reporting.service.dashboard.DashboardWidgetService;
import com.zebrunner.reporting.service.widgettemplate.WidgetTemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.zebrunner.reporting.service.exception.BusinessConstraintError.WIDGET_ON_DASHBOARD_ALREADY_EXISTS;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.WIDGET_NOT_FOUND_BY_ID;

@Slf4j
@Service
@RequiredArgsConstructor
public class WidgetServiceImpl implements WidgetService {

    private final WidgetRepository widgetRepository;

    private final DashboardService dashboardService;
    private final WidgetTemplateService widgetTemplateService;

    @Lazy
    private final DashboardWidgetService dashboardWidgetService;

    @Override
    @Transactional
    public Widget create(Long dashboardId, Widget widget, Long widgetTemplateId) {
        dashboardService.verifyExistenceById(dashboardId);
        WidgetTemplate widgetTemplate = widgetTemplateService.retrieveById(widgetTemplateId);

        dashboardWidgetService.retrieveOptionalInDashboardByTitleAndTemplateId(dashboardId, widget.getTitle(), widgetTemplateId)
                              .ifPresent($ -> {
                                  throw new BusinessConstraintException(WIDGET_ON_DASHBOARD_ALREADY_EXISTS, dashboardId);
                              });

        widget.setType(widgetTemplate.getType().name());
        widget.setWidgetTemplate(widgetTemplate);

        return widgetRepository.save(widget);
    }

    @Override
    @Transactional(readOnly = true)
    public Widget retrieveById(Long id) {
        return widgetRepository.findById(id)
                               .orElseThrow(() -> new ResourceNotFoundException(WIDGET_NOT_FOUND_BY_ID, id));
    }

    @Override
    public void verifyExistenceById(Long id) {
        retrieveById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Widget> retrieveAll() {
        return widgetRepository.findAll();
    }

    @Override
    @Transactional
    public Widget update(Long id, Widget newWidget) {
        Widget widget = retrieveById(id);
        List<Long> dashboardIdsWidgetIn = dashboardWidgetService.retrieveAllByWidgetId(id)
                                                                .stream()
                                                                .map(DashboardWidget::getDashboardId)
                                                                .collect(Collectors.toList());
        Long templateId = widget.getWidgetTemplate().getId();
        dashboardWidgetService.retrieveAllInDashboardsByTitleAndTemplateId(dashboardIdsWidgetIn, newWidget.getTitle(), templateId)
                              .forEach(dashboardWidget -> {
                                  if (!id.equals(dashboardWidget.getWidget().getId())) {
                                      throw new BusinessConstraintException(WIDGET_ON_DASHBOARD_ALREADY_EXISTS, dashboardWidget.getDashboardId());
                                  }
                              });

        widget.setTitle(newWidget.getTitle());
        widget.setDescription(newWidget.getDescription());
        widget.setParamsConfig(newWidget.getParamsConfig());
        widget.setLegendConfig(newWidget.getLegendConfig());

        return widgetRepository.save(widget);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        verifyExistenceById(id);
        widgetRepository.deleteById(id);
    }

}
