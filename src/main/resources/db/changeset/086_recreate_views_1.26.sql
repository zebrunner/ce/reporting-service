--drop obsolete views
DROP VIEW IF EXISTS quarterly_view;
DROP VIEW IF EXISTS last_7_days_view;
DROP VIEW IF EXISTS weekly_view;
DROP VIEW IF EXISTS monthly_view;
DROP VIEW IF EXISTS last_14_days_view;
DROP VIEW IF EXISTS last_30_days_view;
DROP VIEW IF EXISTS nightly_view;
DROP VIEW IF EXISTS last_24_hours_view;
DROP MATERIALIZED VIEW IF EXISTS last_32_days_view;
DROP MATERIALIZED VIEW IF EXISTS last_93_days_view;
DROP MATERIALIZED VIEW IF EXISTS total_view;

DROP VIEW IF EXISTS last_24_hours CASCADE;
DROP VIEW IF EXISTS last_32_days CASCADE;
DROP VIEW IF EXISTS last_93_days CASCADE;

DROP MATERIALIZED VIEW IF EXISTS last_365_days_materialized;
DROP MATERIALIZED VIEW IF EXISTS TOTAL_MATERIALIZED;
DROP VIEW IF EXISTS TOTAL;



--- drop dependent views from total_materialized, year_materialized and month in advance
DROP VIEW IF EXISTS last_365_days;
DROP VIEW IF EXISTS year;
DROP VIEW IF EXISTS TOTAL;

----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS today;
CREATE VIEW today AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON bug_items.id = test_executions.issue_reference_id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= current_date
  AND test_executions.start_time >= current_date
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS last_24_hours;
CREATE VIEW last_24_hours AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON bug_items.id = test_executions.issue_reference_id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= (current_date - interval '1 day')
  AND test_executions.start_time >= (current_date - interval '1 day')
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS week;
CREATE VIEW week AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= date_trunc('week', current_date)
  AND test_executions.start_time >= date_trunc('week', current_date)
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS last_7_days;
CREATE VIEW last_7_days AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= current_date - interval '7 day'
  AND test_executions.start_time >= current_date - interval '7 day'
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS last_14_days;
CREATE VIEW last_14_days AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= current_date - interval '14 day'
  AND test_executions.start_time >= current_date - interval '14 day'
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS month;
CREATE VIEW month AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= date_trunc('month', current_date)
  AND test_executions.start_time >= date_trunc('month', current_date)
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS last_30_days;
CREATE VIEW last_30_days AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= current_date - interval '30 day'
  AND test_executions.start_time >= current_date - interval '30 day'
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS quarter;
CREATE VIEW quarter AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= date_trunc('quarter', current_date)
  AND test_executions.start_time >= date_trunc('quarter', current_date)
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------
DROP VIEW IF EXISTS last_90_days;
CREATE VIEW last_90_days AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON test_executions.issue_reference_id = bug_items.id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= current_date - interval '90 day'
  AND test_executions.start_time >= current_date - interval '90 day'
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- materialized view to collect all historical results till Jan, 1 in the current year!          ---
----------------------------------------------------------------------------------------------------
DROP MATERIALIZED VIEW IF EXISTS TOTAL_MATERIALIZED;
CREATE MATERIALIZED VIEW TOTAL_MATERIALIZED AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON bug_items.id = test_executions.issue_reference_id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at < date_trunc('year', current_date)
  AND test_executions.start_time < date_trunc('year', current_date)
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_ID_INDEX;
CREATE UNIQUE INDEX TOTAL_MATERIALIZED_ID_INDEX ON TOTAL_MATERIALIZED (ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_TEST_RUN_ID_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_TEST_RUN_ID_INDEX ON TOTAL_MATERIALIZED (TEST_RUN_ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_RUN_NAME_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_RUN_NAME_INDEX ON TOTAL_MATERIALIZED (RUN_NAME);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_STARTED_AT_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_STARTED_AT_INDEX ON TOTAL_MATERIALIZED (STARTED_AT);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_ENV_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_ENV_INDEX ON TOTAL_MATERIALIZED (ENV);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BUILD_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BUILD_INDEX ON TOTAL_MATERIALIZED (BUILD);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_RUN_STATUS_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_RUN_STATUS_INDEX ON TOTAL_MATERIALIZED (RUN_STATUS);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_OWNER_ID_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_OWNER_ID_INDEX ON TOTAL_MATERIALIZED (OWNER_ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_OWNER_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_OWNER_INDEX ON TOTAL_MATERIALIZED (OWNER);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_PLATFORM_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_PLATFORM_INDEX ON TOTAL_MATERIALIZED (PLATFORM);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BROWSER_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BROWSER_INDEX ON TOTAL_MATERIALIZED (BROWSER);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_LOCALE_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_LOCALE_INDEX ON TOTAL_MATERIALIZED (LOCALE);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_MESSAGE_HASHCODE_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_MESSAGE_HASHCODE_INDEX ON TOTAL_MATERIALIZED (MESSAGE_HASHCODE);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_PRIORITY_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_PRIORITY_INDEX ON TOTAL_MATERIALIZED (PRIORITY);
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- materialized view to store results from Jan, 1 for current year till completed month          ---
----------------------------------------------------------------------------------------------------
DROP MATERIALIZED VIEW IF EXISTS YEAR_MATERIALIZED;
CREATE MATERIALIZED VIEW YEAR_MATERIALIZED AS
(
SELECT row_number() OVER ()                                                                        AS id,
       test_suite_executions.id                                                                    AS test_run_id,
       test_suite_executions.name                                                                  AS run_name,
       test_suite_executions.started_at                                                            AS started_at,
       test_suite_executions.environment                                                           AS env,
       test_suite_executions.build                                                                 AS build,
       test_suite_executions.status                                                                AS run_status,
       users.id                                                                                    AS owner_id,
       users.username                                                                              AS owner,
       test_execution_configs.platform                                                             AS platform,
       test_execution_configs.platform_version                                                     AS platform_version,
       test_execution_configs.browser                                                              AS browser,
       test_execution_configs.browser_version                                                      AS browser_version,
       test_execution_configs.locale                                                               AS locale,
       bug_items.value                                                                             AS bug,
       test_executions.message_hash_code                                                           AS message_hashcode,
       priority_labels.value                                                                       AS priority,
       SUM(CASE WHEN test_executions.status = 'PASSED' THEN 1 ELSE 0 END)                          AS passed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = FALSE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS failed,
       SUM(
               CASE
                   WHEN test_executions.status = 'FAILED' AND test_executions.known_issue = TRUE
                       THEN 1
                   ELSE 0
                   END
           )                                                                                       AS known_issue,
       SUM(CASE WHEN test_executions.status = 'SKIPPED' THEN 1 ELSE 0 END)                         AS skipped,
       SUM(CASE WHEN test_executions.status = 'ABORTED' THEN 1 ELSE 0 END)                         AS aborted,
       SUM(CASE WHEN test_executions.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)                     AS in_progress,
       COUNT(test_executions.status)                                                               AS total,
       SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint AS total_seconds,
       CASE
           WHEN (date_trunc('month', TEST_EXECUTIONS.START_TIME) = date_trunc('month', current_date))
               THEN ROUND(SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time))) /
                          EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day',
                                                                                       date_trunc('month', current_date) +
                                                                                       interval '1 month') -
                                                                            interval '1 day'))
           ELSE SUM(EXTRACT(epoch FROM (test_executions.finish_time - test_executions.start_time)))::bigint
           END                                                                                     AS total_eta_seconds
FROM test_suite_executions
         JOIN test_executions ON test_suite_executions.id = test_executions.test_suite_execution_id
         JOIN users ON test_executions.maintainer_id = users.id
         LEFT JOIN test_execution_configs ON test_suite_executions.config_id = test_execution_configs.id
         LEFT JOIN issue_references bug_items ON bug_items.id = test_executions.issue_reference_id
         LEFT JOIN test_executions_labels priority_test_execution_labels
                   ON test_executions.id = priority_test_execution_labels.test_execution_id
                       AND priority_test_execution_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
         LEFT JOIN labels priority_labels ON priority_test_execution_labels.label_id = priority_labels.id
    AND priority_labels.key = 'priority'
WHERE test_suite_executions.started_at >= date_trunc('year', current_date)
  and test_suite_executions.started_at < date_trunc('month', current_date)
  AND test_executions.start_time >= date_trunc('year', current_date)
  and test_executions.start_time < date_trunc('month', current_date)
GROUP BY test_suite_executions.id,
         test_execution_configs.id,
         users.id,
         date_trunc('month', TEST_EXECUTIONS.START_TIME),
         bug_items.value,
         test_executions.message_hash_code,
         priority_labels.value
    );

DROP INDEX IF EXISTS YEAR_ID_INDEX;
CREATE UNIQUE INDEX YEAR_ID_INDEX ON YEAR_MATERIALIZED (ID);

DROP INDEX IF EXISTS YEAR_TEST_RUN_ID_INDEX;
CREATE INDEX YEAR_TEST_RUN_ID_INDEX ON YEAR_MATERIALIZED (TEST_RUN_ID);

DROP INDEX IF EXISTS YEAR_RUN_NAME_INDEX;
CREATE INDEX YEAR_RUN_NAME_INDEX ON YEAR_MATERIALIZED (RUN_NAME);

DROP INDEX IF EXISTS YEAR_STARTED_AT_INDEX;
CREATE INDEX YEAR_STARTED_AT_INDEX ON YEAR_MATERIALIZED (STARTED_AT);

DROP INDEX IF EXISTS YEAR_ENV_INDEX;
CREATE INDEX YEAR_ENV_INDEX ON YEAR_MATERIALIZED (ENV);

DROP INDEX IF EXISTS YEAR_BUILD_INDEX;
CREATE INDEX YEAR_BUILD_INDEX ON YEAR_MATERIALIZED (BUILD);

DROP INDEX IF EXISTS YEAR_RUN_STATUS_INDEX;
CREATE INDEX YEAR_RUN_STATUS_INDEX ON TOTAL_MATERIALIZED (RUN_STATUS);

DROP INDEX IF EXISTS YEAR_OWNER_ID_INDEX;
CREATE INDEX YEAR_OWNER_ID_INDEX ON YEAR_MATERIALIZED (OWNER_ID);

DROP INDEX IF EXISTS YEAR_OWNER_INDEX;
CREATE INDEX YEAR_OWNER_INDEX ON YEAR_MATERIALIZED (OWNER);

DROP INDEX IF EXISTS YEAR_PLATFORM_INDEX;
CREATE INDEX YEAR_PLATFORM_INDEX ON YEAR_MATERIALIZED (PLATFORM);

DROP INDEX IF EXISTS YEAR_BROWSER_INDEX;
CREATE INDEX YEAR_BROWSER_INDEX ON YEAR_MATERIALIZED (BROWSER);

DROP INDEX IF EXISTS YEAR_LOCALE_INDEX;
CREATE INDEX YEAR_LOCALE_INDEX ON YEAR_MATERIALIZED (LOCALE);

DROP INDEX IF EXISTS YEAR_MESSAGE_HASHCODE_INDEX;
CREATE INDEX YEAR_MESSAGE_HASHCODE_INDEX ON YEAR_MATERIALIZED (MESSAGE_HASHCODE);

DROP INDEX IF EXISTS YEAR_PRIORITY_INDEX;
CREATE INDEX YEAR_PRIORITY_INDEX ON YEAR_MATERIALIZED (PRIORITY);
----------------------------------------------------------------------------------------------------
CREATE VIEW last_365_days AS
(
SELECT *
FROM TOTAL_MATERIALIZED
WHERE started_at >= current_date - interval '365 day'
UNION ALL
SELECT *
FROM YEAR_MATERIALIZED
UNION ALL
SELECT *
FROM MONTH
    );
----------------------------------------------------------------------------------------------------
CREATE VIEW year AS
(
SELECT *
FROM YEAR_MATERIALIZED
UNION ALL
SELECT *
FROM MONTH
    );
----------------------------------------------------------------------------------------------------
CREATE VIEW TOTAL AS
(
SELECT *
FROM TOTAL_MATERIALIZED
UNION ALL
SELECT *
FROM YEAR_MATERIALIZED
UNION ALL
SELECT *
FROM MONTH
    );
----------------------------------------------------------------------------------------------------
