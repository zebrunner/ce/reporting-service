package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.persistence.H2Config;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {H2Config.class})
@ActiveProfiles("Test")
@Transactional
public class WidgetTemplatesRepositoryTest extends JpaRepositoryGenericTest<WidgetTemplateRepository, WidgetTemplate> {

    @Test
    public void findAll_ShouldFindAllWidgetTemplates_WhenWidgetTemplatesExist() {
        super.findAllTest();
    }

    @ParameterizedTest
    @ValueSource(longs = {1001, 1002, 1003, 1004})
    @SqlGroup({
            @Sql(value = "/db/createWidgetTemplates.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD),
            @Sql(value = "/db/deleteWidgetTemplates.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    })
    public void findById_ShouldFindWidgetTemplate_WhenWidgetTemplateWithThisIdExists(Long id) {
        super.findByIdTest(id);
    }
}
