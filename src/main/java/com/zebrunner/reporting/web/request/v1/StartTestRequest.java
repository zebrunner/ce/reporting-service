package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.OffsetDateTime;
import java.util.Set;

@Data
@JGlobalMap
@NoArgsConstructor
public class StartTestRequest {

    @NotBlank
    private String name;

    private String correlationData;

    @NotBlank
    private String className;

    @NotBlank
    private String methodName;

    @NotNull
    @PastOrPresent
    private OffsetDateTime startedAt;

    private String maintainer;
    private String testCase;

    private Set<@Valid Label> labels = Set.of();

    public void setLabels(Set<Label> labels) {
        if (labels != null) {
            this.labels = labels;
        }
    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Label {

        @NotBlank
        private String key;
        @NotBlank
        private String value;

    }

}
