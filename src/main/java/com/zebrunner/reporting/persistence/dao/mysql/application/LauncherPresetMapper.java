package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.launcher.LauncherPreset;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Optional;

@Mapper
public interface LauncherPresetMapper {

    void create(@Param("preset") LauncherPreset launcherPreset, @Param("launcherId") Long launcherId);

    Optional<LauncherPreset> findById(Long id);

    Optional<LauncherPreset> findByRef(String ref);

    Optional<LauncherPreset> findByIdAndRef(@Param("id") Long id, @Param("ref") String ref);

    boolean existsByNameAndLauncherId(@Param("name") String name, @Param("launcherId") Long launcherId);

    void update(LauncherPreset launcherPreset);

    void updateReference(@Param("id") Long id, @Param("ref") String reference);

    void deleteByIdAndLauncherId(@Param("id") Long id, @Param("launcherId") Long launcherId);

}
