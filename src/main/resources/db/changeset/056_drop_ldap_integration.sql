DELETE FROM integration_settings
WHERE integration_id IN (SELECT id FROM integrations WHERE name = 'LDAP');

DELETE FROM integration_params
WHERE integration_type_id IN (SELECT id FROM integration_types WHERE integration_types.name = 'LDAP');

DELETE FROM test_runs
WHERE job_id IN (SELECT id FROM jobs WHERE automation_server_id IN (SELECT id FROM integrations WHERE integrations.name = 'LDAP'))
   OR
      upstream_job_id IN (SELECT id FROM jobs WHERE automation_server_id IN (SELECT id FROM integrations WHERE integrations.name = 'LDAP'));

DELETE FROM jobs
WHERE automation_server_id IN (SELECT id FROM integrations WHERE integrations.name = 'LDAP');

DELETE FROM integrations
WHERE integration_type_id IN (SELECT id FROM integration_types WHERE integration_types.name = 'LDAP');

DELETE FROM integration_types
WHERE name = 'LDAP';

DELETE FROM integration_groups
WHERE name = 'ACCESS_MANAGEMENT';
