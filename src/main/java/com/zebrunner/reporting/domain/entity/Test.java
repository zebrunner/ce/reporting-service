package com.zebrunner.reporting.domain.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.TypeDef;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "test_executions")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Test {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @EqualsAndHashCode.Include
    private String name;

    @EqualsAndHashCode.Include
    private String correlationData;

    @Column(name = "message")
    @EqualsAndHashCode.Include
    private String reason;
    @EqualsAndHashCode.Include
    @Column(name = "message_hash_code")
    private Integer reasonHashCode;

    @EqualsAndHashCode.Include
    @Column(name = "start_time")
    private OffsetDateTime startedAt;
    @EqualsAndHashCode.Include
    @Column(name = "finish_time")
    private OffsetDateTime endedAt;

    @EqualsAndHashCode.Include
    @Enumerated(EnumType.STRING)
    private Status status;

    private boolean knownIssue;

    @ManyToOne
    @JoinColumn(name = "maintainer_id")
    private User maintainer;

    @ManyToOne
    @JoinColumn(name = "test_function_id")
    private TestCase testCase;

    @Builder.Default
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "test_executions_labels",
            joinColumns = @JoinColumn(name = "test_execution_id"),
            inverseJoinColumns = @JoinColumn(name = "label_id")
    )
    private Set<Label> labels = Set.of();

    @ManyToOne
    @JoinColumn(name = "issue_reference_id")
    private IssueReference issueReference;

    @Builder.Default
    @Column(columnDefinition = "jsonb")
    @org.hibernate.annotations.Type(type = "jsonb")
    private Set<ArtifactReference> artifactReferences = Set.of();

    @ManyToOne
    @JoinColumn(name = "test_suite_execution_id")
    private TestRun testRun;

    /**
     * Indicates if given test execution logically failed or not
     */
    public boolean isLogicallyFailed() {
        return this.status == Status.FAILED || this.status == Status.SKIPPED;
    }


    public enum Status {
        IN_PROGRESS,
        PASSED,
        FAILED,
        SKIPPED,
        ABORTED;

        public static Set<Status> getLogicallyFailed() {
            return Set.of(FAILED, SKIPPED);
        }

    }

}
