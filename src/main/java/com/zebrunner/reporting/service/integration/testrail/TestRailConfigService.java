package com.zebrunner.reporting.service.integration.testrail;

import com.zebrunner.reporting.domain.entity.integration.TestRailConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;

public interface TestRailConfigService {

    TestRailConfig update(TestRailConfig newConfig);

    TestRailConfig retrieve();

    /**
     * Returns a configuration in an active state - meaning that it can be used with integrated service right away.
     * If such configuration does not exists an exception will be thrown.
     */
    TestRailConfig retrieveActive();

    void patchEnabledProperty(Boolean enabled);

    ReachabilityCheckResult isReachable(TestRailConfig testRailConfig);

}
