package com.zebrunner.reporting.web.converter.v1;

import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.web.response.v1.dashboard.WidgetTemplateResponseItem;

public class WidgetTemplateResponseItemConverter {

    public static WidgetTemplateResponseItem toResponseItem(WidgetTemplate template) {
        return WidgetTemplateResponseItem.builder()
                                         .id(template.getId())
                                         .name(template.getName())
                                         .description(template.getDescription())
                                         .type(template.getType())
                                         .chartConfig(template.getChartConfig())
                                         .paramsConfig(template.getParamsConfig())
                                         .legendConfig(template.getLegendConfig())
                                         .hidden(template.getHidden())
                                         .build();
    }

}
