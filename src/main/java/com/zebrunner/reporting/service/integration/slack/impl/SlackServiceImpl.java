package com.zebrunner.reporting.service.integration.slack.impl;

import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import com.slack.api.model.Attachment;
import com.slack.api.webhook.Payload;
import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.entity.integration.SlackConfig;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import com.zebrunner.reporting.service.TestRunResultSummarySender;
import com.zebrunner.reporting.service.integration.slack.SlackConfigService;
import com.zebrunner.reporting.service.integration.slack.utils.AttachmentBuilder;
import com.zebrunner.reporting.service.util.JsonUtils;
import com.zebrunner.reporting.service.util.URLResolver;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class SlackServiceImpl implements TestRunResultSummarySender {

    private static final String PREVIEW_FORMAT = "${status}: ${name} #${testRunId}";

    private final static int CHANNELS_LIMIT_FOR_TR_SUMMARY = 20;

    private final ExecutorService executorService = Executors.newFixedThreadPool(CHANNELS_LIMIT_FOR_TR_SUMMARY);

    private final URLResolver urlResolver;
    private final MethodsClient slackApiClient;
    private final SlackConfigService slackConfigService;

    @Override
    public NotificationTarget.Type getType() {
        return NotificationTarget.Type.SLACK_CHANNELS;
    }

    @Override
    public void sendTestRunResultSummary(String target, TestRunResultSummary summary) {
        Set<String> channels = toChannels(target);
        if (!CollectionUtils.isEmpty(channels)) {
            SlackConfig config = slackConfigService.retrieveActive();
            String preview = buildPreviewMessage(summary);
            Attachment attachment = AttachmentBuilder.build(urlResolver.buildWebURL(), summary);
            channels.forEach(channel -> executorService.submit(() -> postMessage(config, channel, preview, attachment)));
        } else {
            log.debug("Test run (id '{}') summaries not sent: no valid channels", summary.getTestRunId());
        }
    }

    private Set<String> toChannels(String target) {
        return Arrays.stream(target.split("[;,]"))
                     .map(String::trim)
                     .limit(CHANNELS_LIMIT_FOR_TR_SUMMARY)
                     .collect(Collectors.toSet());
    }

    private String buildPreviewMessage(TestRunResultSummary summary) {
        return PREVIEW_FORMAT.replace("${testRunId}", summary.getTestRunId().toString())
                             .replace("${status}", summary.getStatus())
                             .replace("${name}", summary.getName());
    }

    private void postMessage(SlackConfig config, String channel, String preview, Attachment attachment) {
        if (config.getToken().contains("http")) {
            postWithWebhook(config, channel, preview, attachment);
        } else {
            postWithToken(config, channel, preview, attachment);
        }
    }

    private void postWithToken(SlackConfig config, String channel, String preview, Attachment attachment) {
        ChatPostMessageRequest request = ChatPostMessageRequest.builder()
                                                               .token(config.getToken())
                                                               .username(config.getBotName())
                                                               .iconUrl(config.getBotIconUrl())
                                                               .channel(channel)
                                                               .text(preview)
                                                               .attachments(List.of(attachment))
                                                               .build();
        try {
            ChatPostMessageResponse response = slackApiClient.chatPostMessage(request);
            if (!response.isOk()) {
                log.warn("Unable to push Slack notification to channel '{}': {}", channel, response.getError());
            }
        } catch (Exception e) {
            log.error("Unable to push Slack notification to channel '" + channel + "'", e);
        }
    }

    @SuppressWarnings("rawtypes")
    private void postWithWebhook(SlackConfig config, String channel, String preview, Attachment attachment) {
        Payload payload = Payload.builder()
                                 .text(preview)
                                 .channel(channel)
                                 .attachments(List.of(attachment))
                                 .build();
        try {
            HttpResponse response = Unirest.post(config.getToken())
                                           .body(JsonUtils.writeValue(payload))
                                           .asEmpty();
            if (response.getStatus() != 200) {
                log.warn("Unable to push Slack notification to channel '{}': {}", channel, response.getStatus());
            }
        } catch (Exception e) {
            log.error("Unable to push Slack notification to channel '" + channel + "'", e);
        }
    }

}
