package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.DashboardSwitch;
import com.zebrunner.reporting.persistence.repository.DashboardSwitchRepository;
import com.zebrunner.reporting.service.DashboardSwitchService;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DashboardSwitchServiceImpl implements DashboardSwitchService {

    private final DashboardSwitchRepository dashboardSwitchRepository;

    @Override
    @Transactional
    public void save(Long dashboardId, Long userId) {
        boolean isUpdated = dashboardSwitchRepository.update(dashboardId, userId) > 0;
        if (!isUpdated) {
            DashboardSwitch dashboardSwitch = DashboardSwitch.builder()
                                                             .userId(userId)
                                                             .dashboardId(dashboardId)
                                                             .build();
            dashboardSwitchRepository.save(dashboardSwitch);
        }
    }

    @Override
    @Transactional
    public void delete(Long dashboardId, Long userId) {
        if (!dashboardSwitchRepository.existsByDashboardIdAndUserId(dashboardId, userId)) {
            throw new ResourceNotFoundException(ResourceNotFoundError.DASHBOARD_SWITCH_NOT_FOUND);
        }
        dashboardSwitchRepository.deleteByDashboardIdAndUserId(dashboardId, userId);
    }

}
