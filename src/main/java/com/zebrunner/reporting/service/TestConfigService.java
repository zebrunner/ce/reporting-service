package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.TestConfig;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.db.config.Argument;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestConfigMapper;
import com.zebrunner.reporting.service.util.XmlUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Consumer;

@Service
@RequiredArgsConstructor
public class TestConfigService {

    private static final String DEFAULT_PLATFORM = "API";

    @Lazy
    private final TestRunService testRunService;
    private final TestConfigMapper testConfigMapper;

    @Transactional
    public TestConfig getOrCreate(TestConfig testConfig) {
        return testConfigMapper.search(testConfig)
                               .orElseGet(() -> {
                                   testConfigMapper.create(testConfig);
                                   return testConfig;
                               });
    }

    @Transactional
    public TestConfig createTestConfigForTestRun(String configXML) {
        List<Argument> testRunConfig = XmlUtils.readConfiguration(configXML).getArg();

        TestConfig config = new TestConfig().init(testRunConfig);

        return getOrCreate(config);
    }

    /**
     * Updates test run config with provided non-null values.
     * <p>
     * Due to the fact that we are not using serializable isolation level for db transactions,
     * the 'write skew' anomaly can occur on concurrent creation of new config and subsequent assignment to test run.
     * We don't consider such cases as a problem and simply store any config for test run.
     *
     * @param testRunId test run id to update configs
     * @param newConfig partially filled entity with new test run configs
     * @throws ResourceNotFoundException with TEST_RUN_NOT_FOUND_BY_ID error
     *                                   when there is no test run with given id
     */
    @Transactional
    public void updateTestRunConfig(Long testRunId, TestConfig newConfig) {
        TestRun testRun = testRunService.getTestRunById(testRunId);
        TestConfig runConfig = testRun.getConfig();
        if (runConfig == null) {
            runConfig = new TestConfig();
        }

        setIfNotEmpty(newConfig.getBrowser(), runConfig::setBrowser);
        setIfNotEmpty(newConfig.getBrowserVersion(), runConfig::setBrowserVersion);
        setIfNotEmpty(newConfig.getPlatform(), runConfig::setPlatform);
        setIfNotEmpty(newConfig.getPlatformVersion(), runConfig::setPlatformVersion);
        setIfNotEmpty(newConfig.getDevice(), runConfig::setDevice);

        newConfig = getOrCreate(runConfig);
        testRun.setConfig(newConfig);

        testRunService.update(testRun);
    }

    public void setApiPlatformIfNoPlatformAndBrowser(Long testRunId) {
        TestRun testRun = testRunService.getTestRunById(testRunId);
        TestConfig runConfig = testRun.getConfig();
        if (runConfig == null) {
            runConfig = new TestConfig();
        }

        if (runConfig.getPlatform() == null && runConfig.getBrowser() == null) {
            runConfig.setPlatform(DEFAULT_PLATFORM);

            TestConfig createdTestConfig = getOrCreate(runConfig);
            testRun.setConfig(createdTestConfig);

            testRunService.update(testRun);
        }
    }

    public void setPlatform(Long id, String platformName, String platformVersion) {
        TestRun testRun = testRunService.getTestRunById(id);
        TestConfig testConfig = testRun.getConfig();
        if (testConfig == null) {
            testConfig = new TestConfig();
        }

        if (platformName != null || platformVersion != null) {
            setIfNotEmpty(platformName, testConfig::setPlatform);
            setIfNotEmpty(platformVersion, testConfig::setPlatformVersion);

            TestConfig createdTestExecutionConfig = getOrCreate(testConfig);
            testRun.setConfig(createdTestExecutionConfig);
            testRunService.update(testRun);
        }
    }

    private void setIfNotEmpty(String value, Consumer<String> setter) {
        if (StringUtils.isNotBlank(value)) {
            setter.accept(value);
        }
    }

    @Transactional(readOnly = true)
    public List<String> getPlatforms() {
        return testConfigMapper.findPlatforms();
    }

    @Transactional(readOnly = true)
    public List<String> getBrowsers() {
        return testConfigMapper.findBrowsers();
    }

    @Transactional(readOnly = true)
    public List<String> getLocales() {
        return testConfigMapper.findLocales();
    }

    @Transactional(readOnly = true)
    @Cacheable(value = "environments", key = "T( com.zebrunner.reporting.persistence.PersistenceConfig).DEFAULT_SCHEMA + ':' + #result", unless = "#result == null || #result.isEmpty()")
    public List<String> getEnvironments() {
        return testConfigMapper.findEnvironments();
    }

}
