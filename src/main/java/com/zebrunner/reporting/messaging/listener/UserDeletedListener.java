package com.zebrunner.reporting.messaging.listener;

import com.zebrunner.reporting.domain.push.events.UserDeletedMessage;
import com.zebrunner.reporting.messaging.config.Queue;
import com.zebrunner.reporting.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserDeletedListener {

    private final UserService userService;

    @RabbitListener(queues = Queue.USER_DELETED)
    public void handle(UserDeletedMessage user) {
        userService.deleteByName(user.getName());
    }

}
