package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import com.zebrunner.reporting.domain.dto.core.SimpleSearchResult;
import com.zebrunner.reporting.domain.dto.elasticsearch.LogEntry;
import com.zebrunner.reporting.domain.dto.elasticsearch.Screenshot;
import com.zebrunner.reporting.service.TestArtifactsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/test-runs/{testRunId}/tests/{testId}", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestArtifactsController {

    private final TestArtifactsService artifactsService;

    @GetMapping("/logs")
    public SimpleSearchResult<LogEntry> getLogs(
            @PathVariable("testRunId") Long testRunId,
            @PathVariable("testId") Long testId,
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        SearchCriteria searchCriteria = new SearchCriteria(page, pageSize);
        return artifactsService.fetchLogs(testRunId, testId, searchCriteria);
    }

    @GetMapping("/screenshots")
    public SimpleSearchResult<Screenshot> getScreenshots(
            @PathVariable("testRunId") Long testRunId,
            @PathVariable("testId") Long testId,
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize
    ) {
        SearchCriteria searchCriteria = new SearchCriteria(page, pageSize);
        return artifactsService.fetchScreenshots(testRunId, testId, searchCriteria);
    }

}
