package com.zebrunner.reporting.web;

import com.zebrunner.reporting.service.TestConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "api/tests/runs", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class TestRunSearchAttributesController {

    private final TestConfigService testConfigService;

    @GetMapping("/environments")
    public List<String> getEnvironments() {
        return testConfigService.getEnvironments();
    }

    @GetMapping("/platforms")
    public List<String> getPlatforms() {
        return testConfigService.getPlatforms();
    }

    @GetMapping("/browsers")
    public List<String> getBrowsers() {
        return testConfigService.getBrowsers();
    }

    @GetMapping("/locales")
    public List<String> getLocales() {
        return testConfigService.getLocales();
    }


    @GetMapping("/filter-values")
    public Map<String,List<String>> getFilterValues() {
        return Map.of(
                "environments", testConfigService.getEnvironments(),
                "platforms", testConfigService.getPlatforms(),
                "browsers", testConfigService.getBrowsers(),
                "locales", testConfigService.getLocales()
        );
    }

}
