package com.zebrunner.reporting.web.request.v1.jenkins;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

@Data
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
public class CreateJenkinsConfigRequest {

    @NotBlank
    private String name;

    @URL
    @NotBlank
    private String url;

    @NotBlank
    private String username;

    @NotBlank
    private String token;

    private String launcherJobName;

    private boolean enabled;

}
