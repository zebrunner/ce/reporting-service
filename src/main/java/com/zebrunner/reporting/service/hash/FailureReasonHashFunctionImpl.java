package com.zebrunner.reporting.service.hash;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FailureReasonHashFunctionImpl implements FailureReasonHashFunction {

    private static final List<String> SELENIUM_ERRORS = List.of(
            "org.openqa.selenium.remote.UnreachableBrowserException",
            "org.openqa.selenium.TimeoutException",
            "Session"
    );

    @Value("${service.failure-reason-hash.max-reason-length}")
    private final Integer maxReasonLength;

    // the reason hash code is used to group tests with similar errors within a given period of time.
    // 'similar' means that the errors are close to each other, but might not be the same.
    // for example, there are a few selenium errors that should be considered as the same error,
    // but technically they are different one.
    @Override
    public Integer calculateHash(@Nullable String reason) {
        return Optional.ofNullable(reason)
                       .map(reasonString -> SELENIUM_ERRORS.stream()
                                                           .filter(reasonString::startsWith)
                                                           .findFirst()
                                                           .orElse(reason))
                       .map(this::calculateHashCode)
                       .orElse(null);
    }

    private Integer calculateHashCode(String reason) {
        if (reason != null) {
            // next patterns are: "at methodName(className.java:rawNumber)" or "at methodName(Native method)"
            // example: "at org.testng.internal.TestInvoker.retryFailed(TestInvoker.java:214)"
            String normalizedReason = reason
                    .replaceAll("at org.testng.internal.TestInvoker.retryFailed\\([a-zA-Z0-9.:]+\\)", "")
                    .replaceAll("at org.testng.internal.TestInvoker.invokeTestMethod\\([a-zA-Z0-9.:]+\\)", "")
                    .replaceAll("\\d+", "*")
                    .replaceAll("\\[.*]", "*");
            return normalizedReason.substring(0, Math.min(maxReasonLength, normalizedReason.length()))
                                   .hashCode();
        }

        return null;
    }

}
