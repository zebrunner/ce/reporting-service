package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.TestSuite;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestSuiteMapper;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.testsuite.TestSuiteReassignmentListener;
import com.zebrunner.reporting.service.user.UserReassignmentListener;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestSuiteService implements UserReassignmentListener {

    private final TestSuiteMapper testSuiteMapper;
    private final List<TestSuiteReassignmentListener> testSuiteReassignmentListeners;

    public TestSuite getTestSuiteById(long id) {
        return testSuiteMapper.findById(id)
                              .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TEST_SUITE_NOT_FOUND_BY_ID, id));
    }

    @Transactional
    public TestSuite createOrUpdateTestSuite(TestSuite newTestSuite) {
        return testSuiteMapper.findByNameAndFileNameAndUserId(newTestSuite.getName(), newTestSuite.getFileName(), newTestSuite.getUser().getId())
                              .map(testSuite -> {
                                  if (testSuite.equals(newTestSuite)) {
                                      return testSuite;
                                  } else {
                                      newTestSuite.setId(testSuite.getId());
                                      testSuiteMapper.update(newTestSuite);
                                      return newTestSuite;
                                  }
                              }).orElseGet(() -> {
                    testSuiteMapper.create(newTestSuite);
                    return newTestSuite;
                });
    }

    @Override
    @Transactional
    public void onUserReassignment(Long fromId, Long toId) {
        List<TestSuite> fromTestSuites = testSuiteMapper.findAllByUserId(fromId);
        List<TestSuite> toTestSuites = testSuiteMapper.findAllByUserId(toId);

        List<Long> testSuitesIdsToReassign = new ArrayList<>();
        List<Long> testSuitesIdsToDelete = new ArrayList<>();
        fromTestSuites.forEach(fromTestSuite -> {
            Optional<TestSuite> maybeToTestSuite = toTestSuites.stream()
                                                               .filter(toTestSuite -> equalsKeysWhenReassignUser(fromTestSuite, toTestSuite))
                                                               .findFirst();
            maybeToTestSuite.ifPresentOrElse(toTestSuite -> {
                testSuiteReassignmentListeners.forEach(listener -> listener.onTestSuiteReassignment(fromTestSuite.getId(), toTestSuite.getId()));
                testSuitesIdsToDelete.add(fromTestSuite.getId());
            }, () -> {
                testSuitesIdsToReassign.add(fromTestSuite.getId());
            });
        });

        if (!testSuitesIdsToDelete.isEmpty()) {
            testSuiteMapper.deleteInBatchByIds(testSuitesIdsToDelete);
        }

        if (!testSuitesIdsToReassign.isEmpty()) {
            testSuiteMapper.reassignInBatchUser(testSuitesIdsToReassign, toId);
        }
    }

    private boolean equalsKeysWhenReassignUser(TestSuite firstTestSuite, TestSuite secondTestSuite) {
        boolean areSameNames = firstTestSuite.getName().equals(secondTestSuite.getName());
        boolean areSameFileNames = firstTestSuite.getFileName().equals(secondTestSuite.getFileName());

        return areSameNames && areSameFileNames;
    }

}
