package com.zebrunner.reporting.service.util;

import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.config.TestSessionArtifactProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestSessionArtifactsManager {

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss")
                                                                             .withZone(ZoneOffset.UTC);

    private static final String SESSION_ID_PLACEHOLDER = "<session-id>";
    private static final String UTC_TIME_PLACEHOLDER = "<utc-time>";

    private final Test2Service test2Service;
    private final CapabilitiesManager capabilitiesManager;
    private final TestSessionArtifactProperties artifactProperties;

    public void attachVncArtifact(TestSession session) {
        if (capabilitiesManager.isVncEnabled(session)) {
            capabilitiesManager.getVncLink(session)
                               .map(link -> link.replaceAll(SESSION_ID_PLACEHOLDER, session.getSessionId()))
                               .map(link -> ArtifactReference.builder()
                                                             .name(getVncArtifactName(session))
                                                             .value(link)
                                                             .build())
                               .ifPresent(artifactReference -> attachArtifactReference(session, artifactReference));
        }
    }

    public void detachVncArtifact(TestSession session) {
        String vncArtifactName = getVncArtifactName(session);
        session.getArtifactReferences()
               .removeIf(artifactReference -> Objects.equals(artifactReference.getName(), vncArtifactName));
    }

    private String getVncArtifactName(TestSession session) {
        String utcTime = TIME_FORMATTER.format(session.getStartedAt());
        return artifactProperties.getVnc()
                                 .getNamePattern()
                                 .replaceAll(UTC_TIME_PLACEHOLDER, utcTime);
    }

    public void attachCompletionArtifacts(TestSession session) {
        String videoName = artifactProperties.getVideo().getName();
        String logName = artifactProperties.getLog().getName();
        String metadataName = artifactProperties.getMetadata().getName();

        attachArtifact(capabilitiesManager::getVideoLink, session, videoName);
        attachArtifact(capabilitiesManager::getLogLink, session, logName);
        attachArtifact(capabilitiesManager::getMetadataLink, session, metadataName);
    }

    private void attachArtifact(Function<TestSession, Optional<String>> linkExtractor, TestSession session, String artifactName) {
        linkExtractor.apply(session)
                     .map(link -> link.replaceAll(SESSION_ID_PLACEHOLDER, session.getSessionId()))
                     .map(link -> ArtifactReference.builder().name(artifactName).value(link).build())
                     .ifPresent(artifactReference -> attachArtifactReference(session, artifactReference));
    }

    private void attachArtifactReference(TestSession session, ArtifactReference reference) {
        session.getArtifactReferences().removeIf(testReference -> Objects.equals(testReference.getName(), reference.getName()));
        session.getArtifactReferences().add(reference);
    }

    public void copySessionArtifactsToTests(TestSession session) {
        for (Test test : session.getTests()) {
            test2Service.attachArtifactReferences(test.getId(), session.getArtifactReferences());
        }
    }

}
