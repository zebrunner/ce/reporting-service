package com.zebrunner.reporting.messaging.listener;

import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.domain.push.events.ArtifactUploadedMessage;
import com.zebrunner.reporting.messaging.config.Queue;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestRun2Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
public class TestArtifactListener {

    private final Test2Service test2Service;
    private final TestRun2Service testRun2Service;

    @RabbitListener(queues = Queue.SAVE_TEST_ARTIFACT_REFERENCE)
    public void saveTestArtifact(ArtifactUploadedMessage message) {
        try {
            ArtifactReference reference = ArtifactReference.builder()
                                                           .name(message.getName())
                                                           .value(message.getKey())
                                                           .build();
            if (message.getTestId() != null) {
                test2Service.attachArtifactReferences(Long.parseLong(message.getTestId()), Set.of(reference));
            } else {
                testRun2Service.attachArtifactReferences(Long.parseLong(message.getTestRunId()), Set.of(reference));
            }
        } catch (Exception e) {
            log.error("Could not save reference to test or test run artifact: " + message, e);
        }
    }

}
