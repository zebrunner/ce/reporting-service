package com.zebrunner.reporting.web.request.v1.slack;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
public class UpdateSlackConfigRequest {

    @NotBlank
    private String token;
    @NotBlank
    private String botName;
    private String botIconUrl;

    private boolean enabled;

}
