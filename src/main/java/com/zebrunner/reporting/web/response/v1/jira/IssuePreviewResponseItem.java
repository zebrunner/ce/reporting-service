package com.zebrunner.reporting.web.response.v1.jira;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IssuePreviewResponseItem {

    private String key;
    private String status;
    private String assignee;
    private String reporter;
    private String summary;

}
