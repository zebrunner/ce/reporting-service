package com.zebrunner.reporting.service.integration.jira.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import com.zebrunner.reporting.domain.integration.jira.IssuePreview;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.httpclient.jira.JiraClient;
import com.zebrunner.reporting.service.httpclient.jira.model.Issue;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.integration.jira.JiraService;
import com.zebrunner.reporting.service.legacy.IssueConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class JiraServiceImpl implements JiraService {

    private final CryptoService cryptoService;
    private final JiraConfigService jiraConfigService;

    @Override
    public List<IssuePreview> retrieveIssuesByIds(Set<String> issuesIds) {
        JiraConfig config = jiraConfigService.retrieveActive();
        return getClient(config).searchIssuesByIds(issuesIds)
                                .getIssues()
                                .stream()
                                .map(IssueConverter::issueToPreview)
                                .collect(Collectors.toList());
    }

    @Override
    public IssuePreview retrieveIssueById(String issueId) {
        JiraConfig config = jiraConfigService.retrieveActive();
        Issue issue = getClient(config).getOptionalIssueById(issueId)
                                       .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.JIRA_UNABLE_TO_GET_ISSUE_BY_ID, issueId));
        return IssueConverter.issueToPreview(issue);
    }

    @Override
    public boolean isIssueCompleted(String issueId) {
        JiraConfig config = jiraConfigService.retrieveActive();
        Issue issue = getClient(config).getOptionalIssueById(issueId)
                                       .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.JIRA_UNABLE_TO_GET_ISSUE_BY_ID, issueId));
        // chain calls should not generate NPE since this data is always included into resource response structure
        return "done".equals(issue.getFields().getStatus().getStatusCategory().getKey());
    }

    private JiraClient getClient(JiraConfig config) {
        String decryptedToken = config.isEncrypted()
                                ? cryptoService.decrypt(config.getToken())
                                : config.getToken();
        return new JiraClient(config.getUrl(), config.getUsername(), decryptedToken);
    }

}
