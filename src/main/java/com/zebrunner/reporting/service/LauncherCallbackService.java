package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.ExternalSystemException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.db.launcher.LauncherCallback;
import com.zebrunner.reporting.domain.db.launcher.LauncherCallbackResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.LauncherCallbackMapper;
import com.zebrunner.reporting.service.exception.ExternalSystemError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.util.JsonUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
@RequiredArgsConstructor
public class LauncherCallbackService {

    private static final HttpClient httpClient = HttpClient.newHttpClient();

    private final LauncherCallbackMapper launcherCallbackMapper;
    private final TestRunService testRunService;

    @Transactional()
    public LauncherCallback create(LauncherCallback callback) {
        String ref = generateRef();
        callback.setRef(ref);
        launcherCallbackMapper.create(callback);
        return callback;
    }

    @Transactional(readOnly = true)
    public LauncherCallback retrieveByReference(String ref) {
        return launcherCallbackMapper.findByRef(ref)
                                     .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.LAUNCHER_CALLBACK_NOT_FOUND_BY_REF, ref));
    }

    // TODO: 9/13/19 create real launcher callback info response object according future logic
    @Transactional(readOnly = true)
    public TestRun buildInfo(String ref) {
        LauncherCallback callback = retrieveByReference(ref);
        return testRunService.getTestRunByCiRunId(callback.getCiRunId());
    }

    @Transactional(readOnly = true)
    public void notifyOnTestRunFinish(String ciRunId) {
        launcherCallbackMapper.findByCiRunId(ciRunId).ifPresent(callback -> {
            LauncherCallbackResult result = buildCallbackResult(callback.getCiRunId());
            sendCallback(callback.getUrl(), result);
        });
    }

    private String generateRef() {
        return RandomStringUtils.randomAlphabetic(20);
    }

    private LauncherCallbackResult buildCallbackResult(String ciRunId) {
        TestRun testRun = testRunService.getTestRunByCiRunId(ciRunId);
        String htmlReport = testRunService.compileResultsReportAsHtmlString(ciRunId);
        return new LauncherCallbackResult(testRun, htmlReport);
    }

    private void sendCallback(String url, LauncherCallbackResult result) {
        String payload = JsonUtils.writeValue(result);
        URI uri = new DefaultUriBuilderFactory(url).builder()
                                                   .build();
        HttpRequest request = HttpRequest.newBuilder()
                                         .uri(uri)
                                         .POST(HttpRequest.BodyPublishers.ofString(payload))
                                         .build();
        try {
            httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (InterruptedException | IOException e) {
            throw new ExternalSystemException(ExternalSystemError.CANNOT_TO_SEND_CALLBACK_TO_URL, e, url + e.getMessage());
        }
    }

}
