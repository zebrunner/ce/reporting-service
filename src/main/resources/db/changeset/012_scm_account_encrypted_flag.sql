ALTER TABLE scm
  ADD COLUMN access_token_encrypted BOOLEAN DEFAULT TRUE NOT NULL;

UPDATE scm
SET access_token_encrypted = FALSE
WHERE access_token = '${SCM_DEFAULT_ACCOUNT_ACCESS_TOKEN}';
