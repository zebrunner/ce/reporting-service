package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.domain.entity.integration.TestRailConfig;
import com.zebrunner.reporting.domain.integration.testrail.TestCasePreview;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.service.integration.testrail.TestRailConfigService;
import com.zebrunner.reporting.service.integration.testrail.TestRailService;
import com.zebrunner.reporting.web.patch.PatchAction;
import com.zebrunner.reporting.web.patch.PatchOperation;
import com.zebrunner.reporting.web.patch.PatchRequest;
import com.zebrunner.reporting.web.request.v1.testrail.CheckTestRailConnectionRequest;
import com.zebrunner.reporting.web.request.v1.testrail.UpdateTestRailIntegrationRequest;
import com.zebrunner.reporting.web.response.v1.CheckConnectionResponse;
import com.zebrunner.reporting.web.response.v1.testrail.GetTestCasePreviewResponse;
import com.zebrunner.reporting.web.response.v1.testrail.GetTestRailConfigResponse;
import com.zebrunner.reporting.web.response.v1.testrail.UpdateTestRailConfigResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController("v1TestRailController")
@RequiredArgsConstructor
@RequestMapping(path = "/v1/integrations/testrail", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestRailController {

    private final JMapper mapper;
    private final TestRailConfigService configService;
    private final TestRailService testRailService;

    @PutMapping
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public UpdateTestRailConfigResponse update(@Valid @RequestBody UpdateTestRailIntegrationRequest request) {
        TestRailConfig newConfig = mapper.map(request, TestRailConfig.class);
        TestRailConfig config = configService.update(newConfig);
        return mapper.map(config, UpdateTestRailConfigResponse.class);
    }

    @GetMapping
    @PreAuthorize("hasPermission('reporting:integrations:read')")
    public GetTestRailConfigResponse retrieve() {
        TestRailConfig testRailConfig = configService.retrieve();
        return mapper.map(testRailConfig, GetTestRailConfigResponse.class);
    }

    @GetMapping("/test-cases/{testCaseId}")
    public GetTestCasePreviewResponse.Item getTestRailCaseById(@PathVariable("testCaseId") Long testCaseId) {
        TestCasePreview testRailCase = testRailService.retrieveTestCasePreviewById(testCaseId);
        return mapper.map(testRailCase, GetTestCasePreviewResponse.Item.class);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasPermission('reporting:integrations:update')")
    public void patch(@PatchAction(op = PatchOperation.REPLACE, path = "/enabled")
                      @RequestBody PatchRequest patchRequest) {
        Boolean enabled = patchRequest.getReplaceValue("/enabled", Boolean.class);
        if (enabled != null) {
            configService.patchEnabledProperty(enabled);
        }
    }

    @PostMapping("/connectivity-checks")
    public CheckConnectionResponse checkConnection(@Valid @RequestBody CheckTestRailConnectionRequest request) {
        TestRailConfig config = mapper.map(request, TestRailConfig.class);
        ReachabilityCheckResult result = configService.isReachable(config);
        return mapper.map(result, CheckConnectionResponse.class);
    }

}
