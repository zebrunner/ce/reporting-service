package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.dto.reporting.TestRunStart;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.entity.RerunMetadata;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import com.zebrunner.reporting.domain.entity.TestSuite;
import com.zebrunner.reporting.domain.entity.User;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.persistence.repository.TestRunRepository;
import com.zebrunner.reporting.service.Job2Service;
import com.zebrunner.reporting.service.LabelService;
import com.zebrunner.reporting.service.LauncherCallbackService;
import com.zebrunner.reporting.service.NotificationTargetService;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestConfigService;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestRunResultSummaryService;
import com.zebrunner.reporting.service.TestRunService;
import com.zebrunner.reporting.service.TestSessionService;
import com.zebrunner.reporting.service.TestSuite2Service;
import com.zebrunner.reporting.service.user.UserReassignmentListener;
import com.zebrunner.reporting.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_ID;

@Service
@RequiredArgsConstructor
public class TestRun2ServiceImpl implements TestRun2Service, UserReassignmentListener {

    private static final String LOCALE_LABEL = "com.zebrunner.app/sut.locale";

    @Lazy
    private final UserService userService;
    private final Job2Service job2Service;
    private final LabelService labelService;
    private final Test2Service test2Service;
    @Lazy
    private final TestRunService testRunService;
    private final TestRunRepository testRunRepository;
    private final TestSuite2Service testSuite2Service;
    private final TestConfigService testConfigService;
    @Lazy
    private final TestSessionService testSessionService;
    private final TransactionTemplate transactionTemplate;
    @Lazy
    private final LauncherCallbackService launcherCallbackService;
    private final NotificationTargetService notificationTargetService;
    @Lazy
    private final TestRunResultSummaryService testRunResultSummaryService;

    @Override
    public TestRun retrieveById(Long id) {
        return testRunRepository.findById(id)
                                .orElseThrow(() -> new ResourceNotFoundException(TEST_RUN_NOT_FOUND_BY_ID, id));
    }

    @Override
    public boolean existsById(Long id) {
        return testRunRepository.existsById(id);
    }

    @Override
    public List<TestRun> retrieve() {
        return testRunRepository.findAll();
    }

    @Override
    public Optional<TestRun> retrieveOptionalByCiRunId(String ciRunId) {
        return testRunRepository.findByCiRunId(ciRunId);
    }

    @Override
    @Transactional
    public TestRun attachLabels(Long testRunId, Set<Label> labels) {
        saveTestRunLocale(testRunId, labels);

        TestRun testRun = retrieveById(testRunId);
        if (CollectionUtils.isNotEmpty(labels)) {
            Set<Label> existingLabels = testRun.getLabels();
            Set<Label> newLabels = labelService.save(labels);
            existingLabels.addAll(newLabels);
        }
        return testRunRepository.save(testRun);
    }

    private void saveTestRunLocale(Long testRunId, Set<Label> labels) {
        labels.stream()
              .filter(label -> LOCALE_LABEL.equals(label.getKey()))
              .findFirst()
              .ifPresent(label -> testRunService.updateLocale(testRunId, label.getValue()));

        labels.removeIf(label -> LOCALE_LABEL.equals(label.getKey()));
    }

    @Override
    @Transactional
    public Set<ArtifactReference> attachArtifactReferences(Long testRunId, Set<ArtifactReference> references) {
        TestRun testRun = retrieveById(testRunId);
        Set<ArtifactReference> testRunArtifactReferences = testRun.getArtifactReferences();
        references.forEach(reference -> testRunArtifactReferences.removeIf(
                testRunReference -> Objects.equals(testRunReference.getName(), reference.getName())));
        testRunArtifactReferences.addAll(references);
        testRunRepository.save(testRun);
        return testRun.getArtifactReferences();
    }

    @Override
    @Transactional
    public TestRun start(Long userId, TestRunStart testRunStart) {
        // the given ci run id may already exist in db on rerun of the test run
        // in such cases, we should only reset the test run data and should not register a new run
        TestRun testRun = Optional.ofNullable(testRunStart.getUuid())
                                  .flatMap(testRunRepository::findByCiRunId)
                                  .orElseGet(TestRun::new);

        User user = userService.retrieveNoNullById(userId);

        populatePrimaryInformation(testRun, user, testRunStart);
        populateTestSuite(testRun, user, testRunStart);
        populateJobs(testRun, user, testRunStart);
        populateNotificationTargets(testRun, testRunStart);

        // ci context should be set only to already built object because of one-to-one hibernate mapping
        populateCiContext(testRun, testRunStart);
        testRun.initStatistic();

        return testRunRepository.saveAndFlush(testRun);
    }

    @Override
    @Transactional
    public TestRun patch(Long id, String build) {
        TestRun testRun = retrieveById(id);

        if (StringUtils.isNotEmpty(build)) {
            testRun.setBuild(build);
        }

        return testRunRepository.saveAndFlush(testRun);
    }

    private void populatePrimaryInformation(TestRun testRun, User runner, TestRunStart testRunStart) {
        // generate a ci run if it was not provided
        String ciRunId = Optional.ofNullable(testRunStart.getUuid())
                                 .orElseGet(() -> UUID.randomUUID().toString());

        testRun.setName(testRunStart.getName());
        testRun.setCiRunId(ciRunId);
        testRun.setStartedAt(testRunStart.getStartedAt());
        testRun.setStatus(testRunStart.getStatus());
        testRun.setUser(runner);

        testRun.setComment(null);
        testRun.setReviewed(false);
        testRun.setElapsed(null);

        testRun.setFramework(testRunStart.getFramework());
        testRun.setEnvironment(testRunStart.getEnvironment());
        testRun.setBuild(testRunStart.getBuild());
    }

    private void populateTestSuite(TestRun testRun, User suiteOwner, TestRunStart testRunStart) {
        TestSuite testSuite = TestSuite.builder()
                                       .name(testRunStart.getName())
                                       .fileName(testRunStart.getName())
                                       .user(suiteOwner)
                                       .build();
        testSuite = testSuite2Service.getOrCreate(testSuite);

        testRun.setTestSuite(testSuite);
    }

    private void populateJobs(TestRun testRun, User jobOwner, TestRunStart testRunStart) {
        testRunStart.getJenkinsJobUrl()
                    .flatMap(jobUrl -> job2Service.retrieveOrCreateIfIntegrationExists(jobUrl, jobOwner))
                    .ifPresent(testRun::setJob);
        testRunStart.getJenkinsJobNumber()
                    .ifPresent(testRun::setJobNumber);

        testRunStart.getParentJenkinsJobUrl()
                    .flatMap(jobUrl -> job2Service.retrieveOrCreateIfIntegrationExists(jobUrl, jobOwner))
                    .ifPresent(testRun::setUpstreamJob);
        testRunStart.getParentJenkinsJobNumber()
                    .ifPresent(testRun::setUpstreamJobNumber);
    }

    private void populateCiContext(TestRun testRun, TestRunStart testRunStart) {
        TestRunStart.CiContext ciContext = testRunStart.getCiContext();
        if (ciContext != null) {
            TestRunCiContext testRunCiContext = Optional.ofNullable(testRun.getCiContext())
                                                        .map(TestRunCiContext::toBuilder)
                                                        .orElseGet(TestRunCiContext::builder)
                                                        .type(ciContext.getType())
                                                        .envVariables(ciContext.getEnvVariables())
                                                        .testSuiteExecution(testRun)
                                                        .build();
            testRun.setCiContext(testRunCiContext);
        } else {
            testRun.setCiContext(null);
        }
    }

    private void populateNotificationTargets(TestRun testRun, TestRunStart testRunStart) {
        Set<NotificationTarget> notificationTargets = testRunStart.getNotificationTargets();
        if (CollectionUtils.isNotEmpty(notificationTargets)) {
            notificationTargets = notificationTargetService.save(notificationTargets);
            testRun.setNotificationTargets(notificationTargets);
        }
    }

    @Override
    public TestRun finish(Long id, OffsetDateTime endedAt) {
        TestRun completedRun = transactionTemplate.execute(status -> {
            TestRun testRun = retrieveById(id);
            test2Service.swapStatusesByTestRunId(id, Test.Status.IN_PROGRESS, Test.Status.SKIPPED);

            setEndedAt(testRun, endedAt);
            calculateTestRunResult(testRun);

            testConfigService.setApiPlatformIfNoPlatformAndBrowser(testRun.getId());

            testSessionService.finishNotCompleted(id, endedAt.toInstant());

            launcherCallbackService.notifyOnTestRunFinish(testRun.getCiRunId());

            return testRun;
        });

        sendSummary(completedRun);

        return completedRun;
    }

    private void sendSummary(TestRun testRun) {
        Set<NotificationTarget> notificationTargets = testRun.getNotificationTargets();
        TestRunResultSummary resultSummary = testRunResultSummaryService.retrieveByTestRunId(testRun.getId());
        notificationTargetService.execute(notificationTargets, resultSummary);
    }

    @Override
    @Transactional
    public void recalculateTestRunResult(Long id) {
        TestRun testRun = this.retrieveById(id);
        if (testRun.getStatus() != TestRun.Status.IN_PROGRESS) {
            calculateTestRunResult(testRun);
        }
    }

    private void calculateTestRunResult(TestRun testRun) {
        boolean hasKnownIssue = test2Service.existsByTestRunIdAndKnownIssue(testRun.getId());
        testRun.setKnownIssue(hasKnownIssue);

        // we need to calculate test run status only for not-aborted test runs
        if (testRun.getStatus() != TestRun.Status.ABORTED) {
            // set failed status if there is at least one test with failed status and not known issue
            if (test2Service.existsByTestRunIdAndLogicallyFailedAndNotKnownIssue(testRun.getId())) {
                testRun.setStatus(TestRun.Status.FAILED);
            } else {
                // set test run status based on count of tests
                int testsCount = test2Service.countByTestRunId(testRun.getId());
                testRun.setStatus(testsCount > 0 ? TestRun.Status.PASSED : TestRun.Status.SKIPPED);
            }
        }

        testRunRepository.save(testRun);
    }

    @Override
    @Transactional
    public void abort(Long id, String comment) {
        TestRun testRun = retrieveById(id);
        if (testRun.getStatus() == TestRun.Status.IN_PROGRESS || testRun.getStatus() == TestRun.Status.QUEUED) {
            testRun.setStatus(TestRun.Status.ABORTED);
            testRun.setComment(comment);
            testRunRepository.save(testRun);

            test2Service.swapStatusesByTestRunId(id, Test.Status.IN_PROGRESS, Test.Status.ABORTED);

            finish(id, OffsetDateTime.now());
            // TODO: 13.05.21 we should send a websocket notification upon successful abortion
        }
    }

    @Override
    @Transactional
    public void saveQueuedRerun(Long testRunId, TestRunCiContext newCiContext, RerunMetadata rerunMetadata) {
        TestRun testRun = retrieveById(testRunId);

        testRun.setStatus(TestRun.Status.QUEUED);
        testRun.setStartedAt(OffsetDateTime.now());
        testRun.addRerunMetadata(rerunMetadata);

        TestRunCiContext ciContext = testRun.getCiContext();
        ciContext.setType(newCiContext.getType());
        ciContext.setEnvVariables(newCiContext.getEnvVariables());

        testRunRepository.save(testRun);
    }

    @Override
    @Transactional
    public void setPlatform(Long id, String platformName, String platformVersion) {
        this.retrieveById(id);
        testConfigService.setPlatform(id, platformName, platformVersion);
    }

    private void setEndedAt(TestRun testRun, OffsetDateTime endedAt) {
        testRun.setEndedAt(endedAt);
        if (testRun.getStartedAt() != null) {
            Duration duration = Duration.between(testRun.getStartedAt(), testRun.getEndedAt());
            testRun.setElapsed((int) duration.getSeconds());
        }
    }

    @Override
    @Transactional
    @CacheEvict(value = "environments", allEntries = true)
    public void deleteById(Long id) {
        if (!testRunRepository.existsById(id)) {
            throw new ResourceNotFoundException(TEST_RUN_NOT_FOUND_BY_ID, id);
        }
        testRunRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void onUserReassignment(Long fromId, Long toId) {
        testRunRepository.reassignUser(fromId, toId);
    }

}
