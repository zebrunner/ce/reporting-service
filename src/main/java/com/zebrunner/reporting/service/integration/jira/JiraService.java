package com.zebrunner.reporting.service.integration.jira;

import com.zebrunner.reporting.domain.integration.jira.IssuePreview;

import java.util.List;
import java.util.Set;

public interface JiraService {

    List<IssuePreview> retrieveIssuesByIds(Set<String> issuesIds);

    IssuePreview retrieveIssueById(String issueId);

    /**
     * Checks if an issue with given id is considered completed/done according to Jira workflow.
     * Issue is considered completed if its status belongs to COMPLETE/DONE status category.
     * <p>
     * More info on status categories:
     * @see <a href="https://jira.atlassian.com/browse/JRACLOUD-36241">Ability to add more categories to statuses</a>
     * @see <a href="https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-workflow-status-categories/#api-group-workflow-status-categories">Workflow status categories</a>
     * @param issueId id of issue to check
     * @return true - if issues is completed according to Jira workflow, false - otherwise
     */
    boolean isIssueCompleted(String issueId);

}
