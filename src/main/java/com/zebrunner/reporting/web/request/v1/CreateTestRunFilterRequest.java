package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Map;

@Data
@JGlobalMap
@NoArgsConstructor
public class CreateTestRunFilterRequest {

    @NotBlank
    private String name;

    @NotEmpty
    private Map<String, List<String>> items;

    private Boolean isPrivate;

}
