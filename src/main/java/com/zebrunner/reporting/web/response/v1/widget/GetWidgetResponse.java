package com.zebrunner.reporting.web.response.v1.widget;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.web.response.v1.dashboard.WidgetTemplateResponseItem;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JGlobalMap
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetWidgetResponse {

    private Long id;
    private String title;
    private String description;
    private String paramsConfig;
    private String legendConfig;
    private String type;
    private WidgetTemplateResponseItem widgetTemplate;

}
