package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.db.Test;
import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import com.zebrunner.reporting.domain.dto.core.SimpleSearchResult;
import com.zebrunner.reporting.domain.dto.elasticsearch.LogEntry;
import com.zebrunner.reporting.domain.dto.elasticsearch.Screenshot;
import com.zebrunner.reporting.service.TestArtifactsService;
import com.zebrunner.reporting.service.TestService;
import com.zebrunner.reporting.service.util.DateTimeUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestArtifactsServiceImpl implements TestArtifactsService {

    private static final String KIND_LOG = "log";
    private static final String KIND_SCREENSHOT = "screenshot";
    private static final String TEST_RUN_DATA_INDEX_PREFIX = "test-run-data-";

    private final ElasticsearchRestTemplate elasticsearch;
    private final TestService testService;

    @Override
    public SimpleSearchResult<LogEntry> fetchLogs(Long testRunId, Long testId, SearchCriteria searchCriteria) {
        Test test = testService.getTestByIdAndTestRunId(testId, testRunId);

        String[] indexNames = getIndexNamesToSearch(test.getStartTime(), test.getFinishTime());
        try {
            SearchHits<LogEntry> searchHits = searchLogs(testRunId, testId, searchCriteria, indexNames);
            return toSearchResult(searchCriteria, searchHits);
        } catch (DataAccessException e) {
            return SimpleSearchResult.noData(searchCriteria.getPage(), searchCriteria.getPageSize());
        }
    }

    private SearchHits<LogEntry> searchLogs(Long testRunId, Long testId, SearchCriteria searchCriteria, String[] indexNames) {
        return searchArtifacts(testRunId, testId, KIND_LOG, searchCriteria, indexNames, LogEntry.class);
    }

    @Override
    public SimpleSearchResult<Screenshot> fetchScreenshots(Long testRunId, Long testId, SearchCriteria searchCriteria) {
        Test test = testService.getTestByIdAndTestRunId(testId, testRunId);

        String[] indexNames = getIndexNamesToSearch(test.getStartTime(), test.getFinishTime());
        try {
            SearchHits<Screenshot> searchHits = searchScreenshots(testRunId, testId, searchCriteria, indexNames);
            return toSearchResult(searchCriteria, searchHits);
        } catch (DataAccessException e) {
            log.warn("Call to ElasticSearch resulted into an error", e);
            return SimpleSearchResult.noData(searchCriteria.getPage(), searchCriteria.getPageSize());
        }
    }

    private SearchHits<Screenshot> searchScreenshots(Long testRunId, Long testId, SearchCriteria searchCriteria, String[] indexNames) {
        return searchArtifacts(testRunId, testId, KIND_SCREENSHOT, searchCriteria, indexNames, Screenshot.class);
    }

    private String[] getIndexNamesToSearch(Date start, Date end) {
        LocalDate startDate = DateTimeUtil.toLocalDate(start);
        LocalDate endDate = DateTimeUtil.toLocalDate(end);

        if (startDate.equals(endDate)) {
            // test run started and ended within same day
            String indexName = toCorrespondingIndexName(startDate);
            return List.of(indexName).toArray(String[]::new);
        } else {
            // test run started on one day and ended on another OR end date is null (TR is still in progress)
            endDate = endDate == null ? resolveEndDate(startDate) : endDate;
            return collectIndexNamesForRange(startDate, endDate);
        }
    }

    private LocalDate resolveEndDate(LocalDate startDate) {
        return startDate.isEqual(LocalDate.now())
                ? startDate
                : startDate.plusDays(1);
    }

    private String[] collectIndexNamesForRange(LocalDate rangeStart, LocalDate rangeEnd) {
        return LongStream.range(rangeStart.toEpochDay(), rangeEnd.toEpochDay() + 1) // end interpreted as inclusive
                         .mapToObj(LocalDate::ofEpochDay)
                         .map(this::toCorrespondingIndexName)
                         .toArray(String[]::new);
    }

    private String toCorrespondingIndexName(LocalDate date) {
        return TEST_RUN_DATA_INDEX_PREFIX + DateTimeFormatter.ofPattern("yyyy.MM.dd", Locale.ENGLISH).format(date);
    }

    private <T> SearchHits<T> searchArtifacts(Long testRunId, Long testId, String kind, SearchCriteria searchCriteria,
                                              String[] indexNames, Class<T> valueType) {
        QueryBuilder queryBuilder = new BoolQueryBuilder().must(termQuery("kind", kind))
                                                          .must(termQuery("testRunId", testRunId))
                                                          .must(termQuery("testId", testId));

        NativeSearchQuery searchQuery = new NativeSearchQuery(queryBuilder);

        PageRequest pageRequest = PageRequest.of(
                searchCriteria.getPage() - 1,
                searchCriteria.getPageSize(),
                Sort.by(Sort.Direction.ASC, "timestamp") // hardcoded on purpose - currently the only sort option supported
        );
        searchQuery.setPageable(pageRequest);

        return elasticsearch.search(searchQuery, valueType, IndexCoordinates.of(indexNames));
    }

    private <T> SimpleSearchResult<T> toSearchResult(SearchCriteria searchCriteria, SearchHits<T> searchHits) {
        long totalHits = searchHits.getTotalHits();
        int pageSize = searchCriteria.getPageSize();
        long totalPages = (int) Math.ceil((double) totalHits / pageSize);
        List<T> results = searchHits.getSearchHits().stream().map(SearchHit::getContent).collect(Collectors.toList());

        return new SimpleSearchResult<>(
                searchCriteria.getPage(),
                pageSize,
                totalPages,
                totalHits,
                results
        );
    }

}
