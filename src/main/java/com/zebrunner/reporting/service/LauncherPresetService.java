package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.launcher.LauncherPreset;
import com.zebrunner.reporting.persistence.dao.mysql.application.LauncherPresetMapper;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.util.URLResolver;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class LauncherPresetService {

    private static final String WEBHOOK_API_URL_PATTERN = "%s/reporting-service/api/launchers/hooks/%s";

    private final LauncherPresetMapper launcherPresetMapper;
    private final URLResolver urlResolver;

    @Transactional()
    public LauncherPreset create(LauncherPreset launcherPreset, Long launcherId) {
        launcherPreset.setId(null);
        if (!canPersist(launcherPreset, launcherId)) {
            throw new BusinessConstraintException(BusinessConstraintError.LAUNCHER_PRESET_IN_USE, launcherPreset.getName(), launcherId);
        }

        String ref = generateRef();
        launcherPreset.setRef(ref);
        launcherPresetMapper.create(launcherPreset, launcherId);
        return launcherPreset;
    }

    @Transactional(readOnly = true)
    public LauncherPreset retrieveById(Long id) {
        return launcherPresetMapper.findById(id)
                                   .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.LAUNCHER_PRESET_NOT_FOUND_BY_ID, id));
    }

    @Transactional(readOnly = true)
    public LauncherPreset retrieveByIdAndReference(Long id, String ref) {
        return launcherPresetMapper.findByIdAndRef(id, ref)
                                   .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.LAUNCHER_PRESET_NOT_FOUND_BY_ID, id));
    }

    @Transactional(readOnly = true)
    public LauncherPreset retrieveByRef(String ref) {
        return launcherPresetMapper.findByRef(ref)
                                   .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.LAUNCHER_PRESET_NOT_FOUND_BY_REF, ref));
    }

    @Transactional
    public LauncherPreset update(LauncherPreset launcherPreset, Long launcherId) {
        LauncherPreset dbLauncherPreset = retrieveById(launcherPreset.getId());
        boolean persistDenied = !dbLauncherPreset.getName().equals(launcherPreset.getName()) && !canPersist(launcherPreset, launcherId);
        if (persistDenied) {
            throw new BusinessConstraintException(BusinessConstraintError.LAUNCHER_PRESET_IN_USE, launcherPreset.getName(), launcherId);
        }

        launcherPreset.setRef(dbLauncherPreset.getRef());
        launcherPresetMapper.update(launcherPreset);
        return launcherPreset;
    }

    @Transactional
    public void deleteByIdAndLauncherId(Long id, Long launcherId) {
        launcherPresetMapper.deleteByIdAndLauncherId(id, launcherId);
    }

    @Transactional
    public void updateReference(Long id, String ref) {
        launcherPresetMapper.updateReference(id, ref);
    }

    @Transactional(readOnly = true)
    public String buildWebHookUrl(Long id) {
        LauncherPreset launcherPreset = retrieveById(id);
        String webserviceUrl = urlResolver.buildWebserviceUrl();
        return String.format(WEBHOOK_API_URL_PATTERN, webserviceUrl, launcherPreset.getRef());
    }

    @Transactional
    public void revokeReference(Long id, String oldRef, Long launcherId) {
        LauncherPreset dbLauncherPreset = retrieveByIdAndReference(id, oldRef);
        boolean presetNotExists = canPersist(dbLauncherPreset, launcherId);
        if (presetNotExists) {
            throw new BusinessConstraintException(BusinessConstraintError.LAUNCHER_PRESET_IN_USE, dbLauncherPreset.getName(), launcherId);
        }

        String newRef = generateRef();
        updateReference(id, newRef);
    }

    private boolean canPersist(LauncherPreset launcherPreset, Long launcherId) {
        return !launcherPresetMapper.existsByNameAndLauncherId(launcherPreset.getName(), launcherId);
    }

    private String generateRef() {
        return RandomStringUtils.randomAlphabetic(20);
    }
}
