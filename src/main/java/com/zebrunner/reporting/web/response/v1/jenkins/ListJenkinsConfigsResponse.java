package com.zebrunner.reporting.web.response.v1.jenkins;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
public class ListJenkinsConfigsResponse {

    List<Item> items;

    @Data
    @JGlobalMap
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Item {

        private Long id;
        private String name;
        private String url;
        private String username;
        private String token;
        private String launcherJobName;
        private boolean enabled;

    }

}
