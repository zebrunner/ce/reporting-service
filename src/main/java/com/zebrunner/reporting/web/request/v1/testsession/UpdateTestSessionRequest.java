package com.zebrunner.reporting.web.request.v1.testsession;

import com.googlecode.jmapper.annotations.JMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.PastOrPresent;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class UpdateTestSessionRequest {

    @JMap
    @PastOrPresent
    private Instant endedAt;

    private Set<Long> testIds = new HashSet<>();

}
