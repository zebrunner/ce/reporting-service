package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface TestRepository extends JpaRepository<Test, Long> {

    boolean existsByTestRunIdAndId(Long testRunId, Long id);

    boolean existsByTestRunIdAndKnownIssue(Long testRunId, boolean knownIssue);

    boolean existsByTestRunIdAndStatusInAndKnownIssue(Long testRunId, Collection<Test.Status> statuses, boolean knownIssue);

    int countByTestRunId(Long testRunId);

    Optional<Test> findByTestRunIdAndId(Long testRunId, Long id);

    List<Test> findAllByIdIn(Collection<Long> ids);

    List<Test> findAllByTestRunIdAndIdIn(Long testRunId, Collection<Long> ids);

    List<Test> findAllByTestRunId(Long testRunId);

    @Modifying
    @Query("UPDATE Test t SET t.maintainer.id = :toUserId WHERE t.maintainer.id = :fromUserId")
    void reassignMaintainer(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Modifying
    @Query("UPDATE Test t SET t.status = :toStatus WHERE t.status = :fromStatus AND t.testRun.id = :testRunId")
    void swapStatusesByTestRunId(@Param("testRunId") Long testRunId,
                                 @Param("fromStatus") Test.Status fromStatus,
                                 @Param("toStatus") Test.Status toStatus);

}
