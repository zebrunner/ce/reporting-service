package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.integration.IssueReferenceAssignmentEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface IssueReferenceAssignmentEventRepository extends JpaRepository<IssueReferenceAssignmentEvent, Long> {

    @Query("SELECT irae " +
           "FROM IssueReferenceAssignmentEvent irae " +
           "JOIN FETCH irae.issueReference " +
           "WHERE irae.testCaseId = :testCaseId " +
           "AND irae.action = :action " +
           "ORDER BY irae.id DESC")
    Set<IssueReferenceAssignmentEvent> findByTestCaseIdAndActionOrderedByIdDesc(Long testCaseId, IssueReferenceAssignmentEvent.Action action);

    @Query("SELECT COUNT(irae) > 0 " +
           "FROM IssueReferenceAssignmentEvent irae " +
           "JOIN irae.issueReference " +
           "WHERE irae.id = (" +
               "SELECT MAX(irae1.id) FROM IssueReferenceAssignmentEvent irae1 " +
               "WHERE irae1.testCaseId = :testCaseId " +
               "AND irae1.failureReasonHash = :hash " +
               "AND irae1.issueReference.id = :issueReferenceId) " +
           "AND irae.action = 'UNLINK'")
    boolean isIssueUnlinkedFromTestCase(Long testCaseId, int hash, Long issueReferenceId);

    @Query("SELECT irae " +
           "FROM IssueReferenceAssignmentEvent irae " +
           "JOIN FETCH irae.issueReference " +
           "WHERE irae.id = (" +
               "SELECT MAX(irae1.id) FROM IssueReferenceAssignmentEvent irae1 " +
               "WHERE irae1.testCaseId = :testCaseId " +
               "AND irae1.failureReasonHash = :hash) " +
           "AND irae.issueReference.id = :issueReferenceId " +
           "AND irae.action = 'LINK'")
    Optional<IssueReferenceAssignmentEvent> findLatestLinkToTestCaseByHashAndIssue(Long testCaseId, int hash, Long issueReferenceId);

}
