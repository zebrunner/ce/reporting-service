package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.db.TestCaseManagementData;
import com.zebrunner.reporting.domain.db.TestInfo;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.db.config.Configuration;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestInfoMapper;
import com.zebrunner.reporting.service.legacy.LabelConverter;
import com.zebrunner.reporting.service.util.URLResolver;
import com.zebrunner.reporting.service.util.XmlUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TcmDataService {

    private final TestInfoMapper testInfoMapper;
    private final TestRunService testRunService;
    private final LabelService labelService;
    private final URLResolver urlResolver;

    private static final Map<String, String> TCM_LABELS = Map.of(
            "TESTRAIL_TESTCASE_UUID", "com.zebrunner.app/tcm.testrail.testcase-id",
            "testrail_assignee", "com.zebrunner.app/tcm.testrail.assignee",
            "testrail_milestone", "com.zebrunner.app/tcm.testrail.milestone",
            "QTEST_TESTCASE_UUID", "com.zebrunner.app/tcm.qtest.testcase-id",
            "qtest_cycle_name", "com.zebrunner.app/tcm.qtest.cycle-name"
    );

    @Transactional(readOnly = true)
    public TestCaseManagementData export(String ciRunId, String tool) {
        TestRun testRun = testRunService.getTestRunByCiRunIdFull(ciRunId);

        Date finishedAt = getFinishedAt(testRun);
        String environment = testRun.getEnvironment();
        List<TestInfo> testInfo = testInfoMapper.findByLabelKeyAndTestRunCiRunId(ciRunId, getTestCaseLabelKey(tool));
        Map<String, String> customParams = getCustomParams(tool, testRun);
        String reportingServiceUrl = urlResolver.buildWebURL();

        return TestCaseManagementData.builder()
                                     .testRunName(testRun.getName())
                                     .testInfo(testInfo)
                                     .finishedAt(finishedAt)
                                     .startedAt(testRun.getStartedAt())
                                     .createdAfter(testRun.getCreatedAt())
                                     .env(environment)
                                     .testRunId(testRun.getId())
                                     .reportingServiceUrl(reportingServiceUrl)
                                     .customParams(customParams)
                                     .build();
    }

    public Set<Label> parseTcmLabels(String configXml) {
        Configuration configuration = XmlUtils.readConfiguration(configXml);
        Set<Label> labels = configuration.getArg()
                                         .stream()
                                         .filter(arg -> TCM_LABELS.get(arg.getKey()) != null && !ObjectUtils.isEmpty(arg.getValue()))
                                         .map(arg -> new Label(TCM_LABELS.get(arg.getKey()), arg.getValue()))
                                         .collect(Collectors.toSet());

        return labelService.save(labels);
    }

    private String getTestCaseLabelKey(String tool) {
        return TCM_LABELS.values()
                         .stream()
                         .filter(value -> value.contains(tool) && value.contains("testcase"))
                         .findFirst()
                         .orElse(null);
    }

    private Map<String, String> getCustomParams(String tool, TestRun testRun) {
        Set<String> labelsKeys = TCM_LABELS.values()
                                           .stream()
                                           .filter(key -> key.contains(tool))
                                           .collect(Collectors.toSet());

        return LabelConverter.toNewer(testRun.getLabels())
                             .stream()
                             .filter(l -> labelsKeys.contains(l.getKey()))
                             .collect(Collectors.toMap(Label::getKey, Label::getValue, (v1, v2) -> v2));
    }

    private Date getFinishedAt(TestRun testRun) {
        // finishedAt value generation based on startedAt & elapsed
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(testRun.getStartedAt());
        if (testRun.getElapsed() != null) {
            calendar.add(Calendar.SECOND, testRun.getElapsed());
        }
        return calendar.getTime();
    }

}
