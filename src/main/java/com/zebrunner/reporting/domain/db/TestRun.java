package com.zebrunner.reporting.domain.db;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TestRun extends AbstractEntity {

    private String name;
    private String ciRunId;
    private User user;
    private TestSuite testSuite;
    private Status status;
    private Job job;
    private Integer buildNumber;
    private Job upstreamJob;
    private Integer upstreamJobBuildNumber;
    private Initiator startedBy;
    private boolean knownIssue;
    private Date startedAt;
    private Date endedAt;
    private Integer elapsed;
    private String comments;
    private String channels;
    private TestConfig config;
    private String environment;
    private String build;

    private Integer passed;
    private Integer failed;
    private Integer failedAsKnown;
    private Integer skipped;
    private Integer inProgress;
    private Integer aborted;
    private boolean reviewed;
    private String framework;

    @JsonProperty("artifacts")
    private Set<ArtifactReference> artifactReferences = new HashSet<>();

    private Set<Label> labels;

    public enum Initiator {
        SCHEDULER,
        UPSTREAM_JOB,
        HUMAN
    }

}
