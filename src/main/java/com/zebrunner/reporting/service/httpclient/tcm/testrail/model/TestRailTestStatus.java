package com.zebrunner.reporting.service.httpclient.tcm.testrail.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TestRailTestStatus {

    PASSED(1),
    BLOCKED(2),
    UNTESTED(3),
    RETEST(4),
    FAILED(5);
    final int value;

}
