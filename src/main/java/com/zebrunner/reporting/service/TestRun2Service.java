package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.dto.reporting.TestRunStart;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.entity.RerunMetadata;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import com.zebrunner.reporting.domain.vo.ArtifactReference;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TestRun2Service {

    TestRun retrieveById(Long id);

    boolean existsById(Long id);

    List<TestRun> retrieve();

    Optional<TestRun> retrieveOptionalByCiRunId(String ciRunId);

    TestRun attachLabels(Long testRunId, Set<Label> labels);

    Set<ArtifactReference> attachArtifactReferences(Long testRunId, Set<ArtifactReference> references);

    TestRun start(Long userId, TestRunStart testRunStart);

    // signature of this method can be refactored when new patchable fields appear
    TestRun patch(Long id, String build);

    TestRun finish(Long id, OffsetDateTime endedAt);

    /**
     * Marks QUEUED or IN_PROGRESS test run as ABORTED and
     * sets ABORTED status to every IN_PROGRESS test which belongs to the test run.
     * <p>
     * From test run lifecycle perspective, ABORTED is terminating status.
     * Because of this, this this method also invokes the {@link #finish(Long, OffsetDateTime)}
     * if test run is actually aborted.
     *
     * @param id      test run to abort
     * @param comment comment to attach to the test run
     */
    void abort(Long id, String comment);

    /**
     * This method is necessary to save intermediate results when rerun is triggered on ci.
     *
     * @param testRunId     test run that was reran.
     * @param newCiContext  the new state of ci context corresponding to the test run.
     *                      As for now, we should display links to actual ci builds which executed tests.
     *                      It means that we should display link to previous ci build rather then
     *                      link to just starting builds initiated for rerun.
     *                      Because of this, in most of the cases,
     *                      this argument is just an updated version of already persisted ci context.
     * @param rerunMetadata metadata that reflects the just initiated rerun.
     */
    void saveQueuedRerun(Long testRunId, TestRunCiContext newCiContext, RerunMetadata rerunMetadata);

    void setPlatform(Long id, String platformName, String platformVersion);

    void deleteById(Long id);

    void recalculateTestRunResult(Long id);

}
