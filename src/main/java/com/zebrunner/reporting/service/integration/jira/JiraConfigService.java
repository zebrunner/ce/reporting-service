package com.zebrunner.reporting.service.integration.jira;

import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;

import java.util.Optional;

public interface JiraConfigService {

    JiraConfig update(JiraConfig newConfig);

    Optional<JiraConfig> retrieveOptional();

    JiraConfig retrieve();

    default JiraConfig retrieveActive() {
        return retrieveActive(true);
    }

    JiraConfig retrieveActive(boolean decryptValues);

    void patchEnabledProperty(Boolean enabled);

    boolean isEnabled();

    ReachabilityCheckResult isReachable(JiraConfig config);

}
