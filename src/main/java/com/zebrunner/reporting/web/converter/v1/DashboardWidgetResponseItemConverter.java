package com.zebrunner.reporting.web.converter.v1;

import com.zebrunner.reporting.domain.entity.DashboardWidget;
import com.zebrunner.reporting.web.response.v1.dashboard.DashboardWidgetResponseItem;
import com.zebrunner.reporting.web.response.v1.dashboard.WidgetTemplateResponseItem;

public class DashboardWidgetResponseItemConverter {

    public static DashboardWidgetResponseItem toResponseItem(DashboardWidget widget) {
        WidgetTemplateResponseItem template = WidgetTemplateResponseItemConverter.toResponseItem(widget.getWidget().getWidgetTemplate());
        return DashboardWidgetResponseItem.builder()
                                          .id(widget.getId())
                                          .title(widget.getWidget().getTitle())
                                          .description(widget.getWidget().getDescription())
                                          .paramsConfig(widget.getWidget().getParamsConfig())
                                          .legendConfig(widget.getWidget().getLegendConfig())
                                          .type(widget.getWidget().getType())
                                          .location(widget.getLocation())
                                          .widgetTemplate(template)
                                          .build();
    }

}
