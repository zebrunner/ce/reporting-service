package com.zebrunner.reporting.domain.dto.reporting;

import com.zebrunner.reporting.domain.entity.Label;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestSave {

    private String name;
    private String correlationData;
    private String className;
    private String methodName;
    private OffsetDateTime startedAt;
    private String maintainer;
    private String testCase;
    private Set<Label> labels = Set.of();

}
