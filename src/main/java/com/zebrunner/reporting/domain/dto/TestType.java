package com.zebrunner.reporting.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class TestType extends AbstractType {

    @NotNull
    private String name;
    private Status status;
    private String testArgs;
    @NotNull
    private Long testRunId;
    @NotNull
    private Long testCaseId;
    private String testGroup;
    private String message;
    private Integer messageHashCode;
    private Long startTime;
    private Long finishTime;
    private String configXML;
    private Map<String, Long> testMetrics;
    private boolean knownIssue;
    private boolean needRerun;
    private String dependsOnMethods;
    private String testClass;
    @Valid
    private Set<ArtifactReference> artifacts = new HashSet<>();
    @Valid
    private Set<LabelDTO> labels;

}
