package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestRunStatistic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestRunStatisticRepository extends JpaRepository<TestRunStatistic, Integer> {
}
