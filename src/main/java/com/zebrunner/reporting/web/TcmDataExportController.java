package com.zebrunner.reporting.web;

import com.zebrunner.reporting.domain.db.TestCaseManagementData;
import com.zebrunner.reporting.domain.dto.label.TestCaseManagementDataDTO;
import com.zebrunner.reporting.service.TcmDataService;
import com.zebrunner.reporting.web.util.swagger.ApiResponseStatuses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.dozer.Mapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("Tags operations")
@RequestMapping(path = "api/tcm", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class TcmDataExportController {

    private final TcmDataService tcmDataService;
    private final Mapper mapper;

    @ApiResponseStatuses
    @ApiOperation(value = "Retrieves integration information", nickname = "getTestIntegrationInfo", httpMethod = "GET", response = TestCaseManagementDataDTO.class)
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", paramType = "header")})
    @GetMapping("/export/{ciRunId}")
    public TestCaseManagementDataDTO getTcmData(@PathVariable("ciRunId") String ciRunId, @RequestParam("tool") String tool) {
        TestCaseManagementData data = tcmDataService.export(ciRunId, tool);
        return mapper.map(data, TestCaseManagementDataDTO.class);
    }

}