package com.zebrunner.reporting.web.response.v1;

import lombok.Value;

@Value
public class SuccessResponseItem<T> {

    Long testId;
    boolean succeeded = true;
    T issue;

    private SuccessResponseItem(Long testId, T issue) {
        this.testId = testId;
        this.issue = issue;
    }

    public static <T> SuccessResponseItem<T> succeeded(Long testId, T data) {
        return new SuccessResponseItem<>(testId, data);
    }

}
