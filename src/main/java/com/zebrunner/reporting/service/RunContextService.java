package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.dto.ExchangeRunContextResult;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.vo.RunContext;

import java.util.Collection;

public interface RunContextService {

    RunContext generateNew();

    RunContext generateForRerunAsString(String testRunUuid, Collection<Test.Status> anyOfStatuses, Boolean knownIssue);

    ExchangeRunContextResult exchange(RunContext runContext);

}
