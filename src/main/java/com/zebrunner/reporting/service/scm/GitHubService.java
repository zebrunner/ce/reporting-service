package com.zebrunner.reporting.service.scm;

import com.zebrunner.common.eh.exception.ExternalSystemException;
import com.zebrunner.common.eh.exception.InternalServiceException;
import com.zebrunner.reporting.domain.db.ScmAccount;
import com.zebrunner.reporting.domain.dto.scm.Organization;
import com.zebrunner.reporting.domain.dto.scm.Repository;
import com.zebrunner.reporting.domain.dto.scm.ScmConfig;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.ExternalSystemError;
import com.zebrunner.reporting.service.exception.InternalServiceError;
import com.zebrunner.reporting.service.util.GitHubClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.kohsuke.github.GHPerson;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class GitHubService implements IScmService {

    private final GitHubClient gitHubClient;
    private final CryptoService cryptoService;

    public String getAccessToken(String code) {
        return gitHubClient.getAccessToken(code);
    }

    public String getUsername(String token) {
        return gitHubClient.getUsername(token);
    }

    @Override
    public List<Repository> getRepositories(ScmAccount scmAccount, String organizationName, List<String> existingRepos) {
        GitHub gitHub = connectToGitHub(scmAccount);

        try {
            GHPerson tokenOwner = gitHub.getMyself();
            GHPerson person = StringUtils.isBlank(organizationName) || tokenOwner.getLogin().equals(organizationName)
                    ? gitHub.getMyself()
                    : gitHub.getOrganization(organizationName);

            return person.listRepositories().asList().stream()
                         .filter(repository -> isRepositoryOwner(person.getLogin(), repository))
                         .map(GitHubService::mapRepository)
                         .filter(repository -> !existingRepos.contains(repository.getUrl()))
                         .collect(Collectors.toList());
        } catch (IOException e) {
            throw new ExternalSystemException(ExternalSystemError.GITHUB_UNABLE_TO_GET_REPOSITORIES, e.getMessage());
        }
    }

    @Override
    public Repository getRepository(ScmAccount scmAccount) {
        String organizationName = scmAccount.getOrganizationName();
        String repositoryName = scmAccount.getRepositoryName();
        GHRepository repository = null;
        if (!StringUtils.isBlank(organizationName) && !StringUtils.isBlank(repositoryName)) {
            try {
                GitHub gitHub = connectToGitHub(scmAccount);
                String repositoryAbsoluteName = organizationName + "/" + repositoryName;
                repository = gitHub.getRepository(repositoryAbsoluteName);
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        return repository == null ? null : mapRepository(repository);
    }

    @Override
    public List<Organization> getOrganizations(ScmAccount scmAccount) {
        GitHub gitHub = connectToGitHub(scmAccount);
        try {
            List<Organization> organizations = gitHub.getMyself().getAllOrganizations().stream().map(organization -> {
                Organization result = new Organization(organization.getLogin());
                result.setAvatarURL(organization.getAvatarUrl());
                return result;
            }).collect(Collectors.toList());
            Organization myself = new Organization(gitHub.getMyself().getLogin());
            myself.setAvatarURL(gitHub.getMyself().getAvatarUrl());
            organizations.add(myself);
            return organizations;
        } catch (IOException e) {
            throw new ExternalSystemException(ExternalSystemError.GITHUB_UNABLE_TO_GET_ORGANIZATIONS, e.getMessage());
        }
    }

    @Override
    public ScmConfig getScmConfig() {
        return gitHubClient.getConfig();
    }

    @Override
    public ScmAccount.Name getScmAccountName() {
        return gitHubClient.getAccountName();
    }

    @Override
    public String getLoginName(ScmAccount scmAccount) {
        String result = null;
        try {
            GitHub gitHub = connectToGitHub(scmAccount);
            result = gitHub.getMyself().getLogin();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    private static boolean isRepositoryOwner(String loginName, GHRepository repository) {
        boolean result = false;
        try {
            result = repository.getOwner().getLogin().equals(loginName);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    private static Repository mapRepository(GHRepository repository) {
        Repository repo = new Repository(repository.getName());
        repo.setDefaultBranch(repository.getDefaultBranch());
        repo.setPrivate(repository.isPrivate());
        repo.setUrl(repository.getHtmlUrl().toString());
        return repo;
    }

    private GitHub connectToGitHub(ScmAccount scmAccount) {
        String decryptedAccessToken = getDecryptedAccessToken(scmAccount);
        try {
            switch (scmAccount.getName()) {
                case GITHUB:
                    return GitHub.connectUsingOAuth(decryptedAccessToken);
                case GITHUB_ENTERPRISE:
                    String apiVersion = gitHubClient.getApiVersion();
                    return GitHub.connectToEnterpriseWithOAuth(apiVersion, scmAccount.getLogin(), decryptedAccessToken);
                default:
                    throw new IllegalStateException("Unexpected value: " + scmAccount.getName());
            }
        } catch (IOException e) {
            throw new ExternalSystemException(ExternalSystemError.UNABLE_CONNECT_TO_GITHUB, e.getMessage());
        }
    }

    private String getDecryptedAccessToken(ScmAccount scmAccount) {
        try {
            String decryptedAccessToken = "";
            if (StringUtils.isNotEmpty(scmAccount.getAccessToken())) {
                if (scmAccount.isAccessTokenEncrypted()) {
                    decryptedAccessToken = cryptoService.decrypt(scmAccount.getAccessToken());
                } else {
                    decryptedAccessToken = scmAccount.getAccessToken();
                }
            }
            return decryptedAccessToken;
        } catch (Exception e) {
            throw new InternalServiceException(InternalServiceError.UNABLE_TO_DECRYPT_TOKEN);
        }
    }

}
