package com.zebrunner.reporting.service.httpclient.utils;

import kong.unirest.ObjectMapper;
import lombok.SneakyThrows;

public class JacksonObjectMapperWrapper implements ObjectMapper {

    private final com.fasterxml.jackson.databind.ObjectMapper objectMapper;

    public JacksonObjectMapperWrapper(com.fasterxml.jackson.databind.ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    @SneakyThrows
    public <T> T readValue(String value, Class<T> valueType) {
        return objectMapper.readValue(value, valueType);
    }

    @Override
    @SneakyThrows
    public String writeValue(Object value) {
        return objectMapper.writeValueAsString(value);
    }

}
