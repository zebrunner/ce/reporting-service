package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.domain.dto.reporting.TestRunStart;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.push.TestRunPush;
import com.zebrunner.reporting.domain.push.TestRunStatisticPush;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.messaging.websocket.Topic;
import com.zebrunner.reporting.service.LauncherCallbackService;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestRunService;
import com.zebrunner.reporting.service.TestRunStatisticsService;
import com.zebrunner.reporting.service.ci.CiServerFacade;
import com.zebrunner.reporting.web.converter.v1.SaveTestRunResponseConverter;
import com.zebrunner.reporting.web.patch.PatchAction;
import com.zebrunner.reporting.web.patch.PatchOperation;
import com.zebrunner.reporting.web.patch.PatchRequest;
import com.zebrunner.reporting.web.request.v1.AttachArtifactReferencesRequest;
import com.zebrunner.reporting.web.request.v1.AttachLabelsRequest;
import com.zebrunner.reporting.web.request.v1.ChangeTestSuiteExecutionPlatformRequest;
import com.zebrunner.reporting.web.request.v1.FinishTestRunRequest;
import com.zebrunner.reporting.web.request.v1.StartTestRunRequest;
import com.zebrunner.reporting.web.response.v1.ListTestRunsResponse;
import com.zebrunner.reporting.web.response.v1.SaveTestRunResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@CrossOrigin
@RestController("reportingTestRunController")
@RequiredArgsConstructor
@RequestMapping(path = "/v1/test-runs", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestRunController {

    private final JMapper mapper;
    private final CiServerFacade ciServerFacade;
    private final TestRunService testRunService;
    private final TestRun2Service testRun2Service;
    private final SimpMessagingTemplate messagingTemplate;
    private final TestRunStatisticsService statisticsService;
    private final LauncherCallbackService launcherCallbackService;
    private final SaveTestRunResponseConverter saveTestRunResponseConverter;

    @GetMapping
    public ListTestRunsResponse list() {
        List<com.zebrunner.reporting.domain.entity.TestRun> runs = testRun2Service.retrieve();
        List<ListTestRunsResponse.TestRunItem> items = mapper.mapToList(runs, ListTestRunsResponse.TestRunItem.class);
        return new ListTestRunsResponse(items);
    }

    @PostMapping
    public SaveTestRunResponse start(@RequestBody @Valid StartTestRunRequest startTestRunRequest,
                                     @PrincipalId Integer userId) {
        TestRunStart testRunStart = mapper.map(startTestRunRequest, TestRunStart.class);
        if (startTestRunRequest.getConfig() != null) {
            testRunStart.setEnvironment(startTestRunRequest.getConfig().getEnvironment());
            testRunStart.setBuild(startTestRunRequest.getConfig().getBuild());
        }

        com.zebrunner.reporting.domain.entity.TestRun testRun = testRun2Service
                .start(userId.longValue(), testRunStart);

        com.zebrunner.reporting.domain.db.TestRun fullTestRun = testRunService.getTestRunByIdFull(testRun.getId());
        notifyAboutRunByWebsocket(fullTestRun);

        return saveTestRunResponseConverter.toSaveResponse(testRun);
    }

    @PutMapping("/{testRunId}")
    public SaveTestRunResponse finish(@PathVariable("testRunId") Long testRunId,
                                      @RequestBody @Valid FinishTestRunRequest finishTestRunRequest) {
        TestRun testRun = testRun2Service.finish(testRunId, finishTestRunRequest.getEndedAt());

        com.zebrunner.reporting.domain.db.TestRun testRunFull = testRunService.getTestRunByIdFull(testRunId);
        launcherCallbackService.notifyOnTestRunFinish(testRunFull.getCiRunId());

        notifyAboutRunByWebsocket(testRunFull);

        return saveTestRunResponseConverter.toSaveResponse(testRun);
    }

    @PatchMapping("/{testRunId}")
    public SaveTestRunResponse patch(@PathVariable("testRunId") Long testRunId,
                                     @PatchAction(op = PatchOperation.REPLACE, path = "/config/build")
                                     @RequestBody PatchRequest patchRequest) {
        String build = patchRequest.getReplaceValue("/config/build", String.class);

        TestRun testRun = testRun2Service.patch(testRunId, build);

        com.zebrunner.reporting.domain.db.TestRun testRunFull = testRunService.getTestRunByIdFull(testRunId);
        testRunFull.getConfig().setPlatformVersion(UUID.randomUUID().toString());
        notifyAboutRunByWebsocket(testRunFull);

        return saveTestRunResponseConverter.toSaveResponse(testRun);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{testRunId}/platform")
    public void setPlatform(@PathVariable("testRunId") Long testRunId,
                            @RequestBody @Valid ChangeTestSuiteExecutionPlatformRequest request) {
        testRun2Service.setPlatform(testRunId, request.getName(), request.getVersion());

        com.zebrunner.reporting.domain.db.TestRun testRunFull = testRunService.getTestRunByIdFull(testRunId);
        notifyAboutRunByWebsocket(testRunFull);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        testRun2Service.deleteById(id);
    }

    private void notifyAboutRunByWebsocket(com.zebrunner.reporting.domain.db.TestRun testRun) {
        testRunService.hideJobUrlsIfNeed(List.of(testRun));
        notifyAboutTestRunStatisticsByWebsocket(testRun.getId());

        if (testRun.getJob() == null) {
            ciServerFacade.populateLegacyJobsAndBuildNumbers(List.of(testRun));
        }
        messagingTemplate.convertAndSend(Topic.testRuns(), new TestRunPush(testRun));
    }

    private void notifyAboutTestRunStatisticsByWebsocket(Long runId) {
        TestRunStatistics testRunStatistics = statisticsService.get(runId);
        messagingTemplate.convertAndSend(Topic.testRunStatistics(), new TestRunStatisticPush(testRunStatistics));
    }

    @PutMapping("/{id}/labels")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void attachLabels(@PathVariable("id") Long id,
                             @RequestBody @Valid AttachLabelsRequest request) {
        Set<Label> labels = mapper.mapToSet(request.getItems(), Label.class);
        testRun2Service.attachLabels(id, labels);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}/artifact-references")
    public void attachArtifactReferences(@PathVariable("id") Long id,
                                         @RequestBody @Valid AttachArtifactReferencesRequest request) {
        Set<ArtifactReference> references = mapper.mapToSet(request.getItems(), ArtifactReference.class);
        testRun2Service.attachArtifactReferences(id, references);
    }

}
