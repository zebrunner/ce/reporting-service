package com.zebrunner.reporting.web.response.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
public class CheckConnectionResponse {

    private boolean reachable;
    private String message;

}
