package com.zebrunner.reporting.domain.dto.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchResult<T> extends SearchCriteria{

    private long totalPages;
    private long totalResults;
    private Collection<T> results;

    @Builder
    public SearchResult(SearchCriteria searchCriteria, long totalPages, long totalResults, Collection<T> results) {
        super(searchCriteria.getQuery(), searchCriteria.getSortBy(), searchCriteria.getSortOrder(), searchCriteria.getPage(), searchCriteria.getPageSize());

        this.totalPages = totalPages;
        this.totalResults = totalResults;
        this.results = results;

    }

}
