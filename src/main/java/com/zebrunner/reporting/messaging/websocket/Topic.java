package com.zebrunner.reporting.messaging.websocket;

import com.zebrunner.reporting.domain.push.AbstractPush;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.zebrunner.reporting.persistence.PersistenceConfig.DEFAULT_SCHEMA;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Topic {

    public static String testRunStatistics() {
        return buildPath(AbstractPush.Type.TEST_RUN_STATISTICS, DEFAULT_SCHEMA);
    }

    public static String testRuns() {
        return buildPath(AbstractPush.Type.TEST_RUN, DEFAULT_SCHEMA);
    }

    public static String tests(Long testRunId) {
        return buildPath(AbstractPush.Type.TEST, DEFAULT_SCHEMA, testRunId);
    }

    public static String launchers() {
        return buildPath(AbstractPush.Type.LAUNCHER, DEFAULT_SCHEMA);
    }

    public static String launcherRuns() {
        return buildPath(AbstractPush.Type.LAUNCHER_RUN, DEFAULT_SCHEMA);
    }

    private static String buildPath(AbstractPush.Type type, Object... parameters) {
        return type.buildWebsocketPath(parameters);
    }

}
