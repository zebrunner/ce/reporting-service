package com.zebrunner.reporting.service.integration.testrail.impl;

import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.integration.TestRailConfig;
import com.zebrunner.reporting.domain.integration.testrail.TestCasePreview;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.TestRailClient;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.response.GetTestCaseResponse;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.response.TestRailPriorityResponse;
import com.zebrunner.reporting.service.integration.testrail.TestRailConfigService;
import com.zebrunner.reporting.service.integration.testrail.TestRailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestRailServiceImpl implements TestRailService {

    private final CryptoService cryptoService;
    private final TestRailConfigService testRailConfigService;

    @Override
    public TestCasePreview retrieveTestCasePreviewById(Long testCaseId) {
        TestRailConfig config = testRailConfigService.retrieveActive();

        TestRailClient testRailClient = initTestRailClient(config);
        GetTestCaseResponse testCaseResponse = testRailClient.getOptionalTestCaseById(testCaseId)
                                                             .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.TESTRAIL_TEST_CASE_IS_NOT_FOUND_BY_ID, testCaseId));

        Optional<TestRailPriorityResponse> maybePriority = testRailClient.getOptionalPriorityById(testCaseResponse.getPriorityId());
        return toTestCasePreview(testCaseResponse, maybePriority);
    }

    private TestCasePreview toTestCasePreview(GetTestCaseResponse caseResponse, Optional<TestRailPriorityResponse> maybePriority) {
        TestCasePreview.TestCasePreviewBuilder testCaseBuilder = TestCasePreview.builder();
        maybePriority.ifPresent(priority -> testCaseBuilder.priority(priority.getName()));

        if (!CollectionUtils.isEmpty(caseResponse.getSeparatedSteps())) {
            List<TestCasePreview.TestCaseStep> steps = caseResponse.getSeparatedSteps()
                                                                   .stream()
                                                                   .map(this::toStep)
                                                                   .collect(Collectors.toList());
            testCaseBuilder.separatedSteps(steps);
        }

        return testCaseBuilder.title(caseResponse.getTitle())
                              .steps(caseResponse.getSteps())
                              .expected(caseResponse.getExpected())
                              .preconditions(caseResponse.getPreconditions())
                              .build();
    }

    private TestCasePreview.TestCaseStep toStep(GetTestCaseResponse.SeparatedStep step) {
        return TestCasePreview.TestCaseStep.builder()
                                           .content(step.getContent())
                                           .expected(step.getExpected())
                                           .build();
    }

    private TestRailClient initTestRailClient(TestRailConfig testRailConfig) {
        return new TestRailClient(
                testRailConfig.getUrl(),
                testRailConfig.getUsername(),
                testRailConfig.isEncrypted()
                        ? cryptoService.decrypt(testRailConfig.getPassword())
                        : testRailConfig.getPassword(),
                testRailConfig.isEnabled());
    }

}
