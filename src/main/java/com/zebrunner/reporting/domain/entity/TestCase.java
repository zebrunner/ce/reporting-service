package com.zebrunner.reporting.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.domain.db.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "test_functions")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TestCase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String testClass;
    private String testMethod;
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private Status status = Status.UNKNOWN;
    private String info;

    @ManyToOne
    @JoinColumn(name = "test_suite_id")
    private TestSuite testSuite;

}
