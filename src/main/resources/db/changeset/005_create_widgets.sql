INSERT INTO DASHBOARDS (TITLE, HIDDEN, SYSTEM, POSITION, EDITABLE) VALUES ('General', FALSE, FALSE, 0, TRUE);
INSERT INTO DASHBOARDS (TITLE, HIDDEN, SYSTEM, POSITION, EDITABLE) VALUES ('Personal', FALSE, FALSE, 1001, TRUE);
INSERT INTO DASHBOARDS (TITLE, HIDDEN, SYSTEM, POSITION, EDITABLE) VALUES ('Failures analysis', TRUE, TRUE, 1002, TRUE);
INSERT INTO DASHBOARDS (TITLE, HIDDEN, SYSTEM, POSITION, EDITABLE) VALUES ('User Performance', TRUE, FALSE, 1003, TRUE);
INSERT INTO DASHBOARDS (TITLE, HIDDEN, SYSTEM, POSITION, EDITABLE) VALUES ('Stability', TRUE, TRUE, 1004, TRUE);

INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('LAST 24 HOURS', 'PIE', false, 'Consolidated information for last 24 hours', '{
  "PERIOD": "Last 24 Hours",
  "dashboardName": "Personal",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "RUNS": "",  
  "JOBS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (PIE)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('MONTHLY TESTS IMPLEMENTATION PROGRESS (NUMBER OF TEST METHODS IMPLEMENTED BY PERSON)', 'BAR', false, 'A number of new automated cases per month.', '{
  "PERIOD": "Total",
  "dashboardName": "User Performance",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "BUILDS": "",  
  "RUNS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""  
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'TEST CASES DEVELOPMENT TREND'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('TEST CASE STABILITY (%)', 'PIE', false, 'Aggregated stability metric for a test case.', '{
  "testCaseId": "1",
  "dashboardName": "Stability"
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'TEST CASE STABILITY'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('30 DAYS TESTS TREND', 'LINE', false, 'Consolidated test status trend for last 30 days.', '{
  "PERIOD": "Last 30 Days",
  "dashboardName": "General",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "RUNS": "",  
  "JOBS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (LINE)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('LAST 24 HOURS FAILURES', 'TABLE', false, 'Summarized information about tests failures grouped by reason.', '{
  "PERIOD": "Last 24 Hours",
  "JIRA_URL": "https://mycompany.atlassian.net/browse",
  "ERROR_COUNT": "0",
  "ENV": [],
  "PLATFORM": [],
  "LOCALE": [],
  "BROWSER": [],
  "PRIORITY": [],
  "USERS": "",
  "BUILDS": "",
  "RUNS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "dashboardName": "Personal",
  "currentUserId": "1",
  "userId": "1"
}', '{
  "COUNT": true,
  "REPORT": true,
  "MESSAGE": true,
  "ISSUE": true
}', (SELECT id FROM management.widget_templates WHERE name = 'FAILURES BY REASON'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('TEST CASE STABILITY TREND (%)', 'LINE', false, 'Test case stability trend on a monthly basis.', '{
  "testCaseId": 1,
  "dashboardName": "Stability"
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'TEST CASE STABILITY TREND'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('TEST CASE DURATION DETAILS (SEC)', 'LINE', false, 'All kind of duration metrics per test case.', '{
  "testCaseId": 1,
  "dashboardName": "Stability"
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'TEST CASE DURATION TREND'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('30 DAYS TREND', 'LINE', false, 'Consolidated test status trend for last 30 days', '{
  "PERIOD": "Last 30 Days",
  "dashboardName": "Personal",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "RUNS": "",  
  "JOBS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (LINE)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('TOTAL PERSONAL TESTS TREND', 'LINE', false, 'Totally consolidated personal test status trend.', '{
  "PERIOD": "Total",
  "dashboardName": "User Performance",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "RUNS": "",  
  "JOBS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL,(SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (LINE)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('FAILURE INFO', 'TABLE', false, 'High-level information about failure.', '{
  "PERIOD": "Total",
  "dashboardName": "Failures analysis",
  "hashcode": -1
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'FAILURES INFO'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('TEST CASE INFO', 'TABLE', false, 'Detailed test case information.', '{
  "testCaseId": 1,
  "dashboardName": "Stability"
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'TEST CASE INFO'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('FAILURE DETAILS', 'TABLE', false, 'All tests/jobs with a similar failure.', '{
  "PERIOD": "Last 24 Hours",
  "hashcode": -1,
  "dashboardName": "Failures analysis",
  "JIRA_URL": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'FAILURES DETAILS'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('30 DAYS PASS RATE BY OWNER (%)', 'BAR', false, 'Pass rate percent by owner for last 30 days.', '{
  "PERIOD": "Last 30 Days",
  "GROUP_BY": "OWNER",  
  "dashboardName": "General",  
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "BUILDS": "",  
  "RUNS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (BAR)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('30 DAYS TEST DETAILS', 'TABLE', false, 'Detailed information about passed, failed, skipped, etc tests for last 30 days.', '{
  "PERIOD": "Last 30 Days",
  "GROUP_BY": "OWNER",
  "ENV": [],
  "PLATFORM": [],
  "LOCALE": [],
  "BROWSER": [],
  "PRIORITY": [],
  "USERS": "",
  "BUILDS": "",
  "RUNS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "dashboardName": "General"
}', '{
  "OWNER": true,
  "RUN_NAME": false,
  "BUILD": false,
  "PASS": true,
  "FAIL": true,
  "DEFECT": false,
  "SKIP": true,
  "ABORT": false,
  "TOTAL": true,
  "PASSED (%)": true,
  "FAILED (%)": false,
  "KNOWN ISSUE (%)": false,
  "SKIPPED (%)": false,
  "FAIL RATE (%)": false
}', (SELECT id FROM management.widget_templates WHERE name = 'TESTS SUMMARY'));

INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('PERSONAL TOTAL TESTS (MAN-HOURS)', 'BAR', false, 'Monthly personal automation ROI by tests execution. 160+ hours for UI tests indicate that your execution ROI is very good.', '{
  "dashboardName": "User Performance",
  "ENV": [],
  "PLATFORM": [],  
  "LOCALE": [],
  "BROWSER":[],  
  "PRIORITY": [],
  "FEATURES": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'TESTS EXECUTION ROI (MAN-HOURS)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('30 DAYS TOTAL', 'PIE', false, 'Consolidated test status information for last 30 days.', '{
  "PERIOD": "Last 30 Days",
  "dashboardName": "General",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "RUNS": "",  
  "JOBS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (PIE)'));
INSERT INTO WIDGETS (TITLE, TYPE, REFRESHABLE, DESCRIPTION, PARAMS_CONFIG, LEGEND_CONFIG, WIDGET_TEMPLATE_ID) VALUES ('PERSONAL TOTAL RATE', 'PIE', false, 'Totally consolidated personal test status information.', '{
  "PERIOD": "Total",
  "dashboardName": "User Performance",
  "ENV": [],
  "PLATFORM": [],
  "BROWSER": [],
  "LOCALE": [],
  "PRIORITY": [],
  "FEATURES": "",
  "RUNS": "",  
  "JOBS": "",
  "MILESTONE": "",
  "MILESTONE_VERSION": "",
  "USERS": ""
}', NULL, (SELECT id FROM management.widget_templates WHERE name = 'PASS RATE (PIE)'));


--30 DAYS TEST DETAILS
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (1, 14, NULL, NULL, '{"x":0,"y":22,"width":4,"height":11}');
--30 DAYS PASS RATE BY OWNER (%)
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (1, 13, NULL, NULL, '{"x":4,"y":0,"width":8,"height":11}');
--30 DAYS TOTAL
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (1, 16, NULL, NULL, '{"x":0,"y":0,"width":4,"height":11}');
--30 DAYS TESTS TREND
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (1, 4, NULL, NULL, '{"x":4,"y":22,"width":8,"height":11}');

--30 DAYS TESTS PERSONAL TREND
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (2, 8, NULL, NULL, '{"x":4,"y":0,"width":8,"height":11}');
--LAST 24 HOURS PERSONAL
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (2, 1, NULL, NULL, '{"x":0,"y":0,"width":4,"height":11}');
--WEEKLY PERSONAL FAILURES
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (2, 5, NULL, NULL, '{"x":0,"y":74,"width":12,"height":9}');

--FAILURE INFO
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (3, 10, NULL, NULL, '{"x":0,"y":0,"width":12,"height":12}');
--LAST 24 HOURS FAILURE DETAILS
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (3, 12, NULL, NULL, '{"x":0,"y":12,"width":12,"height":10}');

--TOTAL PERSONAL TESTS TREND
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (4, 9, NULL, NULL, '{"x":0,"y":33,"width":12,"height":11}');
--PERSONAL TOTAL TESTS (MAN-HOURS)
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (4, 15, NULL, NULL, '{"x":4,"y":0,"width":8,"height":11}');
--PERSONAL TOTAL RATE
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (4, 17, NULL, NULL, '{"x":0,"y":0,"width":4,"height":11}');
--MONTHLY TESTS IMPLEMENTATION PROGRESS (NUMBER OF TEST METHODS IMPLEMENTED BY PERSON)
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (4, 2, NULL, NULL, '{"x":0,"y":22,"width":12,"height":11}');

--TEST CASE STABILITY (%)
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (5, 3, NULL, NULL, '{"x":0,"y":0,"width":4,"height":11}');
--TEST CASE STABILITY TREND (%)
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (5, 6, NULL, NULL, '{"x":0,"y":11,"width":12,"height":11}');
--TEST CASE DURATION DETAILS (SEC)
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (5, 7, NULL, NULL, '{"x":0,"y":22,"width":12,"height":11}');
--TEST CASE INFO
INSERT INTO DASHBOARDS_WIDGETS (DASHBOARD_ID, WIDGET_ID, POSITION, SIZE, LOCATION) VALUES (5, 11, NULL, NULL, '{"x":4,"y":0,"width":8,"height":11}');
