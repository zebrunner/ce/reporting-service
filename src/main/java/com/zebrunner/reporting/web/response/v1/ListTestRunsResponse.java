package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.reporting.domain.entity.TestRun;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListTestRunsResponse {

    private List<TestRunItem> items;

    @Data
    @NoArgsConstructor
    @JGlobalMap
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class TestRunItem {

        private Integer id;
        private String ciRunId;
        private LocalDateTime startedAt;
        private LocalDateTime endedAt;
        private String status;

        private Set<LabelItem> labels;
        private Set<ArtifactReferenceItem> artifactReferences;

        private TestRunStatistic statistic;

        @JMapConversion(from = "status", to = "status")
        public String convertStatus(TestRun.Status status) {
            return status.name();
        }

        @Data
        @NoArgsConstructor
        @JGlobalMap
        public static class LabelItem {

            private String key;
            private String value;

        }

        @Data
        @NoArgsConstructor
        @JGlobalMap
        public static class ArtifactReferenceItem {

            private String name;
            private String value;

        }

        @Data
        @NoArgsConstructor
        @JGlobalMap
        public static class TestRunStatistic {

            private Integer passedAmount;
            private Integer failedAmount;
            private Integer knownIssuesAmount;
            private Integer skippedAmount;
            private Integer inProgressAmount;
            private Integer abortedAmount;
            private Integer queuedAmount;

        }

    }

}
