package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.Dashboard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DashboardRepository extends JpaRepository<Dashboard, Long> {

    List<Dashboard> findAllByIsDefaultIsTrue();

    @Query(
            value = "SELECT d FROM Dashboard d JOIN DashboardSwitch ds ON d.id = ds.dashboardId " +
                    "WHERE ds.userId = :userId AND d.hidden = FALSE " +
                    "ORDER BY ds.switchedAt DESC",

            countQuery = "SELECT COUNT(d) FROM Dashboard d JOIN DashboardSwitch ds ON d.id = ds.dashboardId " +
                         "WHERE ds.userId = :userId AND d.hidden = FALSE"
    )
    Page<Dashboard> findLastByUserIdAndProjectIdOrderBySwitchedAtDesc(Long userId, Pageable pageable);

    Optional<Dashboard> findByTitleIgnoreCase(String title);

    Page<Dashboard> findAllByHiddenIsFalse(Pageable pageable);

    @Query(
            value = "SELECT d FROM Dashboard d WHERE LOWER(d.title) LIKE LOWER(CONCAT('%',:query,'%')) AND d.hidden = FALSE ORDER BY d.title ASC",
            countQuery = "SELECT COUNT(d) FROM Dashboard d WHERE LOWER(d.title) LIKE LOWER(CONCAT('%',:query,'%')) AND d.hidden = FALSE "
    )
    Page<Dashboard> findAllBySearchQuery(String query, Pageable pageable);

    boolean existsByTitleIgnoreCase(String title);

}
