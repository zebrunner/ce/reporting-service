package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.web.validation.UniqueNames;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@NoArgsConstructor
public class AttachArtifactReferencesRequest {

    @NotEmpty
    @UniqueNames
    private Set<@Valid ArtifactReference> items;

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class ArtifactReference {

        @NotBlank
        private String name;
        @NotBlank
        private String value;

    }

}
