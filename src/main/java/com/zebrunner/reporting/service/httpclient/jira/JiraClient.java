package com.zebrunner.reporting.service.httpclient.jira;

import com.zebrunner.common.eh.exception.ExternalSystemException;
import com.zebrunner.reporting.service.exception.ExternalSystemError;
import com.zebrunner.reporting.service.httpclient.jira.model.Issue;
import com.zebrunner.reporting.service.httpclient.jira.response.SearchIssuesResponse;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.Set;

@Slf4j
public class JiraClient {

    private final String serverUrl;
    private final String username;
    private final String token;

    public JiraClient(String url, String username, String token) {
        this.serverUrl = url;
        this.username = username;
        this.token = token;
    }

    /**
     * @see <a href="https://developer.atlassian.com/cloud/jira/platform/rest/v2/api-group-issue-search">
     *     Search issues endpoints
     *     </a>
     */
    public SearchIssuesResponse searchIssuesByIds(Set<String> ids) {
        try {
            String query = String.format("issueKey in (%s)", String.join(",", ids));
            HttpResponse<SearchIssuesResponse> response = Unirest.get(serverUrl + "/rest/api/3/search")
                                                                 .basicAuth(username, token)
                                                                 .queryString("jql", query)
                                                                 .queryString("validateQuery", "none")
                                                                 .queryString("fields", "summary,status,assignee,reporter")
                                                                 .asObject(SearchIssuesResponse.class);
            return response.getBody();
        } catch (Exception e) {
            log.warn("[Jira] Something went wrong while searching issues: ", e);
            throw new ExternalSystemException(ExternalSystemError.JIRA_UNABLE_TO_SEARCH_ISSUES);
        }
    }

    /**
     * @see <a href="https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-issues/#api-rest-api-3-issue-issueidorkey-get">
     *     Get issue endpoint
     *     </a>
     */
    public Optional<Issue> getOptionalIssueById(String id) {
        try {
            HttpResponse<Issue> response = Unirest.get(serverUrl + "/rest/api/3/issue/{issueId}")
                                                  .routeParam("issueId", id)
                                                  .basicAuth(username, token)
                                                  .asObject(Issue.class);
            return response.isSuccess()
                   ? Optional.of(response.getBody())
                   : Optional.empty();
        } catch (Exception e) {
            log.warn("[Jira] something went wrong while getting issue by id '{}': ", id, e);
            throw new ExternalSystemException(ExternalSystemError.JIRA_UNABLE_TO_GET_ISSUE_BY_ID, id);
        }
    }

}
