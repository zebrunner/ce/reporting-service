package com.zebrunner.reporting.domain.dto.reporting;

import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestRunStart {

    private String uuid;
    private String name;
    private OffsetDateTime startedAt;
    private TestRun.Status status;
    private String framework;
    private String environment;
    private String build;
    private JenkinsContext jenkinsContext;
    private CiContext ciContext;
    private Set<NotificationTarget> notificationTargets;

    public Optional<String> getJenkinsJobUrl() {
        return Optional.ofNullable(jenkinsContext)
                       .map(JenkinsContext::getJobUrl);
    }

    public Optional<Integer> getJenkinsJobNumber() {
        return Optional.ofNullable(jenkinsContext)
                       .map(JenkinsContext::getJobNumber);
    }

    public Optional<String> getParentJenkinsJobUrl() {
        return Optional.ofNullable(jenkinsContext)
                       .map(JenkinsContext::getParentJobUrl);
    }

    public Optional<Integer> getParentJenkinsJobNumber() {
        return Optional.ofNullable(jenkinsContext)
                       .map(JenkinsContext::getParentJobNumber);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class JenkinsContext {

        private String jobUrl;
        private Integer jobNumber;
        private String parentJobUrl;
        private Integer parentJobNumber;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CiContext {

        private TestRunCiContext.Type type;
        private Map<String, String> envVariables = Map.of();

    }

}
