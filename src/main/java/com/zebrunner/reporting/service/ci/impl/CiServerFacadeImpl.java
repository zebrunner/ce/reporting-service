package com.zebrunner.reporting.service.ci.impl;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.reporting.domain.db.AbstractEntity;
import com.zebrunner.reporting.domain.dto.BuildParameterType;
import com.zebrunner.reporting.domain.entity.RerunMetadata;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;
import com.zebrunner.reporting.domain.vo.RunContext;
import com.zebrunner.reporting.messaging.websocket.WebsocketNotificationSender;
import com.zebrunner.reporting.persistence.repository.TestRunCiContextRepository;
import com.zebrunner.reporting.service.RunContextService;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.ci.CiServerFacade;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CiServerFacadeImpl implements CiServerFacade {

    private final TestRun2Service testRun2Service;
    private final TestRunCiContextRepository testRunCiContextRepository;
    private final WebsocketNotificationSender websocketNotificationSender;
    private final RunContextService runContextService;
    private final JenkinsClient jenkinsClient;

    @Override
    public List<BuildParameterType> retrieveLegacyBuildParameters(Long testRunId) {
        TestRunCiContext ciContext = testRunCiContextRepository.findById(testRunId)
                                                               .orElseThrow(() -> new BusinessConstraintException(BusinessConstraintError.UNKNOWN_CI_ENVIRONMENT));
        return jenkinsClient.retrieveLegacyBuildParameters(ciContext);
    }

    @Override
    public void populateLegacyJobsAndBuildNumbers(Collection<com.zebrunner.reporting.domain.db.TestRun> testRuns) {
        Map<Long, com.zebrunner.reporting.domain.db.TestRun> idToTestRun
                = testRuns.stream()
                          .filter(testRun -> testRun.getJob() == null)
                          .collect(Collectors.toMap(AbstractEntity::getId, Function.identity()));
        List<TestRunCiContext> ciContexts = testRunCiContextRepository.findAllById(idToTestRun.keySet());

        for (TestRunCiContext ciContext : ciContexts) {
            com.zebrunner.reporting.domain.db.TestRun testRun = idToTestRun.get(ciContext.getTestSuiteExecutionId());
            jenkinsClient.populateLegacyJobAndBuildNumber(testRun, ciContext);
        }
    }

    @Override
    @SneakyThrows
    public void build(Long baseTestRunId, Map<String, String> parameters, Long userId) {
        TestRunCiContext ciContext = testRunCiContextRepository.findById(baseTestRunId)
                                                               .orElseThrow(() -> new BusinessConstraintException(BusinessConstraintError.UNKNOWN_CI_ENVIRONMENT));

        RunContext runContext = runContextService.generateNew();
        jenkinsClient.build(ciContext, parameters, runContext.asJsonString());
    }

    @Override
    public void abort(Long testRunId) {
        TestRunCiContext ciContext = testRunCiContextRepository.findById(testRunId)
                                                               .orElseThrow(() -> new BusinessConstraintException(BusinessConstraintError.UNKNOWN_CI_ENVIRONMENT));
        jenkinsClient.abort(ciContext);
    }

    @Override
    @SneakyThrows
    public void rerun(Long testRunId, boolean onlyFailures, Long userId) {
        TestRun testRun = testRun2Service.retrieveById(testRunId);
        TestRunCiContext ciContext = Optional.ofNullable(testRun.getCiContext())
                                             .orElseThrow(() -> new BusinessConstraintException(BusinessConstraintError.UNKNOWN_CI_ENVIRONMENT));

        RunContext runContext = buildRunContextForRerun(testRun, onlyFailures);
        String serializedRunContext = runContext.asJsonString();

        TestRunCiContext newCiContext = jenkinsClient.rerun(ciContext, serializedRunContext);

        RerunMetadata rerunMetadata = new RerunMetadata(userId, Instant.now(), serializedRunContext);
        testRun2Service.saveQueuedRerun(testRunId, newCiContext, rerunMetadata);
        websocketNotificationSender.notifyAboutTestRun(testRunId);
    }

    private RunContext buildRunContextForRerun(TestRun testRun, boolean onlyFailures) {
        Set<Test.Status> statuses = null;
        Boolean knownIssue = null;
        if (onlyFailures) {
            statuses = Set.of(Test.Status.FAILED, Test.Status.ABORTED, Test.Status.SKIPPED);
            knownIssue = false;
        }

        String testSuiteExecutionUuid = testRun.getCiRunId();
        RunContext runContext = runContextService.generateForRerunAsString(testSuiteExecutionUuid, statuses, knownIssue);

        log.info("Run context for rerun of test run with id '{}' is '{}'", testRun.getId(), runContext);
        return runContext;
    }

    @Override
    public boolean tryAbortExecutingNowQueuedBuild(Long testRunId) {
        Optional<TestRunCiContext> maybeCiContext = testRunCiContextRepository.findById(testRunId);
        if (maybeCiContext.isPresent()) {
            TestRunCiContext ciContext = maybeCiContext.get();
            return jenkinsClient.tryAbortExecutingNowQueuedBuild(ciContext);
        }
        return false;
    }

}
