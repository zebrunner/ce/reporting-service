package com.zebrunner.reporting.service.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class URLResolver {

    @Value("${service.web-url}")
    private String webURL;

    @Value("${service.api-url}")
    private String webserviceURL;

    public String buildWebURL() {
        return webURL;
    }

    public String buildWebserviceUrl() {
        return webserviceURL;
    }

}
