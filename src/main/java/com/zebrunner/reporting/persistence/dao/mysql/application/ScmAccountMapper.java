package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.ScmAccount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface ScmAccountMapper {

    void create(ScmAccount scmAccount);

    Optional<ScmAccount> findById(Long id);

    Optional<ScmAccount> findByRepo(String repo);

    List<ScmAccount> findAll();

    void update(ScmAccount scmAccount);

    void deleteById(Long id);

    void reassignUser(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

}
