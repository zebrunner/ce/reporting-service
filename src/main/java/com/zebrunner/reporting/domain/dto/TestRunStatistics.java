package com.zebrunner.reporting.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TestRunStatistics implements Serializable {

    private static final long serialVersionUID = -1915862891525912222L;

    private long testRunId;
    private int passed;
    private int failed;
    private int failedAsKnown;
    private int skipped;
    private int inProgress;
    private int aborted;
    private boolean reviewed;

}
