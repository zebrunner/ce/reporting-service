package com.zebrunner.reporting.domain.push.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDeletedMessage {

    private Integer id;
    private String name;

}
