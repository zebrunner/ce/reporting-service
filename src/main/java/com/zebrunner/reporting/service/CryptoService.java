package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.Setting;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.KeyGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CryptoService {

    @Value("${service.crypto-salt}")
    private final String salt;
    private final SettingsService settingsService;
    @Lazy
    private final List<CryptoDriven<?>> cryptoDrivenServices;

    public String encrypt(String strToEncrypt) {
        BasicTextEncryptor basicTextEncryptor = getBasicTextEncryptor();
        return basicTextEncryptor.encrypt(strToEncrypt);
    }

    public String decrypt(String strToDecrypt) {
        BasicTextEncryptor basicTextEncryptor = getBasicTextEncryptor();
        return basicTextEncryptor.decrypt(strToDecrypt);
    }

    @Transactional
    public void generateKeyIfNeed() {
        String result = getCryptoKey();
        if (StringUtils.isBlank(result)) {
            reencrypt();
        }
    }

    @SuppressWarnings("unchecked")
    private void reencrypt() {
        Map<CryptoDriven, Collection<?>> tempMap = new HashMap<>(collectCollectionsToReencrypt());
        tempMap.forEach(this::decryptCryptoDrivenServiceCollection);

        regenerateKey();

        tempMap.forEach((cryptoDrivenService, collection) -> {
            encryptCryptoDrivenServiceCollection(cryptoDrivenService, collection);
            cryptoDrivenService.afterReencryptOperation(collection);
        });
    }

    @Transactional
    public void regenerateKey() {
        try {
            int cryptoKeySize = getCryptoKeySize();
            String cryptoKeyType = getCryptoKeyType();

            String newKey = Base64.getEncoder().encodeToString(generateKey(cryptoKeyType, cryptoKeySize));
            settingsService.saveNewKey(newKey);
        } catch (Exception e) {
            throw new RuntimeException("Could not generate a new crypto key.", e);
        }
    }

    private Map<CryptoDriven, Collection<?>> collectCollectionsToReencrypt() {
        return cryptoDrivenServices.stream()
                                   .collect(Collectors.toMap(
                                           Function.identity(),
                                           CryptoDriven::getEncryptedCollection
                                   ));
    }

    @SuppressWarnings("unchecked")
    private void decryptCryptoDrivenServiceCollection(CryptoDriven cryptoDrivenService, Collection<?> collection) {
        collection.forEach(item -> {
            String encryptedValue = cryptoDrivenService.getEncryptedValue(item);
            String decryptedValue = decrypt(encryptedValue);
            cryptoDrivenService.setEncryptedValue(item, decryptedValue);
        });
    }

    @SuppressWarnings("unchecked")
    private void encryptCryptoDrivenServiceCollection(CryptoDriven cryptoDrivenService, Collection<?> collection) {
        collection.forEach(item -> {
            String decryptedValue = cryptoDrivenService.getEncryptedValue(item);
            String encryptedValue = encrypt(decryptedValue);
            cryptoDrivenService.setEncryptedValue(item, encryptedValue);
        });
    }

    private static byte[] generateKey(String keyType, int size) throws NoSuchAlgorithmException {
        log.debug("generating key use algorithm: '" + keyType + "'; size: " + size);
        KeyGenerator keyGenerator = KeyGenerator.getInstance(keyType);
        keyGenerator.init(size);
        return keyGenerator.generateKey().getEncoded();
    }

    private BasicTextEncryptor getBasicTextEncryptor() {
        String key = getCryptoKey();
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(key + salt);
        return encryptor;
    }

    private String getCryptoKey() {
        Setting setting = settingsService.getSettingByName("KEY");
        return setting.getValue();
    }

    private String getCryptoKeyType() {
        Setting setting = settingsService.getSettingByName("CRYPTO_KEY_TYPE");
        return setting.getValue();
    }

    private int getCryptoKeySize() {
        Setting setting = settingsService.getSettingByName("CRYPTO_KEY_SIZE");
        return Integer.parseInt(setting.getValue());
    }

}
