package com.zebrunner.reporting.domain.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "dashboards")
@NamedEntityGraph(
        name = "dashboard.expanded",
        attributeNodes = {
                @NamedAttributeNode(value = "widgets", subgraph = "widgets-subgraph")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "widgets-subgraph",
                        attributeNodes = {
                                @NamedAttributeNode("widget")
                        }
                )
        }
)
public class Dashboard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private boolean hidden;
    private boolean editable;
    private boolean isDefault;

    @CreationTimestamp
    private Instant createdAt;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "dashboard_id", insertable = false, updatable = false)
    private List<DashboardWidget> widgets = new ArrayList<>();

}
