package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@NoArgsConstructor
public class AttachLabelsRequest {

    @NotEmpty
    private Set<@Valid Label> items;

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Label {

        @NotBlank
        private String key;
        @NotBlank
        private String value;

    }

}
