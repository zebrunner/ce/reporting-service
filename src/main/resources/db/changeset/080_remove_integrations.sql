ALTER TABLE jobs
    DROP CONSTRAINT IF EXISTS jobs_automation_server_id_fkey;
ALTER TABLE jobs
    DROP COLUMN IF EXISTS automation_server_id;

ALTER TABLE launcher_presets
    DROP CONSTRAINT IF EXISTS fk_launcher_preset_integrations1;
ALTER TABLE launcher_presets
    DROP COLUMN IF EXISTS provider_id;

DROP TABLE IF EXISTS integration_settings;
DROP TABLE IF EXISTS integration_params;
DROP TABLE IF EXISTS integrations;
DROP TABLE IF EXISTS integration_types;
DROP TABLE IF EXISTS integration_groups;
