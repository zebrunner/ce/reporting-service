package com.zebrunner.reporting.domain.dto.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class SearchCriteria {

    private String query;
    private String sortBy;
    private SortOrder sortOrder;
    private final int page;
    private final int pageSize;

    public SortOrder getSortOrder() {
        return sortBy != null && sortOrder == null ? SortOrder.ASC : sortOrder;
    }

}

