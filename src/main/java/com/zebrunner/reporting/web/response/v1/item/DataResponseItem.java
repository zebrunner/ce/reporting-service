package com.zebrunner.reporting.web.response.v1.item;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataResponseItem<T> extends ResponseItem {

    private T data;

    private DataResponseItem(String key, boolean succeeded, T data) {
        super(key, succeeded);
        this.data = data;
    }

    public static <T> DataResponseItem<T> succeeded(String key, T data) {
        return new DataResponseItem<>(key, true, data);
    }

}
