package com.zebrunner.reporting.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("service.test-session.test-artifact")
public class TestSessionArtifactProperties {

    private Artifact video;
    private Artifact log;
    private Artifact metadata;
    private VNC vnc;

    @Data
    public static class Artifact {

        private String name;
        private String defaultLinkPattern;

    }

    @Data
    public static class VNC {

        private String namePattern;
        private String defaultLinkPattern;

    }

}
