package com.zebrunner.reporting.service.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.zebrunner.reporting.domain.entity.TestSession;
import com.zebrunner.reporting.service.config.TestSessionArtifactProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.zebrunner.reporting.service.util.CapabilityJsonUtils.getTextualCapability;
import static com.zebrunner.reporting.service.util.CapabilityJsonUtils.isCapabilityFalse;

@Service
@RequiredArgsConstructor
public class CapabilitiesManager {

    private static final String ZEBRUNNER_OPTIONS = "zebrunner:options";

    private static final List<Function<JsonNode, JsonNode>> NAME_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("name"),
            jsonNode -> jsonNode.path("sessionName")
    );
    private static final List<Function<JsonNode, JsonNode>> BROWSER_NAME_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("browserName")
    );
    private static final List<Function<JsonNode, JsonNode>> BROWSER_VERSION_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("browserVersion"),
            jsonNode -> jsonNode.path("version")
    );
    private static final List<Function<JsonNode, JsonNode>> PLATFORM_NAME_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("slotCapabilities").path("platformName"),
            jsonNode -> jsonNode.path("desired").path("platformName"),
            jsonNode -> jsonNode.path("platformName"),
            jsonNode -> jsonNode.path("platform")
    );
    private static final List<Function<JsonNode, JsonNode>> PLATFORM_VERSION_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("slotCapabilities").path("platformVersion"),
            jsonNode -> jsonNode.path("slotCapabilities").path("version"),
            jsonNode -> jsonNode.path("platformVersion")
    );
    private static final List<Function<JsonNode, JsonNode>> DEVICE_NAME_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("slotCapabilities").path("deviceName"),
            jsonNode -> jsonNode.path("deviceModel")
    );
    private static final List<Function<JsonNode, JsonNode>> VNC_LINK_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("slotCapabilities").path("vncLink"),
            jsonNode -> jsonNode.path("vncLink"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("vncLink")
    );
    private static final List<Function<JsonNode, JsonNode>> ENABLE_VIDEO_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("enableVideo"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("enableVideo")
    );
    private static final List<Function<JsonNode, JsonNode>> VIDEO_LINK_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("videoLink"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("videoLink")
    );
    private static final List<Function<JsonNode, JsonNode>> ENABLE_LOG_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("enableLog"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("enableLog")
    );
    private static final List<Function<JsonNode, JsonNode>> LOG_LINK_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("logLink"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("logLink")
    );
    private static final List<Function<JsonNode, JsonNode>> ENABLE_METADATA_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("enableMetadata"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("enableMetadata")
    );
    private static final List<Function<JsonNode, JsonNode>> METADATA_LINK_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("metadataLink"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("metadataLink")
    );
    private static final List<Function<JsonNode, JsonNode>> ENABLE_VNC_EXTRACTORS = List.of(
            jsonNode -> jsonNode.path("enableVNC"),
            jsonNode -> jsonNode.path("enableVnc"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("enableVNC"),
            jsonNode -> jsonNode.path(ZEBRUNNER_OPTIONS).path("enableVnc")
    );

    private final TestSessionArtifactProperties artifactProperties;

    public Optional<String> getName(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), NAME_EXTRACTORS)
                .or(() -> getTextualCapability(testSession.getDesiredCapabilities(), NAME_EXTRACTORS));
    }

    public Optional<String> getBrowserName(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), BROWSER_NAME_EXTRACTORS);
    }

    public Optional<String> getSessionName(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), NAME_EXTRACTORS)
                .or(() -> getTextualCapability(testSession.getDesiredCapabilities(), NAME_EXTRACTORS));
    }

    public Optional<String> getBrowserVersion(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), BROWSER_VERSION_EXTRACTORS);
    }

    public Optional<String> getPlatformName(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), PLATFORM_NAME_EXTRACTORS);
    }

    public Optional<String> getPlatformVersion(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), PLATFORM_VERSION_EXTRACTORS);
    }

    public Optional<String> getDeviceName(TestSession testSession) {
        return getTextualCapability(testSession.getCapabilities(), DEVICE_NAME_EXTRACTORS);
    }

    public Optional<String> getVideoLink(TestSession testSession) {
        String defaultLinkPattern = artifactProperties.getVideo().getDefaultLinkPattern();

        return !isCapabilityFalse(testSession.getDesiredCapabilities(), ENABLE_VIDEO_EXTRACTORS)
                ? getTextualCapability(testSession.getDesiredCapabilities(), VIDEO_LINK_EXTRACTORS).or(() -> Optional.of(defaultLinkPattern))
                : Optional.empty();
    }

    public Optional<String> getLogLink(TestSession testSession) {
        String defaultLinkPattern = artifactProperties.getLog().getDefaultLinkPattern();

        return !isCapabilityFalse(testSession.getDesiredCapabilities(), ENABLE_LOG_EXTRACTORS)
                ? getTextualCapability(testSession.getDesiredCapabilities(), LOG_LINK_EXTRACTORS).or(() -> Optional.of(defaultLinkPattern))
                : Optional.empty();
    }

    public Optional<String> getMetadataLink(TestSession testSession) {
        String defaultLinkPattern = artifactProperties.getMetadata().getDefaultLinkPattern();

        return !isCapabilityFalse(testSession.getDesiredCapabilities(), ENABLE_METADATA_EXTRACTORS)
                ? getTextualCapability(testSession.getDesiredCapabilities(), METADATA_LINK_EXTRACTORS).or(() -> Optional.of(defaultLinkPattern))
                : Optional.empty();
    }

    public boolean isVncEnabled(TestSession session) {
        return isCapabilityFalse(session.getDesiredCapabilities(), ENABLE_VNC_EXTRACTORS);
    }

    public Optional<String> getVncLink(TestSession session) {
        return getTextualCapability(session.getCapabilities(), VNC_LINK_EXTRACTORS)
                .or(() -> getTextualCapability(session.getDesiredCapabilities(), VNC_LINK_EXTRACTORS));
    }

}
