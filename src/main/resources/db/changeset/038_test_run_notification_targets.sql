CREATE TABLE notification_targets (
  id BIGSERIAL,
  type VARCHAR(127) NOT NULL,
  value VARCHAR(3999) NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (type, value)
);
CREATE TRIGGER update_timestamp_notification_targets
  BEFORE INSERT OR UPDATE ON notification_targets
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();

CREATE TABLE test_runs_notification_targets (
  id BIGSERIAL,
  test_run_id BIGINT NOT NULL,
  notification_target_id BIGINT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (test_run_id) REFERENCES test_runs (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  FOREIGN KEY (notification_target_id) REFERENCES notification_targets (id),
  UNIQUE (test_run_id, notification_target_id)
);
CREATE TRIGGER update_timestamp_test_runs_notification_targets
  BEFORE INSERT OR UPDATE ON test_runs_notification_targets
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();
