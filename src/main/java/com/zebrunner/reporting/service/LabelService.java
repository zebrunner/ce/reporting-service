package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.Label;

import java.util.Collection;
import java.util.Set;

public interface LabelService {

    Set<Label> save(Collection<Label> labels);

}
