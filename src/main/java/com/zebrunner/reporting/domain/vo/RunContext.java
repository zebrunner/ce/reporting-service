package com.zebrunner.reporting.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RunContext {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private String testRunUuid;
    private Mode mode;
    private RerunCriteria rerunCriteria = new RerunCriteria();

    public RunContext(String testRunUuid, Mode mode) {
        this.testRunUuid = testRunUuid;
        this.mode = mode;
    }

    @SneakyThrows
    public String asJsonString() {
        return OBJECT_MAPPER.writeValueAsString(this);
    }

    @SneakyThrows
    public static RunContext parseJsonString(String json) {
        return OBJECT_MAPPER.readValue(json, RunContext.class);
    }

    public enum Mode {

        @Deprecated
        LEGACY,
        NEW,
        RERUN

    }

}
