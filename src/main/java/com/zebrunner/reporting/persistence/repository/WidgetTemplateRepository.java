package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WidgetTemplateRepository extends JpaRepository<WidgetTemplate, Long> {

    List<WidgetTemplate> findByOrderByNameAllIgnoreCaseAsc();

}
