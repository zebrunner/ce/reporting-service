CREATE TABLE IF NOT EXISTS dashboard_switches (
    id BIGSERIAL NOT NULL,
    dashboard_id BIGINT NOT NULL,
    user_id BIGINT NOT NULL,
    switched_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    FOREIGN KEY (dashboard_id) REFERENCES dashboards (id) ON DELETE CASCADE,
    UNIQUE (dashboard_id, user_id)
);

ALTER TABLE IF EXISTS dashboards DROP COLUMN position;
