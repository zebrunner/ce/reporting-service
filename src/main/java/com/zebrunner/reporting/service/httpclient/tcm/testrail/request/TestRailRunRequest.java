package com.zebrunner.reporting.service.httpclient.tcm.testrail.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestRailRunRequest {

    private String name;
    private Long milestoneId;
    private Long assignedtoId;
    private Long suiteId;
    private boolean includeAll;
    private List<Long> caseIds;

}
