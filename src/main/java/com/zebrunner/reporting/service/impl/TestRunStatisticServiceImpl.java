package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.TestRunStatistic;
import com.zebrunner.reporting.persistence.repository.TestRunStatisticRepository;
import com.zebrunner.reporting.service.TestRunStatisticService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TestRunStatisticServiceImpl implements TestRunStatisticService {

    private final TestRunStatisticRepository statisticRepository;

    @Override
    public TestRunStatistic getByTestRunId(Long testRunId) {
        return statisticRepository.findById(testRunId.intValue())
                                  .orElseThrow(RuntimeException::new);
    }

    @Override
    public void save(TestRunStatistic statistic) {
        statisticRepository.save(statistic);
    }

}
