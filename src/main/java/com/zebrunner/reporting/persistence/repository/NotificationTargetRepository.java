package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.NotificationTarget;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NotificationTargetRepository extends JpaRepository<NotificationTarget, Long> {

    Optional<NotificationTarget> findByTypeAndValue(NotificationTarget.Type type, String value);

}
