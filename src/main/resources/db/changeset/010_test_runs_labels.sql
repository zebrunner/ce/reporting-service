ALTER TABLE tags RENAME TO labels;
ALTER TABLE labels RENAME COLUMN name TO key;
ALTER TABLE labels RENAME CONSTRAINT tags_pkey TO labels_pkey;

ALTER INDEX tags_name RENAME TO labels_key;
ALTER INDEX tags_unique RENAME TO labels_unique;
ALTER INDEX tags_value RENAME TO labels_value;

ALTER TABLE test_tags RENAME TO tests_labels;
ALTER TABLE tests_labels RENAME COLUMN tag_id TO label_id;
-- ALTER TABLE tests_labels RENAME CONSTRAINT test_tags_pkey TO tests_labels_pkey;
-- ALTER TABLE tests_labels RENAME CONSTRAINT fk_tags_test_tags1 TO fk_labels_test_labels1;
-- ALTER TABLE tests_labels RENAME CONSTRAINT fk_tests_test_tags1 TO fk_tests_test_labels1;

ALTER INDEX fk_tags_test_tags1_idx RENAME TO fk_labels_tests_labels1_idx;
ALTER INDEX fk_tests_test_tags1_idx RENAME TO fk_tests_tests_labels1_idx;
ALTER INDEX test_tags_unique RENAME TO tests_labels_unique;

CREATE TABLE test_runs_labels (
  id SERIAL,
  test_run_id INT NOT NULL,
  label_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT now(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (test_run_id) REFERENCES test_runs (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  FOREIGN KEY (label_id) REFERENCES labels (id),
  UNIQUE (test_run_id, label_id)
);
CREATE TRIGGER update_timestamp_test_runs_labels
  BEFORE INSERT OR UPDATE ON test_runs_labels
  FOR EACH ROW EXECUTE PROCEDURE update_modified_at();
