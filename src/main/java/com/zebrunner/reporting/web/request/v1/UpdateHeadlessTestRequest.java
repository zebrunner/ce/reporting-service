package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@JGlobalMap
@NoArgsConstructor
public class UpdateHeadlessTestRequest {

    @NotBlank
    private String name;
    private String correlationData;
    @NotBlank
    private String className;
    @NotBlank
    private String methodName;
    private String maintainer;
    private String testCase;

    private Set<@Valid Label> labels = Set.of();

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Label {

        @NotBlank
        private String key;
        @NotBlank
        private String value;

    }

}
