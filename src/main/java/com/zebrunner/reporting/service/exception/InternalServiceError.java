package com.zebrunner.reporting.service.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum InternalServiceError implements com.zebrunner.common.eh.error.InternalServiceError {

    WIDGET_INVALID_CHART_QUERY(2100),
    UNPROCESSABLE_XML_ENTITY(2101),
    MALFORMED_FREEMARKER_TEMPLATE(2102),
    UNPROCESSABLE_JSON_ENTITY(2103),
    UNABLE_TO_PARSE_URI(2104),
    UNABLE_TO_CREATE_INPUT_STREAM(2105),
    CANNOT_CREATE_ADAPTER(2106),
    INCORRECT_ADAPTER_CONSTRUCTOR(2107),
    UNABLE_TO_DECRYPT_TOKEN(2108),
    UNABLE_TO_GENERATE_CHART_QUERY(2109),
    EMPTY_SQL_QUERY_CANNOT_BE_EXECUTED(2110),
    SERVICE_METHOD_IS_NOT_IMPLEMENTED(2111);

    private final int code;

}
