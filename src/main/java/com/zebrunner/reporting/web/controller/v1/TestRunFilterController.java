package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.common.auth.web.annotation.HasPermission;
import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.reporting.domain.entity.TestRunFilter;
import com.zebrunner.reporting.domain.entity.UserFavoriteTestRunFilter;
import com.zebrunner.reporting.service.TestRunFilterService;
import com.zebrunner.reporting.service.UserFavoriteTestRunFilterService;
import com.zebrunner.reporting.web.patch.PatchAction;
import com.zebrunner.reporting.web.patch.PatchOperation;
import com.zebrunner.reporting.web.patch.PatchRequest;
import com.zebrunner.reporting.web.request.v1.CreateTestRunFilterRequest;
import com.zebrunner.reporting.web.response.v1.TestRunFilterResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Validated
@CrossOrigin
@RestController("reportingTestRunFiltersController")
@RequiredArgsConstructor
@RequestMapping(path = "/v1/test-run-filters", produces = MediaType.APPLICATION_JSON_VALUE)
public class TestRunFilterController {

    private final JMapper jMapper;
    private final TestRunFilterService testRunFilterService;
    private final UserFavoriteTestRunFilterService userFavoriteTestRunFilterService;

    @GetMapping
    public List<TestRunFilterResponse> getTestRunFilters(@PrincipalId Integer userId) {
        List<TestRunFilter> testRunFilters = testRunFilterService.retrieveByUserId(userId.longValue());
        List<UserFavoriteTestRunFilter> favoriteFilters = userFavoriteTestRunFilterService
                .retrieveFavoriteFilters(userId);
        return mapFavoriteFilters(jMapper.mapToList(testRunFilters, TestRunFilterResponse.class), favoriteFilters);
    }

    private List<TestRunFilterResponse> mapFavoriteFilters(List<TestRunFilterResponse> responses, List<UserFavoriteTestRunFilter> userFavoriteFilters) {
        Set<Integer> favoriteFilters = userFavoriteFilters.stream()
                                                          .map(UserFavoriteTestRunFilter::getTestRunFilterId)
                                                          .collect(Collectors.toSet());
        return responses.stream()
                        .peek(filter -> filter.setIsFavorite(favoriteFilters.contains(filter.getId())))
                        .sorted(Comparator.comparing(TestRunFilterResponse::getIsFavorite)
                                          .thenComparing(TestRunFilterResponse::getCreatedAt)
                                          .reversed())
                        .collect(Collectors.toList());
    }

    @PostMapping
    public TestRunFilterResponse createTestRunFilter(@RequestBody @Validated CreateTestRunFilterRequest request,
                                                     @PrincipalId Integer userId) {
        TestRunFilter testRunFilterToCreate = jMapper.map(request, TestRunFilter.class);
        TestRunFilter testRunFilter = testRunFilterService.create(testRunFilterToCreate, userId.longValue());
        return jMapper.map(testRunFilter, TestRunFilterResponse.class);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void patch(@PathVariable("id") Integer id,
                      @PrincipalId Integer userId,
                      @PatchAction(op = PatchOperation.REPLACE, path = "/isPrivate")
                      @PatchAction(op = PatchOperation.REPLACE, path = "/isFavorite")
                      @PatchAction(op = PatchOperation.REPLACE, path = "/items")
                      @Valid @RequestBody PatchRequest patchRequest,
                      @HasPermission("reporting:filters:create-public") boolean markFilterAsPublicPermission) {
        @SuppressWarnings("unchecked")
        Map<String, List<String>> testRunFilterItems = patchRequest.getReplaceValue("/items", Map.class);
        Boolean isFavorite = patchRequest.getReplaceValue("/isFavorite", Boolean.class);
        Boolean isPrivate = markFilterAsPublicPermission
                ? patchRequest.getReplaceValue("/isPrivate", Boolean.class)
                : null;
        TestRunFilter testRunFilter = TestRunFilter.builder()
                                                   .id(id)
                                                   .isPrivate(isPrivate)
                                                   .items(testRunFilterItems)
                                                   .build();
        testRunFilterService.patch(testRunFilter, userId, isFavorite);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id,
                       @HasPermission("reporting:filters:delete-any") boolean deleteAnyFilterPermission,
                       @PrincipalId Integer userId) {
        TestRunFilter testRunFilter = testRunFilterService.retrieveById(id);
        if (deleteAnyFilterPermission || Objects.equals(userId.longValue(), testRunFilter.getOwner().getId())) {
            testRunFilterService.deleteById(id);
        }
    }

}
