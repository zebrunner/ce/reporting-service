ALTER TABLE test_runs ADD COLUMN name VARCHAR(255);

UPDATE test_runs tr
SET name = ts.name
FROM test_suites ts
WHERE tr.test_suite_id = ts.id;
