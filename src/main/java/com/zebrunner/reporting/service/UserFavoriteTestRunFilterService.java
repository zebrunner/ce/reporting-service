package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.UserFavoriteTestRunFilter;

import java.util.List;

public interface UserFavoriteTestRunFilterService {

    List<UserFavoriteTestRunFilter> retrieveFavoriteFilters(Integer userId);

}
