ALTER TABLE test_runs_artifact_references
  DROP CONSTRAINT test_runs_artifact_references_artifact_reference_id_fkey,
  ADD CONSTRAINT test_runs_artifact_references_artifact_reference_id_fkey
    FOREIGN KEY (artifact_reference_id)
      REFERENCES artifact_references (id)
      ON DELETE CASCADE;

ALTER TABLE tests_artifact_references
  DROP CONSTRAINT tests_artifact_references_artifact_reference_id_fkey,
  ADD CONSTRAINT tests_artifact_references_artifact_reference_id_fkey
    FOREIGN KEY (artifact_reference_id)
      REFERENCES artifact_references (id)
      ON DELETE CASCADE;


DROP TABLE IF EXISTS test_sessions_artifact_references;
CREATE TABLE test_sessions_artifact_references (
  id SERIAL,
  test_session_id INT NOT NULL,
  artifact_reference_id INT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (test_session_id) REFERENCES test_sessions_transient (id)
    ON DELETE CASCADE,
  FOREIGN KEY (artifact_reference_id) REFERENCES artifact_references (id)
    ON DELETE CASCADE,
  UNIQUE (test_session_id, artifact_reference_id)
);
CREATE TRIGGER update_timestamp_test_sessions_artifact_references
  BEFORE INSERT OR UPDATE
  ON test_sessions_artifact_references
  FOR EACH ROW
EXECUTE PROCEDURE update_modified_at();
