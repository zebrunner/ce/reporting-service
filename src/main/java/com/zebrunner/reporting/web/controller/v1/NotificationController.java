package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.common.auth.user.AuthenticatedUser;
import com.zebrunner.common.auth.web.annotation.Principal;
import com.zebrunner.reporting.domain.entity.NotificationTarget;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.notification.TestRunResultSummary;
import com.zebrunner.reporting.service.NotificationTargetService;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestRunResultSummaryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/notifications", produces = MediaType.APPLICATION_JSON_VALUE)
public class NotificationController {

    private final TestRun2Service testRun2Service;
    private final TestRunResultSummaryService testRunResultSummaryService;
    private final NotificationTargetService notificationTargetService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void sendNotification(@RequestParam Long testRunId,
                                 @Principal AuthenticatedUser sender) {
        TestRun testRun = testRun2Service.retrieveById(testRunId);
        Set<NotificationTarget> notificationTargets = testRun.getNotificationTargets();
        TestRunResultSummary resultSummary = testRunResultSummaryService.retrieveByTestRunId(testRun.getId());
        resultSummary.setSenderUsername(sender.getUsername());
        notificationTargetService.execute(notificationTargets, resultSummary);
    }

}
