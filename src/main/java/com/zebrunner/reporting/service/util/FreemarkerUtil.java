package com.zebrunner.reporting.service.util;

import com.zebrunner.common.eh.exception.InternalServiceException;
import com.zebrunner.reporting.service.StorageService;
import com.zebrunner.reporting.service.exception.InternalServiceError;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.UUID;

@Slf4j
@Component
@RequiredArgsConstructor
public class FreemarkerUtil {

    private final Configuration freemarker;
    private final StorageService storageService;

    @Value("${templates-directory}")
    private String testRunResultTemplateLocation;

    public String buildContentFromTemplate(String templateName, Object templateModel) {
        String templateKey = constructTemplateKey(templateName);
        try {
            InputStream resource = storageService.get(templateKey).getData();
            return FreeMarkerTemplateUtils.processTemplateIntoString(toTemplate(new InputStreamReader(resource)), templateModel);
        } catch (Exception e) {
            log.error("Problem with free marker template compilation: " + e.getMessage());
            throw new InternalServiceException(InternalServiceError.MALFORMED_FREEMARKER_TEMPLATE, e);
        }
    }

    /**
     * Builds content by merging template with it's model. Template can be referenced by name to or provided
     * as a raw {@link String}
     * 
     * @param templateNameOrData template name to be loaded from resources
     * @param templateModel template model
     * @param isPath flag indicating if input string should be used as a template name or data
     * @return processed content
     * @throws InternalServiceException on freemarker template compilation
     */
    public String buildContentFromTemplate(String templateNameOrData, Object templateModel, boolean isPath) {
        try {
            Template template = isPath ? freemarker.getTemplate(templateNameOrData) // load from resources
                                       : toTemplate(new StringReader(templateNameOrData));
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, templateModel);
        } catch (Exception e) {
            log.error("Problem with free marker template compilation: " + e.getMessage());
            throw new InternalServiceException(InternalServiceError.MALFORMED_FREEMARKER_TEMPLATE, e);
        }
    }

    private Template toTemplate(Reader reader) throws IOException {
        return new Template(UUID.randomUUID().toString(), reader, new Configuration(Configuration.VERSION_2_3_30));
    }

    private String constructTemplateKey(String templateName) {
        if (!testRunResultTemplateLocation.endsWith("/")) {
            testRunResultTemplateLocation = testRunResultTemplateLocation + "/";
        }
        return testRunResultTemplateLocation + templateName;
    }

}
