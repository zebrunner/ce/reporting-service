package com.zebrunner.reporting.messaging.listener;

import com.zebrunner.reporting.domain.db.User;
import com.zebrunner.reporting.domain.push.events.UserSavedMessage;
import com.zebrunner.reporting.messaging.config.Queue;
import com.zebrunner.reporting.service.UserService;
import com.zebrunner.reporting.service.cache.UserCacheableService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserSavedListener {

    private final UserService userService;
    private final UserCacheableService userCacheableService;

    @RabbitListener(queues = Queue.USER_SAVED)
    public void handle(UserSavedMessage message) {
        User user = new User(message.getId());
        user.setEmail(message.getEmail());
        user.setUsername(message.getUsername());

        userService.getByUsername(message.getUsername()).ifPresentOrElse(existingUser -> {
            user.setId(existingUser.getId());
            userCacheableService.updateUser(user);
        }, () -> {
            userService.getByEmail(message.getEmail()).ifPresentOrElse(existingUser -> {
                user.setId(existingUser.getId());
                userService.update(user);
            }, () -> {
                if (!userService.existsById(user.getId())) {
                    userService.create(user);
                } else {
                    userService.update(user);
                }
            });
        });
    }

}
