package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.DashboardSwitch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardSwitchRepository extends JpaRepository<DashboardSwitch, Long> {

    @Modifying
    @Query("UPDATE DashboardSwitch ds SET ds.switchedAt = CURRENT_TIMESTAMP " +
           "WHERE ds.dashboardId = :dashboardId AND ds.userId = :userId")
    int update(@Param("dashboardId") Long dashboardId, @Param("userId") Long userId);

    boolean existsByDashboardIdAndUserId(Long dashboardId, Long userId);

    void deleteByDashboardIdAndUserId(Long dashboardId, Long userId);

}
