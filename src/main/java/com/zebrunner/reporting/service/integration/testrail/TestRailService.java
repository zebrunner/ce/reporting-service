package com.zebrunner.reporting.service.integration.testrail;

import com.zebrunner.reporting.domain.integration.testrail.TestCasePreview;

public interface TestRailService {

    TestCasePreview retrieveTestCasePreviewById(Long testCaseId);

}
