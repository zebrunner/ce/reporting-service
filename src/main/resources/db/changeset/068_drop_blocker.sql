ALTER TABLE work_items DROP COLUMN IF EXISTS blocker;
ALTER TABLE test_runs DROP COLUMN IF EXISTS blocker;
ALTER TABLE test_run_statistics DROP COLUMN IF EXISTS blockers_amount;
ALTER TABLE tests DROP COLUMN IF EXISTS blocker;
