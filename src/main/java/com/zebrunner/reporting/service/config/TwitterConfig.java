package com.zebrunner.reporting.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.twitter.api.impl.TwitterTemplate;

@Configuration
@ConditionalOnProperty(name = "twitter.enabled", havingValue = "true")
public class TwitterConfig {

    @Bean
    public TwitterTemplate twitterTemplate(@Value("${twitter.consumer-key}") String consumerKey,
                                           @Value("${twitter.consumer-secret}") String consumerSecret) {
        return new TwitterTemplate(consumerKey, consumerSecret);
    }

}
