package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.domain.entity.integration.IssueReferenceAssignmentEvent;

import java.util.Set;

public interface IssueReferenceAssignmentEventService {

    default Set<IssueReferenceAssignmentEvent> retrieveTestCaseEventsOfLinkingByTestId(Long testId) {
        return retrieveTestCaseEventsByTestIdAndAction(testId, IssueReferenceAssignmentEvent.Action.LINK);
    }

    Set<IssueReferenceAssignmentEvent> retrieveTestCaseEventsByTestIdAndAction(Long testId, IssueReferenceAssignmentEvent.Action action);

    /**
     * Saves the event of linking the issue to the test case with the mention of the user who does it - reporter
     *
     * @param issueReference    issue that is to be linked
     * @param testCaseId        id of test case to which the issue is to be linked
     * @param failureReasonHash hash of the failure reason associated with the given issue
     * @param reporterId        id of the user who does linking
     */
    IssueReferenceAssignmentEvent saveLinkingIfNeeded(IssueReference issueReference, Long testCaseId, Integer failureReasonHash, Long reporterId);

    /**
     * Saves the event of unlinking the issue from the test case with the mention of the user who does it - reporter,
     * if the same event of unlinking (by test case, hash and issue) wasn't previously saved
     *
     * @param issueReference issue that is to be unlinked
     * @param testCaseId     id of test case to which the issue is to be unlinked
     * @param failureReasonHash hash of the failure reason associated with the given issue
     * @param reporterId     id of the user who does unlinking
     */
    void saveUnlinkingIfNeeded(IssueReference issueReference, Long testCaseId, Integer failureReasonHash, Long reporterId);

}
