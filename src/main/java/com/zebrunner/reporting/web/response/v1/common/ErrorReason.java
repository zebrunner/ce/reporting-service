package com.zebrunner.reporting.web.response.v1.common;

public enum ErrorReason {

    NOT_FOUND,
    NETWORK_ISSUE,
    NO_ACCESS,
    INVALID_INPUT

}
