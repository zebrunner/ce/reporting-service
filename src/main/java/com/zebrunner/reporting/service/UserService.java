package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.User;
import com.zebrunner.reporting.persistence.dao.mysql.application.UserMapper;
import com.zebrunner.reporting.service.cache.UserCacheableService;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserMapper userMapper;
    private final UserCacheableService userCacheableService;

    @Transactional(readOnly = true)
    public Optional<User> getById(long id) {
        return userMapper.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Transactional(readOnly = true)
    public Optional<User> getByEmail(String email) {
        return userMapper.findByEmail(email);
    }

    @Transactional(readOnly = true)
    public User getDefault() {
        return getByUsername("anonymous")
                .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.USER_NOT_FOUND_BY_USERNAME, "anonymous"));
    }

    @Transactional
    public User create(User user) {
        boolean exists = userMapper.existsByUsername(user.getUsername());
        if (exists) {
            throw new BusinessConstraintException(BusinessConstraintError.UNABLE_TO_CREATE_USER_WITH_USERNAME, user.getUsername());
        }
        userMapper.create(user);
        return user;
    }

    @Transactional(rollbackFor = Exception.class)
    public User update(User user) {
        return userCacheableService.updateUser(user);
    }

    @Transactional(readOnly = true)
    public boolean existsById(Long id) {
        return userMapper.existsById(id);
    }

}
