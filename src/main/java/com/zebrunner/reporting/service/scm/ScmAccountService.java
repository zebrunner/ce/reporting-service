package com.zebrunner.reporting.service.scm;

import com.zebrunner.common.eh.exception.ExternalSystemException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.db.ScmAccount;
import com.zebrunner.reporting.domain.dto.scm.Organization;
import com.zebrunner.reporting.domain.dto.scm.Repository;
import com.zebrunner.reporting.persistence.dao.mysql.application.ScmAccountMapper;
import com.zebrunner.reporting.service.CryptoDriven;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.ExternalSystemError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.user.UserReassignmentListener;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ScmAccountService implements CryptoDriven<ScmAccount>, UserReassignmentListener {

    private final ScmAccountMapper scmAccountMapper;
    private final GitHubService gitHubService;
    private final CryptoService cryptoService;

    @Transactional
    public ScmAccount createScmAccount(ScmAccount scmAccount) {
        encryptTokenIfNeeded(scmAccount);
        scmAccountMapper.create(scmAccount);
        return scmAccount;
    }

    private void encryptTokenIfNeeded(ScmAccount scmAccount) {
        if (StringUtils.isNotBlank(scmAccount.getAccessToken()) && !scmAccount.isAccessTokenEncrypted()) {
            String encryptedToken = cryptoService.encrypt(scmAccount.getAccessToken());
            scmAccount.setAccessTokenEncrypted(true);
            scmAccount.setAccessToken(encryptedToken);
        }
    }

    @Transactional
    public ScmAccount createScmAccount(String code) {
        String token = gitHubService.getAccessToken(code);
        if (StringUtils.isEmpty(token)) {
            throw new ExternalSystemException(ExternalSystemError.GITHUB_CANNOT_RECOGNIZE_YOUR_AUTHORITY);
        }
        String username = gitHubService.getUsername(token);
        ScmAccount.Name scmAccountName = gitHubService.getScmAccountName();
        ScmAccount scmAccount = new ScmAccount(username, token, scmAccountName);
        return createScmAccount(scmAccount);
    }

    @Transactional(readOnly = true)
    public ScmAccount getScmAccountById(Long id) {
        return scmAccountMapper.findById(id)
                               .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.SCM_ACCOUNT_NOT_FOUND_BY_ID, id));
    }

    public ScmAccount getScmAccountByRepo(String repo) {
        return scmAccountMapper.findByRepo(repo)
                               .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.SCM_ACCOUNT_NOT_FOUND_FOR_REPO, repo));
    }

    @Transactional(readOnly = true)
    public List<ScmAccount> getAllScmAccounts() {
        return scmAccountMapper.findAll();
    }

    @Transactional(readOnly = true)
    public List<Organization> getScmAccountOrganizations(Long id) {
        ScmAccount scmAccount = getScmAccountById(id);
        return gitHubService.getOrganizations(scmAccount);
    }

    @Transactional(readOnly = true)
    public List<Repository> getScmAccountRepositories(Long id, String organizationName) {
        ScmAccount scmAccount = getScmAccountById(id);
        List<ScmAccount> allAccounts = getAllScmAccounts();
        List<String> existingRepos = allAccounts.stream()
                                                .map(ScmAccount::getRepositoryURL)
                                                .collect(Collectors.toList());
        return gitHubService.getRepositories(scmAccount, organizationName, existingRepos);
    }

    public ScmAccount updateScmAccount(ScmAccount scmAccount) {
        encryptTokenIfNeeded(scmAccount);
        scmAccountMapper.update(scmAccount);
        return scmAccount;
    }

    public void deleteScmAccountById(Long id) {
        scmAccountMapper.deleteById(id);
    }

    @Transactional(readOnly = true)
    public String getDefaultBranch(Long id) {
        ScmAccount scmAccount = getScmAccountById(id);
        Repository repository = gitHubService.getRepository(scmAccount);
        if (repository == null) {
            throw new ExternalSystemException(ExternalSystemError.GITHUB_UNABLE_TO_OBTAIN_DEFAULT_BRANCH_NAME);
        }
        return repository.getDefaultBranch();
    }

    public void encryptTokens() {
        List<ScmAccount> scmAccounts = getAllScmAccounts();
        scmAccounts.stream()
                   .filter(scmAccount -> StringUtils.isNotBlank(scmAccount.getAccessToken()) && !scmAccount
                           .isAccessTokenEncrypted())
                   .forEach(scmAccount -> {
                       String token = scmAccount.getAccessToken();
                       String encryptedToken = cryptoService.encrypt(token);

                       scmAccount.setAccessToken(encryptedToken);
                       scmAccount.setAccessTokenEncrypted(true);

                       updateScmAccount(scmAccount);
                   });
    }

    @Override
    public Collection<ScmAccount> getEncryptedCollection() {
        List<ScmAccount> scmAccounts = scmAccountMapper.findAll();
        scmAccounts.removeIf(scmAccount -> scmAccount.getAccessToken() == null || !scmAccount.isAccessTokenEncrypted());
        return scmAccounts;
    }

    @Override
    public void afterReencryptOperation(Collection<ScmAccount> reencryptedCollection) {
        reencryptedCollection.forEach(this::updateScmAccount);
    }

    @Override
    public String getEncryptedValue(ScmAccount entity) {
        return entity.getAccessToken();
    }

    @Override
    public void setEncryptedValue(ScmAccount entity, String encryptedString) {
        entity.setAccessToken(encryptedString);
        entity.setAccessTokenEncrypted(true);
    }

    @Override
    public void onUserReassignment(Long fromId, Long toId) {
        scmAccountMapper.reassignUser(fromId, toId);
    }

}
