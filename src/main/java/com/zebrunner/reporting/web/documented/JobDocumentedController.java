package com.zebrunner.reporting.web.documented;

import com.zebrunner.reporting.domain.dto.JobDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api("Jobs API")
public interface JobDocumentedController {

    @ApiOperation(
            value = "Creates or updates a job",
            notes = "Creates a job if it does not exist. Otherwise, updates it",
            nickname = "createJob",
            httpMethod = "POST",
            response = JobDTO.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "jobDTO", paramType = "body", dataType = "JobDTO", required = true, value = "The job to create or update")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the created or updated job", response = JobDTO.class)
    })
    JobDTO createJob(JobDTO jobDTO);

}
