package com.zebrunner.reporting.service.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UnauthorizedAccessError implements com.zebrunner.common.eh.error.UnauthorizedAccessError {

    ILLEGAL_FILTER_MODIFICATION(2500);

    private final int code;

}
