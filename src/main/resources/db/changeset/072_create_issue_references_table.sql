-- issues
DROP TABLE IF EXISTS issue_references CASCADE;
CREATE TABLE IF NOT EXISTS issue_references(
    id                BIGSERIAL       NOT NULL,
    type              VARCHAR(50)     NOT NULL DEFAULT 'JIRA',
    value             VARCHAR(100)    NOT NULL,

    modified_at       TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_at        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE (value, type)
);
CREATE TRIGGER update_timestamp_issue_references
    BEFORE INSERT OR UPDATE
    ON issue_references
    FOR EACH ROW EXECUTE PROCEDURE update_modified_at();

INSERT INTO issue_references (type, value)
SELECT DISTINCT 'JIRA', jira_id FROM work_items wi
WHERE wi.test_case_id IS NOT NULL AND hash_code IS NOT NULL;

--events
DROP TABLE IF EXISTS issue_reference_assignment_events CASCADE;
CREATE TABLE IF NOT EXISTS issue_reference_assignment_events(
    id                          BIGSERIAL       NOT NULL,
    test_case_id                BIGINT          NOT NULL,
    issue_reference_id          BIGINT          NOT NULL,
    user_id                     BIGINT          NOT NULL DEFAULT 1,
    failure_reason_hash         INT             NOT NULL,
    action                      VARCHAR(20)     NOT NULL,

    modified_at       TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_at        TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (test_case_id) REFERENCES test_cases (id) ON DELETE CASCADE,
    FOREIGN KEY (issue_reference_id) REFERENCES issue_references (id) ON DELETE CASCADE
);
CREATE INDEX issue_reference_assignment_events_test_case_id_failure_hash_idx
    ON issue_reference_assignment_events (test_case_id, failure_reason_hash);
CREATE TRIGGER update_timestamp_issue_reference_assignment_events
    BEFORE INSERT OR UPDATE
    ON issue_reference_assignment_events
    FOR EACH ROW EXECUTE PROCEDURE update_modified_at();

INSERT INTO issue_reference_assignment_events
(test_case_id, issue_reference_id, failure_reason_hash, action, created_at, modified_at)
SELECT
    wi.test_case_id,
    ir.id,
    wi.hash_code,
    (CASE
         WHEN EXISTS (SELECT twi.work_item_id FROM test_work_items twi WHERE twi.work_item_id = wi.id)
             THEN 'LINK'
         ELSE ('UNLINK') END),
    wi.created_at,
    wi.modified_at
FROM work_items wi JOIN issue_references ir ON wi.jira_id = ir.value
WHERE wi.test_case_id IS NOT NULL AND wi.hash_code IS NOT NULL;

-- tests
AlTER TABLE IF EXISTS tests
    DROP COLUMN IF EXISTS issue_reference_id,
    ADD COLUMN IF NOT EXISTS issue_reference_id BIGINT NULL;

ALTER TABLE tests DROP CONSTRAINT IF EXISTS tests_issue_reference_id_fkey;
ALTER TABLE tests
    ADD CONSTRAINT tests_issue_reference_id_fkey
        FOREIGN KEY (issue_reference_id) REFERENCES issue_references (id)
            ON DELETE CASCADE;

UPDATE tests t
SET issue_reference_id = (
    SELECT ir.id FROM issue_references ir
    WHERE ir.value = (
        SELECT jira_id FROM work_items wi
            JOIN test_work_items twi ON wi.id = twi.work_item_id
            WHERE twi.test_id = t.id
            ORDER BY twi.id DESC
            LIMIT 1
        )
)
WHERE EXISTS (SELECT id from test_work_items twi where twi.test_id = t.id);
