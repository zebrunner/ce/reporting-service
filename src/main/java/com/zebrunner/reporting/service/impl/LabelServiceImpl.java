package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.persistence.repository.LabelRepository;
import com.zebrunner.reporting.service.LabelService;
import com.zebrunner.reporting.service.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class LabelServiceImpl implements LabelService {

    private final LabelRepository labelRepository;

    @Override
    @Transactional
    public Set<Label> save(Collection<Label> labels) {
        return StreamUtils.mapToSet(labels, this::readOrCreate);
    }

    /**
     * Either reads already existing labels or persists a new one
     *
     * @param label label to be read or persisted
     * @return persisted label
     */
    private Label readOrCreate(Label label) {
        return labelRepository.findByKeyAndValue(label.getKey(), label.getValue())
                              .orElseGet(() -> labelRepository.save(label));
    }

}
