package com.zebrunner.reporting.web.request.v1.jira;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;

@Data
@JGlobalMap
@NoArgsConstructor
public class CheckJiraConnectionRequest {

    @URL
    @NotBlank
    private String url;

    @NotBlank
    private String username;

    @NotBlank
    private String token;
    private boolean encrypted;

}
