package com.zebrunner.reporting.domain.notification;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestRunResultSummary {

    private Long testRunId;
    private String status;
    private String name;
    // Duration in seconds
    private Long duration;
    private String environment;
    private String build;

//    private Integer totalTestsAmount;
    private Integer passedAmount;
    private Integer failedAmount;
    private Integer knownIssuesAmount;
    private Integer skippedAmount;
    private Integer abortedAmount;
    private String comment;
    private String senderUsername;

    private String locale;
    private String platform;
    private String platformVersion;
    private String browser;
    private String browserVersion;

}
