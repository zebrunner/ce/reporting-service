package com.zebrunner.reporting.web.response.v1.slack;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JGlobalMap
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateSlackConfigResponse {

    private String token;
    private String botName;
    private String botIconUrl;
    private boolean enabled;
    private boolean encrypted;

}
