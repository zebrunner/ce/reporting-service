package com.zebrunner.reporting.service.util;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CapabilityJsonUtils {

    public static Optional<String> getTextualCapability(JsonNode rootCapabilitiesNode,
                                                        Collection<Function<JsonNode, JsonNode>> capabilityExtractors) {
        return Optional.ofNullable(rootCapabilitiesNode)
                       .filter(JsonNode::isObject)
                       .flatMap(capabilities -> capabilityExtractors.stream()
                                                                    .map(extractor -> extractor.apply(capabilities))
                                                                    .filter(Objects::nonNull)
                                                                    .filter(JsonNode::isTextual)
                                                                    .findFirst())
                       .map(JsonNode::asText);
    }

    public static boolean isCapabilityFalse(JsonNode rootCapabilitiesNode,
                                            Collection<Function<JsonNode, JsonNode>> capabilityExtractors) {
        return Optional.ofNullable(rootCapabilitiesNode)
                       .flatMap(capabilities -> capabilityExtractors.stream()
                                                                    .map(extractor -> extractor.apply(capabilities))
                                                                    .filter(Objects::nonNull)
                                                                    .filter(capability -> capability.isTextual() || capability.isBoolean())
                                                                    .map(JsonNode::asText)
                                                                    .findFirst()
                                                                    .map("false"::equalsIgnoreCase))
                       .orElse(false);
    }

}
