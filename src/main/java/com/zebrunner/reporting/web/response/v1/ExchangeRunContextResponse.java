package com.zebrunner.reporting.web.response.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.vo.RunContext;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRunContextResponse {

    @Deprecated
    private boolean rerunOnlyFailedTests;

    private String testRunUuid;
    private RunContext.Mode mode;

    private boolean runAllowed;
    private String reason;

    private boolean runOnlySpecificTests;
    private List<Test> testsToRun = List.of();

    private String fullExecutionPlanContext;

    @Deprecated
    public String getId() {
        return testRunUuid;
    }

    @Deprecated
    public boolean isRunExists() {
        return CollectionUtils.isNotEmpty(testsToRun);
    }

    @Deprecated
    public List<Test> getTests() {
        return testsToRun;
    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Test {

        private Long id;
        private String name;
        private String correlationData;
        private com.zebrunner.reporting.domain.entity.Test.Status status;
        private OffsetDateTime startedAt;
        private OffsetDateTime endedAt;

    }

    public static class ExchangeRunContextResponseBuilder {

        public ExchangeRunContextResponseBuilder fullExecutionPlanContext(RunContext fullExecutionPlanContext) {
            this.fullExecutionPlanContext = fullExecutionPlanContext != null
                    ? fullExecutionPlanContext.asJsonString()
                    : null;
            return this;
        }

    }

}
