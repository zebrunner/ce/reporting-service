package com.zebrunner.reporting.service.util;

import com.zebrunner.common.eh.exception.InternalServiceException;
import com.zebrunner.reporting.domain.db.config.Configuration;
import com.zebrunner.reporting.service.exception.InternalServiceError;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class XmlUtils {

    public static Configuration readConfiguration(String configXML) {
        Configuration configuration = new Configuration();
        try {
            if (StringUtils.isNotEmpty(configXML)) {
                ByteArrayInputStream xmlBA = new ByteArrayInputStream(configXML.getBytes());
                configuration = (Configuration) JAXBContext.newInstance(Configuration.class)
                                                           .createUnmarshaller()
                                                           .unmarshal(xmlBA);
                IOUtils.closeQuietly(xmlBA);
            }
        } catch (JAXBException e) {
            throw new InternalServiceException(InternalServiceError.UNPROCESSABLE_XML_ENTITY, e);
        }
        return configuration;
    }


}
