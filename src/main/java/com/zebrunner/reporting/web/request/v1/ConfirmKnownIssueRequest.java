package com.zebrunner.reporting.web.request.v1;

import lombok.Data;

@Data
public class ConfirmKnownIssueRequest {

    private String failureReason;

}
