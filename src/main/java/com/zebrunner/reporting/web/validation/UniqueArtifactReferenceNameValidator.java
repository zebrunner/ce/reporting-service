package com.zebrunner.reporting.web.validation;

import com.zebrunner.reporting.service.util.StreamUtils;
import com.zebrunner.reporting.web.request.v1.AttachArtifactReferencesRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class UniqueArtifactReferenceNameValidator implements ConstraintValidator<UniqueNames, Collection<AttachArtifactReferencesRequest.ArtifactReference>> {

    @Override
    public boolean isValid(Collection<AttachArtifactReferencesRequest.ArtifactReference> value, ConstraintValidatorContext context) {
        List<String> referenceNames = StreamUtils.mapToList(value, AttachArtifactReferencesRequest.ArtifactReference::getName);
        return new HashSet<>(referenceNames).size() == referenceNames.size();
    }

}
