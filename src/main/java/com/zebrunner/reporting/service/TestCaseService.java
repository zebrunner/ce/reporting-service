package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.TestCase;
import com.zebrunner.reporting.persistence.dao.mysql.application.TestCaseMapper;
import com.zebrunner.reporting.service.cache.TestCaseCache;
import com.zebrunner.reporting.service.testsuite.TestSuiteReassignmentListener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestCaseService implements TestSuiteReassignmentListener {

    private final TestCaseCache testCaseCache;
    private final TestCaseMapper testCaseMapper;

    @Transactional(readOnly = true)
    public List<TestCase> retrieveByIds(List<Long> testIds) {
        return testCaseMapper.findByIds(testIds);
    }

    @Transactional
    public TestCase save(TestCase testCase) {
        String testClass = testCase.getTestClass();
        String testMethod = testCase.getTestMethod();

        return this.retrieveByTestClassAndTestMethod(testClass, testMethod)
                   .orElseGet(() -> this.create(testCase));
    }

    private Optional<TestCase> retrieveByTestClassAndTestMethod(String testClass, String testMethod) {
        return testCaseCache.getByTestClassAndTestMethod(testClass, testMethod);
    }

    private TestCase create(TestCase testCase) {
        if (testCase.getStatus() == null) {
            testCase.setStatus(Status.UNKNOWN);
        }
        testCaseMapper.create(testCase);
        return testCase;
    }

    @Transactional
    public void updateStatuses(List<Long> ids, Status status) {
        testCaseMapper.updateStatuses(ids, status);
    }

    @Override
    @Transactional
    public void onTestSuiteReassignment(Long fromId, Long toId) {
        testCaseMapper.reassignTestSuite(fromId, toId);
    }

}
