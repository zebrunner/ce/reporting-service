package com.zebrunner.reporting.web.patch;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

public class PatchRequestValidator extends BasePatchRequestValidator<PatchAction> {

    @Override
    public void initialize(PatchAction constraintAnnotation) {
        operationToAllowedPaths = new EnumMap<>(Map.of(constraintAnnotation.op(), Set.of(constraintAnnotation.path())));
    }

}
