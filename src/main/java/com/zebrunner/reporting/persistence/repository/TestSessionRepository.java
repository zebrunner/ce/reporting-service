package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestSessionRepository extends JpaRepository<TestSession, Long> {

    @Query("SELECT DISTINCT ts " +
            "FROM TestSession ts " +
            "LEFT JOIN FETCH ts.tests " +
            "WHERE ts.testRunId = :testRunId")
    List<TestSession> findByTestRunId(@Param("testRunId") Long testRunId);

    @Query("SELECT DISTINCT ts " +
            "FROM TestSession ts " +
            "LEFT JOIN FETCH ts.tests " +
            "WHERE ts.id IN ( " +
            "        SELECT DISTINCT ts2.id " +
            "        FROM TestSession ts2 " +
            "        JOIN ts2.tests t2 " +
            "        WHERE ts2.testRunId = :testRunId " +
            "          AND t2.id = :testId " +
            ")")
    List<TestSession> findByTestRunIdAndTestId(@Param("testRunId") Long testRunId, @Param("testId") Long testId);

    List<TestSession> findByTestRunIdAndEndedAtIsNull(@Param("testRunId") Long testRunId);

    boolean existsBySessionId(String sessionId);

    int countByTestRunId(Long testRunId);

}
