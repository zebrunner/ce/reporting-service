--drop obsolete views
DROP VIEW IF EXISTS quarterly_view;
DROP VIEW IF EXISTS last_7_days_view;
DROP VIEW IF EXISTS weekly_view;
DROP VIEW IF EXISTS monthly_view;
DROP VIEW IF EXISTS last_14_days_view;
DROP VIEW IF EXISTS last_30_days_view;
DROP VIEW IF EXISTS nightly_view;
DROP VIEW IF EXISTS last_24_hours_view;
DROP MATERIALIZED VIEW IF EXISTS last_32_days_view;
DROP MATERIALIZED VIEW IF EXISTS last_93_days_view;
DROP MATERIALIZED VIEW IF EXISTS total_view;

--create new views
DROP VIEW IF EXISTS last_24_hours CASCADE;
DROP VIEW IF EXISTS last_32_days CASCADE;
DROP VIEW IF EXISTS last_93_days CASCADE;

CREATE VIEW last_24_hours AS (
    SELECT
        row_number() OVER ()                                                    AS id,
        test_runs.id                                                            AS test_run_id,
        test_runs.name                                                          AS run_name,
        test_runs.build_number                                                  AS build_number,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS milestone_version,
        test_runs.environment                                                   AS env,
        test_runs.build                                                         AS build,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner,
        CASE
            WHEN (test_configs.platform IS NULL)
                THEN ''
            ELSE LOWER(test_configs.platform)
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        CASE
            WHEN (test_configs.browser IS NULL)
                THEN ''
            ELSE LOWER(test_configs.browser)
        END                                                                     AS browser,
        test_configs.browser_version                                            AS browser_version,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        upstream_jobs.name                                                      AS milestone,
        bug_items.value                                                         AS bug,
        tests.message_hash_code                                                 AS message_hashcode,
        priority_labels.value                                                   AS priority,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        CASE
            WHEN (date_trunc('month', TESTS.START_TIME) = date_trunc('month', current_date))
            THEN ROUND(SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time))) / EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day', date_trunc('month', current_date) + interval '1 month') - interval '1 day'))
            ELSE SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint
        END                                                                     AS total_eta_seconds
    FROM test_runs
    JOIN tests ON test_runs.id = tests.test_run_id
    JOIN users ON tests.maintainer_id = users.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    LEFT JOIN issue_references bug_items ON bug_items.id = tests.issue_reference_id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    WHERE test_runs.started_at >= (current_date - interval '1 day')
        AND tests.start_time >= (current_date - interval '1 day')
    GROUP BY
        test_runs.id,
        test_configs.id,
        users.id,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        date_trunc('month', TESTS.START_TIME),
        bug_items.value,
		tests.message_hash_code,
        priority_labels.value
);

CREATE VIEW today AS (
    SELECT * FROM last_24_hours WHERE started_at >= current_date
);

CREATE VIEW last_32_days AS (
    SELECT
        row_number() OVER ()                                                    AS id,
        test_runs.id                                                            AS test_run_id,
        test_runs.name                                                          AS run_name,
        test_runs.build_number                                                  AS build_number,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS milestone_version,
        test_runs.environment                                                   AS env,
        test_runs.build                                                         AS build,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner,
        CASE
            WHEN (test_configs.platform IS NULL)
                THEN ''
            ELSE LOWER(test_configs.platform)
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        CASE
            WHEN (test_configs.browser IS NULL)
                THEN ''
            ELSE LOWER(test_configs.browser)
        END                                                                     AS browser,
        test_configs.browser_version                                            AS browser_version,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        upstream_jobs.name                                                      AS milestone,
        bug_items.value                                                         AS bug,
        tests.message_hash_code                                                 AS message_hashcode,
        priority_labels.value                                                   AS priority,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        CASE
            WHEN (date_trunc('month', TESTS.START_TIME) = date_trunc('month', current_date))
            THEN ROUND(SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time))) / EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day', date_trunc('month', current_date) + interval '1 month') - interval '1 day'))
            ELSE SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint
        END                                                                     AS total_eta_seconds
    FROM test_runs
    JOIN tests ON test_runs.id = tests.test_run_id
    JOIN users ON tests.maintainer_id = users.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    LEFT JOIN issue_references bug_items ON tests.issue_reference_id = bug_items.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    WHERE test_runs.started_at >= (current_date - interval '32 day')
        AND tests.start_time >= (current_date - interval '32 day')
    GROUP BY
        test_runs.id,
        test_configs.id,
        users.id,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        date_trunc('month', TESTS.START_TIME),
        bug_items.value,
		tests.message_hash_code,
        priority_labels.value
);

CREATE VIEW last_7_days AS (
    SELECT * FROM last_32_days WHERE started_at >= current_date - interval '7 day'
);

CREATE VIEW last_14_days AS (
    SELECT * FROM last_32_days WHERE started_at >= (current_date - interval '14 day')
);

CREATE VIEW last_30_days AS (
    SELECT * FROM last_32_days WHERE started_at >= (current_date - interval '30 day')
);

CREATE VIEW week AS (
    SELECT * FROM last_32_days WHERE started_at >= date_trunc('week', current_date)
);

CREATE VIEW month AS (
    SELECT * FROM last_32_days WHERE started_at >= date_trunc('month', current_date)
);

CREATE VIEW last_93_days AS (
    SELECT
        row_number() OVER ()                                                    AS id,
        test_runs.id                                                            AS test_run_id,
        test_runs.name                                                          AS run_name,
        test_runs.build_number                                                  AS build_number,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS milestone_version,
        test_runs.environment                                                   AS env,
        test_runs.build                                                         AS build,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner,
        CASE
            WHEN (test_configs.platform IS NULL)
                THEN ''
            ELSE LOWER(test_configs.platform)
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        CASE
            WHEN (test_configs.browser IS NULL)
                THEN ''
            ELSE LOWER(test_configs.browser)
        END                                                                     AS browser,
        test_configs.browser_version                                            AS browser_version,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        upstream_jobs.name                                                      AS milestone,
        bug_items.value                                                         AS bug,
        tests.message_hash_code                                                 AS message_hashcode,
        priority_labels.value                                                   AS priority,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        CASE
            WHEN (date_trunc('month', TESTS.START_TIME) = date_trunc('month', current_date))
            THEN ROUND(SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time))) / EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day', date_trunc('month', current_date) + interval '1 month') - interval '1 day'))
            ELSE SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint
        END                                                                     AS total_eta_seconds
    FROM test_runs
    JOIN tests ON test_runs.id = tests.test_run_id
    JOIN users ON tests.maintainer_id = users.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    LEFT JOIN issue_references bug_items ON tests.issue_reference_id = bug_items.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    WHERE test_runs.started_at >= (current_date - interval '93 day')
        AND tests.start_time >= (current_date - interval '93 day')
    GROUP BY
        test_runs.id,
        test_configs.id,
        users.id,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        date_trunc('month', TESTS.START_TIME),
        bug_items.value,
		tests.message_hash_code,
        priority_labels.value
);

CREATE VIEW last_90_days AS (
    SELECT * FROM last_93_days WHERE started_at >= (current_date - interval '90 day')
);

CREATE VIEW quarter AS (
    SELECT * FROM last_93_days WHERE started_at >= date_trunc('quarter', current_date)
);

DROP MATERIALIZED VIEW IF EXISTS last_365_days_materialized;
CREATE MATERIALIZED VIEW last_365_days_materialized AS (
    SELECT
        row_number() OVER ()                                                    AS id,
        test_runs.id                                                            AS test_run_id,
        test_runs.name                                                          AS run_name,
        test_runs.build_number                                                  AS build_number,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS milestone_version,
        test_runs.environment                                                   AS env,
        test_runs.build                                                         AS build,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner,
        CASE
            WHEN (test_configs.platform IS NULL)
                THEN ''
            ELSE LOWER(test_configs.platform)
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        CASE
            WHEN (test_configs.browser IS NULL)
                THEN ''
            ELSE LOWER(test_configs.browser)
        END                                                                     AS browser,
        test_configs.browser_version                                            AS browser_version,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        upstream_jobs.name                                                      AS milestone,
        bug_items.value                                                         AS bug,
        tests.message_hash_code                                                 AS message_hashcode,
        priority_labels.value                                                   AS priority,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        CASE
            WHEN (date_trunc('month', TESTS.START_TIME) = date_trunc('month', current_date))
            THEN ROUND(SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time))) / EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day', date_trunc('month', current_date) + interval '1 month') - interval '1 day'))
            ELSE SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint
        END                                                                     AS total_eta_seconds
    FROM test_runs
    JOIN tests ON test_runs.id = tests.test_run_id
    JOIN users ON tests.maintainer_id = users.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    LEFT JOIN issue_references bug_items ON tests.issue_reference_id = bug_items.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    WHERE test_runs.started_at >= (current_date - interval '365 day') and test_runs.started_at < date_trunc('month', current_date)
        AND tests.start_time >= (current_date - interval '365 day') and tests.start_time < date_trunc('month', current_date)
    GROUP BY
        test_runs.id,
        test_configs.id,
        users.id,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        date_trunc('month', TESTS.START_TIME),
        bug_items.value,
		tests.message_hash_code,
        priority_labels.value

);

DROP INDEX IF EXISTS LAST_365_DAYS_ID_INDEX;
CREATE UNIQUE INDEX LAST_365_DAYS_ID_INDEX ON LAST_365_DAYS_MATERIALIZED (ID);

DROP INDEX IF EXISTS LAST_365_DAYS_TEST_RUN_ID_INDEX;
CREATE INDEX LAST_365_DAYS_TEST_RUN_ID_INDEX ON LAST_365_DAYS_MATERIALIZED (TEST_RUN_ID);

DROP INDEX IF EXISTS LAST_365_DAYS_RUN_NAME_INDEX;
CREATE INDEX LAST_365_DAYS_RUN_NAME_INDEX ON LAST_365_DAYS_MATERIALIZED (RUN_NAME);

DROP INDEX IF EXISTS LAST_365_DAYS_BUILD_NUMBER_INDEX;
CREATE INDEX LAST_365_DAYS_BUILD_NUMBER_INDEX ON LAST_365_DAYS_MATERIALIZED (BUILD_NUMBER);

DROP INDEX IF EXISTS LAST_365_DAYS_STARTED_AT_INDEX;
CREATE INDEX LAST_365_DAYS_STARTED_AT_INDEX ON LAST_365_DAYS_MATERIALIZED (STARTED_AT);

DROP INDEX IF EXISTS LAST_365_DAYS_UPSTREAM_JOB_ID_INDEX;
CREATE INDEX LAST_365_DAYS_UPSTREAM_JOB_ID_INDEX ON LAST_365_DAYS_MATERIALIZED (UPSTREAM_JOB_ID);

DROP INDEX IF EXISTS LAST_365_DAYS_MILESTONE_VERSION_INDEX;
CREATE INDEX LAST_365_DAYS_MILESTONE_VERSION_INDEX ON LAST_365_DAYS_MATERIALIZED (MILESTONE_VERSION);

DROP INDEX IF EXISTS LAST_365_DAYS_ENV_INDEX;
CREATE INDEX LAST_365_DAYS_ENV_INDEX ON LAST_365_DAYS_MATERIALIZED (ENV);

DROP INDEX IF EXISTS LAST_365_DAYS_BUILD_INDEX;
CREATE INDEX LAST_365_DAYS_BUILD_INDEX ON LAST_365_DAYS_MATERIALIZED (BUILD);

DROP INDEX IF EXISTS LAST_365_DAYS_OWNER_ID_INDEX;
CREATE INDEX LAST_365_DAYS_OWNER_ID_INDEX ON LAST_365_DAYS_MATERIALIZED (OWNER_ID);

DROP INDEX IF EXISTS LAST_365_DAYS_OWNER_INDEX;
CREATE INDEX LAST_365_DAYS_OWNER_INDEX ON LAST_365_DAYS_MATERIALIZED (OWNER);

DROP INDEX IF EXISTS LAST_365_DAYS_PLATFORM_INDEX;
CREATE INDEX LAST_365_DAYS_PLATFORM_INDEX ON LAST_365_DAYS_MATERIALIZED (PLATFORM);

DROP INDEX IF EXISTS LAST_365_DAYS_PLATFORM_VERSION_INDEX;
CREATE INDEX LAST_365_DAYS_PLATFORM_VERSION_INDEX ON LAST_365_DAYS_MATERIALIZED (PLATFORM_VERSION);

DROP INDEX IF EXISTS LAST_365_DAYS_BROWSER_INDEX;
CREATE INDEX LAST_365_DAYS_BROWSER_INDEX ON LAST_365_DAYS_MATERIALIZED (BROWSER);

DROP INDEX IF EXISTS LAST_365_DAYS_BROWSER_VERSION_INDEX;
CREATE INDEX LAST_365_DAYS_BROWSER_VERSION_INDEX ON LAST_365_DAYS_MATERIALIZED (BROWSER_VERSION);

DROP INDEX IF EXISTS LAST_365_DAYS_LOCALE_INDEX;
CREATE INDEX LAST_365_DAYS_LOCALE_INDEX ON LAST_365_DAYS_MATERIALIZED (LOCALE);

DROP INDEX IF EXISTS LAST_365_DAYS_JOB_INDEX;
CREATE INDEX LAST_365_DAYS_JOB_INDEX ON LAST_365_DAYS_MATERIALIZED (JOB_URL);

DROP INDEX IF EXISTS LAST_365_DAYS_JOB_INDEX;
CREATE INDEX LAST_365_DAYS_JOB_INDEX ON LAST_365_DAYS_MATERIALIZED (JOB);

DROP INDEX IF EXISTS LAST_365_DAYS_UPSTREAM_JOB_URL_INDEX;
CREATE INDEX LAST_365_DAYS_UPSTREAM_JOB_URL_INDEX ON LAST_365_DAYS_MATERIALIZED (UPSTREAM_JOB_URL);

DROP INDEX IF EXISTS LAST_365_DAYS_MILESTONE_INDEX;
CREATE INDEX LAST_365_DAYS_MILESTONE_INDEX ON LAST_365_DAYS_MATERIALIZED (MILESTONE);

DROP INDEX IF EXISTS LAST_365_DAYS_BUG_INDEX;
CREATE INDEX LAST_365_DAYS_BUG_INDEX ON LAST_365_DAYS_MATERIALIZED (BUG);

DROP INDEX IF EXISTS LAST_365_DAYS_MESSAGE_HASHCODE_INDEX;
CREATE INDEX LAST_365_DAYS_MESSAGE_HASHCODE_INDEX ON LAST_365_DAYS_MATERIALIZED (MESSAGE_HASHCODE);

DROP INDEX IF EXISTS LAST_365_DAYS_PRIORITY_INDEX;
CREATE INDEX LAST_365_DAYS_PRIORITY_INDEX ON LAST_365_DAYS_MATERIALIZED (PRIORITY);

DROP INDEX IF EXISTS LAST_365_DAYS_PASSED_INDEX;
CREATE INDEX LAST_365_DAYS_PASSED_INDEX ON LAST_365_DAYS_MATERIALIZED (PASSED);

DROP INDEX IF EXISTS LAST_365_DAYS_FAILED_INDEX;
CREATE INDEX LAST_365_DAYS_FAILED_INDEX ON LAST_365_DAYS_MATERIALIZED (FAILED);

DROP INDEX IF EXISTS LAST_365_DAYS_KNOWN_ISSUE_INDEX;
CREATE INDEX LAST_365_DAYS_KNOWN_ISSUE_INDEX ON LAST_365_DAYS_MATERIALIZED (KNOWN_ISSUE);

DROP INDEX IF EXISTS LAST_365_DAYS_SKIPPED_INDEX;
CREATE INDEX LAST_365_DAYS_SKIPPED_INDEX ON LAST_365_DAYS_MATERIALIZED (SKIPPED);

DROP INDEX IF EXISTS LAST_365_DAYS_ABORTED_INDEX;
CREATE INDEX LAST_365_DAYS_ABORTED_INDEX ON LAST_365_DAYS_MATERIALIZED (ABORTED);

DROP INDEX IF EXISTS LAST_365_DAYS_IN_PROGRESS_INDEX;
CREATE INDEX LAST_365_DAYS_IN_PROGRESS_INDEX ON LAST_365_DAYS_MATERIALIZED (IN_PROGRESS);

DROP INDEX IF EXISTS LAST_365_DAYS_TOTAL_INDEX;
CREATE INDEX LAST_365_DAYS_TOTAL_INDEX ON LAST_365_DAYS_MATERIALIZED (TOTAL);

DROP INDEX IF EXISTS LAST_365_DAYS_TOTAL_SECONDS_INDEX;
CREATE INDEX LAST_365_DAYS_TOTAL_SECONDS_INDEX ON LAST_365_DAYS_MATERIALIZED (TOTAL_SECONDS);

DROP INDEX IF EXISTS LAST_365_DAYS_TOTAL_ETA_SECONDS_INDEX;
CREATE INDEX LAST_365_DAYS_TOTAL_ETA_SECONDS_INDEX ON LAST_365_DAYS_MATERIALIZED (TOTAL_ETA_SECONDS);

DROP VIEW IF EXISTS last_365_days;
CREATE VIEW last_365_days AS (
SELECT * FROM last_365_days_MATERIALIZED
UNION ALL
SELECT * FROM MONTH
);

CREATE VIEW year AS (
    SELECT * FROM last_365_days WHERE started_at >= date_trunc('year', current_date)
);

DROP MATERIALIZED VIEW IF EXISTS TOTAL_MATERIALIZED;
CREATE MATERIALIZED VIEW TOTAL_MATERIALIZED AS (	
    SELECT
        row_number() OVER ()                                                    AS id,
        test_runs.id                                                            AS test_run_id,
        test_runs.name                                                          AS run_name,
        test_runs.build_number                                                  AS build_number,
        test_runs.started_at                                                    AS started_at,
        test_runs.upstream_job_id                                               AS upstream_job_id,
        test_runs.upstream_job_build_number                                     AS milestone_version,
        test_runs.environment                                                   AS env,
        test_runs.build                                                         AS build,
        users.id                                                                AS owner_id,
        users.username                                                          AS owner,
        CASE
            WHEN (test_configs.platform IS NULL)
                THEN ''
            ELSE LOWER(test_configs.platform)
        END                                                                     AS platform,
        test_configs.platform_version                                           AS platform_version,
        CASE
            WHEN (test_configs.browser IS NULL)
                THEN ''
            ELSE LOWER(test_configs.browser)
        END                                                                     AS browser,
        test_configs.browser_version                                            AS browser_version,
        test_configs.locale                                                     AS locale,
        jobs.job_url                                                            AS job_url,
        jobs.name                                                               AS job,
        upstream_jobs.job_url                                                   AS upstream_job_url,
        upstream_jobs.name                                                      AS milestone,
        bug_items.value                                                         AS bug,
        tests.message_hash_code                                                 AS message_hashcode,
        priority_labels.value                                                   AS priority,
        SUM(CASE WHEN tests.status = 'PASSED' THEN 1 ELSE 0 END)                AS passed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = FALSE
                THEN 1
                ELSE 0
            END
        )                                                                       AS failed,
        SUM(
            CASE
                WHEN tests.status = 'FAILED' AND tests.known_issue = TRUE
                THEN 1
                ELSE 0
            END
        )                                                                       AS known_issue,
        SUM(CASE WHEN tests.status = 'SKIPPED' THEN 1 ELSE 0 END)               AS skipped,
        SUM(CASE WHEN tests.status = 'ABORTED' THEN 1 ELSE 0 END)               AS aborted,
        SUM(CASE WHEN tests.status = 'IN_PROGRESS' THEN 1 ELSE 0 END)           AS in_progress,
        COUNT(tests.status)                                                     AS total,
        SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint AS total_seconds,
        CASE
            WHEN (date_trunc('month', TESTS.START_TIME) = date_trunc('month', current_date))
            THEN ROUND(SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time))) / EXTRACT(day from current_date) * EXTRACT(day FROM date_trunc('day', date_trunc('month', current_date) + interval '1 month') - interval '1 day'))
            ELSE SUM(EXTRACT(epoch FROM (tests.finish_time - tests.start_time)))::bigint
        END                                                                     AS total_eta_seconds
    FROM test_runs
    JOIN tests ON test_runs.id = tests.test_run_id
    JOIN users ON tests.maintainer_id = users.id
    LEFT JOIN test_configs ON test_runs.config_id = test_configs.id
    LEFT JOIN jobs jobs ON test_runs.job_id = jobs.id
    LEFT JOIN jobs upstream_jobs ON test_runs.upstream_job_id = upstream_jobs.id
    LEFT JOIN issue_references bug_items ON tests.issue_reference_id = bug_items.id
    LEFT JOIN tests_labels priority_test_labels ON tests.id = priority_test_labels.test_id
        AND priority_test_labels.label_id IN (SELECT id FROM labels WHERE key = 'priority')
    LEFT JOIN labels priority_labels ON priority_test_labels.label_id = priority_labels.id
        AND priority_labels.key = 'priority'
    WHERE test_runs.started_at < date_trunc('month', current_date)
        AND tests.start_time < date_trunc('month', current_date)
    GROUP BY
        test_runs.id,
        test_configs.id,
        users.id,
        jobs.job_url,
        jobs.name,
        upstream_jobs.name,
        upstream_jobs.job_url,
        date_trunc('month', TESTS.START_TIME),
        bug_items.value,
		tests.message_hash_code,
        priority_labels.value
);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_ID_INDEX;
CREATE UNIQUE INDEX TOTAL_MATERIALIZED_ID_INDEX ON TOTAL_MATERIALIZED (ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_TEST_RUN_ID_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_TEST_RUN_ID_INDEX ON TOTAL_MATERIALIZED (TEST_RUN_ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_RUN_NAME_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_RUN_NAME_INDEX ON TOTAL_MATERIALIZED (RUN_NAME);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BUILD_NUMBER_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BUILD_NUMBER_INDEX ON TOTAL_MATERIALIZED (BUILD_NUMBER);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_STARTED_AT_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_STARTED_AT_INDEX ON TOTAL_MATERIALIZED (STARTED_AT);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_UPSTREAM_JOB_ID_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_UPSTREAM_JOB_ID_INDEX ON TOTAL_MATERIALIZED (UPSTREAM_JOB_ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_MILESTONE_VERSION_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_MILESTONE_VERSION_INDEX ON TOTAL_MATERIALIZED (MILESTONE_VERSION);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_ENV_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_ENV_INDEX ON TOTAL_MATERIALIZED (ENV);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BUILD_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BUILD_INDEX ON TOTAL_MATERIALIZED (BUILD);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_OWNER_ID_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_OWNER_ID_INDEX ON TOTAL_MATERIALIZED (OWNER_ID);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_OWNER_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_OWNER_INDEX ON TOTAL_MATERIALIZED (OWNER);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_PLATFORM_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_PLATFORM_INDEX ON TOTAL_MATERIALIZED (PLATFORM);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_PLATFORM_VERSION_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_PLATFORM_VERSION_INDEX ON TOTAL_MATERIALIZED (PLATFORM_VERSION);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BROWSER_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BROWSER_INDEX ON TOTAL_MATERIALIZED (BROWSER);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BROWSER_VERSION_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BROWSER_VERSION_INDEX ON TOTAL_MATERIALIZED (BROWSER_VERSION);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_LOCALE_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_LOCALE_INDEX ON TOTAL_MATERIALIZED (LOCALE);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_JOB_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_JOB_INDEX ON TOTAL_MATERIALIZED (JOB_URL);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_JOB_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_JOB_INDEX ON TOTAL_MATERIALIZED (JOB);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_UPSTREAM_JOB_URL_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_UPSTREAM_JOB_URL_INDEX ON TOTAL_MATERIALIZED (UPSTREAM_JOB_URL);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_MILESTONE_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_MILESTONE_INDEX ON TOTAL_MATERIALIZED (MILESTONE);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_BUG_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_BUG_INDEX ON TOTAL_MATERIALIZED (BUG);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_MESSAGE_HASHCODE_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_MESSAGE_HASHCODE_INDEX ON TOTAL_MATERIALIZED (MESSAGE_HASHCODE);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_PRIORITY_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_PRIORITY_INDEX ON TOTAL_MATERIALIZED (PRIORITY);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_PASSED_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_PASSED_INDEX ON TOTAL_MATERIALIZED (PASSED);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_FAILED_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_FAILED_INDEX ON TOTAL_MATERIALIZED (FAILED);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_KNOWN_ISSUE_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_KNOWN_ISSUE_INDEX ON TOTAL_MATERIALIZED (KNOWN_ISSUE);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_SKIPPED_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_SKIPPED_INDEX ON TOTAL_MATERIALIZED (SKIPPED);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_ABORTED_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_ABORTED_INDEX ON TOTAL_MATERIALIZED (ABORTED);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_IN_PROGRESS_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_IN_PROGRESS_INDEX ON TOTAL_MATERIALIZED (IN_PROGRESS);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_TOTAL_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_TOTAL_INDEX ON TOTAL_MATERIALIZED (TOTAL);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_TOTAL_SECONDS_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_TOTAL_SECONDS_INDEX ON TOTAL_MATERIALIZED (TOTAL_SECONDS);

DROP INDEX IF EXISTS TOTAL_MATERIALIZED_TOTAL_ETA_SECONDS_INDEX;
CREATE INDEX TOTAL_MATERIALIZED_TOTAL_ETA_SECONDS_INDEX ON TOTAL_MATERIALIZED (TOTAL_ETA_SECONDS);


DROP VIEW IF EXISTS TOTAL;
CREATE VIEW TOTAL AS (
SELECT * FROM TOTAL_MATERIALIZED
UNION ALL
SELECT * FROM MONTH
);
