package com.zebrunner.reporting.web;

import com.zebrunner.reporting.service.util.URLResolver;
import com.zebrunner.reporting.web.documented.ServiceMetadataDocumentedController;
import com.zebrunner.reporting.web.util.swagger.ApiResponseStatuses;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.zebrunner.reporting.persistence.PersistenceConfig.DEFAULT_SCHEMA;

// TODO by nsidorevich on 7/22/20: app health should be tracked via actuator. Better solution for version needed
@Deprecated
@CrossOrigin
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class ServiceMetadataController implements ServiceMetadataDocumentedController {

    private final URLResolver urlResolver;

    @Setter(onMethod = @__(@Value("${service.version}")))
    private String serviceVersion;

    @Setter(onMethod = @__(@Value("${service.use-artifact-proxy}")))
    private boolean useArtifactProxy;

    @GetMapping("/api/status")
    @Override
    public String getStatus() {
        return "UP";
    }

    @ApiResponseStatuses
    @ApiOperation(value = "Retrieves the version value/number", nickname = "getVersion", httpMethod = "GET", response = Map.class)
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", paramType = "header")})
    @GetMapping("/api/config/version")
    public Map<String, Object> getVersion() {
        return Map.of(
                "service", serviceVersion
        );
    }

    @GetMapping("/api/auth/tenant")
    public Map<String, Object> getTenancyInfo() {
        return Map.of(
                "tenant", DEFAULT_SCHEMA,
                "serviceUrl", urlResolver.buildWebURL(),
                "multitenant", false,
                "useArtifactsProxy", useArtifactProxy
        );
    }

}
