package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.Test;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.OffsetDateTime;
import java.util.Set;

@Data
@JGlobalMap
@NoArgsConstructor
public class FinishTestRequest {

    @NotNull
    @PastOrPresent
    private OffsetDateTime endedAt;
    @NotNull
    private Test.Status result;
    private String reason;

    // todo: for backward compatibility
    private Set<@Valid Label> labels = Set.of();

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class Label {

        @NotBlank
        private String key;
        @NotBlank
        private String value;

    }

}
