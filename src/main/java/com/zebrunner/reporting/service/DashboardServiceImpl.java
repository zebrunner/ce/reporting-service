package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import com.zebrunner.reporting.domain.entity.Dashboard;
import com.zebrunner.reporting.domain.push.events.Attachment;
import com.zebrunner.reporting.persistence.repository.DashboardRepository;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.util.SearchUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class DashboardServiceImpl implements DashboardService {

    private static final String DEFAULT_SORT_OPTION = "title";
    private static final Set<String> SUPPORTED_SORT_OPTIONS = Set.of("title", "createdAt");

    private final DashboardRepository dashboardRepository;

    private final EmailService emailService;

    @Override
    @Transactional
    public Dashboard create(Dashboard dashboard) {
        checkExistenceByTitle(dashboard.getTitle());
        dashboard.setEditable(true);
        return dashboardRepository.save(dashboard);
    }

    private void checkExistenceByTitle(String title) {
        Set<String> defaultDashboards = retrieveDefaultDashboardsTitles();
        if (defaultDashboards.contains(title.toLowerCase())) {
            throw new BusinessConstraintException(BusinessConstraintError.DEFAULT_DASHBOARD_CAN_NOT_BE_DUPLICATED);
        }

        if (dashboardRepository.existsByTitleIgnoreCase(title)) {
            throw new BusinessConstraintException(BusinessConstraintError.DASHBOARD_WITH_SUCH_TITLE_ALREADY_EXISTS);
        }
    }

    private Set<String> retrieveDefaultDashboardsTitles() {
        return dashboardRepository.findAllByIsDefaultIsTrue()
                                  .stream()
                                  .map(Dashboard::getTitle)
                                  .map(String::toLowerCase)
                                  .collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    public Page<Dashboard> search(SearchCriteria searchCriteria) {
        Sort sort = SearchUtils.getSort(searchCriteria, SUPPORTED_SORT_OPTIONS, DEFAULT_SORT_OPTION);
        Pageable pageable = PageRequest.of(searchCriteria.getPage() - 1, searchCriteria.getPageSize(), sort);
        String query = searchCriteria.getQuery();
        if (query.isEmpty()) {
            return dashboardRepository.findAllByHiddenIsFalse(pageable);
        } else {
            return dashboardRepository.findAllBySearchQuery(query, pageable);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Dashboard retrieveById(Long id) {
        return dashboardRepository.findById(id)
                                  .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.DASHBOARD_NOT_FOUND_BY_ID, id));
    }

    @Override
    public Dashboard retrieveByTitle(String title) {
        return dashboardRepository.findByTitleIgnoreCase(title)
                                  .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.DASHBOARD_NOT_FOUND_BY_TITLE, title));

    }

    @Override
    @Transactional(readOnly = true)
    public List<Dashboard> retrieveAll() {
        return dashboardRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Dashboard> retrieveLast(Long userId, Integer limit) {
        Pageable pageable = PageRequest.of(0, limit);
        return dashboardRepository.findLastByUserIdAndProjectIdOrderBySwitchedAtDesc(userId, pageable);
    }

    @Override
    public void verifyExistenceById(Long id) {
        retrieveById(id);
    }

    @Override
    @Transactional
    public Dashboard update(Long id, Dashboard updatedDashboard) {
        Dashboard dashboard = retrieveById(id);
        // only editable dashboard can be modified, throw exception otherwise
        if (dashboard.isEditable()) {
            updateTitle(dashboard, updatedDashboard);
            return dashboardRepository.save(dashboard);
        } else {
            throw new BusinessConstraintException(BusinessConstraintError.DASHBOARD_CAN_NOT_BE_UPDATED);
        }
    }

    private void updateTitle(Dashboard dashboard, Dashboard newDashboard) {
        String newTitle = newDashboard.getTitle();

        Set<String> defaultDashboardsTitles = retrieveDefaultDashboardsTitles();
        if (defaultDashboardsTitles.contains(newTitle.toLowerCase())) {
            throw new BusinessConstraintException(BusinessConstraintError.DEFAULT_DASHBOARD_CAN_NOT_BE_DUPLICATED);
        }

        boolean sameTitles = dashboard.getTitle().equalsIgnoreCase(newTitle);
        if (!sameTitles && dashboardRepository.existsByTitleIgnoreCase(newTitle)) {
            throw new BusinessConstraintException(BusinessConstraintError.DASHBOARD_WITH_SUCH_TITLE_ALREADY_EXISTS);
        }

        dashboard.setTitle(newTitle);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        Dashboard dashboard = retrieveById(id);
        // only editable dashboard can be deleted, throw exception otherwise
        if (dashboard.isEditable()) {
            dashboardRepository.deleteById(id);
        } else {
            throw new BusinessConstraintException(BusinessConstraintError.DASHBOARD_CAN_NOT_BE_UPDATED);
        }
    }

    @Override
    public void sendByEmail(Long id, String subject, String body, List<Attachment> attachments, Set<String> toEmails) {
        verifyExistenceById(id);
        if (!toEmails.isEmpty()) {
            emailService.sendDashboardEmail(subject, body, attachments, toEmails);
        }
    }

}
