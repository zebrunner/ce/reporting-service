package com.zebrunner.reporting.service.testcase;

public interface TestCaseReassignmentListener {

    void onTestCaseReassignment(Long fromId, Long toId);

}
