package com.zebrunner.reporting.web;

import com.zebrunner.common.auth.web.annotation.PrincipalId;
import com.zebrunner.common.eh.exception.InternalServiceException;
import com.zebrunner.reporting.domain.dto.DashboardType;
import com.zebrunner.reporting.domain.dto.EmailType;
import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import com.zebrunner.reporting.domain.dto.core.SearchResult;
import com.zebrunner.reporting.domain.dto.core.SortOrder;
import com.zebrunner.reporting.domain.entity.Dashboard;
import com.zebrunner.reporting.domain.entity.DashboardWidget;
import com.zebrunner.reporting.domain.push.events.Attachment;
import com.zebrunner.reporting.service.DashboardService;
import com.zebrunner.reporting.service.dashboard.DashboardWidgetService;
import com.zebrunner.reporting.service.exception.InternalServiceError;
import com.zebrunner.reporting.service.util.EmailUtils;
import com.zebrunner.reporting.service.widgettemplate.WidgetTemplateService;
import com.zebrunner.reporting.web.converter.v1.DashboardWidgetResponseItemConverter;
import com.zebrunner.reporting.web.request.v1.dashboard.AddDashboardWidgetRequest;
import com.zebrunner.reporting.web.request.v1.dashboard.CreateDashboardRequest;
import com.zebrunner.reporting.web.request.v1.dashboard.UpdateDashboardRequest;
import com.zebrunner.reporting.web.request.v1.dashboard.UpdateDashboardWidgetRequest;
import com.zebrunner.reporting.web.response.v1.GetDashboardsResponse;
import com.zebrunner.reporting.web.response.v1.dashboard.AddDashboardWidgetResponse;
import com.zebrunner.reporting.web.response.v1.dashboard.CreateDashboardResponse;
import com.zebrunner.reporting.web.response.v1.dashboard.DashboardWidgetResponseItem;
import com.zebrunner.reporting.web.response.v1.dashboard.UpdateDashboardResponse;
import com.zebrunner.reporting.web.util.JMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@CrossOrigin
@RequestMapping(path = "api/dashboards", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class DashboardController {

    private final DashboardService dashboardService;
    private final WidgetTemplateService widgetTemplateService;
    private final DashboardWidgetService dashboardWidgetService;
    private final Mapper mapper;
    private final JMapper jMapper;

    @PostMapping
    @PreAuthorize("hasPermission('reporting:dashboards:update')")
    public CreateDashboardResponse createDashboard(@RequestBody @Valid CreateDashboardRequest dashboardType) {
        Dashboard dashboard = jMapper.map(dashboardType, Dashboard.class);
        dashboard = dashboardService.create(dashboard);
        return jMapper.map(dashboard, CreateDashboardResponse.class);
    }


    @GetMapping("/search")
    public SearchResult<GetDashboardsResponse.Item> search(@RequestParam(name = "page", defaultValue = "1") @Positive Integer page,
                                                           @RequestParam(name = "pageSize", defaultValue = "5") @Positive Integer pageSize,
                                                           @RequestParam(name = "sortOrder", defaultValue = "ASC") SortOrder sortOrder,
                                                           @RequestParam(name = "query", defaultValue = "") String query,
                                                           @RequestParam(name = "sortBy", defaultValue = "title") String sortBy,
                                                           @PrincipalId Integer userId) {
        SearchCriteria searchCriteria = new SearchCriteria(query, sortBy, sortOrder, page, pageSize);
        Page<Dashboard> resultPage = sortBy.equals("switchedAt")
                ? dashboardService.retrieveLast(userId.longValue(), pageSize)
                : dashboardService.search(searchCriteria);
        List<GetDashboardsResponse.Item> items = jMapper.mapToList(resultPage.getContent(), GetDashboardsResponse.Item.class);
        return new SearchResult<>(searchCriteria, resultPage.getTotalPages(), resultPage.getTotalElements(), items);
    }

    @GetMapping
    public List<DashboardType> getAllDashboards() {
        return dashboardService.retrieveAll()
                               .stream()
                               .map(dashboard -> mapper.map(dashboard, DashboardType.class))
                               .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public DashboardType getDashboardById(@PathVariable("id") long id) {
        Dashboard dashboard = dashboardService.retrieveById(id);
        dashboard.getWidgets()
                 .forEach(dashboardWidget -> widgetTemplateService.setNotNullParamsValues(dashboardWidget.getWidget().getWidgetTemplate()));

        return mapper.map(dashboard, DashboardType.class);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasPermission('reporting:dashboards:delete')")
    public void deleteDashboard(@PathVariable("id") Long id) {
        dashboardService.deleteById(id);
    }

    @GetMapping(params = "title")
    public DashboardType getByName(@RequestParam("title") String title) {
        Dashboard dashboard = dashboardService.retrieveByTitle(title);
        return mapper.map(dashboard, DashboardType.class);
    }

    @PutMapping
    @PreAuthorize("hasPermission('reporting:dashboards:update')")
    public UpdateDashboardResponse updateDashboard(@Valid @RequestBody UpdateDashboardRequest request) {
        Dashboard dashboard = jMapper.map(request, Dashboard.class);
        dashboard = dashboardService.update(request.getId(), dashboard);
        return jMapper.map(dashboard, UpdateDashboardResponse.class);
    }

    @PostMapping("/{dashboardId}/widgets")
    @PreAuthorize("hasPermission('reporting:widgets:update')")
    public AddDashboardWidgetResponse addDashboardWidget(@PathVariable("dashboardId") Long dashboardId,
                                                         @RequestParam("widgetId") Long widgetId,
                                                         @Valid @RequestBody AddDashboardWidgetRequest request) {
        DashboardWidget widget = dashboardWidgetService.addToDashboard(dashboardId, widgetId, request.getLocation());
        return jMapper.map(widget, AddDashboardWidgetResponse.class);
    }

    @DeleteMapping("/{dashboardId}/widgets/{widgetId}")
    @PreAuthorize("hasPermission('reporting:widgets:update')")
    public void deleteDashboardWidget(@PathVariable("dashboardId") Long dashboardId,
                                      @PathVariable("widgetId") Long widgetId) {
        dashboardWidgetService.removeFromDashboard(dashboardId, widgetId);
    }

    @PutMapping("/{dashboardId}/widgets/all")
    @PreAuthorize("hasPermission('reporting:widgets:update')")
    public List<DashboardWidgetResponseItem> updateDashboardWidgets(@PathVariable("dashboardId") Long dashboardId,
                                                                    @RequestBody List<@Valid UpdateDashboardWidgetRequest> newWidgets) {
        List<DashboardWidget> widgets = newWidgets.stream()
                                                  .map(newWidget -> jMapper.map(newWidget, DashboardWidget.class))
                                                  .peek(newWidget -> newWidget.setDashboardId(dashboardId))
                                                  .collect(Collectors.toList());
        return dashboardWidgetService.batchUpdate(dashboardId, widgets)
                                     .stream()
                                     .map(DashboardWidgetResponseItemConverter::toResponseItem)
                                     .collect(Collectors.toList());
    }

    @SneakyThrows
    @PostMapping("/{dashboardId}/email")
    public void sendByEmail(@PathVariable("dashboardId") Long dashboardId,
                            @RequestPart("file") MultipartFile file,
                            @RequestPart("email") EmailType email) {
        Set<String> toEmails = EmailUtils.obtainValidOnly(email.getRecipients());
        Attachment attachment = convertFileToAttachment(file);
        dashboardService.sendByEmail(dashboardId, email.getSubject(), email.getText(), List.of(attachment), toEmails);
    }

    private Attachment convertFileToAttachment(MultipartFile file) {
        try {
            return new Attachment(file.getOriginalFilename(), file.getBytes());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new InternalServiceException(InternalServiceError.UNABLE_TO_CREATE_INPUT_STREAM, e);
        }
    }

}
