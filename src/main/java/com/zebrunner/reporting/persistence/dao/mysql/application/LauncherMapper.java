package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.launcher.Launcher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface LauncherMapper {

    void create(Launcher launcher);

    void batchCreate(@Param("launchers") List<Launcher> launchers);

    Optional<Launcher> findById(Long id);

    Optional<Launcher> findByPresetRef(String presetRef);

    List<Launcher> findByUserId(Long userId);

    List<Launcher> findByScmAccountIdAndAutoScan(Long scmAccountId);

    boolean existsById(Long id);

    void update(Launcher launcher);

    void batchUpdate(@Param("launchers") List<Launcher> launchers);

    void deleteById(Long id);

    void batchDelete(@Param("ids") List<Long> ids);

}
