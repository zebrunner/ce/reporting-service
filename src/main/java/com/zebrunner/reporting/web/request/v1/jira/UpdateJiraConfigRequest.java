package com.zebrunner.reporting.web.request.v1.jira;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.integration.JiraConfig;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@JGlobalMap
@NoArgsConstructor
public class UpdateJiraConfigRequest {

    @NotBlank
    private String url;
    @NotBlank
    private String username;
    @NotBlank
    private String token;
    @NotNull(message = "{error.jira.type.not.specified}")
    private JiraConfig.Type type;
    @NotNull
    private Boolean enabled;

}
