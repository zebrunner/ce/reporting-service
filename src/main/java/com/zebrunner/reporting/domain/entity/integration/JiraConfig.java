package com.zebrunner.reporting.domain.entity.integration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "integration_jira_configs")
public class JiraConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private Long id;
    private String url;
    private String username;
    private String token;
    @Enumerated(EnumType.STRING)
    private Type type;

    @EqualsAndHashCode.Exclude
    private boolean enabled;

    @EqualsAndHashCode.Exclude
    private boolean encrypted;

    public enum Type {
        SERVER_DC, CLOUD
    }

}
