package com.zebrunner.reporting.web.documented;

import com.zebrunner.reporting.web.util.swagger.ApiResponseStatuses;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.Map;

@Api("Service metadata API")
public interface ServiceMetadataDocumentedController {

    @ApiOperation(
            value = "Checks application health",
            notes = "Returns common information about the application",
            nickname = "getStatus",
            httpMethod = "GET",
            response = String.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns common information about the application", response = String.class)
    })
    String getStatus();

    @ApiResponseStatuses
    @ApiOperation(
            value = "Retrieves the version value/number",
            nickname = "getVersion",
            httpMethod = "GET",
            response = Map.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns service version and service URL", response = String.class)
    })
    Map<String, Object> getVersion();

}
