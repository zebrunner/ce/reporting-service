package com.zebrunner.reporting.service.httpclient.tcm.testrail;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.model.TestRailTestStatus;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.response.GetTestCaseResponse;
import com.zebrunner.reporting.service.httpclient.tcm.testrail.response.TestRailPriorityResponse;
import com.zebrunner.reporting.service.httpclient.utils.JacksonObjectMapperWrapper;
import kong.unirest.Config;
import kong.unirest.HttpResponse;
import kong.unirest.UnirestInstance;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Getter
@NoArgsConstructor
public class TestRailClient {

    public static final String API_V2_PATH = "/index.php?/api/v2";

    public static final Map<Test.Status, TestRailTestStatus> REPORTING_TO_TESTRAIL_TEST_STATUSES = Map.of(
            Test.Status.PASSED, TestRailTestStatus.PASSED,
            Test.Status.FAILED, TestRailTestStatus.FAILED,
            Test.Status.SKIPPED, TestRailTestStatus.UNTESTED,
            Test.Status.ABORTED, TestRailTestStatus.UNTESTED,
            Test.Status.IN_PROGRESS, TestRailTestStatus.UNTESTED
    );

    private String apiUrl;
    private UnirestInstance restClient;
    private boolean enabled;

    public TestRailClient(String url, String username, String password, Boolean enabled) {
        this.enabled = enabled;
        apiUrl = url + API_V2_PATH;
        restClient = initClient(username, password);
    }

    public ReachabilityCheckResult isServerReachable() {
        try {
            HttpResponse<String> response = restClient.get(apiUrl + "/get_statuses")
                                                      .asString();
            return response.isSuccess()
                    ? ReachabilityCheckResult.reachable()
                    : ReachabilityCheckResult.unreachable(response.getStatus(), response.getBody());
        } catch (Exception e) {
            return ReachabilityCheckResult.unreachable(e);
        }
    }

    public Optional<GetTestCaseResponse> getOptionalTestCaseById(Long testCaseId) {
        HttpResponse<GetTestCaseResponse> response = restClient.get(apiUrl + "/get_case/" + testCaseId)
                                                               .asObject(GetTestCaseResponse.class);
        return response.isSuccess()
               ? Optional.ofNullable(response.getBody())
               : Optional.empty();
    }

    public Optional<TestRailPriorityResponse> getOptionalPriorityById(Long priorityId) {
        HttpResponse<TestRailPriorityResponse[]> response = restClient.get(apiUrl + "/get_priorities")
                                                                      .asObject(TestRailPriorityResponse[].class);

        if (response.isSuccess()) {
            return Arrays.stream(response.getBody())
                         .filter(priority -> priority.getId().equals(priorityId))
                         .findFirst();
        }
        return Optional.empty();
    }

    private UnirestInstance initClient(String username, String password) {
        Config config = new Config();
        config.setDefaultBasicAuth(username, password);
        config.addDefaultHeader("Accept", "application/json");
        config.addDefaultHeader("Content-Type", "application/json");
        config.setObjectMapper(new JacksonObjectMapperWrapper(getObjectMapper()));
        return new UnirestInstance(config);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }

}
