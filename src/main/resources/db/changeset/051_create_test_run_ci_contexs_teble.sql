CREATE TABLE IF NOT EXISTS test_run_ci_contexts (
  test_run_id BIGINT,
  type VARCHAR(63) NOT NULL,
  env_variables jsonb NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  modified_at TIMESTAMP NOT NULL,
  PRIMARY KEY (test_run_id),
  FOREIGN KEY (test_run_id) REFERENCES test_runs (id) ON DELETE CASCADE
);
CREATE TRIGGER update_timestamp_test_run_ci_contexts
  BEFORE INSERT OR UPDATE
  ON test_run_ci_contexts
  FOR EACH ROW
EXECUTE PROCEDURE update_modified_at();
