package com.zebrunner.reporting.domain.entity.integration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "integration_slack_configs")
public class SlackConfig {

    @Id
    @EqualsAndHashCode.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String token;
    private String botName;
    private String botIconUrl;

    @EqualsAndHashCode.Exclude
    private boolean enabled;

    @EqualsAndHashCode.Exclude
    private boolean encrypted;

}
