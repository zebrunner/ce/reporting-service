package com.zebrunner.reporting.service;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.domain.entity.TestSession;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface TestSessionService {

    List<TestSession> retrieveByTestRunId(Long testRunId);

    List<TestSession> retrieveByTestRunIdAndTestId(Long testRunId, Long testId);

    /**
     * Stores the given entity in database and propagates capabilities to test run level.
     * <p>
     * Capabilities are propagated to test run level only for the very first test session.
     * The capabilities that will be propagated are 'browserName', 'browserVersion', 'platformName'.
     * On test run level, these capabilities are stored as part of test run config in appropriate fields.
     * <p>
     * This method does not care about 'endedAt' property of the entity - it stores the value as is.
     * <p>
     * The provided links to tests are resolved softly - if there is no test with provided id, the test will be simply ignored.
     *
     * @param session entity to be saved
     * @param testIds
     * @return persisted state of the entity
     * @throws BusinessConstraintException with TEST_SESSION_WITH_SESSION_ID_ALREADY_EXISTS error
     *                                     when there is a test session with given session id
     * @throws ResourceNotFoundException   with TEST_RUN_NOT_FOUND_BY_ID error
     *                                     when there is no test run with given id
     */
    TestSession create(TestSession session, Collection<Long> testIds);

    /**
     * Updates endedAt property of the already stored entity and\or attaches links to tests.
     * <p>
     * The given 'endedAt' property is stored only if it is after already stored 'startedAt' value,
     * otherwise an exception will be thrown.
     * <p>
     * The provided links to tests are resolved softly - if there is no test with provided id, the test will be simply ignored.
     *
     * @param session entity with populated test references and\or endedAt property.
     * @param testIds
     * @return persisted state of the entity
     * @throws BusinessConstraintException with ENDED_AT_MUST_BE_AFTER_STARTED_AT error
     *                                     when given 'endedAt' value is not after already stored 'startedAt' value
     * @throws ResourceNotFoundException   with TEST_SESSION_NOT_EXIST_BY_ID error
     *                                     when there is no session with given id (means db id, nor session id)
     */
    TestSession update(TestSession session, Collection<Long> testIds);

    void finishNotCompleted(Long testRunId, Instant endedAt);

    void attachArtifactReferences(Long id, Set<ArtifactReference> references);

}
