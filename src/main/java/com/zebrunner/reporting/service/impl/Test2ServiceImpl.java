package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.BusinessConstraintException;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.dto.TestIssueReference;
import com.zebrunner.reporting.domain.dto.TestIssueReferenceAssignmentResult;
import com.zebrunner.reporting.domain.dto.reporting.TestFinish;
import com.zebrunner.reporting.domain.dto.reporting.TestSave;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestCase;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.entity.TestRunStatistic;
import com.zebrunner.reporting.domain.entity.User;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.domain.entity.integration.IssueReferenceAssignmentEvent;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.persistence.repository.TestRepository;
import com.zebrunner.reporting.service.IssueReferenceAssignmentEventService;
import com.zebrunner.reporting.service.IssueReferenceService;
import com.zebrunner.reporting.service.LabelService;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestCase2Service;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.TestRunStatisticService;
import com.zebrunner.reporting.service.exception.BusinessConstraintError;
import com.zebrunner.reporting.service.hash.FailureReasonHashFunction;
import com.zebrunner.reporting.service.integration.jira.JiraConfigService;
import com.zebrunner.reporting.service.integration.jira.JiraService;
import com.zebrunner.reporting.service.user.UserReassignmentListener;
import com.zebrunner.reporting.service.user.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.zebrunner.reporting.service.exception.BusinessConstraintError.ISSUE_CANNOT_BE_ASSIGNED_TO_TEST_WITH_EMPTY_REASON;
import static com.zebrunner.reporting.service.exception.BusinessConstraintError.KNOWN_ISSUE_CANNOT_BE_ATTACHED_TO_TEST_STATUS;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_NOT_FOUND_BY_ID;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_NOT_FOUND_BY_TEST_RUN_ID_AND_ID;
import static com.zebrunner.reporting.service.exception.ResourceNotFoundError.TEST_RUN_NOT_FOUND_BY_CI_RUN_ID;

@Slf4j
@Service
@RequiredArgsConstructor
public class Test2ServiceImpl implements Test2Service, UserReassignmentListener {

    private static final String DEFAULT_TEST_NAME = "system";
    private static final String DEFAULT_TEST_CLASS = "system";
    private static final String DEFAULT_TEST_METHOD = "system";

    private final JiraService jiraService;
    private final LabelService labelService;
    private final TestRepository testRepository;
    private final JiraConfigService jiraConfigService;
    private final FailureReasonHashFunction hashFunction;
    private final IssueReferenceService issueReferenceService;
    private final TestRunStatisticService testRunStatisticService;

    @Lazy
    private final UserService userService;
    @Lazy
    private final TestRun2Service testRun2Service;
    @Lazy
    private final TestCase2Service testCase2Service;
    @Lazy
    private final IssueReferenceAssignmentEventService issueEventService;

    @Override
    public Test retrieveById(Long id) {
        return testRepository.findById(id)
                             .orElseThrow(() -> new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID, id));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Test> retrieveByCiRunId(String ciRunId, Collection<Test.Status> statuses, Collection<Long> testIds) {
        TestRun testRun = testRun2Service.retrieveOptionalByCiRunId(ciRunId)
                                         .orElseThrow(() -> new ResourceNotFoundException(TEST_RUN_NOT_FOUND_BY_CI_RUN_ID, ciRunId));
        List<Test> tests = testRepository.findAllByTestRunId(testRun.getId());

        tests.removeIf(test -> test.isKnownIssue()
                               || (!CollectionUtils.isEmpty(testIds) && !testIds.contains(test.getId()))
                               || (!CollectionUtils.isEmpty(statuses) && !statuses.contains(test.getStatus()))
        );

        return tests;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Test> retrieveByTestRunId(Long testRunId) {
        return testRepository.findAllByTestRunId(testRunId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Test> retrieveAllByTestRunIdAndIds(Long testRunId, Collection<Long> ids) {
        return testRepository.findAllByTestRunIdAndIdIn(testRunId, ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Test> retrieveAllByIds(Collection<Long> ids) {
        return testRepository.findAllByIdIn(ids);
    }

    @Override
    public boolean existsByTestRunIdAndKnownIssue(Long testRunId) {
        return testRepository.existsByTestRunIdAndKnownIssue(testRunId, true);
    }

    @Override
    public boolean existsByTestRunIdAndLogicallyFailedAndNotKnownIssue(Long testRunId) {
        Set<Test.Status> logicallyFailedStatuses = Test.Status.getLogicallyFailed();
        return testRepository.existsByTestRunIdAndStatusInAndKnownIssue(testRunId, logicallyFailedStatuses, false);
    }

    @Override
    public int countByTestRunId(Long testRunId) {
        return testRepository.countByTestRunId(testRunId);
    }

    @Override
    public boolean existsByTestRunIdAndId(Long testRunId, Long id) {
        return testRepository.existsByTestRunIdAndId(testRunId, id);
    }

    @Override
    @Transactional
    public Test attachLabels(Long testId, Collection<Label> labels) {
        Test test = retrieveById(testId);
        Set<Label> existingLabels = test.getLabels();
        Set<Label> newLabels = labelService.save(labels);
        existingLabels.addAll(newLabels);
        return testRepository.save(test);
    }

    @Override
    @Transactional
    public void attachArtifactReferences(Long testId, Set<ArtifactReference> references) {
        Test test = retrieveById(testId);
        Set<ArtifactReference> testArtifactReferences = test.getArtifactReferences();
        references.forEach(reference -> testArtifactReferences.removeIf(
                testReference -> Objects.equals(testReference.getName(), reference.getName())));
        testArtifactReferences.addAll(references);
        testRepository.save(test);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (!testRepository.existsById(id)) {
            throw new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID, id);
        }
        testRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByTestRunIdAndId(Long testRunId, Long id) {
        if (!testRepository.existsByTestRunIdAndId(testRunId, id)) {
            throw new ResourceNotFoundException(TEST_NOT_FOUND_BY_TEST_RUN_ID_AND_ID, testRunId, id);
        }
        testRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Test start(Long testRunId, TestSave testSave) {
        TestRun testRun = testRun2Service.retrieveById(testRunId);

        TestCase testCase = getTestCase(testSave, testRun);
        Set<Label> labels = labelService.save(testSave.getLabels());
        User maintainer = userService.retrieveByUsername(testSave.getMaintainer())
                                     .orElseGet(userService::retrieveAnonymous);

        Test test = Test.builder()
                        .name(obtainTestName(testSave.getName()))
                        .correlationData(testSave.getCorrelationData())
                        .testCase(testCase)
                        .maintainer(maintainer)
                        .testRun(testRun)
                        .startedAt(testSave.getStartedAt())
                        .status(Test.Status.IN_PROGRESS)
                        .labels(labels)
                        .build();
        test = testRepository.save(test);

        updateStatisticOnTestStart(testRun.getStatistic());

        return test;
    }

    private void updateStatisticOnTestStart(TestRunStatistic statistic) {
        statistic.trackTestStart();
        testRunStatisticService.save(statistic);
    }

    @Override
    @Transactional
    public Test restart(Long testRunId, Long testId, TestSave testSave) {
        Test existingTest = testRepository.findByTestRunIdAndId(testRunId, testId)
                                          .orElseThrow(() -> new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID));

        TestCase testCase = getTestCase(testSave, existingTest.getTestRun());
        Set<Label> labels = labelService.save(testSave.getLabels());
        User maintainer = userService.retrieveByUsername(testSave.getMaintainer())
                                     .orElseGet(userService::retrieveAnonymous);

        existingTest.setName(obtainTestName(testSave.getName()));
        existingTest.setCorrelationData(testSave.getCorrelationData());
        existingTest.setStatus(Test.Status.IN_PROGRESS);
        existingTest.setStartedAt(testSave.getStartedAt());
        existingTest.setReason(null);
        existingTest.setKnownIssue(false);

        existingTest.setMaintainer(maintainer);
        existingTest.setTestCase(testCase);
        existingTest.setIssueReference(null);
        existingTest.getLabels().addAll(labels);

        return testRepository.save(existingTest);
    }

    private String obtainTestName(String name) {
        return name != null && !name.isBlank()
                ? name.trim()
                : DEFAULT_TEST_NAME;
    }

    private TestCase getTestCase(TestSave testSave, TestRun testRun) {

        String className = testSave.getClassName();
        String methodName = testSave.getMethodName();
        TestCase testCase = TestCase.builder()
                                    .testClass(className != null ? className : DEFAULT_TEST_CLASS)
                                    .testMethod(methodName != null ? methodName : DEFAULT_TEST_METHOD)
                                    .info(testSave.getTestCase())
                                    .testSuite(testRun.getTestSuite())
                                    .build();
        return testCase2Service.save(testCase);
    }

    @Override
    @Transactional
    public Test update(Long testRunId, Long testId, TestSave testSave) {
        TestRun testRun = testRun2Service.retrieveById(testRunId);
        Test test = testRepository.findByTestRunIdAndId(testRunId, testId)
                                  .orElseThrow(() -> new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID, testId));

        TestCase testCase = getTestCase(testSave, testRun);
        Set<Label> labels = labelService.save(testSave.getLabels());
        User maintainer = userService.retrieveByUsername(testSave.getMaintainer())
                                     .orElseGet(userService::retrieveAnonymous);

        test.setName(testSave.getName());
        test.setCorrelationData(testSave.getCorrelationData());
        test.setMaintainer(maintainer);
        test.setTestCase(testCase);
        test.getLabels().addAll(labels);

        return testRepository.save(test);
    }

    @Override
    public void swapStatusesByTestRunId(Long testRunId, Test.Status fromStatus, Test.Status toStatus) {
        testRepository.swapStatusesByTestRunId(testRunId, fromStatus, toStatus);
    }

    @Override
    @Transactional
    public Test finish(Long testRunId, Long testId, TestFinish testFinish) {
        TestRun testRun = testRun2Service.retrieveById(testRunId);

        Test test = testRepository.findByTestRunIdAndId(testRunId, testId)
                                  .orElseThrow(() -> new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID, testId));

        Set<Label> labels = labelService.save(testFinish.getLabels());

        test.setEndedAt(testFinish.getEndedAt());
        test.getLabels().addAll(labels);
        test.setStatus(testFinish.getResult());
        test.setReason(testFinish.getReason());
        test.setReasonHashCode(hashFunction.calculateHash(testFinish.getReason()));
        tryAssignKnownIssue(test);

        test = testRepository.save(test);
        updateStatisticOnTestFinish(testRun.getStatistic(), test.getStatus());

        return test;
    }

    private void updateStatisticOnTestFinish(TestRunStatistic statistic, Test.Status status) {
        if (Test.Status.FAILED.equals(status)) {
            statistic.trackTestFailure();
        }
        if (Test.Status.ABORTED.equals(status)) {
            statistic.trackTestAbortion();
        }
        if (Test.Status.SKIPPED.equals(status)) {
            statistic.trackTestSkip();
        }
        if (Test.Status.PASSED.equals(status)) {
            statistic.trackTestPass();
        }
        testRunStatisticService.save(statistic);
    }

    private void tryAssignKnownIssue(Test test) {
        if (Test.Status.FAILED == test.getStatus() && jiraConfigService.isEnabled()) {
            this.retrieveNotClosedKnownIssue(test.getTestCase().getId(), test.getReason())
                .ifPresent(issueReference -> {
                    test.setKnownIssue(true);
                    test.setIssueReference(issueReference);
                });
        }
    }

    @Override
    @Transactional(readOnly = true)
    public boolean confirmKnownIssue(Long testRunId, Long testId, String failureReason) {
        Test test = testRepository.findByTestRunIdAndId(testRunId, testId)
                                  .orElseThrow(() -> new ResourceNotFoundException(TEST_NOT_FOUND_BY_ID, testId));
        Long testCaseId = test.getTestCase().getId();
        return this.retrieveNotClosedKnownIssue(testCaseId, failureReason)
                   .isPresent();
    }

    private Optional<IssueReference> retrieveNotClosedKnownIssue(Long testCaseId, String reason) {
        Integer reasonHashCode = hashFunction.calculateHash(reason);
        return issueReferenceService.retrieveLinkedToTestCaseByFailureReasonHash(testCaseId, reasonHashCode)
                                    .filter(issueReference -> !isCompletedByType(issueReference));
    }

    private boolean isCompletedByType(IssueReference issueReference) {
        String issueId = issueReference.getValue();
        if (IssueReference.Type.JIRA == issueReference.getType()) {
            try {
                return jiraService.isIssueCompleted(issueId);
            } catch (RuntimeException e) {
                log.warn("Failed to check if issue '{}' of type JIRA - assuming it is not", issueId, e);
                return false;
            }
        } else {
            log.warn("Failed to check if issue '{}' of type {} - currently unsupported", issueId, issueReference.getType());
            return false;
        }
    }

    private IssueReferenceAssignmentEvent linkIssueWithoutTestRunResultRecalculation(Test test,
                                                                                     IssueReference issueReference,
                                                                                     Long reporterId) {
        if (!test.isLogicallyFailed()) {
            throw new BusinessConstraintException(KNOWN_ISSUE_CANNOT_BE_ATTACHED_TO_TEST_STATUS, test.getStatus());
        }

        IssueReference issueReferenceToLink = issueReferenceService.retrieveOrCreate(issueReference);
        Long testCaseId = test.getTestCase().getId();
        Integer hash = hashFunction.calculateHash(test.getReason());

        IssueReferenceAssignmentEvent event = issueEventService.saveLinkingIfNeeded(issueReferenceToLink, testCaseId, hash, reporterId);

        test.setIssueReference(issueReferenceToLink);
        test.setKnownIssue(true);

        return event;
    }
    @Override
    @Transactional
    public TestIssueReferenceAssignmentResult linkIssues(List<TestIssueReference> testIssueReferences, Long reporterId) {
        Map<Long, Test> testIdToTest = mapTestIssueReferencesToTests(testIssueReferences);

        List<TestIssueReferenceAssignmentResult.Item> testNotFound = testIssueReferences.stream()
                                                                                        .filter(reference -> testIdToTest.get(reference.getTestId()) == null)
                                                                                        .map(this::buildNotAssignedTestIssueReference)
                                                                                        .collect(Collectors.toList());

        List<TestIssueReferenceAssignmentResult.Item> testNotLogicallyFailed = testIssueReferences.stream()
                                                                                                  .filter(reference -> testIdToTest.get(reference.getTestId()) != null)
                                                                                                  .filter(reference -> !testIdToTest.get(reference.getTestId()).isLogicallyFailed())
                                                                                                  .map(this::buildNotAssignedTestIssueReference)
                                                                                                  .collect(Collectors.toList());

        Map<Long, IssueReferenceAssignmentEvent> testIdToAssignedIssueReference = testIssueReferences.stream()
                                                                                                     .filter(reference -> testIdToTest.get(reference.getTestId()) != null)
                                                                                                     .filter(reference -> testIdToTest.get(reference.getTestId()).isLogicallyFailed())
                                                                                                     .map(reference -> Map.of(reference.getTestId(),
                                                                                                             assignIssueReference(reporterId, testIdToTest.get(reference.getTestId()), reference)))
                                                                                                     .flatMap(map -> map.entrySet().stream())
                                                                                                     .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        testIdToAssignedIssueReference.keySet()
                                      .stream()
                                      .map(testIdToTest::get)
                                      .map(test -> test.getTestRun().getId())
                                      .distinct()
                                      .forEach(testRun2Service::recalculateTestRunResult);

        List<TestIssueReferenceAssignmentResult.Item> assignedToTest = testIdToAssignedIssueReference.entrySet()
                                                                                                     .stream()
                                                                                                     .map(entry -> buildAssignmentResult(entry.getKey(), entry.getValue()))
                                                                                                     .collect(Collectors.toList());
        return new TestIssueReferenceAssignmentResult(testNotFound, testNotLogicallyFailed, assignedToTest);
    }

    private Map<Long, Test> mapTestIssueReferencesToTests(List<TestIssueReference> testIssueReferences) {
        List<Long> testIds = testIssueReferences.stream()
                                                .map(TestIssueReference::getTestId)
                                                .collect(Collectors.toList());
        return retrieveAllByIds(testIds).stream().collect(Collectors.toMap(Test::getId, Function.identity(), (v1, v2) -> v1));
    }

    private IssueReferenceAssignmentEvent assignIssueReference(Long reporterId, Test test, TestIssueReference testIssueReference) {
        if (!test.isLogicallyFailed()) {
            throw new BusinessConstraintException(KNOWN_ISSUE_CANNOT_BE_ATTACHED_TO_TEST_STATUS, test.getStatus());
        }

        if (test.getReason() == null) {
            throw new BusinessConstraintException(ISSUE_CANNOT_BE_ASSIGNED_TO_TEST_WITH_EMPTY_REASON, test.getId());
        }

        IssueReference issueReference = issueReferenceService.retrieveOrCreate(testIssueReference.getValue(), testIssueReference.getType());
        Long testCaseId = test.getTestCase().getId();
        Integer hash = hashFunction.calculateHash(test.getReason());

        IssueReferenceAssignmentEvent event = issueEventService.saveLinkingIfNeeded(issueReference, testCaseId, hash, reporterId);

        test.setIssueReference(issueReference);
        test.setKnownIssue(true);
        return event;
    }

    private TestIssueReferenceAssignmentResult.Item buildAssignmentResult(Long testId, IssueReferenceAssignmentEvent issueReferenceAssignmentEvent) {
        IssueReference issueReference = issueReferenceAssignmentEvent.getIssueReference();
        return TestIssueReferenceAssignmentResult.Item.builder()
                                                      .testId(testId)
                                                      .id(issueReference.getId())
                                                      .value(issueReference.getValue())
                                                      .type(issueReference.getType())
                                                      .createdAt(issueReferenceAssignmentEvent.getCreatedAt())
                                                      .build();
    }

    private TestIssueReferenceAssignmentResult.Item buildNotAssignedTestIssueReference(TestIssueReference testIssueReference) {
        return TestIssueReferenceAssignmentResult.Item.builder()
                                                      .testId(testIssueReference.getTestId())
                                                      .value(testIssueReference.getValue())
                                                      .type(testIssueReference.getType())
                                                      .build();
    }

    @Override
    @Transactional
    public void unlinkIssue(Long testId, Long issueReferenceId, Long reporterId) {
        Test test = retrieveById(testId);
        IssueReference testIssueReference = test.getIssueReference();
        if (testIssueReference == null || !testIssueReference.getId().equals(issueReferenceId)) {
            throw new BusinessConstraintException(BusinessConstraintError.ISSUE_NOT_LINKED_TO_TEST, issueReferenceId, testId);
        }

        Integer hash = hashFunction.calculateHash(test.getReason());
        issueEventService.saveUnlinkingIfNeeded(testIssueReference, test.getTestCase().getId(), hash, reporterId);

        test.setIssueReference(null);
        test.setKnownIssue(false);
        testRepository.save(test);

        testRun2Service.recalculateTestRunResult(test.getTestRun().getId());
    }

    @Override
    @Transactional
    public void onUserReassignment(Long fromId, Long toId) {
        testRepository.reassignMaintainer(fromId, toId);
    }

}
