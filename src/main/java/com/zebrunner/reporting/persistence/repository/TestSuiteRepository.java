package com.zebrunner.reporting.persistence.repository;

import com.zebrunner.reporting.domain.entity.TestSuite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestSuiteRepository extends JpaRepository<TestSuite, Long> {

    Optional<TestSuite> findByNameAndFileNameAndUserId(String name, String fileName, Long userId);

}
