package com.zebrunner.reporting.web.request.v1.issue;

import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IssueReferenceToTestBatch {

    private Long userId;
    private Map<Long, IssueReference> testToIssue;

}
