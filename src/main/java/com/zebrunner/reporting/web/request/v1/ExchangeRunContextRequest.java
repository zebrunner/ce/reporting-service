package com.zebrunner.reporting.web.request.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.vo.RunContext;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class ExchangeRunContextRequest {

    @Deprecated
    private String id;
    @Deprecated
    private Set<Long> testIds;
    @Deprecated
    private boolean rerunOnlyFailures;
    @Deprecated
    private Set<Test.Status> statuses;

    private String testRunUuid;
    private RunContext.Mode mode;
    private RerunCriteria rerunCriteria = new RerunCriteria();

    public void setRerunCriteria(RerunCriteria rerunCriteria) {
        if (rerunCriteria != null) {
            this.rerunCriteria = rerunCriteria;
        }
    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    public static class RerunCriteria {

        private Set<Test.Status> anyOfStatuses;
        private Boolean knownIssue;

    }

}
