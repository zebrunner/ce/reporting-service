package com.zebrunner.reporting.web.request.v1;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class PushTestRailResultsRequest {

    @NotNull
    private Long projectId;
    @NotNull
    private Long suiteId;

    private Long milestoneId;
    private String createdBy;
    private LocalDateTime createdAfter;

}
