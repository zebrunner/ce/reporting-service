package com.zebrunner.reporting.domain.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Test extends AbstractEntity implements Comparable<Test> {

    private String name;
    private String correlationData;
    private Status status;
    private String testArgs;
    private Long testRunId;
    private Long testCaseId;
    private String testGroup;
    private String message;
    private Integer messageHashCode;
    private Date startTime;
    private Date finishTime;
    private List<IssueReference> issueReferences;
    private boolean knownIssue;
    private boolean needRerun;
    private String owner;
    private String dependsOnMethods;
    private String testClass;
    @JsonProperty("artifacts")
    private Set<ArtifactReference> artifactReferences = new HashSet<>();
    private Set<Label> labels;
    private Integer testSessionsCount;
    private Long maintainerId;

    public Test() {
    }

    @JsonIgnore
    public boolean isLogicallyFailed() {
        return this.status == Status.FAILED || this.status == Status.SKIPPED;
    }


    // used in test run results template
    public String getNotNullTestGroup() {
        if (testGroup != null) {
            return testGroup;

            // we do not store test package in testGroup field anymore. so, extracting it from full class name
        } else if (testClass != null && testClass.contains(".")) {
            return testClass.substring(0, testClass.lastIndexOf('.'));

        } else {
            return "n/a";
        }
    }

    @Override
    public int compareTo(Test test) {
        if (Arrays.asList(Status.ABORTED, Status.SKIPPED, Status.FAILED).contains(this.getStatus())) {
            return -1;
        } else {
            return 0;
        }
    }

}
