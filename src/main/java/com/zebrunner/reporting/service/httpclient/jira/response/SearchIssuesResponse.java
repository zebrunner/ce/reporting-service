package com.zebrunner.reporting.service.httpclient.jira.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zebrunner.reporting.service.httpclient.jira.model.Issue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchIssuesResponse {

    private List<Issue> issues;

}
