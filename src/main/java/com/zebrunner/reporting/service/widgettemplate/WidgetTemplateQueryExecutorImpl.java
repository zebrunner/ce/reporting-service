package com.zebrunner.reporting.service.widgettemplate;

import com.zebrunner.common.eh.exception.InternalServiceException;
import com.zebrunner.reporting.domain.entity.WidgetTemplate;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import com.zebrunner.reporting.service.util.FreemarkerUtil;
import com.zebrunner.reporting.service.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.zebrunner.reporting.service.exception.InternalServiceError.UNABLE_TO_GENERATE_CHART_QUERY;
import static com.zebrunner.reporting.service.exception.InternalServiceError.WIDGET_INVALID_CHART_QUERY;

@Slf4j
@Service
@RequiredArgsConstructor
public class WidgetTemplateQueryExecutorImpl implements WidgetTemplateQueryExecutor {

    private static final String DEFAULT_PERIOD = "Last 14 Days";

    private final JdbcTemplate jdbcTemplate;

    private final FreemarkerUtil freemarker;

    private final Map<String, String> periodToWhereClause = new HashMap<>();

    @PostConstruct
    public void init() {
        periodToWhereClause.put("Last 24 Hours", "test_suite_executions.started_at >= (current_date - interval '1 day')");
        periodToWhereClause.put("Last 7 Days", "test_suite_executions.started_at >= (current_date - interval '7 day')");
        periodToWhereClause.put("Last 14 Days", "test_suite_executions.started_at >= (current_date - interval '14 day')");
        periodToWhereClause.put("Last 30 Days", "test_suite_executions.started_at >= (current_date - interval '30 day')");
        periodToWhereClause.put("Last 90 Days", "test_suite_executions.started_at >= (current_date - interval '90 day')");
        periodToWhereClause.put("Last 365 Days", "test_suite_executions.started_at >= (current_date - interval '365 day')");
        periodToWhereClause.put("Today", "test_suite_executions.started_at >= current_date");
        periodToWhereClause.put("Week", "test_suite_executions.started_at >= date_trunc('week'::text, current_date)");
        periodToWhereClause.put("Month", "test_suite_executions.started_at >= date_trunc('month'::text, current_date)");
        periodToWhereClause.put("Year", "test_suite_executions.started_at >= date_trunc('year'::text, current_date)");
        periodToWhereClause.put("Total", "1=1");
    }

    @Override
    public List<Object> executeTemplateParamQuery(WidgetTemplateParameter widgetTemplateParameter, String period) {
        String query = widgetTemplateParameter.getValuesQuery();
        List<Object> parameterValues = Collections.emptyList();
        if (!StringUtils.isEmpty(query)) {
            String queryWithWhereClause = query.replaceAll(Pattern.quote("${whereClause}"), generateWhereClause(period));
            List<Map<String, Object>> queryResult = Collections.emptyList();
            try {
                queryResult = jdbcTemplate.queryForList(queryWithWhereClause);
            } catch (Exception e) {
                // Exception is not thrown in order to let the other parameter queries to be executed
                log.warn("String is not sql or is not valid: " + queryWithWhereClause, e);
            }
            parameterValues = extractParameterValuesFromQueryResult(queryResult);
        }

        return widgetTemplateParameter.getValues() != null
                ? StreamUtils.concat(widgetTemplateParameter.getValues(), parameterValues)
                : parameterValues;
    }

    private String generateWhereClause(String period) {
        StringBuilder queryBuilder = new StringBuilder("WHERE 1=1");

        String periodWhereClause = Optional.ofNullable(period)
                                           .map(periodToWhereClause::get)
                                           .orElseGet(() -> periodToWhereClause.get(DEFAULT_PERIOD));
        if (periodWhereClause != null) {
            queryBuilder.append(" AND ").append(periodWhereClause);
        }

        return queryBuilder.toString();
    }

    private List<Object> extractParameterValuesFromQueryResult(List<Map<String, Object>> queryResult) {
        return queryResult.stream()
                          .filter(Objects::nonNull)
                          .flatMap(map -> map.values().stream())
                          .collect(Collectors.toList());
    }

    @Override
    public List<Map<String, Object>> executeTemplateSql(WidgetTemplate widgetTemplate, Map<String, Object> queryParameters, Integer userId) {
        String templateSqlQuery = getTemplateSqlQuery(widgetTemplate.getSql(), queryParameters, userId);
        try {
            return jdbcTemplate.queryForList(templateSqlQuery);
        } catch (Exception e) {
            log.error("Unable to execute SQL template: " + e.getMessage(), e);
            throw new InternalServiceException(WIDGET_INVALID_CHART_QUERY, e);
        }
    }

    private String getTemplateSqlQuery(String queryTemplate, Map<String, Object> queryParameters, Integer userId) {
        try {
            queryParameters.putIfAbsent("currentUserId", userId);
            queryParameters.putIfAbsent("testFunctionId", 0);
            queryParameters.putIfAbsent("hashcode", 0);
            return freemarker.buildContentFromTemplate(queryTemplate, queryParameters, false);
        } catch (Exception e) {
            log.error("Unable to generate SQL query: " + e.getMessage(), e);
            throw new InternalServiceException(UNABLE_TO_GENERATE_CHART_QUERY, e);
        }
    }

}
