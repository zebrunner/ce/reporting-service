UPDATE dashboards SET hidden = FALSE;

UPDATE dashboards SET hidden = TRUE WHERE title IN ('Failures analysis', 'User Performance', 'Stability');