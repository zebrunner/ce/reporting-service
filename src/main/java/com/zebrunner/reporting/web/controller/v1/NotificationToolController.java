package com.zebrunner.reporting.web.controller.v1;

import com.zebrunner.reporting.service.integration.slack.SlackConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/notification-tools", produces = MediaType.APPLICATION_JSON_VALUE)
public class NotificationToolController {

    private final SlackConfigService slackConfigService;

    @GetMapping
    public Map<String, Boolean> getAll() {
        boolean isSlackEnabled = slackConfigService.retrieve().isEnabled();
        return Map.of("SLACK", isSlackEnabled);
    }

}
