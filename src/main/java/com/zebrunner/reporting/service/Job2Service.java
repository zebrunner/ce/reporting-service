package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.entity.Job;
import com.zebrunner.reporting.domain.entity.User;

import java.util.Optional;

public interface Job2Service {

    Optional<Job> retrieveOrCreateIfIntegrationExists(String jobUrl, User jobOwner);

}
