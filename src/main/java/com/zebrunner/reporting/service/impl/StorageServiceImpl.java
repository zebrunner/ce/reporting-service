package com.zebrunner.reporting.service.impl;

import com.zebrunner.common.eh.exception.MalformedInputException;
import com.zebrunner.common.s3.AwsS3Properties;
import com.zebrunner.reporting.domain.dto.BinaryObject;
import com.zebrunner.reporting.service.StorageService;
import com.zebrunner.reporting.service.exception.MalformedInputError;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;

import java.util.UUID;

import static com.zebrunner.reporting.persistence.PersistenceConfig.DEFAULT_SCHEMA;

@Service
@RequiredArgsConstructor
public class StorageServiceImpl implements StorageService {

    private static final String[] ALLOWED_IMAGE_CONTENT_TYPES = {"image/png", "image/jpeg"};
    private static final String[] APP_EXTENSIONS = {"app", "ipa", "apk", "apks"};

    private static final long MAX_IMAGE_SIZE = 2 * 1024 * 1024;         // 2 MB
    private static final long MAX_APP_PACKAGE_SIZE = 100 * 1024 * 1024; // 100 MB

    private final S3Client s3Client;
    private final AwsS3Properties awsProperties;

    @Override
    public String save(BinaryObject binaryObject) {
        validateObject(binaryObject);
        String key = buildObjectKey(binaryObject);
        String storageKey = DEFAULT_SCHEMA + "/" + key;
        s3Client.putObject(
                rb -> rb.bucket(awsProperties.getBucket()).key(storageKey).acl(ObjectCannedACL.PRIVATE).build(),
                RequestBody.fromInputStream(binaryObject.getData(), binaryObject.getSize())
        );
        return key;
    }

    @Override
    public BinaryObject get(String key) {
        ResponseInputStream<GetObjectResponse> response = s3Client
                .getObject(rb -> rb.bucket(awsProperties.getBucket()).key(key).build());
        return BinaryObject.builder()
                           .data(response)
                           .name(getObjectName(key))
                           .contentType(response.response().contentType())
                           .key(key)
                           .size(response.response().contentLength())
                           .build();
    }

    @Override
    public void remove(String key) {
        s3Client.deleteObject(rb -> rb.bucket(awsProperties.getBucket()).key(key).build());
    }

    private void validateObject(BinaryObject binaryObject) {
        BinaryObject.Type type = binaryObject.getType();
        switch (type) {
            case ORG_ASSET:
            case USER_ASSET:
                if (binaryObject.getSize() > MAX_IMAGE_SIZE) {
                    throw new MalformedInputException(MalformedInputError.INVALID_FILE_SIZE, 2);
                }
                if (!ArrayUtils.contains(ALLOWED_IMAGE_CONTENT_TYPES, binaryObject.getContentType())) {
                    throw new MalformedInputException(MalformedInputError.INVALID_FILE_FORMAT, "JPEG or PNG image");
                }
                break;
            case APP_PACKAGE:
                String extension = FilenameUtils.getExtension(binaryObject.getName());
                if (binaryObject.getSize() > MAX_APP_PACKAGE_SIZE) {
                    throw new MalformedInputException(MalformedInputError.INVALID_FILE_SIZE, 100);
                }
                if (!ArrayUtils.contains(APP_EXTENSIONS, extension)) {
                    throw new MalformedInputException(MalformedInputError.INVALID_FILE_FORMAT, "APP, IPA or APK");
                }
                break;
        }
    }

    private String buildObjectKey(BinaryObject binaryObject) {
        String name = UUID.randomUUID().toString();
        return getKeyPrefix(binaryObject.getType()) + "/"
                + name + "."
                + FilenameUtils.getExtension(binaryObject.getName());
    }

    private String getKeyPrefix(BinaryObject.Type type) {
        switch (type) {
            case ORG_ASSET:
                return "assets/org";
            case USER_ASSET:
                return "assets/user";
            case APP_PACKAGE:
                return "artifacts/applications";
            default:
                return "";
        }
    }

    private String getObjectName(String key) {
        int nameIndex = key.lastIndexOf("/");
        return nameIndex != -1 ? key.substring(nameIndex + 1) : key;
    }

}
