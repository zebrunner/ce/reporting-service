package com.zebrunner.reporting.service.dashboard;

import com.zebrunner.reporting.domain.entity.DashboardWidget;

import java.util.List;
import java.util.Optional;

public interface DashboardWidgetService {

    List<DashboardWidget> retrieveAllByWidgetId(Long widgetId);

    List<DashboardWidget> retrieveAllInDashboardsByTitleAndTemplateId(List<Long> dashboardIds, String title, Long templateId);

    Optional<DashboardWidget> retrieveOptionalInDashboardByTitleAndTemplateId(Long dashboardId, String title, Long templateId);

    DashboardWidget addToDashboard(Long dashboardId, Long widgetId, String location);

    List<DashboardWidget> batchUpdate(Long dashboardId, List<DashboardWidget> widgets);

    void removeFromDashboard(Long dashboardId, Long widgetId);

}
