package com.zebrunner.reporting.service.integration.jenkins;

import com.zebrunner.reporting.domain.entity.integration.JenkinsConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;

import java.util.List;
import java.util.Optional;

public interface JenkinsConfigService {

    List<JenkinsConfig> retrieveAll(boolean decryptValues);

    List<JenkinsConfig> retrieveAllActive(boolean decryptValues);

    default Optional<JenkinsConfig> retrieveOptionalById(Long id) {
        return retrieveOptionalById(id, true);
    }

    Optional<JenkinsConfig> retrieveOptionalById(Long id, boolean decryptValues);

    default JenkinsConfig retrieveById(Long id) {
        return retrieveById(id, true);
    }

    JenkinsConfig retrieveById(Long id, boolean decryptValues);

    JenkinsConfig retrieveActiveById(Long id, boolean decryptValues);

    default Optional<JenkinsConfig> retrieveOptionalByUrl(String url) {
        return retrieveOptionalByUrl(url, true);
    }

    Optional<JenkinsConfig> retrieveOptionalByUrl(String url, boolean decryptValues);

    JenkinsConfig create(JenkinsConfig config);

    JenkinsConfig update(Long id, JenkinsConfig config);

    void patchEnabledProperty(Long id, boolean enabled);

    void deleteById(Long id);

    ReachabilityCheckResult isReachable(JenkinsConfig config);

}
