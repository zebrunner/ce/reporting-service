package com.zebrunner.reporting.service.legacy;

import com.zebrunner.reporting.domain.integration.jira.IssuePreview;
import com.zebrunner.reporting.service.httpclient.jira.model.Issue;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class IssueConverter {

    public static IssuePreview issueToPreview(Issue issue) {
        String assigneeName = issue.getFields().getAssignee() != null
                              ? issue.getFields().getAssignee().getDisplayName()
                              : null;
        String reporterName = issue.getFields().getReporter() != null
                              ? issue.getFields().getReporter().getDisplayName()
                              : null;

        return IssuePreview.builder()
                           .key(issue.getKey())
                           .assignee(assigneeName)
                           .reporter(reporterName)
                           .summary(issue.getFields().getSummary())
                           .status(issue.getFields().getStatus().getName())
                           .build();
    }

}
