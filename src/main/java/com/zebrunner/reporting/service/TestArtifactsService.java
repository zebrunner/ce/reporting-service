package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.dto.core.SearchCriteria;
import com.zebrunner.reporting.domain.dto.core.SimpleSearchResult;
import com.zebrunner.reporting.domain.dto.elasticsearch.LogEntry;
import com.zebrunner.reporting.domain.dto.elasticsearch.Screenshot;

public interface TestArtifactsService {

    SimpleSearchResult<LogEntry> fetchLogs(Long testRunId, Long testId, SearchCriteria searchCriteria);

    SimpleSearchResult<Screenshot> fetchScreenshots(Long testRunId, Long testId, SearchCriteria searchCriteria);

}
