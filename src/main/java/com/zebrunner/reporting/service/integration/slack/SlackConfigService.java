package com.zebrunner.reporting.service.integration.slack;

import com.zebrunner.reporting.domain.entity.integration.SlackConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;

public interface SlackConfigService {

    SlackConfig update(SlackConfig newConfig);

    SlackConfig retrieve();

    /**
     * Returns a configuration in an active state with decrypted secrets - meaning that it can be used with integrated
     * service right away. If such configuration does not exists an exception will be thrown.
     */
    default SlackConfig retrieveActive() {
        return retrieveActive(true);
    }

    SlackConfig retrieveActive(boolean decryptValues);

    void patchEnabledProperty(boolean enabled);

    /**
     * Checks if Slack is reachable (settings are valid) for given Slack configuration
     * @param config Slack configuration to check
     */
    ReachabilityCheckResult isReachable(SlackConfig config);

}
