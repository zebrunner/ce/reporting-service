package com.zebrunner.reporting.web.response.v1;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMap;
import com.zebrunner.reporting.domain.vo.WidgetTemplateParameter;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Value
public class GetDashboardsResponse {

    List<Item> items;

    @Data
    @JGlobalMap
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Item {

        private Long id;
        private String title;
        private boolean editable;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Instant createdAt;
        private List<Widget> widgets;

    }

    @Data
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Widget {

        @JMap("${widget.id}")
        private Long id;
        @JMap("${widget.title}")
        private String title;
        @JMap("${widget.description}")
        private String description;
        @JMap("${widget.paramsConfig}")
        private String paramsConfig;
        @JMap("${widget.legendConfig}")
        private String legendConfig;
        @JMap("${widget.type}")
        private String type;
        @JMap("location")
        private String location;

        @JMap("${widget.widgetTemplate}")
        private WidgetTemplate widgetTemplate;

    }

    @Data
    @JGlobalMap
    @NoArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class WidgetTemplate {

        private Long id;
        private String name;
        private String description;
        private com.zebrunner.reporting.domain.entity.WidgetTemplate.Type type;
        private String chartConfig;
        private Map<String, WidgetTemplateParameter> paramsConfig;
        private String legendConfig;

    }

}
