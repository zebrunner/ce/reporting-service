package com.zebrunner.reporting.web.response.v1.testsession;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.reporting.domain.entity.TestSession;
import lombok.Data;
import lombok.Value;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Value
public class ListTestSessionsResponse {

    List<Item> items;

    @Data
    @JGlobalMap(excluded = "name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Item {

        private Long id;
        private String name;
        private Long testRunId;
        private String sessionId;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Instant initiatedAt;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Instant startedAt;
        @JsonFormat(shape = JsonFormat.Shape.STRING)
        private Instant endedAt;
        private TestSession.Status status;
        private String browserName;
        private String browserVersion;
        private String platformName;
        private String platformVersion;
        private String deviceName;
        private List<Test> tests;
        private List<ArtifactReference> artifactReferences;

        public Long getDurationInSeconds() {
            if (startedAt != null && endedAt != null) {
                return Duration.between(startedAt, endedAt).getSeconds();
            } else {
                return null;
            }
        }

        @Data
        @JGlobalMap
        public static class Test {

            private Integer id;
            private String name;
            private com.zebrunner.reporting.domain.entity.Test.Status status;

        }

        @Data
        @JGlobalMap
        public static class ArtifactReference {

            private String name;
            private String value;

        }

    }

}
