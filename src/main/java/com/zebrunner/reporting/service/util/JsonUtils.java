package com.zebrunner.reporting.service.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsonUtils {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @SneakyThrows
    public static <T> T readValue(String content, Class<T> valueType) {
        return OBJECT_MAPPER.readValue(content, valueType);
    }

    @SneakyThrows
    public static <T> T readValue(byte[] content, Class<T> valueType) {
        return OBJECT_MAPPER.readValue(content, valueType);
    }

    @SneakyThrows
    public static <T> T readValue(String content, TypeReference<T> valueTypeReference) {
        return OBJECT_MAPPER.readValue(content, valueTypeReference);
    }

    @SneakyThrows
    public static String writeValue(Object value) {
        return OBJECT_MAPPER.writeValueAsString(value);
    }

}
