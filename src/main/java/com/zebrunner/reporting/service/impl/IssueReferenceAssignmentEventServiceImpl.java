package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.domain.entity.integration.IssueReferenceAssignmentEvent;
import com.zebrunner.reporting.persistence.repository.IssueReferenceAssignmentEventRepository;
import com.zebrunner.reporting.service.IssueReferenceAssignmentEventService;
import com.zebrunner.reporting.service.Test2Service;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
@AllArgsConstructor
public class IssueReferenceAssignmentEventServiceImpl implements IssueReferenceAssignmentEventService {

    private final Test2Service test2Service;
    private final IssueReferenceAssignmentEventRepository issueEventRepository;

    @Override
    @Transactional(readOnly = true)
    public Set<IssueReferenceAssignmentEvent> retrieveTestCaseEventsByTestIdAndAction(Long testId, IssueReferenceAssignmentEvent.Action action) {
        Test test = test2Service.retrieveById(testId);
        Long testCaseId = test.getTestCase().getId();
        return issueEventRepository.findByTestCaseIdAndActionOrderedByIdDesc(testCaseId, action);
    }

    @Override
    @Transactional
    public IssueReferenceAssignmentEvent saveLinkingIfNeeded(IssueReference issueReference, Long testCaseId, Integer failureReasonHash, Long reporterId) {
        return issueEventRepository.findLatestLinkToTestCaseByHashAndIssue(testCaseId, failureReasonHash, issueReference.getId())
                                   .orElseGet(() -> {
                                       IssueReferenceAssignmentEvent linking = IssueReferenceAssignmentEvent.linkingOf(issueReference, testCaseId, failureReasonHash, reporterId);
                                       return issueEventRepository.save(linking);
                                   });
    }

    @Override
    @Transactional
    public void saveUnlinkingIfNeeded(IssueReference issueReference, Long testCaseId, Integer failureReasonHash, Long reporterId) {
        boolean unlinked = issueEventRepository.isIssueUnlinkedFromTestCase(testCaseId, failureReasonHash, issueReference.getId());
        if (!unlinked) {
            IssueReferenceAssignmentEvent unliking =
                    IssueReferenceAssignmentEvent.unlikingOf(issueReference, testCaseId, failureReasonHash, reporterId);
            issueEventRepository.save(unliking);
        }
    }

}
