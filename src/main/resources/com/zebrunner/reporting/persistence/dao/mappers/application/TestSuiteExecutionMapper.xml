<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="com.zebrunner.reporting.persistence.dao.mysql.application.TestRunMapper">

    <insert id="create" useGeneratedKeys="true" keyProperty="id">
        INSERT INTO test_suite_executions (
            name,
            ci_run_id,
            user_id,
            test_suite_id,
            status,
            job_id,
            upstream_job_id,
            upstream_job_build_number,
            build_number,
            config_id,
            started_by,
            known_issue,
            started_at,
            elapsed,
            comments,
            channels,
            framework,
            environment,
            build
        ) VALUES (
            #{name},
            #{ciRunId},
            #{user.id},
            #{testSuite.id},
            #{status},
            #{job.id},
            #{upstreamJob.id},
            #{upstreamJobBuildNumber},
            #{buildNumber},
            #{config.id},
            #{startedBy},
            #{knownIssue},
            #{startedAt},
            #{elapsed},
            #{comments},
            #{channels},
            #{framework},
            #{environment},
            #{build}
        )
    </insert>

    <sql id="getTestRun">
        SELECT
            tr.id                        AS test_suite_execution_id,
            tr.name                      AS test_suite_execution_name,
            tr.ci_run_id                 AS test_suite_execution_ci_run_id,
            tr.user_id                   AS test_suite_execution_user_id,
            tr.test_suite_id             AS test_suite_execution_test_suite_id,
            tr.status                    AS test_suite_execution_status,
            tr.job_id                    AS test_suite_execution_job_id,
            tr.upstream_job_id           AS test_suite_execution_upstream_job_id,
            tr.upstream_job_build_number AS test_suite_execution_upstream_job_build_number,
            tr.build_number              AS test_suite_execution_build_number,
            tr.config_id                 AS test_suite_execution_config_id,
            tr.started_by                AS test_suite_execution_started_by,
            tr.known_issue               AS test_suite_execution_known_issue,
            tr.started_at                AS test_suite_execution_started_at,
            tr.ended_at                  AS test_suite_execution_ended_at,
            tr.elapsed                   AS test_suite_execution_elapsed,
            tr.comments                  AS test_suite_execution_comments,
            tr.channels                  AS test_suite_execution_channels,
            tr.reviewed                  AS test_suite_execution_reviewed,
            tr.framework                 AS test_suite_execution_framework,
            tr.modified_at               AS test_suite_execution_modified_at,
            tr.created_at                AS test_suite_execution_created_at,
            tr.environment               AS test_suite_execution_environment,
            tr.build                     AS test_suite_execution_build,

            c.id                         AS test_suite_execution_config_id,
            c.platform                   AS test_suite_execution_config_platform,
            c.platform_version           AS test_suite_execution_config_platform_version,
            c.browser                    AS test_suite_execution_config_browser,
            c.browser_version            AS test_suite_execution_config_browser_version,
            c.locale                     AS test_suite_execution_config_locale,
            c.device                     AS test_suite_execution_config_device,
            c.modified_at                AS test_suite_execution_config_modified_at,
            c.created_at                 AS test_suite_execution_config_created_at,

            l.id                         AS label_id,
            l.key                        AS label_key,
            l.value                      AS label_value
        FROM test_suite_executions tr
        LEFT JOIN test_execution_configs c ON tr.config_id = c.id
        LEFT JOIN test_suite_executions_labels trl ON tr.id = trl.test_suite_execution_id
        LEFT JOIN labels l ON trl.label_id = l.id
    </sql>

    <sql id="getTestRunFullBody">
            TR.ID                           AS TEST_SUITE_EXECUTION_ID,
            TR.NAME                         AS TEST_SUITE_EXECUTION_NAME,
            TR.CI_RUN_ID                    AS TEST_SUITE_EXECUTION_CI_RUN_ID,
            TR.STATUS                       AS TEST_SUITE_EXECUTION_STATUS,
            TR.UPSTREAM_JOB_ID              AS TEST_SUITE_EXECUTION_UPSTREAM_JOB_ID,
            TR.UPSTREAM_JOB_BUILD_NUMBER    AS TEST_SUITE_EXECUTION_UPSTREAM_JOB_BUILD_NUMBER,
            TR.BUILD_NUMBER                 AS TEST_SUITE_EXECUTION_BUILD_NUMBER,
            TR.STARTED_BY                   AS TEST_SUITE_EXECUTION_STARTED_BY,
            TR.KNOWN_ISSUE                  AS TEST_SUITE_EXECUTION_KNOWN_ISSUE,
            TR.STARTED_AT                   AS TEST_SUITE_EXECUTION_STARTED_AT,
            TR.ENDED_AT                     AS TEST_SUITE_EXECUTION_ENDED_AT,
            TR.ELAPSED                      AS TEST_SUITE_EXECUTION_ELAPSED,
            TR.COMMENTS                     AS TEST_SUITE_EXECUTION_COMMENTS,
            TR.CHANNELS                     AS TEST_SUITE_EXECUTION_CHANNELS,
            TR.REVIEWED                     AS TEST_SUITE_EXECUTION_REVIEWED,
            TR.FRAMEWORK                    AS TEST_SUITE_EXECUTION_FRAMEWORK,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'PASSED') AS TEST_SUITE_EXECUTION_PASSED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'FAILED') AS TEST_SUITE_EXECUTION_FAILED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'FAILED' AND T.KNOWN_ISSUE = TRUE) AS TEST_SUITE_EXECUTION_FAILED_AS_KNOWN,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'SKIPPED') AS TEST_SUITE_EXECUTION_SKIPPED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'IN_PROGRESS') AS TEST_SUITE_EXECUTION_IN_PROGRESS,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'ABORTED') AS TEST_SUITE_EXECUTION_ABORTED,
            TR.MODIFIED_AT      AS TEST_SUITE_EXECUTION_MODIFIED_AT,
            TR.CREATED_AT       AS TEST_SUITE_EXECUTION_CREATED_AT,
            TR.ENVIRONMENT      AS TEST_SUITE_EXECUTION_ENVIRONMENT,
            TR.BUILD            AS TEST_SUITE_EXECUTION_BUILD,

            U.ID                AS TEST_SUITE_EXECUTION_USER_ID,
            U.USERNAME          AS TEST_SUITE_EXECUTION_USER_USER_NAME,
            U.EMAIL             AS TEST_SUITE_EXECUTION_USER_EMAIL,

            C.ID                AS TEST_SUITE_EXECUTION_CONFIG_ID,
            C.PLATFORM          AS TEST_SUITE_EXECUTION_CONFIG_PLATFORM,
            C.PLATFORM_VERSION  AS TEST_SUITE_EXECUTION_CONFIG_PLATFORM_VERSION,
            C.BROWSER           AS TEST_SUITE_EXECUTION_CONFIG_BROWSER,
            C.BROWSER_VERSION   AS TEST_SUITE_EXECUTION_CONFIG_BROWSER_VERSION,
            C.LOCALE            AS TEST_SUITE_EXECUTION_CONFIG_LOCALE,
            C.DEVICE            AS TEST_SUITE_EXECUTION_CONFIG_DEVICE,
            C.MODIFIED_AT       AS TEST_SUITE_EXECUTION_CONFIG_MODIFIED_AT,
            C.CREATED_AT        AS TEST_SUITE_EXECUTION_CONFIG_CREATED_AT,

            TS.ID               AS TEST_SUITE_EXECUTION_TEST_SUITE_ID,
            TS.NAME             AS TEST_SUITE_EXECUTION_TEST_SUITE_NAME,
            TS.FILE_NAME        AS TEST_SUITE_EXECUTION_TEST_SUITE_FILE_NAME,
            TS.DESCRIPTION      AS TEST_SUITE_EXECUTION_TEST_SUITE_DESCRIPTION,

            J.ID                AS TEST_SUITE_EXECUTION_JOB_ID,
            J.NAME              AS TEST_SUITE_EXECUTION_JOB_NAME,
            J.JOB_URL           AS TEST_SUITE_EXECUTION_JOB_JOB_URL,
            J.JENKINS_HOST      AS TEST_SUITE_EXECUTION_JOB_JENKINS_HOST,

            TEST_SUITE_EXECUTION_ARTIFACT_REFERENCES ->> 'name' as  TEST_SUITE_EXECUTION_ARTIFACT_NAME,
            TEST_SUITE_EXECUTION_ARTIFACT_REFERENCES ->> 'value' as TEST_SUITE_EXECUTION_ARTIFACT_LINK,

            l.id                         AS label_id,
            l.key                        AS label_key,
            l.value                      AS label_value
    </sql>

    <sql id="getTestRunFull">
        SELECT
        <include refid="getTestRunFullBody"/>
        FROM TEST_SUITE_EXECUTIONS TR
        <include refid="testRunFullJoinPart"/>
    </sql>

    <sql id="testRunFullJoinPart">
        LEFT JOIN users u ON tr.user_id = u.id
        LEFT JOIN test_execution_configs c ON tr.config_id = c.id
        LEFT JOIN jobs j ON tr.job_id = j.id
        LEFT JOIN test_suites ts ON tr.test_suite_id = ts.id
        LEFT JOIN test_suite_executions_labels trl ON tr.id = trl.test_suite_execution_id
        LEFT JOIN labels l ON trl.label_id = l.id
        LEFT JOIN LATERAL jsonb_array_elements(tr.artifact_references) TEST_SUITE_EXECUTION_ARTIFACT_REFERENCES
            ON tr.artifact_references::text != '[]'::text
    </sql>

    <select id="findById" resultMap="TestRunResultMap">
        <include refid="getTestRun"/>
        WHERE tr.id = #{id}
    </select>

    <select id="findByCiRunId" resultMap="TestRunResultMap">
        <include refid="getTestRun"/>
        WHERE tr.ci_run_id = #{cirunid}
    </select>

    <select id="findStatistics" resultMap="TestRunStatisticResultMap">
        SELECT
            TR.ID                                                                               AS TEST_SUITE_EXECUTION_ID,
            TR.REVIEWED                                                                         AS TEST_SUITE_EXECUTION_REVIEWED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'PASSED')  AS TEST_SUITE_EXECUTION_PASSED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'FAILED')  AS TEST_SUITE_EXECUTION_FAILED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'FAILED' AND T.KNOWN_ISSUE = TRUE) AS TEST_SUITE_EXECUTION_FAILED_AS_KNOWN,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'SKIPPED') AS TEST_SUITE_EXECUTION_SKIPPED,
            (SELECT COUNT(*) FROM TEST_EXECUTIONS T WHERE T.TEST_SUITE_EXECUTION_ID = TR.ID AND T.STATUS = 'ABORTED') AS TEST_SUITE_EXECUTION_ABORTED
        FROM TEST_SUITE_EXECUTIONS TR
        WHERE tr.id = #{id}
    </select>

    <sql id="testRunSearchWhereClause">
        <trim prefix="WHERE" prefixOverrides="AND |OR ">
            <if test="null != id">
               AND TR.ID = #{id}
            </if>
            <if test="null != query">
                <![CDATA[
                    AND (
                        LOWER(TR.NAME) LIKE LOWER(CONCAT('%', #{query}, '%'))
                        OR LOWER(CONCAT(J.JOB_URL, '/', TR.BUILD_NUMBER)) LIKE LOWER(CONCAT('%', #{query}, '%'))
                        OR LOWER(TR.BUILD) LIKE LOWER(CONCAT('%', #{query}, '%'))
                    )
                ]]>
            </if>
            <if test="null != testSuiteId">
               AND TS.ID = #{testSuiteId}
            </if>
            <if test="null != environment and !environment.isEmpty()">
                   AND LOWER(ENVIRONMENT) IN
                <foreach item="envItem" index="index" collection="environment" open="(" separator="," close=")">
                    LOWER(#{envItem})
                </foreach>
            </if>
            <if test="null != platform and !platform.isEmpty()">
                AND LOWER(C.PLATFORM) IN
                <foreach item="platformItem" index="index" collection="platform" open="(" separator="," close=")">
                    LOWER(#{platformItem})
                </foreach>
            </if>
            <if test="null != browser and !browser.isEmpty()">
                AND LOWER(C.BROWSER) IN
                <foreach item="browserItem" index="index" collection="browser" open="(" separator="," close=")">
                    LOWER(#{browserItem})
                </foreach>
            </if>
            <if test="null != locale and !locale.isEmpty()">
                AND LOWER(C.LOCALE) IN
                <foreach item="localeItem" index="index" collection="locale" open="(" separator="," close=")">
                    LOWER(#{localeItem})
                </foreach>
            </if>
            <if test="null != date">
                <![CDATA[AND TR.STARTED_AT BETWEEN #{date}::timestamp AND #{date}::timestamp + '24:00:00'::interval]]>
            </if>
            <if test="null != fromDate">
                <![CDATA[AND TR.STARTED_AT::date >= #{fromDate}::date]]>
            </if>
            <if test="null != toDate">
                <![CDATA[AND TR.STARTED_AT::date <= #{toDate}::date]]>
            </if>
            <if test="null != status and !status.isEmpty()">
                AND TR.STATUS IN
                <foreach item="statusItem" index="index" collection="status" open="(" separator="," close=")">
                    #{statusItem}
                </foreach>
            </if>
            <if test="null != reviewed">
                AND TR.REVIEWED = #{reviewed}
            </if>
        </trim>
    </sql>

    <sql id="testRunSearchIds">
        SELECT
            DISTINCT (TR.ID) AS id, TR.STARTED_AT
        FROM TEST_SUITE_EXECUTIONS TR
        LEFT JOIN TEST_EXECUTION_CONFIGS C ON TR.CONFIG_ID = C.ID
        LEFT JOIN JOBS J ON TR.JOB_ID = J.ID
        LEFT JOIN TEST_SUITES TS ON TR.TEST_SUITE_ID = TS.ID
        <include refid="testRunSearchWhereClause"/>
    </sql>

    <sql id="testRunSearchWithClause">
        WITH TEST_SUITE_EXECUTION_SEARCH_IDS AS (
        <include refid="testRunSearchIds"/>
        <if test="sortOrder.toString() == 'ASC'">
            ORDER BY TR.STARTED_AT ASC
        </if>
        <if test="sortOrder.toString() == 'DESC'">
            ORDER BY TR.STARTED_AT DESC
        </if>
            LIMIT #{pageSize} OFFSET #{offset}
        )
    </sql>

    <sql id="testRunSearchCountWithClause">
        WITH TEST_SUITE_EXECUTION_SEARCH_IDS AS (
        <include refid="testRunSearchIds"/>
        )
    </sql>

    <select id="search" resultMap="TestRunResultMap">
        <include refid="testRunSearchWithClause"/>
        SELECT
        <include refid="getTestRunFullBody"/>
        FROM TEST_SUITE_EXECUTION_SEARCH_IDS TRSI
        LEFT JOIN TEST_SUITE_EXECUTIONS TR ON TRSI.ID = TR.ID
        <include refid="testRunFullJoinPart"/>
    </select>

    <select id="findSearchCount" resultType="java.lang.Integer">
        <include refid="testRunSearchCountWithClause"/>
        SELECT COUNT(*) FROM TEST_SUITE_EXECUTION_SEARCH_IDS
    </select>

    <select id="findForSmartRerun" resultMap="NightlyViewResultMap">
        SELECT
        NV.UPSTREAM_JOB_ID,
        NV.UPSTREAM_JOB_BUILD_NUMBER,
        NV.JOB_NAME,
        NV.JOB_URL,
        NV.BUILD_NUMBER,
        NV.TEST_SUITE_NAME,
        NV.TEST_SUITE_EXECUTION_ID
        FROM NIGHTLY_VIEW NV
        <trim prefix="WHERE" prefixOverrides="AND |OR ">
            <![CDATA[ AND NV.UPSTREAM_JOB_ID = #{upstreamJobId}
                      AND NV.UPSTREAM_JOB_BUILD_NUMBER = #{upstreamJobBuildNumber}
                      AND NV.TEST_SUITE_EXECUTION_STATUS <> 'IN_PROGRESS' ]]>
            <if test="null != owner and owner != ''">
                <![CDATA[ AND LOWER(NV.OWNER_USERNAME) LIKE LOWER(CONCAT('%', #{owner}, '%'))]]>
            </if>
            <if test="null != hashcode">
                <![CDATA[ AND NV.MESSAGE_HASHCODE = #{hashcode}]]>
            </if>
            <if test="null != failurePercent">
                <![CDATA[ AND (100.0 * (NV.FAILED + NV.SKIPPED + NV.ABORTED)/NV.TOTAL)::integer > #{failurePercent} ]]>
            </if>
            <if test="null != cause and cause != ''">
                <![CDATA[ AND LOWER(NV.MESSAGE) LIKE LOWER(CONCAT('%', #{cause}, '%')) ]]>
            </if>
        </trim>
        GROUP BY
        NV.UPSTREAM_JOB_ID,
        NV.UPSTREAM_JOB_BUILD_NUMBER,
        NV.JOB_NAME,
        NV.JOB_URL,
        NV.BUILD_NUMBER,
        NV.TEST_SUITE_NAME,
        NV.TEST_SUITE_EXECUTION_ID
    </select>

    <select id="findByIdFull" resultMap="TestRunResultMap">
        <include refid="getTestRunFull"/>
        WHERE TR.ID = #{id}
    </select>

    <select id="findByCiRunIdFull" resultMap="TestRunResultMap">
        <include refid="getTestRunFull"/>
        WHERE TR.CI_RUN_ID = #{ciRunId}
    </select>

    <update id="update">
        UPDATE
            TEST_SUITE_EXECUTIONS
        SET
            CI_RUN_ID = #{ciRunId},
            USER_ID = #{user.id},
            TEST_SUITE_ID = #{testSuite.id},
            STATUS = #{status},
            JOB_ID = #{job.id},
            UPSTREAM_JOB_ID = #{upstreamJob.id},
            UPSTREAM_JOB_BUILD_NUMBER = #{upstreamJobBuildNumber},
            BUILD_NUMBER = #{buildNumber},
            CONFIG_ID = #{config.id},
            STARTED_BY = #{startedBy},
            KNOWN_ISSUE = #{knownIssue},
            STARTED_AT = #{startedAt},
            ENDED_AT = #{endedAt},
            ELAPSED = #{elapsed},
            COMMENTS = #{comments},
            CHANNELS = #{channels},
            REVIEWED = #{reviewed},
            ENVIRONMENT = #{environment},
            BUILD = #{build}
        WHERE
            ID = #{id}
    </update>

    <update id="reassignTestSuite">
        UPDATE TEST_SUITE_EXECUTIONS SET TEST_SUITE_ID = #{toTestSuiteId} WHERE TEST_SUITE_ID = #{fromTestSuiteId}
    </update>

    <resultMap type="com.zebrunner.reporting.domain.db.TestRun" id="NightlyViewResultMap" autoMapping="false">
        <id column="TEST_SUITE_EXECUTION_ID" property="id"/>
        <id column="TEST_SUITE_EXECUTION_NAME" property="name"/>
        <result column="OWNER_ID" property="user.id"/>
        <result column="OWNER_USERNAME" property="user.username"/>
        <result column="EMAIL" property="user.email"/>
        <result column="TEST_SUITE_NAME" property="testSuite.name"/>
        <result column="TEST_SUITE_EXECUTION_ID" property="id"/>
        <result column="UPSTREAM_JOB_ID" property="upstreamJob.id"/>
        <result column="UPSTREAM_JOB_BUILD_NUMBER" property="upstreamJobBuildNumber"/>
        <result column="BUILD_NUMBER" property="buildNumber"/>
        <result column="STARTED" property="startedAt"/>
        <result column="ELAPSED" property="elapsed"/>
        <result column="JOB_URL" property="job.jobURL"/>
        <result column="JOB_NAME" property="job.name"/>
    </resultMap>

    <resultMap type="com.zebrunner.reporting.domain.db.TestRun" id="TestRunResultMap" autoMapping="false">
        <id column="TEST_SUITE_EXECUTION_ID" property="id"/>

        <result column="TEST_SUITE_EXECUTION_USER_ID" property="user.id"/>
        <result column="TEST_SUITE_EXECUTION_USER_USER_NAME" property="user.username"/>
        <result column="TEST_SUITE_EXECUTION_USER_EMAIL" property="user.email"/>

        <result column="TEST_SUITE_EXECUTION_CONFIG_ID" property="config.id"/>
        <result column="TEST_SUITE_EXECUTION_ENVIRONMENT" property="config.env"/>
        <result column="TEST_SUITE_EXECUTION_BUILD" property="config.appVersion"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_PLATFORM" property="config.platform"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_PLATFORM_VERSION" property="config.platformVersion"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_BROWSER" property="config.browser"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_BROWSER_VERSION" property="config.browserVersion"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_LOCALE" property="config.locale"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_DEVICE" property="config.device"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_MODIFIED_AT" property="config.modifiedAt"/>
        <result column="TEST_SUITE_EXECUTION_CONFIG_CREATED_AT" property="config.createdAt"/>

        <result column="TEST_SUITE_EXECUTION_TEST_SUITE_ID" property="testSuite.id"/>
        <result column="TEST_SUITE_EXECUTION_TEST_SUITE_NAME" property="testSuite.name"/>
        <result column="TEST_SUITE_EXECUTION_TEST_SUITE_FILE_NAME" property="testSuite.fileName"/>
        <result column="TEST_SUITE_EXECUTION_TEST_SUITE_DESCRIPTION" property="testSuite.description"/>

        <result column="TEST_SUITE_EXECUTION_CI_RUN_ID" property="ciRunId"/>
        <result column="TEST_SUITE_EXECUTION_NAME" property="name"/>
        <result column="TEST_SUITE_EXECUTION_STATUS" property="status"/>
        <result column="TEST_SUITE_EXECUTION_UPSTREAM_JOB_ID" property="upstreamJob.id"/>
        <result column="TEST_SUITE_EXECUTION_UPSTREAM_JOB_BUILD_NUMBER" property="upstreamJobBuildNumber"/>
        <result column="TEST_SUITE_EXECUTION_BUILD_NUMBER" property="buildNumber"/>
        <result column="TEST_SUITE_EXECUTION_STARTED_BY" property="startedBy"/>
        <result column="TEST_SUITE_EXECUTION_KNOWN_ISSUE" property="knownIssue"/>
        <result column="TEST_SUITE_EXECUTION_STARTED_AT" property="startedAt"/>
        <result column="TEST_SUITE_EXECUTION_ENDED_AT" property="endedAt"/>
        <result column="TEST_SUITE_EXECUTION_ELAPSED" property="elapsed"/>
        <result column="TEST_SUITE_EXECUTION_COMMENTS" property="comments"/>
        <result column="TEST_SUITE_EXECUTION_CHANNELS" property="channels"/>
        <result column="TEST_SUITE_EXECUTION_REVIEWED" property="reviewed"/>
        <result column="TEST_SUITE_EXECUTION_PASSED" property="passed"/>
        <result column="TEST_SUITE_EXECUTION_FAILED" property="failed"/>
        <result column="TEST_SUITE_EXECUTION_FAILED_AS_KNOWN" property="failedAsKnown"/>
        <result column="TEST_SUITE_EXECUTION_SKIPPED" property="skipped"/>
        <result column="TEST_SUITE_EXECUTION_IN_PROGRESS" property="inProgress"/>
        <result column="TEST_SUITE_EXECUTION_ABORTED" property="aborted"/>
        <result column="TEST_SUITE_EXECUTION_FRAMEWORK" property="framework"/>
        <result column="TEST_SUITE_EXECUTION_MODIFIED_AT" property="modifiedAt"/>
        <result column="TEST_SUITE_EXECUTION_CREATED_AT" property="createdAt"/>
        <result column="TEST_SUITE_EXECUTION_ENVIRONMENT" property="environment"/>
        <result column="TEST_SUITE_EXECUTION_BUILD" property="build"/>

        <result column="TEST_SUITE_EXECUTION_JOB_ID" property="job.id"/>
        <result column="TEST_SUITE_EXECUTION_JOB_NAME" property="job.name"/>
        <result column="TEST_SUITE_EXECUTION_JOB_JOB_URL" property="job.jobURL"/>
        <result column="TEST_SUITE_EXECUTION_JOB_JENKINS_HOST" property="job.jenkinsHost"/>

        <collection property="artifactReferences"
                    ofType="com.zebrunner.reporting.domain.vo.ArtifactReference"
                    resultMap="ArtifactReferenceResultMap"/>

        <collection property="labels"
                    ofType="com.zebrunner.reporting.domain.db.Label"
                    resultMap="LabelResultMap" />

    </resultMap>

    <resultMap id="TestRunStatisticResultMap" type="com.zebrunner.reporting.domain.dto.TestRunStatistics" autoMapping="false">
        <id column="TEST_SUITE_EXECUTION_ID" property="testRunId"/>
        <result column="TEST_SUITE_EXECUTION_REVIEWED" property="reviewed"/>
        <result column="TEST_SUITE_EXECUTION_PASSED" property="passed"/>
        <result column="TEST_SUITE_EXECUTION_FAILED" property="failed"/>
        <result column="TEST_SUITE_EXECUTION_FAILED_AS_KNOWN" property="failedAsKnown"/>
        <result column="TEST_SUITE_EXECUTION_SKIPPED" property="skipped"/>
        <result column="TEST_SUITE_EXECUTION_ABORTED" property="aborted"/>
    </resultMap>

    <resultMap type="com.zebrunner.reporting.domain.db.TestRunResult" id="TestRunResultsResultMap" autoMapping="false">
        <id column="TEST_SUITE_EXECUTION_ID" property="testRunId"/>
        <result column="TEST_SUITE_EXECUTION_STATUS" property="status"/>
        <result column="TEST_SUITE_EXECUTION_ELAPSED" property="elapsed"/>
    </resultMap>

    <resultMap type="com.zebrunner.reporting.domain.vo.ArtifactReference" id="ArtifactReferenceResultMap" autoMapping="false">
        <result column="TEST_SUITE_EXECUTION_ARTIFACT_NAME" property="name"/>
        <result column="TEST_SUITE_EXECUTION_ARTIFACT_LINK" property="value"/>
    </resultMap>

    <resultMap id="LabelResultMap" type="com.zebrunner.reporting.domain.db.Label" autoMapping="false">
        <id column="LABEL_ID" property="id"/>
        <result column="LABEL_KEY" property="key"/>
        <result column="LABEL_VALUE" property="value"/>
    </resultMap>

</mapper>
