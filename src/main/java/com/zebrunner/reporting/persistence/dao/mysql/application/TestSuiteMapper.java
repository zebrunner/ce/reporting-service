package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.TestSuite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TestSuiteMapper {

    void create(TestSuite testSuite);

    Optional<TestSuite> findById(long id);

    Optional<TestSuite> findByNameAndFileNameAndUserId(@Param("name") String name, @Param("fileName") String fileName, @Param("userId") long userId);

    List<TestSuite> findAllByUserId(@Param("userId") Long userId);

    void update(TestSuite testSuite);

    void reassignInBatchUser(List<Long> ids, @Param("toUserId") Long toUserId);

    void deleteInBatchByIds(List<Long> ids);

}
