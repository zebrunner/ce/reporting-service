package com.zebrunner.reporting.domain.dto.elasticsearch;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogEntry {

    private String level;
    private String message;
    private Long timestamp;

}

