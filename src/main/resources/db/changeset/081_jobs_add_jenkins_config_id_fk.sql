ALTER TABLE jobs ADD COLUMN IF NOT EXISTS jenkins_config_id BIGINT;

ALTER TABLE jobs
ADD CONSTRAINT jobs_jenkins_config_id_fkey
FOREIGN KEY (jenkins_config_id) REFERENCES integration_jenkins_configs(id)
ON DELETE SET NULL;
