package com.zebrunner.reporting.service.user;

import com.zebrunner.reporting.domain.entity.User;

import java.util.Optional;

public interface UserService {

    String ANONYMOUS_USERNAME = "anonymous";

    User retrieveAnonymous();

    Optional<User> retrieveByUsername(String username);

    User retrieveNoNullById(Long id);

    void deleteByName(String name);

}
