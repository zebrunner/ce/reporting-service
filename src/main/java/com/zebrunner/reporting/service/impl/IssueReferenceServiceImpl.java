package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.entity.integration.IssueReference;
import com.zebrunner.reporting.persistence.repository.IssueReferenceRepository;
import com.zebrunner.reporting.service.IssueReferenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class IssueReferenceServiceImpl implements IssueReferenceService {

    private final IssueReferenceRepository issueReferenceRepository;

    @Override
    @Transactional
    public IssueReference retrieveOrCreate(String value, IssueReference.Type type) {
        return issueReferenceRepository.findByTypeAndValue(type, value)
                                       .orElseGet(() -> {
                                           IssueReference issueReference = new IssueReference(value, type);
                                           return issueReferenceRepository.save(issueReference);
                                       });
    }

    @Override
    @Transactional
    public IssueReference retrieveOrCreate(IssueReference issueReference) {
        return issueReferenceRepository.findByTypeAndValue(issueReference.getType(), issueReference.getValue())
                                       .orElseGet(() -> issueReferenceRepository.save(issueReference));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<IssueReference> retrieveLinkedToTestCaseByFailureReasonHash(Long testCaseId, Integer failureReasonHash) {
        return issueReferenceRepository.findLastLinkedByTestCaseIdAndFailureReasonHash(testCaseId, failureReasonHash);
    }

}
