package com.zebrunner.reporting.service.impl;

import com.zebrunner.reporting.domain.dto.ExchangeRunContextResult;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.entity.TestRun;
import com.zebrunner.reporting.domain.vo.RerunCriteria;
import com.zebrunner.reporting.domain.vo.RunContext;
import com.zebrunner.reporting.service.RunContextService;
import com.zebrunner.reporting.service.Test2Service;
import com.zebrunner.reporting.service.TestRun2Service;
import com.zebrunner.reporting.service.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class RunContextServiceImpl implements RunContextService {

    private final Test2Service test2Service;
    private final TestRun2Service testRun2Service;

    @Override
    public RunContext generateNew() {
        String testRunUuid = UUID.randomUUID().toString();
        return new RunContext(testRunUuid, RunContext.Mode.NEW);
    }

    @Override
    public RunContext generateForRerunAsString(String testRunUuid, Collection<Test.Status> anyOfStatuses, Boolean knownIssue) {
        RerunCriteria rerunCriteria = new RerunCriteria(new HashSet<>(anyOfStatuses), knownIssue);
        return new RunContext(testRunUuid, RunContext.Mode.RERUN, rerunCriteria);
    }

    @Override
    @Transactional(readOnly = true)
    public ExchangeRunContextResult exchange(RunContext runContext) {
        String ciRunId = runContext.getTestRunUuid();
        TestRun testRun = testRun2Service.retrieveOptionalByCiRunId(ciRunId)
                                         .orElse(null);

        RunContext.Mode mode = runContext.getMode();
        if (mode == RunContext.Mode.NEW) {
            return testRun == null || testRun.getStatus() == TestRun.Status.QUEUED
                    ? ExchangeRunContextResult.runAllowed()
                    : ExchangeRunContextResult.runNotAllowed("Test Run with such UUID already exists in Zebrunner.");
        }

        // to be removed
        if (mode == RunContext.Mode.LEGACY) {
            return testRun == null
                    ? ExchangeRunContextResult.runAllowed()
                    : ExchangeRunContextResult.runAllowedFor(test2Service.retrieveByTestRunId(testRun.getId()));
        }

        // rerun
        if (testRun == null) {
            return ExchangeRunContextResult.runNotAllowed("Test Run with given UUID does not exists.");
        }

        List<Test> tests = test2Service.retrieveByTestRunId(testRun.getId());

        Predicate<Test> filteringPredicate = this.formFilteringPredicate(runContext.getRerunCriteria());
        tests = StreamUtils.filterToList(tests, filteringPredicate);

        RunContext fullExecutionContext = this.generateFullExecutionPlanContextForRerun(ciRunId);
        return ExchangeRunContextResult.runAllowedFor(tests, fullExecutionContext);
    }

    private Predicate<Test> formFilteringPredicate(RerunCriteria rerunCriteria) {
        Predicate<Test> filter = test -> true;

        Set<Test.Status> anyOfStatuses = rerunCriteria.getAnyOfStatuses();
        if (CollectionUtils.isNotEmpty(anyOfStatuses)) {
            filter = filter.and(test -> anyOfStatuses.contains(test.getStatus()));
        }

        Boolean knownIssue = rerunCriteria.getKnownIssue();
        if (knownIssue != null) {
            filter = filter.and(test -> knownIssue == test.isKnownIssue());
        }

        return filter;
    }

    private RunContext generateFullExecutionPlanContextForRerun(String testSuiteExecutionUuid) {
        return new RunContext(testSuiteExecutionUuid, RunContext.Mode.RERUN, null);
    }

}
