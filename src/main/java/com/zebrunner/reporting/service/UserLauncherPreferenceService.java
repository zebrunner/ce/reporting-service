package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.db.launcher.UserLauncherPreference;
import com.zebrunner.reporting.persistence.dao.mysql.application.UserLauncherPreferenceMapper;
import com.zebrunner.reporting.service.user.UserReassignmentListener;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserLauncherPreferenceService implements UserReassignmentListener {

    private final UserLauncherPreferenceMapper preferenceMapper;

    public UserLauncherPreference create(Long launcherId, Long userId, UserLauncherPreference preference) {
        preferenceMapper.create(launcherId, userId, preference);
        return preference;
    }

    public Optional<UserLauncherPreference> getByLauncherIdAndUserId(Long launcherId, Long userId) {
        return preferenceMapper.findByLauncherIdAndUserId(launcherId, userId);
    }

    public UserLauncherPreference update(UserLauncherPreference preference) {
        preferenceMapper.update(preference);
        return preference;
    }

    @Override
    @Transactional
    public void onUserReassignment(Long fromId, Long toId) {
        preferenceMapper.deleteAllByUserId(fromId);
    }

}
