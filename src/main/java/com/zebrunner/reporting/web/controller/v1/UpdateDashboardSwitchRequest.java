package com.zebrunner.reporting.web.controller.v1;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@JGlobalMap
@NoArgsConstructor
public class UpdateDashboardSwitchRequest {

    @NotNull
    private Long dashboardId;

}
