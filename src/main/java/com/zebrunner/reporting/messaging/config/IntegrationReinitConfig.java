package com.zebrunner.reporting.messaging.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class IntegrationReinitConfig {

    public static final String EXCHANGE = "integration.reinit";
    public static final String QUEUE = "integration.reinit." + UUID.randomUUID();
    public static final String SUPPLIER_QUEUE_NAME_HEADER = "SUPPLIER_QUEUE";

    @Bean
    public FanoutExchange integrationReinitExchange() {
        return new FanoutExchange(EXCHANGE);
    }

    @Bean
    public Queue integrationReinitQueue() {
        return new Queue(QUEUE, false, true, true);
    }

    @Bean
    public Binding integrationReinitBinding(Queue integrationReinitQueue, FanoutExchange integrationReinitExchange) {
        return BindingBuilder.bind(integrationReinitQueue)
                             .to(integrationReinitExchange);
    }

}
