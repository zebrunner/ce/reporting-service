package com.zebrunner.reporting.domain.dto;

import com.zebrunner.reporting.domain.dto.widget.WidgetDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DashboardType {

    private Long id;
    private String title;
    private List<WidgetDTO> widgets = new ArrayList<>();
    private boolean editable;
}
