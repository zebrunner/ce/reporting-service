package com.zebrunner.reporting.web.response.v1;

import lombok.Value;

@Value
public class ConfirmKnownIssueResponse {

    public boolean knownIssue;

}
