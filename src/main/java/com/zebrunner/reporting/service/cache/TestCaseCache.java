package com.zebrunner.reporting.service.cache;

import com.zebrunner.reporting.domain.db.TestCase;

import java.util.Optional;

public interface TestCaseCache {

    Optional<TestCase> getByTestClassAndTestMethod(String testClass, String testMethod);

}
