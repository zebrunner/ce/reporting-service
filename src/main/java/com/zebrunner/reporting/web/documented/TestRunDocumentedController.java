package com.zebrunner.reporting.web.documented;

import com.zebrunner.common.eh.web.response.ErrorResponse;
import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.BuildParameterType;
import com.zebrunner.reporting.domain.dto.CommentType;
import com.zebrunner.reporting.domain.dto.EmailType;
import com.zebrunner.reporting.domain.dto.TestRunType;
import com.zebrunner.reporting.domain.dto.TestType;
import com.zebrunner.reporting.domain.vo.ArtifactReference;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.JobSearchCriteria;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.SearchResult;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestRunSearchCriteria;
import com.zebrunner.reporting.web.request.v1.AttachArtifactReferencesRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Api("Test runs API")
public interface TestRunDocumentedController {

    @ApiOperation(
            value = "Creates a test run",
            notes = "Returns the registered test run",
            nickname = "startTestRun",
            httpMethod = "POST",
            response = TestRunType.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "testRunType", paramType = "body", dataType = "TestRunType", required = true, value = "The test run to create")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the registered test run", response = TestRunType.class),
            @ApiResponse(code = 400, message = "Indicates that the startedBy property has an incorrect behavior", response = ErrorResponse.class)
    })
    TestRunType startTestRun(TestRunType testRunType);

    @ApiOperation(
            value = "Updates an existing test run",
            notes = "Returns the updated test run",
            nickname = "updateTestRun",
            httpMethod = "PUT",
            response = TestRunType.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "testRunType", paramType = "body", dataType = "TestRunType", required = true, value = "The test run to update")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the updated test run", response = TestRunType.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    TestRunType updateTestRun(TestRunType testRunType);

    @ApiOperation(
            value = "Finishes a test run",
            notes = "Initializes on finish test run data",
            nickname = "finishTestRun",
            httpMethod = "POST",
            response = TestRunType.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the finished test run", response = TestRunType.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    TestRunType finishTestRun(long id);

    @ApiOperation(
            value = "Aborts a test run",
            notes = "Aborts a test run with IN_PROGRESS status",
            nickname = "abortTestRun",
            httpMethod = "POST",
            response = TestRunType.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "query", dataTypeClass = Long.class, value = "The test run id"),
            @ApiImplicitParam(name = "ciRunId", paramType = "query", dataType = "string", value = "The test run ciRun id"),
            @ApiImplicitParam(name = "abortCause", paramType = "body", dataType = "CommentType", value = "A comment about the abort cause")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the aborted test run", response = TestRunType.class),
            @ApiResponse(code = 404, message = "Indicates that test run does not exist", response = ErrorResponse.class)
    })
    TestRunType abortTestRun(Long id, String ciRunId, CommentType abortCause);

    @ApiOperation(
            value = "Retrieves a test run by its id",
            notes = "Returns the found test run",
            nickname = "getTestRun",
            httpMethod = "GET",
            response = TestRunType.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the found test run", response = TestRunType.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    TestRunType getTestRun(long id);

    @ApiOperation(
            value = "Searches for test runs by criteria",
            notes = "Returns found test runs",
            nickname = "searchTestRuns",
            httpMethod = "GET",
            response = SearchResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "sc", paramType = "body", dataType = "TestRunSearchCriteria", required = true, value = "Search criteria")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns found test runs", response = SearchResult.class)
    })
    SearchResult<TestRun> searchTestRuns(TestRunSearchCriteria sc);

    @ApiOperation(
            value = "Attaches artifacts to the test run",
            notes = "Returns set of attached artifact",
            nickname = "attachArtifacts",
            httpMethod = "POST",
            response = List.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id"),
            @ApiImplicitParam(name = "testRunArtifacts", paramType = "body", dataTypeClass = List.class, required = true, value = "List of the test run artifacts")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the found test run", response = List.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    Set<ArtifactReference> attachArtifacts(Long id, List<AttachArtifactReferencesRequest.ArtifactReference> testRunArtifactReferences);

    @ApiOperation(
            value = "Retrieves a test run by the CI run id",
            notes = "Returns the found test run",
            nickname = "getTestRunByCiRunId",
            httpMethod = "GET",
            response = TestRunType.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "ciRunId", paramType = "query", dataType = "string", required = true, value = "The test run CI run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns the found test run", response = TestRunType.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    TestRunType getTestRunByCiRunId(String ciRunId);

    @ApiOperation(
            value = "Retrieves tests by the test run id",
            notes = "Returns found tests",
            nickname = "getTestRunResults",
            httpMethod = "GET",
            response = List.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns found tests", response = List.class)
    })
    List<TestType> getTestRunResults(long id);

    @ApiOperation(
            value = "Deletes a test run by its id",
            nickname = "deleteTestRun",
            httpMethod = "DELETE"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The test run was deleted successfully")
    })
    void deleteById(Long id);

    @ApiOperation(
            value = "Sends test run results via email",
            notes = "Collects test run result data and sends a report via email",
            nickname = "sendTestRunResultsEmail",
            httpMethod = "POST",
            response = String.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataType = "string", required = true, value = "The test run id"),
            @ApiImplicitParam(name = "email", paramType = "body", dataType = "EmailType", required = true, value = "The email to send the report to"),
            @ApiImplicitParam(name = "showStacktrace", paramType = "query", dataType = "boolean", value = "Indicates test logs visibility")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The email was sent successfully. Returns the email content", response = String.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    void sendTestRunResultsEmail(String id, EmailType email, boolean showStacktrace);

    @ApiOperation(
            value = "Sends test run results via email",
            notes = "Collects test run result data and sends a report via email",
            nickname = "sendTestRunFailureEmail",
            httpMethod = "POST",
            response = String.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataType = "string", required = true, value = "The test run id"),
            @ApiImplicitParam(name = "email", paramType = "body", dataType = "EmailType", required = true, value = "The email to send the report to"),
            @ApiImplicitParam(name = "suiteOwner", paramType = "query", dataType = "boolean", value = "Indicates that the email will be sent to the suite owner"),
            @ApiImplicitParam(name = "suiteRunner", paramType = "query", dataType = "boolean", value = "Indicates that the email will be sent to a suite runner")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The email was sent successfully. Returns the email content", response = String.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    void sendTestRunFailureEmail(String id, EmailType email, boolean suiteOwner, boolean suiteRunner);

    @ApiOperation(
            value = "Builds test run results in HTML format",
            notes = "Returns built test run results in HTML format",
            nickname = "exportTestRunHTML",
            httpMethod = "GET",
            response = String.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataType = "string", required = true, value = "The test run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns built test run results in HTML format", response = String.class)
    })
    String exportTestRunHTML(String id);

    @ApiOperation(
            value = "Marks a test run as reviewed",
            notes = "Attaches a specified comment and marks a test run as reviewed",
            nickname = "markTestRunAsReviewed",
            httpMethod = "POST"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id"),
            @ApiImplicitParam(name = "comment", paramType = "body", dataType = "CommentType", required = true, value = "A test run comment")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The test run was marked as reviewed successfully"),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    void markTestRunAsReviewed(long id, CommentType comment);

    @ApiOperation(
            value = "Reruns a test run by its id",
            notes = "Reruns a test run by its id (all tests or failed only)",
            nickname = "rerunTestRun",
            httpMethod = "GET"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id"),
            @ApiImplicitParam(name = "rerunFailures", paramType = "query", dataType = "boolean", value = "Indicates that only failed tests will be rerun")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The test run was rerun successfully"),
            @ApiResponse(code = 400, message = "Indicates that test run is passed but flag is rerun failures", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Indicates that test run does not exist", response = ErrorResponse.class)
    })
    void rerunTestRun(long id, Integer userId, boolean rerunFailures);

    @ApiOperation(
            value = "Aborts a test run job or debug process",
            nickname = "abortCIJob",
            httpMethod = "GET"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "query", dataTypeClass = Long.class, value = "The test run id"),
            @ApiImplicitParam(name = "ciRunId", paramType = "query", dataType = "string", value = "The test run ciRun id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The test run job or debug process was aborted successfully"),
            @ApiResponse(code = 400, message = "Indicates that the test automation integration does not exist", response = ErrorResponse.class)
    })
    void abortCIJob(Long id, String ciRunId);

    @ApiOperation(
            value = "Builds a test run job",
            notes = "Builds a test run job using custom job parameters",
            nickname = "buildTestRun",
            httpMethod = "POST"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id"),
            @ApiImplicitParam(name = "jobParameters", paramType = "body", dataType = "Map", required = true, value = "The job parameters")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "The test run job was built successfully"),
            @ApiResponse(code = 400, message = "Indicates that the test automation integration does not exist", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Indicates that the test run does not exist", response = ErrorResponse.class)
    })
    void buildTestRun(long id, Integer userId, Map<String, String> jobParameters);

    @ApiOperation(
            value = "Retrieves job parameters from a test run job by its id",
            nickname = "getJobParameters",
            httpMethod = "GET",
            response = List.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", paramType = "header", required = true, value = "The auth token (Bearer)"),
            @ApiImplicitParam(name = "id", paramType = "path", dataTypeClass = Long.class, required = true, value = "The test run id")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Returns found test cases", response = List.class),
            @ApiResponse(code = 400, message = "Indicates that the test automation integration does not exist", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Indicates that test run does not exist", response = ErrorResponse.class)
    })
    List<BuildParameterType> getJobParameters(long id);

}
