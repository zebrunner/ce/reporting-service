package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.TestRunStatistics;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.JobSearchCriteria;
import com.zebrunner.reporting.persistence.dao.mysql.application.search.TestRunSearchCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TestRunMapper {

    void create(TestRun testRun);

    Optional<TestRun> findById(long id);

    Optional<TestRun> findByIdFull(long id);

    Optional<TestRun> findByCiRunId(String ciRunId);

    Optional<TestRun> findByCiRunIdFull(String ciRunId);

    Optional<TestRunStatistics> findStatistics(Long id);

    void update(TestRun testRun);

    void reassignTestSuite(@Param("fromTestSuiteId") Long fromTestSuiteId, @Param("toTestSuiteId") Long toTestSuiteId);

    List<TestRun> search(TestRunSearchCriteria sc);

    List<TestRun> findForSmartRerun(JobSearchCriteria sc);

    Integer findSearchCount(TestRunSearchCriteria sc);

}
