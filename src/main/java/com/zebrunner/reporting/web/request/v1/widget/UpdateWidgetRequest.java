package com.zebrunner.reporting.web.request.v1.widget;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@JGlobalMap
@NoArgsConstructor
public class UpdateWidgetRequest {

    @NotBlank
    private String title;
    private String description;
    private String paramsConfig;
    private String legendConfig;

}
