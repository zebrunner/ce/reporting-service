package com.zebrunner.reporting.domain.push.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArtifactUploadedMessage {

    private String testRunId;
    private String testId;
    private String name;
    private String key;

}
