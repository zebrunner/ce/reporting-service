CREATE TABLE IF NOT EXISTS testrail_settings
(
    id                BIGSERIAL,
    url               VARCHAR(255) NOT NULL,
    username          VARCHAR(255) NOT NULL,
    password          VARCHAR(512) NOT NULL,
    encrypted         BOOLEAN      NOT NULL DEFAULT FALSE,
    enabled           BOOLEAN      NOT NULL DEFAULT FALSE,
    created_at        TIMESTAMP    NOT NULL DEFAULT NOW(),
    modified_at       TIMESTAMP    NOT NULL,
    PRIMARY KEY (ID)
);
CREATE TRIGGER update_timestamp_testrail_settings
    BEFORE INSERT OR UPDATE
    ON testrail_settings
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_at();
