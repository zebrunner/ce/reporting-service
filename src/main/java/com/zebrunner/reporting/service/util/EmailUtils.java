package com.zebrunner.reporting.service.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EmailUtils {

    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    public static Set<String> obtainValidOnly(String emailsLine) {
        if (StringUtils.isEmpty(emailsLine)) {
            return new HashSet<>();
        } else {
            return Arrays.stream(emailsLine.replaceAll("[,;]", " ").split(" "))
                         .filter(email -> EMAIL_PATTERN.matcher(email).matches())
                         .collect(Collectors.toSet());
        }
    }

}
