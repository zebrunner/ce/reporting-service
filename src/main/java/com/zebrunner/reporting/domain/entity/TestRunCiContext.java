package com.zebrunner.reporting.domain.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Map;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Table(name = "test_suite_execution_ci_contexts")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class TestRunCiContext {

    @Id
    private Long testSuiteExecutionId;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Builder.Default
    @Column(columnDefinition = "jsonb")
    @org.hibernate.annotations.Type(type = "jsonb")
    private Map<String, String> envVariables = Map.of();

    @MapsId
    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private TestRun testSuiteExecution;

    public enum Type {

        JENKINS

    }

}
