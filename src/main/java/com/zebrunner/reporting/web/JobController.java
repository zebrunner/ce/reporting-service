package com.zebrunner.reporting.web;

import com.zebrunner.reporting.domain.db.Job;
import com.zebrunner.reporting.domain.dto.JobDTO;
import com.zebrunner.reporting.service.JobsService;
import com.zebrunner.reporting.web.documented.JobDocumentedController;
import lombok.RequiredArgsConstructor;
import org.dozer.Mapper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@CrossOrigin
@RequestMapping(path = "api/jobs", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequiredArgsConstructor
public class JobController implements JobDocumentedController {

    private final Mapper mapper;
    private final JobsService jobsService;

    @PostMapping
    @Override
    public JobDTO createJob(@RequestBody @Valid JobDTO jobDTO) {
        Job job = mapper.map(jobDTO, Job.class);
        Job updatedJob = jobsService.createOrUpdate(job);
        return mapper.map(updatedJob, JobDTO.class);
    }

}
