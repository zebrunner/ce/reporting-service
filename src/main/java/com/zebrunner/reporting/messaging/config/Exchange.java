package com.zebrunner.reporting.messaging.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Exchange {

    public static final String EMAIL = "email";
    public static final String INTEGRATION_SAVED = "integration.saved";

}
