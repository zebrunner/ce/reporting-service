package com.zebrunner.reporting.service.ci;

import com.zebrunner.reporting.domain.db.TestRun;
import com.zebrunner.reporting.domain.dto.BuildParameterType;
import com.zebrunner.reporting.domain.entity.TestRunCiContext;

import java.util.List;
import java.util.Map;

public interface CiServerAdapter {

    TestRunCiContext.Type getType();

    @Deprecated
    List<BuildParameterType> retrieveLegacyBuildParameters(TestRunCiContext ciContext);

    @Deprecated
    void populateLegacyJobAndBuildNumber(TestRun testRun, TestRunCiContext ciContext);

    /**
     * Triggers new build on ci based on ci context of already existing one.
     *
     * @param baseCiContext ci context of already existing ci build.
     * @param parameters    parameters to be passed into build.
     * @param runContext    Zebrunner Agent run context to be passed to new ci build.
     * @return ci context with intermediate information about triggered ci build (such as reference to item in build queue).
     */
    TestRunCiContext build(TestRunCiContext baseCiContext, Map<String, String> parameters, String runContext);

    /**
     * Triggers rerun of test run based on its ci context.
     *
     * @param ciContext  ci context of test run.
     * @param runContext Zebrunner Agent run context to be passed to new ci build.
     * @return ci context with intermediate information about triggered ci build (such as reference to item in build queue).
     */
    TestRunCiContext rerun(TestRunCiContext ciContext, String runContext);

    /**
     * Aborts queued or running ci build.
     * Queued build will be aborted only if the given ci context contains information
     * about ci build which was triggered by Zebrunner.
     */
    void abort(TestRunCiContext ciContext);

    /**
     * Aborts executing right now ci build that was recently initiated by Zebrunner.
     * The ci build will be aborted only if information about the ci build contains reference to item in build queue.
     * <p>
     * This method was created for the scheduled task which aborts stacked ci builds for test runs in QUEUED status.
     *
     * @return true - if ci build was aborted.
     * false - if either test run was not aborted or such kind of scenario is not applicable to the ci server.
     */
    default boolean tryAbortExecutingNowQueuedBuild(TestRunCiContext ciContext) {
        return false;
    }

}
