package com.zebrunner.reporting.web.response.v1.testrail;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JGlobalMap
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateTestRailConfigResponse {

    private String url;
    private String username;
    private String password;
    private boolean encrypted;
    private boolean enabled;

}
