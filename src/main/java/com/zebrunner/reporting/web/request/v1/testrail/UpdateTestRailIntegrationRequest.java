package com.zebrunner.reporting.web.request.v1.testrail;

import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@JGlobalMap(excluded = "connected")
@NoArgsConstructor
public class UpdateTestRailIntegrationRequest {

    @NotBlank
    private String url;
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @NotNull
    private Boolean enabled;

}
