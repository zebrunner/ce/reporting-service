package com.zebrunner.reporting.service;

import com.zebrunner.reporting.domain.dto.TestIssueReference;
import com.zebrunner.reporting.domain.dto.TestIssueReferenceAssignmentResult;
import com.zebrunner.reporting.domain.dto.reporting.TestFinish;
import com.zebrunner.reporting.domain.dto.reporting.TestSave;
import com.zebrunner.reporting.domain.entity.Label;
import com.zebrunner.reporting.domain.entity.Test;
import com.zebrunner.reporting.domain.vo.ArtifactReference;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface Test2Service {

    Test retrieveById(Long id);

    List<Test> retrieveByCiRunId(String ciRunId, Collection<Test.Status> requestedStatuses, Collection<Long> requestedTestIds);

    List<Test> retrieveAllByIds(Collection<Long> ids);

    List<Test> retrieveByTestRunId(Long testRunId);

    List<Test> retrieveAllByTestRunIdAndIds(Long testRunId, Collection<Long> ids);

    boolean existsByTestRunIdAndId(Long testRunId, Long id);

    Test attachLabels(Long testId, Collection<Label> labels);

    void attachArtifactReferences(Long testId, Set<ArtifactReference> references);

    boolean existsByTestRunIdAndKnownIssue(Long testRunId);

    boolean existsByTestRunIdAndLogicallyFailedAndNotKnownIssue(Long testRunId);

    int countByTestRunId(Long testRunId);

    Test start(Long testRunId, TestSave testSave);

    Test restart(Long testRunId, Long testId, TestSave testSave);

    Test update(Long testRunId, Long testId, TestSave testSave);

    void swapStatusesByTestRunId(Long testRunId, Test.Status fromStatus, Test.Status toStatus);

    Test finish(Long testRunId, Long testId, TestFinish testFinish);

    /**
     * Confirms that the given failure reason belongs to list of the test's known issues.
     * This method checks if the test's test case has failures with reason which is very similar to the given failure reason.
     * As of know, the similarity verification is performed by normalization of the given failure reason
     * and subsequent hash code calculation.
     *
     * @return true - if the given failure reason belongs to list of the test's known issues; false - otherwise.
     */
    boolean confirmKnownIssue(Long testRunId, Long testId, String failureReason);

    void deleteById(Long id);

    void deleteByTestRunIdAndId(Long testRunId, Long id);

    TestIssueReferenceAssignmentResult linkIssues(List<TestIssueReference> testIssueReferences, Long reporterId);

    void unlinkIssue(Long testId, Long issueReferenceId, Long reporterId);

}
