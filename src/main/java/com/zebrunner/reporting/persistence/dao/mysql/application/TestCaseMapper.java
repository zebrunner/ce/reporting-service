package com.zebrunner.reporting.persistence.dao.mysql.application;

import com.zebrunner.reporting.domain.db.Status;
import com.zebrunner.reporting.domain.db.TestCase;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Optional;

@Mapper
public interface TestCaseMapper {

    void create(TestCase testCase);

    Optional<TestCase> findByTestClassAndTestMethod(@Param("testClass") String testClass, @Param("testMethod") String testMethod);

    void update(TestCase testCase);

    void updateStatuses(@Param("ids") List<Long> ids, @Param("status") Status status);

    void reassignTestSuite(@Param("fromTestSuiteId") Long fromTestSuiteId, @Param("toTestSuiteId") Long toTestSuiteId);

    List<TestCase> findByIds(@Param("ids") List<Long> ids);

}
