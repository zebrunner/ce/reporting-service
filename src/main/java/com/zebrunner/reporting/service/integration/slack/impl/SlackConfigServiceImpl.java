package com.zebrunner.reporting.service.integration.slack.impl;

import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.request.auth.AuthTestRequest;
import com.slack.api.methods.response.auth.AuthTestResponse;
import com.zebrunner.common.eh.exception.ResourceNotFoundException;
import com.zebrunner.reporting.domain.entity.integration.SlackConfig;
import com.zebrunner.reporting.domain.vo.ReachabilityCheckResult;
import com.zebrunner.reporting.persistence.repository.integration.slack.SlackConfigRepository;
import com.zebrunner.reporting.service.CryptoService;
import com.zebrunner.reporting.service.exception.ResourceNotFoundError;
import com.zebrunner.reporting.service.integration.slack.SlackConfigService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class SlackConfigServiceImpl implements SlackConfigService {

    private final SlackConfigRepository slackConfigRepository;

    private final CryptoService cryptoService;
    private final MethodsClient slackApiClient;

    @Override
    @Transactional
    public SlackConfig update(SlackConfig newConfig) {
        SlackConfig config = retrieve();
        if (!Objects.equals(config.getToken(), newConfig.getToken())) {
            String token = cryptoService.encrypt(newConfig.getToken());
            config.setToken(token);
            config.setEncrypted(true);
        }
        config.setBotName(newConfig.getBotName());
        config.setBotIconUrl(newConfig.getBotIconUrl());
        config.setEnabled(newConfig.isEnabled());

        return slackConfigRepository.save(config);
    }

    @Override
    @Transactional
    public SlackConfig retrieve() {
        return slackConfigRepository.findFirstByOrderById()
                                    .orElseGet(this::createConfigIfNotExist);
    }

    @Override
    public SlackConfig retrieveActive(boolean decryptValues) {
        return slackConfigRepository.findEnabled()
                                    .map(this::checkReachability)
                                    .map(config -> decryptValuesIfRequired(config, decryptValues))
                                    .orElseThrow(() -> new ResourceNotFoundException(ResourceNotFoundError.SLACK_CONFIG_IS_NOT_FOUND_OR_DISABLED));
    }

    private SlackConfig decryptValuesIfRequired(SlackConfig config, boolean decryptValues) {
        if (config.isEncrypted() && decryptValues) {
            config.setToken(cryptoService.decrypt(config.getToken()));
            config.setEncrypted(false);
        }
        return config;
    }

    private SlackConfig checkReachability(SlackConfig config) {
        ReachabilityCheckResult result = isReachable(config);
        if (!result.getReachable()) {
            log.warn("[Slack] Server is not reachable: {}", result.getMessage());
            throw new ResourceNotFoundException(ResourceNotFoundError.SLACK_IS_UNREACHABLE_FOR_GIVEN_CONFIG);
        }

        return config;
    }

    @Override
    @Transactional
    public void patchEnabledProperty(boolean enabled) {
        SlackConfig config = slackConfigRepository.findFirstByOrderById()
                                                  .orElseGet(this::createConfigIfNotExist);
        config.setEnabled(enabled);
        slackConfigRepository.save(config);
    }

    private SlackConfig createConfigIfNotExist() {
        SlackConfig config = SlackConfig.builder()
                                        .token("")
                                        .botName("")
                                        .build();
        return slackConfigRepository.save(config);
    }

    @Override
    @SneakyThrows
    public ReachabilityCheckResult isReachable(SlackConfig config) {
        try {
            String token = config.isEncrypted() ? cryptoService.decrypt(config.getToken()) : config.getToken();
            if (token.contains("http")) {
                return ReachabilityCheckResult.reachable();
            }
            AuthTestResponse response = slackApiClient.authTest(AuthTestRequest.builder().token(token).build());
            return response.isOk()
                   ? ReachabilityCheckResult.reachable()
                   : ReachabilityCheckResult.unreachable(response.getError());
        } catch (Exception e) {
            return ReachabilityCheckResult.unreachable(e);
        }
    }

}
